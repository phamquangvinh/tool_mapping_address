<?php
class CallApi
{
    function call($url, $parameters) {
        $CI =& get_instance();
        $postData = array();
        //create name value pairs seperated by &
        foreach($parameters as $k => $v){
            $postData[] = array($k => $v);
        }
        $jsonEncodedData = json_encode($postData);
        $jsonEncodedData = trim($jsonEncodedData,'[]');
        $jsonEncodedData = str_replace('},{',',',$jsonEncodedData);
        $post = array(
            "parameters" => $jsonEncodedData
        );
        $token = hash_hmac('sha1','SPEEDLINK',$jsonEncodedData.$CI->config->item('api_key'));
        ob_start();
        $curl_request = curl_init();
        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, true);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, true);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, false);

        curl_setopt($curl_request, CURLOPT_POSTFIELDS,http_build_query($post));
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, array(
            'Authentication: ' . $token,
            'content-type: application/x-www-form-urlencoded'
        ));
        $result = curl_exec($curl_request);
        $error = curl_error($curl_request);
        curl_close($curl_request);
        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1],true);
        ob_end_flush();
        if(array_key_exists('error_code',$response)) {
            return array(
                'status'=>($result?($response['error_code'] == 1)?1:$response['error_code']:500),
                'message'=>$response['description'],
                'data'=>($result?($response['error_code'] == 1 || $response['error_code'] == -1 || $response['error_code'] == 404)?$response['data']:array():array())
            );
        }else{
            return array(
                'status'=>($result?1:501),
                'message'=>$error,
                'data'=>$response
            );
        }
    }
}