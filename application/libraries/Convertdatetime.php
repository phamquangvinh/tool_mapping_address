<?php

/**
 * Created by PhpStorm.
 * User: harry.long
 * Date: 7/29/2015
 * Time: 9:53 AM
 */
class ConvertDateTime
{
    /***
     * H�m convert datetime t? DB th�nh datetime hi?n th? theo UTC+7
     */
    public static function convertDateTimeDisplay($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {

            return date('d/m/Y', (strtotime($date_time) + (60 * 60 * 7)));
        }
    }

    /***
     * H�M CONVERT DATE TIME TH�NH TH?I GIAN -7h
     * @param $date_time
     * @return bool|string
     */
    public static function convertDateTimeDisplayForPortal($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {

            return date('d/m/Y h:m A', (strtotime($date_time) + (60 * 60 * 7)));
        }
    }

    public static function convertDateTimeDisplayPlusForTracking($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {

            return date('d/m/Y h:ia', (strtotime($date_time) + (60 * 60 * 7)));
        }
    }

    public static function convertDateTimeDisplayNonePlusForTracking($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {

            return date('d/m/Y h:ia', strtotime($date_time));
        }
    }

    public static function convertDateTimeDBPlusForPortal($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {

            return date('Y-m-d H:i:s', (strtotime($date_time) + (60 * 60 * 7)));
        }
    }

    public static function convertDateTimeDBNonePlusForPortal($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {

            return date('Y-m-d H:i:s', strtotime($date_time));
        }
    }

    public static function convertDateTimeDisplayForWap($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {
            //echo date('Y-m-d\TH:i');
            return date('Y-m-d\TH:i', (strtotime($date_time) + (60 * 60 * 7)));
        }
    }


    public static function convertTimeToTimeDBCRM($date_time, $bool = true)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {
            if ($bool == true) {
                $value = strtotime($date_time) - (60 * 60 * 7);
            } else {
                $value = strtotime($date_time);
            }
            return date('Y-m-d H:i:s', $value);
        }
    }

    /***
     * HAM CONVERT DATE THANH THOI GIAN HIEN THI
     * @param $date
     */
    public static function convertDateDisplay($date)
    {
        if ($date == '' || $date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
            return '';
        } else {
            return date('d/m/Y', strtotime($date));
        }
    }

    /***
     * HAM
     * @param $date
     * @return bool|string
     */
    public static function convertDateDB($date)// d/m/Y
    {
        $str = explode("/", $date);
        $date_value = $str[0] . '-' . $str[1] . '-' . $str[2];
        if ($date_value == '' || $date_value == '0000-00-00') {
            return '';
        } else {
            return date('Y-m-d', strtotime($date_value));
        }
    }

    public static function convertDateDBForPortal($date)// d/m/Y
    {

        $str = explode("/", $date);
        $str1 = explode(" ", $str[2]);
        $str2 = explode(":", $str1[1]);
        if($str2[0] == 00){
            $str2[0] = 12;
        }
        $time = $str2[0] . ':' . $str2[1];
        $date_value = $str[0] . '-' . $str[1] . '-' . $str1[0] . ' ' . $time . ' ' . $str1[2];
        if ($date_value == '' || $date_value == '0000-00-00') {
            return '';
        } else {
            return date('Y-m-d', strtotime($date_value));
        }
    }



    public static function convertTimeDB($time)// h:ia
    {
        $time = trim($time);
        $str = explode(":",$time);
        $result = '';
        if(count($str) > 0 && count($str) == 2){
            $hour = $str[0];
            $min_me = $str[1];
            if($hour > 0 && $hour < 13){
                if($hour == 12){
                    $hour = 0;
                }
                $me = substr($min_me,-2);
                $min = substr($min_me,0,2);
                if($me == 'am'){
                    $result = $hour.':'.$min.':00';
                }else{
                    $result = ($hour + 12 ).':'.$min.':00';
                }
                return $result;
            }else{
                return $result;
            }
        }else{
            return $result;
        }
    }

    public static function convertDateTimeToFormatDB($date_time)// d/m/Y h:ia
    {
        $date_time = trim($date_time);
        $str = explode(" ", $date_time);
        if (count($str) > 0 && count($str) == 2) {
            $date = $str[0];
            $time = $str[1];
            $result_date = ConvertDateTime::convertDateDB($date);
            $result_time = ConvertDateTime::convertTimeDB($time);
            return $result_date.' '.$result_time;
        } else {
            return '';
        }
    }

    public static function getRangeOfThisMonth()
    {
        $start_date = '01/' . date('m/Y');
        switch (date('m')) {
            case '01':
            case '03':
            case '05':
            case'07':
            case'08':
            case'10':
            case'12':
                $end_day = '31';
                break;
            case '02':
                $year = intval(date('Y'));
                if ($year % 400 == 0 || ($year % 4 == 0 && $year % 100 != 0)) {
                    $end_day = '29';
                } else {
                    $end_day = '28';
                }
                break;
            case '04':
            case'06':
            case'09':
            case '11':
            default:
                $end_day = '30';
                break;
        }
        $end_date = $end_day . '/' . date('m/Y');
        return array(
            'start_date' => $start_date,
            'end_date' => $end_date
        );
    }
    public static function convertTimeToTimeDBCRMNonePlus($date_time, $bool = true)
    {

        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {
            return date('Y/m/d h:ia', (strtotime($date_time)));
        }
    }

}