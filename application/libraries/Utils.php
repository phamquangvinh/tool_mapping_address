<?php
class Utils
{
    function getFieldData($array, $field, $idField = null)
    {
        $_out = array();

        if (is_array($array)) {
            if ($idField == null) {
                foreach ($array as $value) {
                    $_out[] = trim($value[$field]);
                }
            } else {
                foreach ($array as $value) {
                    if(is_array($value[$field]) || is_object($value[$field])){
                        $_out[$value[$idField]] = $value[$field];
                    }else{
                        $_out[$value[$idField]] = trim($value[$field]);
                    }
                }
            }
            return $_out;
        } else {
            return false;
        }
    }

    function getFieldDataArray($array, $idField = null)
    {
        $_out = array();

        if (is_array($array)) {
            if ($idField == null) {
                foreach ($array as $value) {
                    $_out[] = trim($value);
                }
            } else {
                foreach ($array as $value) {
                    $_out[$value[$idField]] = $value;
                }
            }
            return $_out;
        } else {
            return false;
        }
    }

    function saveImage($base64_string, $output_file)
    {
        // echo $output_file; die;
        $ifp = fopen($output_file, 'wb');
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return $output_file;
    }

    public static function convertDateToString($date){
        $str = explode("-", $date);
        $date_value = $str[0] . ' tháng ' . $str[1] . ' năm ' . $str[2];
        return $date_value;
    }
}