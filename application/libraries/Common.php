<?php
class Common
{
    public $paymentMethod = array(
        'cash'=>array(
            'name'=>'Cash',
            'name_vn'=>'Tiền mặt',
        ),
        'bank_transfer'=>array(
            'name'=>'Bank transfer',
            'name_vn'=>'Chuyển khoản',
        ),
        'other'=>array(
            'name'=>'Net off',
            'name_vn'=>'Cấn trừ',
        ),
    );
    public function generateSelectOption($list = array(),$disabled = array(),$selected = null,$field = 'id',$key = 'value'){
        $result = '<option></option>';
        if(count($list) > 0){
            foreach($list AS $index => $value){
                $selected_attr = '';
                $disabled_attr = '';
                if($value[$field] == $selected){
                    $selected_attr = ' selected="selected" ';
                }
                if(array_search($value[$field],$disabled) !== false){
                    $disabled_attr = ' disabled ';
                }
                $result .= '<option value="'.$value[$field].'" '.$selected_attr. $disabled_attr.'>'.$value[$key].'</option>';
            }
        }
        return $result;
    }
    public function generateSelectOptionPaymentMethod($selected = null,$language = 'vietnamese'){
        $result = '<option></option>';
        if(count($this->paymentMethod) > 0){
            foreach($this->paymentMethod AS $index => $value){
                if($index == $selected){
                    $selected_attr = ' selected="selected" ';
                }
                $result .= '<option value="'.$index.'" '.$selected_attr.'>'.$value[$language=='vietnamese'?'name_vn':'name'].'</option>';
            }
        }
        return $result;
    }
    public function generateOptionRangeDatetimeSearch($selected = 'Last_30_Days',$language = 'english'){
        if ($selected == 'Today') {
            $html = '<option selected value="Today" from="' . date('d/m/Y') . '" to="' . date('d/m/Y') . '">'.($language=="english"?"Today":"Hôm nay").'</option>';
        } else {
            $html = '<option value="Today" from="' . date('d/m/Y') . '" to="' . date('d/m/Y') . '">'.($language=="english"?"Today":"Hôm nay").'</option>';
        }
        if ($selected == 'Yesterday') {
            $html .= '<option selected value="Yesterday" from="' . date('d/m/Y', strtotime('-1 days')) . '" to="' . date('d/m/Y', strtotime('-1 days')) . '">'.($language=="english"?"Yesterday":"Hôm qua").'</option>';
        } else {
            $html .= '<option value="Yesterday" from="' . date('d/m/Y', strtotime('-1 days')) . '" to="' . date('d/m/Y', strtotime('-1 days')) . '">'.($language=="english"?"Yesterday":"Hôm qua").'</option>';
        }
        if ($selected == 'Last_7_Days') {
            $html .= '<option selected value="Last_7_Days" from="' . date('d/m/Y', strtotime('-6 days')) . '" to="' . date('d/m/Y') . '">'.($language=="english"?"Last 7 days":"7 Ngày trước").'</option>';
        } else {
            $html .= '<option value="Last_7_Days" from="' . date('d/m/Y', strtotime('-6 days')) . '" to="' . date('d/m/Y') . '">'.($language=="english"?"Last 7 days":"7 Ngày trước").'</option>';
        }
        if ($selected == 'This_Month') {
            $html .= '<option selected value="This_Month" from="' . date('d/m/Y', strtotime('first day of this month')) . '" to="' . date('d/m/Y') . '">'.($language=="english"?"This Month":"Tháng này").'</option>';
        } else {
            $html .= '<option value="This_Month" from="' . date('d/m/Y', strtotime('first day of this month')) . '" to="' . date('d/m/Y') . '">'.($language=="english"?"This Month":"Tháng này").'</option>';
        }
        if ($selected == 'Last_30_Days') {
            $html .= '<option selected value="Last_30_Days" from="' . date('d/m/Y', strtotime('-29 days')) . '" to="' . date('d/m/Y') . '">'.($language=="english"?"Last 30 days":"30 Ngày trước").'</option>';
        } else {
            $html .= '<option value="Last_30_Days" from="' . date('d/m/Y', strtotime('-29 days')) . '" to="' . date('d/m/Y') . '">'.($language=="english"?"Last 30 days":"30 Ngày trước").'</option>';
        }
        if ($selected == 'Last_Month') {
            $html .= '<option selected value="Last_Month" from="' . date('d/m/Y', strtotime('first day of last month')) . '" to="' . date('d/m/Y', strtotime('last day of last month')) . '">'.($language=="english"?"Last month":"Tháng trước").'</option>';
        } else {
            $html .= '<option value="Last_Month" from="' . date('d/m/Y', strtotime('first day of last month')) . '" to="' . date('d/m/Y', strtotime('last day of last month')) . '">'.($language=="english"?"Last month":"Tháng trước").'</option>';
        }
        return $html;
    }
    function generateListBookingNo($number_code){
        $today_start = ConvertDateTime::convertTimeToTimeDBCRM(date('Y-m-d 00:00:00'));
        $today_end = ConvertDateTime::convertTimeToTimeDBCRM(date('Y-m-d 23:59:59'));
        $CI =& get_instance();
        $CI->load->model('mcbooking');
        $number_booking = $CI->mcbooking->getNumberBookingInTime($today_start, $today_end);
        $array_code = array();
        for ($i = 0; $i < $number_code; $i++) {
            $code =  'xxxxxx/' . date('dmY') . '/bk-spl';
            $array_code[] = $code;
        }
        return $array_code;
    }
    public static function generateID($char = 'aa')
    {
        $id = strtotime(date('Y-m-d H:i:s'));
        return $id . $char . rand(0, 99999999);
    }
    function get_field_data($array, $field, $idField = null)
    {
        $_out = array();

        if (is_array($array)) {
            if ($idField == null) {
                foreach ($array as $value) {
                    $_out[] = $value[$field];
                }
            } else {                
                foreach ($array as $value) {
                    $_out[$value[$idField]] = $value[$field];
                }
            }
            return $_out;
        } else {
            return false;
        }
    }
    function get_field_data_trim($array, $field, $idField = null)
    {
        $_out = array();

        if (is_array($array)) {
            if ($idField == null) {
                foreach ($array as $value) {                    
                    $_out[] = trim($value[$field]);
                }
            } else {                
                foreach ($array as $value) {
                   
                    $_out[$value[$idField]] = $value[$field];
                }
            }           
            return $_out;
        } else {
            return false;
        }
    }
    function getSelectOptionsCountry($app_list_string,$selected){
        $app_list_string = json_decode($app_list_string);
        $html = '';
        foreach($app_list_string as $key => $value ){
            $html .= '<option id="'.$value->id.'" value="'.$value->id.'" ';
            if($value->id == $selected){
                $html .= ' selected="selected"';
            }
            $html .= '>'.$value->name.'</option>';
        }
        return $html;
    }
    function getSelectOptions($app_list_string,$selected, $country = 'VIETNAM'){
        $app_list_string = json_decode($app_list_string);
        if (is_object($app_list_string)) {
            $app_list_string = get_object_vars($app_list_string);
        }
        $html = '';
        if($selected == ''){
            $html .='<option value=""></option>';
        }
        foreach($app_list_string as $key => $value ){
            if($country == 'VIETNAM'){
                if($value->is_third_party != 1){
                    if($value->c_country == $country){
                        $html .= '<option value="'.$value->id.'" title="'.$value->branch_name.'" ';
                        if($value->id == $selected){
                            $html .= ' selected="selected"';
                        }
                        $html .= '>'.$value->branch_name.'</option>';
                    }
                }
            } else {
                if($value->c_country == $country){
                    $html .= '<option value="'.$value->id.'" title="'.$value->branch_name.'" ';
                    if($value->id == $selected){
                        $html .= ' selected="selected"';
                    }
                    $html .= '>'.$value->branch_name.'</option>';
                }    
            }
        }
        return $html;
    }
}