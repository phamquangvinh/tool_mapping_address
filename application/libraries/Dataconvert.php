<?php

class DataConvert
{
    public function convertDateTimeDisplay($date_time,$format = 'd/m/Y h:ia',$add = false){
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {
            return date($format, (strtotime($date_time) + ($add?(60 * 60 * 7):0) ));
        }
    }
    public static function convertDateDB($date){// d/m/Y
        $str = explode("/", $date);
        $date_value = $str[0] . '-' . $str[1] . '-' . $str[2];
        if ($date_value == '' || $date_value == '0000-00-00') {
            return '';
        } else {
            return date('Y-m-d', strtotime($date_value));
        }
    }
    public static function convertTimeDB($time){// h:ia
        $time = trim($time);
        $str = explode(":", $time);
        $result = '';
        if (count($str) > 0 && count($str) == 2) {
            $hour = $str[0];
            $min_me = $str[1];
            if ($hour > 0 && $hour < 13) {
                if ($hour == 12) {
                    $hour = 0;
                }
                $me = substr($min_me, -2);
                $min = substr($min_me, 0, 2);
                if ($me == 'am') {
                    $result = $hour . ':' . $min . ':00';
                } else {
                    $result = ($hour + 12) . ':' . $min . ':00';
                }
                return $result;
            } else {
                return $result;
            }
        } else {
            return $result;
        }
    }
}