<?php
class Authenticate{
    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
    }

    function initialize() {
        $this->CI->load->helper('language');
        $siteLang = $this->CI->session->userdata('site_lang');
        if ($siteLang) {
            $this->CI->lang->load('message',$siteLang);
        } else {
            $this->CI->session->set_userdata('site_lang', 'english');
            $this->CI->lang->load('message','english');
        }
    }

    public function check_user_login(){
        if($_SERVER['REQUEST_URI'] != '/' && $_SERVER['REQUEST_URI'] != '' && $_SERVER['REQUEST_URI'] != '/index.php/' && $_SERVER['REQUEST_URI'] != '/register' && $_SERVER['REQUEST_URI'] != '/forget-password') {
            if (!$this->CI->session->userdata('login')) {
                redirect($this->CI->config->item('base_url'));
            }
        }
    }
}