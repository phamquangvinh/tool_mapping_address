<?php
/**
 * Created by PhpStorm.
 * User: intern.ict008
 * Date: 10/9/2018
 * Time: 1:33 PM
 */

class Mapping extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getMappingList()
    {
        $query = 'SELECT partner, speedlink_address_code, country_text, city_text, district_text,ward_text, status, note
                    FROM `mapping_data_address_for_partner`
                    WHERE (status = "mapping" OR status = "not_match") LIMIT 200';
        $data = $this->db->query($query)
                            ->result_array();
        return $data;
    }

    public function getPartnerField($country_text, $city_text, $district_text, $ward_text) {
        $data = array();
        // CHANGE string '' for query
        if($country_text == '') {
            $country_text = '/0';
        }
        if($city_text == '') {
            $city_text = '/0';
        }
        if($district_text == '') {
            $district_text = '/0';
        }
        if($ward_text == '') {
            $ward_text = '/0';
        }
        // ADDRESS CODE & ZIP CODE & WARD CODE
        $code = $this->db->query("SELECT code, ward_code, zip_code
                    FROM `speedlink_address_code_copy`
                        WHERE country LIKE  '".$country_text."' 
                        AND province LIKE  '%".$city_text."' AND district LIKE '%".$district_text."' AND ward LIKE '%".$ward_text."'")
                        ->row_array();
        if($code==null) {
            $code = array('country_code'=>null, 'ward_code'=>null, 'zip_code'=>null);
        }
        // COUNTRY CODE
        $country_code = $this->db->query("SELECT country_code FROM `speedlink_address_code_copy`
                                          WHERE country LIKE '".$country_text."'")
                            ->row_array();
        if($country_code==null) {
            $country_code = array('country_code'=>null);
        }
        // CITY_CODE
        $city_code = $this->db->query("SELECT province_code FROM `speedlink_address_code_copy`
                                          WHERE country LIKE '".$country_text."' AND province LIKE '%".$city_text."'")
                            ->row_array();
        if($city_code==null) {
            $city_code = array('province_code'=>null);
        }
        // DISTRICT CODE
        $district_code = $this->db->query("SELECT district_code FROM `speedlink_address_code_copy`
                                          WHERE country LIKE '".$country_text."' 
                                          AND province LIKE '%".$city_text."' AND district LIKE '%".$district_text."'")
                            ->row_array();
        if($district_code==null) {
            $district_code = array('district_code'=>null);
        }
        array_push($data, $code);
        array_push($data, $country_code);
        array_push($data, $city_code);
        array_push($data, $district_code);

        return $data;
    }

    public function suggestCityPart($country_text, $city_text) {
        // PREVENT QUERY '%'
        if($country_text == '') {
            $country_text = '/0';
        }
        if($city_text == '') {
            $city_text = '/0';
        }
        // CHECK EXIST
        $check = "SELECT * FROM `speedlink_address_code_copy` 
                    WHERE country LIKE '" . $country_text . "' AND province LIKE '%".$city_text."' LIMIT 1";

        if(!$this->db->query($check)->row_array()) {
            // IF NOT EXIST THEN GET MAX VALUE
            $query = "SELECT DISTINCT MAX(SUBSTR(`code`,3,3))  AS city_pattern 
                    FROM `speedlink_address_code_copy`
                        WHERE country LIKE '" . $country_text . "'";
            $data = $this->db->query($query)->row_array();
            if($data['city_pattern'] == null) {
                $data['city_pattern'] = '000';
            }
            $status = 1;
        } else {
            $data = $this->db->query("SELECT SUBSTR(`code`,3,3) AS city_pattern 
                                        FROM `speedlink_address_code_copy`
                                        WHERE country LIKE '".$country_text."' AND province LIKE '%".$city_text."'
                                        LIMIT 1")
                                ->row_array();
            $status = 0;
        }
        $data['status'] = $status;
        return $data;
    }

    public function suggestDistrictPart($country_text, $city_text, $district_text) {
        // PREVENT QUERY '%'
        if($country_text == '') {
            $country_text = '/0';
        }
        if($city_text == '') {
            $city_text = '/0';
        }
        if($district_text == '') {
            $district_text = '/0';
        }
        // CHECK EXIST
        $check = "SELECT * FROM `speedlink_address_code_copy` 
                    WHERE country LIKE '" . $country_text . "' AND province LIKE '%".$city_text."' 
                    AND district LIKE '%".$district_text."' LIMIT 1";

        if(!$this->db->query($check)->row_array()) {
            // IF NOT EXIST PLUS 1
            $query = "SELECT DISTINCT MAX(SUBSTR(`code`,6,3))  AS district_pattern 
                    FROM `speedlink_address_code_copy`
                        WHERE country LIKE '" . $country_text . "' AND province LIKE '%".$city_text."'";
            $data = $this->db->query($query)->row_array();
            if($data['district_pattern'] == null) {
                $data['district_pattern'] = '000';
            }
            $status = 1;
        } else {
            $data = $this->db->query("SELECT SUBSTR(`code`,6,3) AS district_pattern 
                                        FROM `speedlink_address_code_copy`
                                        WHERE country LIKE '".$country_text."' AND province LIKE '%".$city_text."' 
                                        AND district LIKE '%".$district_text."' 
                                        LIMIT 1")
                ->row_array();
            $status = 0;
        }
        $data['status'] = $status;
        return $data;
    }

    public function suggestWardPart($country_text, $city_text, $district_text, $ward_text) {
        // PREVENT QUERY '%'
        if($country_text == '') {
            $country_text = '/0';
        }
        if($city_text == '') {
            $city_text = '/0';
        }
        if($district_text == '') {
            $district_text = '/0';
        }
        if($ward_text == '') {
            $ward_text = '/0';
        }
        // CHECK EXIST
        $check = "SELECT * FROM `speedlink_address_code_copy` 
                    WHERE country LIKE '" . $country_text . "' AND province LIKE '%".$city_text."' 
                    AND district LIKE '%".$district_text."' AND ward LIKE '%".$ward_text."' LIMIT 1";

        if(!$this->db->query($check)->row_array()) {
            // IF NOT EXIST PLUS 1
            $query = "SELECT DISTINCT MAX(SUBSTR(`code`,9,3))  AS ward_pattern 
                    FROM `speedlink_address_code_copy`
                        WHERE country LIKE '" . $country_text . "' AND province LIKE '%".$city_text."' 
                        AND district LIKE '%".$district_text."'";
            $data = $this->db->query($query)->row_array();
            if($data['ward_pattern'] == null) {
                $data['ward_pattern'] = '000';
            }
            $status = 1;
        } else {
            $data = $this->db->query("SELECT SUBSTR(`code`,9,3) AS ward_pattern 
                                        FROM `speedlink_address_code_copy`
                                        WHERE country LIKE '" . $country_text . "' AND province LIKE '%".$city_text."' 
                                        AND district LIKE '%".$district_text."' AND ward LIKE '%".$ward_text."' 
                                        LIMIT 1")
                ->row_array();
            $status = 0;
        }
        $data['status'] = $status;
        return $data;
    }

    public function getBranchAndHubCode($code) {
        $query = "SELECT p.branch_code, mh.hubcode
                    FROM speedlink_address_code_copy AS sa
                    INNER JOIN c_mailhub AS mh ON sa.hub_code = mh.id
                    INNER JOIN c_provice AS p ON sa.branch_code = p.id
                    WHERE sa.code = '".$code."'";
        return $this->db->query($query)->row_array();
    }

    public function mappingListPartner($partner) {
        return $this->db->query("SELECT * FROM `mapping_data_address_for_partner` WHERE partner = '".$partner."'")->result_array();
    }

    public function speedlinkCodeList() {
        return $this->db->query("SELECT * FROM speedlink_address_code_copy WHERE country='VIET NAM'")->result_array();
    }

    public function checkExistCodePartner($partner, $code){
        $query = "SELECT * FROM `mapping_data_address_for_partner` 
        WHERE speedlink_address_code_copy = '".$code."' AND partner = '".$partner."'";
        if($this->db->query($query)->row_array() != null) {
            return true;
        } else {
            return false;
        }
    }

    public function suggestZipCode($code) {
        $query = "SELECT zip_code FROM speedlink_address_code_copy WHERE `code` = '".$code."'";
        return $this->db->query($query)->row_array();
    }

    public function getListBranch() {
        $query = "SELECT id, mailhub, branch_code FROM `c_provice` WHERE c_country = 'VIETNAM' AND `deleted` = 0 AND `status`='active' AND branch_code != '(null)' AND mailhub != '(null)'";
        return $this->db->query($query)->result_array();
    }

    public function getHubByBranch($branch_code) {
        $query = "SELECT mh.hubcode 
                    FROM `c_mailhub` AS mh
                    INNER JOIN `c_provice` AS p ON p.mailhub = mh.id
                    WHERE p.branch_code = '".$branch_code."'";
        return $this->db->query($query)->row_array();
    }

    public function getCountryCode($country_text) {
        $query = "SELECT id FROM `c_country` WHERE `name` LIKE '".$country_text."'";
        return $this->db->query($query)->row_array();
    }
    public function getProvinceCode($city_text) {
        $query = "SELECT `name` FROM `c_provice` WHERE `name` REGEXP '".$city_text."' AND deleted = 0 AND `status` = 'active' AND is_province = 1";
        return $this->db->query($query)->row_array();
    }
    public function getDistrictCode($city_code, $district_text) {
        $query = "SELECT d.id
                  FROM c_district AS d 
                  INNER JOIN c_provice_c_district_1_c AS pd ON d.id = pd.c_provice_c_district_1c_district_idb
                  WHERE d.name REGEXP '".$district_text."' AND pd.c_provice_c_district_1c_provice_ida = '".$city_code."' AND pd.deleted = 0";
        return $this->db->query($query)->row_array();
    }
    public function getWardCode($district_code, $ward_text) {
        $query = "SELECT w.id
                  FROM c_wards AS w 
                  INNER JOIN c_wards_c_district_c AS wd ON w.id = wd.c_wards_c_districtc_wards_idb
                  WHERE w.name REGEXP '".$ward_text."' AND c_wards_c_districtc_district_ida = '".$district_code."' AND wd.deleted = 0";
        return $this->db->query($query)->row_array();
    }
    public function getBranchCode($branch) {
        $query = "SELECT id FROM `c_provice` WHERE `branch_code` LIKE '".$branch ."' AND deleted = 0 AND `status` = 'active'";
        return $this->db->query($query)->row_array();
    }
    public function getHubCode($hub) {
        $query = "SELECT id FROM `c_mailhub` WHERE hubcode = '".$hub."' AND deleted = 0 AND `status` = 'active'";
        return $this->db->query($query)->row_array();
    }
    // GET LIST PARTNER
    public function getListPartner() {
        $query = "SELECT DISTINCT partner FROM `mapping_data_address_for_partner`";
        return $this->db->query($query)->result_array();
    }
    // get total record of temporary table
    public function get_total_record() {
        $query = $this->db->select("COUNT(*) AS num")
            ->from('mapping_import_excel')->get()->row_array();
        return $query;
    }

    public function get_records_page($start, $length) {
        return $this->db->query("SELECT * FROM `mapping_import_excel` LIMIT ".$length." OFFSET ".$start)->result_array();
    }

    public function get_import_data() {
        return $this->db->query("SELECT * FROM `mapping_import_excel`")->result_array();
    }
}