<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'MappingController/loginPage';
$route['/'] = 'HomeController';
$route['404_override'] = 'HomeController/reroute404';
$route['translate_uri_dashes'] = FALSE;

//BOOKING
$route["booking-create"] = 'BookingController/create';
$route["booking-create-international"] = 'BookingController/createInternational';
$route["booking-create-multi"] = 'BookingController/createMulti';
$route["booking-import"] = 'BookingController/import';
$route["booking-import-excel"] = 'BookingController/importExel';
$route["booking-read-excel"] = 'BookingController/ImportOrder';
$route["booking-list"] = 'BookingController/listView';
$route["booking-duplicate/(:any)"] = 'BookingController/duplicateOrder/$1';
//USER
$route["user-profile"] = 'UserController/profile';
$route["user-edit"] = 'UserController/profileEdit';
$route["register"] = 'UserController/createUser';
$route["forget-password"] = 'UserController/forgetPassword';
$route["user-changepass"] = 'UserController/userChangePass';
$route["logout"] = 'LoginController/logout';
$route["login"] = 'LoginController/login';
//SERVICE
$route["service-list-shipment"] = 'ServiceController/listShipment';
$route["service-list-soa"] = 'ServiceController/listSOA';
$route["detail-soa/(:any)"] = 'ServiceController/detailSOA/$1';
$route["service-list-cod"] = 'ServiceController/listCOD';
$route["detail-cod/(:any)"] = 'ServiceController/detailCOD/$1';
//HOME
$route["searchFee"] = 'HomeController/searchFee';
$route["apiGetDistrict"] = 'ApiController/getDistrict';
//FEEDBACK
$route["feed-back"] = 'FeedBackController';
//MAPPING
$route["mapping-import"]='MappingController/mappingImport';
$route["mapping-importExcel"]='MappingController/importExcel';
$route["mapping-detail"]='MappingController/mappingDetail';

//MANAGE
$route["mapping-management"] = 'MappingController/mappingManagement';
