<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

$html = '';
$n = count($shipment);
for($i = 0; $i < $n; $i++){
    $awb_online = 'ISPL' . substr(date('Y'), 2) . date('mdHi') . rand(0, 20) ;
    // Charge to 
    $check_shipper = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_receiver = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_3rd = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    switch ($shipment[$i]["charge_to"]) {
        case 'shipper':
            $check_shipper = '<img src="assets/images/checkbox-checked.png" width="15"/>';
            break;
        case 'receiver':
            $check_receiver = '<img src="assets/images/checkbox-checked.png" width="15"/>';
            break;
        case '3rd':
            $check_3rd = '<img src="assets/images/checkbox-checked.png" width="15"/>';
            break;
        default:
            break;
    }
    // payment method
    $check_cash = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_bank_transfer = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_other = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $cash_value = '';
    switch ($shipment[$i]["payment_method"]) {
        case 'cash':
            $check_cash = '<img src="assets/images/checkbox-checked.png" width="15"/>';
            if($shipment[$i]["cash"] >= $shipment[$i]["delivery_cash"]){
                $cash_value = $shipment[$i]["cash"];
            }else{
                $cash_value = $shipment[$i]["delivery_cash"];
            }
            break;
        case 'bank_transfer':
            $check_bank_transfer = '<img src="assets/images/checkbox-checked.png" width="15"/>';
            break;
        case 'other':
            $check_other = '<img src="assets/images/checkbox-checked.png" width="15"/>';
            break;
        default:
            break;
    }
    //insurance value
    $check_insurance_value = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $insurance_value = 0;
    if($shipment[$i]['insurance_value'] > 0){
        $check_insurance_value = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        $insurance_value = number_format($shipment[$i]['insurance_value']);
    }
    //special delivery shipper
    $check_special_delivery_shipper = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    if($shipment[$i]['special_delivery_shipper'] == 'YES'){
        $check_special_delivery_shipper = '<img src="assets/images/checkbox-checked.png" width="15"/>';
    }
    //special delivery receiver
    $check_special_delivery_receiver = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    if($shipment[$i]['special_delivery_receiver'] == 'YES'){
        $check_special_delivery_receiver = '<img src="assets/images/checkbox-checked.png" width="15"/>';
    }
    //special delivery other
    $check_special_delivery_other = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    if($shipment[$i]['special_delivery_other'] == 'YES'){
        $check_special_delivery_other = '<img src="assets/images/checkbox-checked.png" width="15"/>';
    }
    //date time pickup
    $date_pickup = '';
    $time_pickup = '';
    if($shipment[$i]['real_pickup_date'] != '' && $shipment[$i]['real_pickup_date'] != '0000-00-00 00:00:00'){
        $date_time_pickup = date('d-m-Y h:ia', strtotime($shipment[$i]['real_pickup_date']) +7*60*60);
        $arr_date_time_pickup = explode(' ', $date_time_pickup);
        $date_pickup = $arr_date_time_pickup[0];
        $time_pickup = $arr_date_time_pickup[1];
    }
    //is dox
    $check_dox = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_non_dox = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    if($shipment[$i]['service'] == 'dox'){
        $check_dox = '<img src="assets/images/checkbox-checked.png" width="15"/>';
    }else{
        $check_non_dox = '<img src="assets/images/checkbox-checked.png" width="15"/>';
    }
    //is domestic
    $check_domestic = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_standard = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_ecommerce    = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_priority = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_economy = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_international = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_36h          = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $check_48h          = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    if($shipment[$i]['scope'] == 'domestic'){
        $check_domestic = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        //is standard
        if(empty($shipment[$i]['is_priority']) && empty($shipment[$i]['is_road_truck']) && empty($shipment[$i]['is_ecommerce_same_day']) && empty($shipment[$i]['is_ecommerce_next_day'])){
            $check_standard = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        }
        //is ecommerce
        if($shipment[$i]['is_ecommerce_same_day'] == 1 || $shipment[$i]['is_ecommerce_next_day'] == 1){
            $check_ecommerce = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        }
        //is priority
        if($shipment[$i]['is_priority'] == 1){
            $check_priority = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        }
        //is economy chính là field is_road_truck
        if($shipment[$i]['is_road_truck'] == 1){
            $check_economy = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        }
        //is 36
        if($shipment[$i]['is_36'] == 1){
            $check_36h = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        }
        //is 48
        if($shipment[$i]['is_48'] == 1){
            $check_48h = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        }
        //is ras
        $check_ras = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    }else{
        $check_international = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        $check_standard = '<img src="assets/images/checkbox-checked.png" width="15"/>';
    }
    //is cod
    $check_cod = '<img src="assets/images/checkbox-not-checked.png" width="15"/>';
    $cod_value = '';
    if($shipment[$i]['is_cod'] == 1){
        $check_cod = '<img src="assets/images/checkbox-checked.png" width="15"/>';
        $cod_value = number_format($shipment[$i]['cod_value']);
    }



//tabel parent
$html .= '
    <table id="table_parent" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" style="background-color: #f1f1f9">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td height="120px" width="50%" align="left">
                                        <img src="assets/images/logo_speedlink_print.png" height="120px">
                                    </td>
                                    <td width="50%" align="center" style="color: #2e489f">
                                        <p style="font-size:14px"><b>VẬN ĐƠN / <i>SHIPMENT AIRWAY BILL</i></b></p>
                                        <br/>
                                        <b><i>(Non Negotiable)</i></b><br/><br/>
                                        <b><i>Track this shipment: http://speedlink.vn</i></b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" id="btl_payer_account_number_and_insurance" cellpadding="0" cellspacing="0" style="background-color: #fff;">
                                <tr>
                                    <td colspan="4" style="padding: 0px;">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="numbers">1</td>
                                                <td id="cell_1_table_children_1_1" align="left" class="title">
                                                    <b> Hình thức thanh toán và bảo hiểm/ <i>Payer account number and insurance details</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="cell_payer_account_charge_to" width="25%" align="left" rowspan="2" valign="top">
                                        Bên thanh toán / <i>Charge to</i>
                                    </td>
                                    <td width="20%" align="left">
                                        '.$check_shipper.'
                                        Người gửi / <i>Shipper</i>
                                    </td>
                                    <td width="30%" align="left">
                                        '.$check_receiver.'
                                        Người nhận / <i>Receiver</i> 
                                    </td>
                                    <td width="25%" align="left">
                                        '.$check_3rd.'
                                        Bên thứ 3 / <i>3rd party</i> 
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        '.$check_cash.'
                                        Tiền mặt / <i>Cash</i> 
                                    </td>
                                    <td align="left">
                                        '.$check_bank_transfer.'
                                        Chuyển khoản / <i>Bank transfer</i> 
                                    </td>
                                    <td align="left">
                                        '.$check_other.'
                                        Khác / <i>Net off</i>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="cell_payer_account_payer_account_number" align="left" colspan="4" height="30px">
                                        Mã khách hàng / <i>Payer account number</i>: 
                                        <b class="content">'.$shipment[$i]['acc_no'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="cell_payer_account_booking_insurance" align="left" colspan="4">
                                        Bảo hiểm hàng / <i>booking insurance</i> 
                                    </td>
                                </tr>
                                <tr>
                                    <td id="cell_payer_account_insurance_value" align="left" colspan="4">
                                        '.$check_insurance_value.' Yes &nbsp;&nbsp;&nbsp;&nbsp;
                                        Giá trị hàng được bảo hiểm / <i>Insurance value</i>: 
                                        <b class="content">'.$insurance_value.'</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" id="tbl_shipper" cellpadding="0" cellspacing="0" style="background-color: #fff;">
                                <tr>
                                    <td colspan="2" style="padding: 0px; border: 0px;">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="numbers" style="border: 0px;">2</td>
                                                <td class="title" style="border: 0px;">
                                                    <b>Người gửi / <i>Shipper</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="53%" height="40px" valign="top">
                                        Số tài khoản người gửi  / <i>Shipper’s account number</i><br/>
                                        <b class="content">'.$shipment[$i]['shipper_account_no'].'</b>
                                    </td>
                                    <td width="47%" height="40px" valign="top">
                                        Tên người gửi  / <i>Contact name:</i>  
                                        <b class="content">'.$shipment[$i]['shipper_contact_name'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="40px" valign="top">
                                        Tên công ty / <i>Company name:</i> 
                                        <b class="content">'.$shipment[$i]['company_name'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="80px" valign="top">
                                        Địa chỉ / <i>Address:</i>
                                        <b class="content">'.$shipment[$i]['pickup_address'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="40px" valign="top">
                                        Thành phố / <i>City: </i>
                                        <b class="content">'.$shipment[$i]['shipper_city'].'</b>
                                    </td>
                                    <td height="40px" valign="top">
                                        Mã bưu điện / <i>Post code : </i>
                                        <b class="content">'.$shipment[$i]['pin_code_shipper'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="40px" valign="top">
                                        Điện thoại / <i>Phone: </i>
                                        <b class="content">'.$shipment[$i]['phone_shipper'].'</b>
                                    </td>
                                    <td height="40px" valign="top">
                                        Tên nước / <i>Country : </i>
                                        <b class="content">'.$shipment[$i]['country_shipper'].'</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" id="tbl_receiver" cellpadding="0" cellspacing="0" style="background-color: #fff;">
                                <tr>
                                    <td colspan="2" style="padding: 0px; border: 0px;" cellpadding="0" cellspacing="0">
                                        <table width="100%">
                                            <tr>
                                                <td class="numbers" style="border: 0px;">3</td>
                                                <td class="title" style="border: 0px;">
                                                    <b>Người nhận/<i>Receiver</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="40px" valign="top">
                                        Tên công ty / <i>Company name : </i>
                                        <b class="content">'.$shipment[$i]['receiver_company'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="40px" colspan="2" valign="top" style="border-bottom:0px">
                                        Địa chỉ / <i>Address :</i>
                                        <b class="content">'.$shipment[$i]['delivery_address_new'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" style="border-top:0px; border-right:0px">
                                    </td>
                                    <td width="50%" height="40px" style="border-top:0px; border-left:0px" valign="bottom">
                                        Mã bưu điện / <i>Post code : </i>
                                        <b class="content">'.$shipment[$i]['pin_code_delivery'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" height="40px" valign="top">
                                        Thành phố / <i>City : </i>
                                        <b class="content">'.$shipment[$i]['receiver_city'].'</b>
                                    </td>
                                    <td width="50%" height="40px" valign="top">
                                        Tên nước / <i>Country : </i>
                                        <b class="content">'.$shipment[$i]['country_delivery'].'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" height="40px" valign="top">
                                        Điện thoại / <i>Phone : </i>
                                        <b class="content">'.$shipment[$i]['phone_delivery'].'</b>
                                    </td>
                                    <td width="50%" height="40px" valign="top">
                                        Tên người nhận / <i>Contact name : </i>
                                        <b class="content">'.$shipment[$i]['receiver_name'].'</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" id="tbl_special_delivery" cellpadding="0" cellspacing="0" style="background-color: #fff;">
                                <tr>
                                    <td colspan="3" style="padding: 0px; border: 0px;" cellpadding="0" cellspacing="0">
                                        <table width="100%">
                                            <tr>
                                                <td class="numbers" style="border: 0px;">4</td>
                                                <td class="title" style="border: 0px;">
                                                    <b>Yêu Cầu Giao Hàng Đặc Biệt / <i>Special Delivery Instruction</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="33%" valign="top" style="text-align: center; border-right:0px; padding-top: 20px" height="70px";>
                                        <img src="assets/images/checkbox-not-checked.png" width="15"/>&nbsp;
                                        Được kiểm hàng
                                    </td>
                                    <td width="33%" valign="top" style="text-align: center; border-right:0px; padding-top: 20px; border-left:0px;">
                                        <img src="assets/images/checkbox-not-checked.png" width="15"/>&nbsp;
                                        Không kiểm hàng
                                    </td>
                                    <td width="33%" valign="top" style="text-align: center; border-left:0px">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="25%">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td height="190px" align="center">
                                        <span style="font-size:22px;"><b><i>'.$shipment[$i]['tracking_no'].'</i></b></span>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <barcode code="'.(($shipment[$i]['awb_tracking_no'] != '' || $shipment[$i]['awb_tracking_no'] != null) ? $shipment[$i]['awb_tracking_no'] : $awb_online).'" type="C128B" height="2" size="0.8"/><br/>
                                        <span style="font-size:18px;"><b>'.(($shipment[$i]['awb_tracking_no'] != '' || $shipment[$i]['awb_tracking_no'] != null)?$shipment[$i]['awb_tracking_no']:$awb_online).'</b></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px;">
                            <table width="100%" id="tbl_booking_details" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3" style="padding: 0px; border: 0px;">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="numbers" style="padding: 4px; border: 0px;">5</td>
                                                <td class="title" style="padding: 4px; border: 0px;">
                                                    <b>Chi tiết hàng / <i>Booking details</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="55%" valign="top">
                                        <table width="100%" id="tbl_number_weight" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="40%" valign="top" height="100" style="border-left:0px; border-right:1px solid #2e489f; border-top:0px">
                                                    Total number of packages: <br/><br/><br/>
                                                    <b class="content">'.$shipment[$i]['pcs_value'].'</b>
                                                    <br/>
                                                </td>
                                                <td width="60%" valign="top" style="border-left:0px; border-right:0px; border-top:0px">
                                                   Total weight:<br/><br/><br/><br/>
                                                    <b class="content">'.$shipment[$i]['consignment_weight'].'</b>
                                                    <br/><br/><br/>
                                                   kg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; gr
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="100%" valign="top" style="border-bottom: 0px; border-left:0px; border-right:0px" height="60">
                                                    Trọng lượng tính cước <br/>
                                                    <i>Chargeable Weight</i><br/>
                                                    <b><br/><br/></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="100%" valign="bottom" align="right" style="border: 0px;">
                                                    kg/gr
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="45%" valign="top" style="border-right: 0px;">
                                        <table id="tbl_dimension" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td colspan="4" style="text-align: center;">
                                                    Dimension in cm
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%">
                                                    Pieces
                                                </td>
                                                <td width="25%">
                                                    Lenght
                                                </td>
                                                <td width="25%">
                                                    &nbsp;Width
                                                </td>
                                                <td width="25%">
                                                    Height
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="100%" height="35">
                                                                ..........@ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100%" height="35">
                                                                ..........@ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100%" height="35">
                                                                ..........@ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="100%" height="35">
                                                                ..........@ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 5px;">
                            <table width="100%" id="tbl_booking_content" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px; border: 0px;" cellpadding="0" cellspacing="0">
                                        <table width="100%">
                                            <tr>
                                                <td class="numbers" style="border: 0px;">6</td>
                                                <td class="title" style="border: 0px;">
                                                    <b>Nội dung hàng / <i>Description of Content</i></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" height="170px";>
                                                    <b style="font-size:10px;">'.$shipment[$i]['shipment_note'].', '.$shipment[$i]['pickup_address'].'</b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 5px;">
                            <table width="100%" id="tbl_shipper_agreement" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2" style="padding: 0px; border: 0px;" cellpadding="0" cellspacing="0">
                                        <table width="100%">
                                            <tr>
                                                <td class="numbers" style="border: 0px;">7</td>
                                                <td class="title" style="border: 0px;">
                                                    <b>Người gửi xác nhận / <i>Shipper’s agreement</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="50px" colspan="2" align="center" style="border-bottom: 0px;">
                                        <p style="font-size: 7px; line-height: 80px;">(Tôi đồng ý với các điều kiện và điều khoản vận chuyển ở mặt sau của vận đơn này.
                                        Bưu kiện này không gửi kèm tiềm mặt,hàng cấm và hàng nguy hiểm /
                                        <i>I/We agreed the Terms and conditions of Carriage on the back of this waybill.
                                        This shipment does not contain cash,restricted good and dangerous goods)</i></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="150px" valign="top" align="left" width="70%" style="border-top: 0px; border-right: 0px;">
                                        Chữ ký người gửi / <i>Shipper signature</i>
                                    </td>
                                    <td height="" valign="top" align="center" style="border-top: 0px; border-left: 0px;">
                                        Ngày / <i>Date</i>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="27%">
                <table width="100%" cellpadding="0" cellspacing="0" style="background-color: #f1f1f9">
                    <tr>
                        <td height="83px">
                            <table width="100%" id="tbl_branch_code" cellpadding="0" cellspacing="0">
                                <tr>
                                    <tr>
                                        <td valign="top" width="50%" style="background-color:#e3e5f2;">
                                            ORGIN CODE <br/><br/>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="font-size: 24px">'.$shipment[$i]['origin_branch'].'</b>
                                        </td>
                                        <td valign="top" width="50%" style="background-color:#e3e5f2;">
                                            DESTINATION CODE<br/><br/>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <b style="font-size: 24px;">'.$shipment[$i]['dest_branch'].'</b>
                                        </td>
                                    </tr>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 6px;">
                            <table width="100%" id="tbl_product_service" cellpadding="0" cellspacing="0" style="background-color: #fff;">
                                <tr>
                                    <td width="100%" style="padding: 0px; border: 0px;" cellpadding="0" cellspacing="0">
                                        <table width="100%">
                                            <tr>
                                                <td class="numbers" style="border: 0px; padding: 4px;">8</td>
                                                <td class="title" style="border: 0px; padding: 4px;">
                                                    <b>Sản phẩm và dịch vụ / <i>Product and service</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%">
                                        <table width="100%" id="table_product_service_1" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td valign="middle" height="20px">
                                                    '.$check_dox.'
                                                    Thư từ / <i>DOX</i>
                                                </td>
                                                <td valign="middle" height="20px">
                                                    '.$check_non_dox.'
                                                    Hàng hóa / <i>Non-DOX</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle" height="22px">
                                                    '.$check_domestic.'
                                                    Trong nước / <i>Domestic</i>
                                                </td>
                                                <td valign="middle" height="22px">
                                                    '.$check_international.'
                                                    Quốc tế / <i>International</i>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%">
                                        <table width="100%" id="table_product_service_2" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    '.$check_standard.'
                                                    CPN / <i>Standard Express</i>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    '.$check_ecommerce.'
                                                    TMĐT / <i>Ecommerce</i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    '.$check_priority.'
                                                    CPN Hỏa tốc / <i>Priority Express</i>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    '.$check_36h.'
                                                    36h
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    '.$check_economy.'
                                                    CPN Tiết kiệm / <i>Ecomomy Express</i>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    '.$check_48h.'
                                                    48h
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="50%" style="padding: 0px;">
                                                    <table>
                                                        <tr>
                                                            <td width="15%">
                                                                '.$check_cod.'
                                                                COD
                                                                &nbsp;&nbsp;&nbsp;
                                                            <td>
                                                            <td style="border: solid 1px #2e489f; padding: 2px;" height="40px" width="75%" valign="top">
                                                                Số tiền thu hộ / <i>COD amount: </i><br/>
                                                                <b class="content">'.$cod_value.'</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20px">
                                                    '.$check_ras.'
                                                    Dịch vụ khác / <i>Others Services</i>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <br/>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 5px;">
                            <table width="100%" id="tbl_charges" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2" style="padding: 0px; border: 0px;" cellpadding="0" cellspacing="0">
                                        <table width="100%">
                                            <tr>
                                                <td class="numbers" style="border: 0px; padding: 4px;">9</td>
                                                <td class="title" style="border: 0px; padding: 4px;">
                                                    <b>Cước phí / <i>Charges</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="52%" style="background-color:#e3e5f2; border-left: solid 1px #2e489f; border-right: solid 1px #2e489f;">
                                        Phí chuyển phát / <i>Freight charge  :</i>
                                    </td>
                                    <td width="48%" style="background-color:#e3e5f2;">
                                        <b class="content">'.
                                            ($cash_value != 0 ? number_format($cash_value) : '')
                                            //($shipment[$i]['total_selling_price'] != 0 ? number_format($shipment[$i]['total_selling_price']) : '')
                                        .'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#e3e5f2; border-left: solid 1px #2e489f; border-right: solid 1px #2e489f;">
                                        Phí đóng gói / <i>Packing fee:</i>
                                    </td>
                                    <td style="background-color:#e3e5f2;">
                                        <b class="content">
                                        
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#e3e5f2; font-size: 10px; border-left: solid 1px #2e489f; border-right: solid 1px #2e489f;">
                                        Phí bảo hiểm / <i>Insurance fee:</i>
                                    </td>
                                    <td style="background-color:#e3e5f2;">
                                        <b class="content">

                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#e3e5f2; border-left: solid 1px #2e489f; border-right: solid 1px #2e489f;">
                                        Phí thu hộ / <i>COD fee:</i>
                                    </td>
                                    <td style="background-color:#e3e5f2;">
                                        <b class="content">'.''
                                        //($shipment[$i]['cod_charge'] != 0 ? number_format($shipment[$i]['cod_charge']) : '')
                                        .'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#e3e5f2; border-left: solid 1px #2e489f; border-right: solid 1px #2e489f;">
                                        Phí khác / <i>Other charges:</i>
                                    </td>
                                    <td style="background-color:#e3e5f2;">
                                        <b class="content">'.''
                                            //($shipment[$i]['total_selling_vas'] != 0 ? number_format($shipment[$i]['total_selling_vas']) : '')
                                        .'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color:#e3e5f2; border: solid 1px #2e489f; border-top: 0px;" height="50px" valign="top">
                                        Tổng cộng / <i>Total:</i>
                                    </td>
                                    <td style="background-color:#e3e5f2; border-bottom: solid 1px #2e489f;">
                                        <b class="content">'.''
                                            //$shipment[$i]['amount']
                                        .'</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 4px;">
                            <table width="100%" id="tbl_receiver_by" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="padding: 0px; border: 0px;" cellpadding="0" cellspacing="0">
                                        <table width="100%">
                                            <tr>
                                                <td class="title" style="border: 0px; padding: 4px;">
                                                    <b>Người nhận / <i>Receiver by:</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" style="background-color:#e3e5f2; border-left: solid 1px #2e489f;">
                                        Giờ / <i>Time :</i>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" style="background-color:#e3e5f2; border-left: solid 1px #2e489f;">
                                        Ngày / <i>Date :</i>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" height="72px" style="background-color:#e3e5f2; border-left: solid 1px #2e489f; border-bottom: solid 1px #2e489f;">
                                        Chữ ký người nhận / <i>Receiver signature :</i>
                                        <br/><br/>
                                        (Họ tên / <i>Full name</i>)
                                        <br/><br/><br/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 8px;">
                            <table width="100%" id="tbl_picked_up_by" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2" style="padding: 0px; border: 0px;">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="title" style="border: 0px; padding: 4px;">
                                                    <b>Ngày giờ gửi / <i>Picked up by:</i></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" style="background-color:#e3e5f2; border-left: solid 1px #2e489f; border-right: solid 1px #2e489f;">
                                        (Số tuyến / <i>Route no.</i>)  :
                                    </td>
                                    <td width="50%" style="background-color:#e3e5f2;">
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" style="background-color:#e3e5f2; border-left: solid 1px #2e489f; border-right: solid 1px #2e489f;">
                                        Giờ / <i>Time :</i>
                                    </td>
                                    <td width="50%" style="background-color:#e3e5f2;">
                                        <b class="content">'.$time_pickup.'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" style="background-color:#e3e5f2; border-left: solid 1px #2e489f; border-right: solid 1px #2e489f;">
                                        Ngày / <i>Date :</i>
                                    </td>
                                    <td width="50%" style="background-color:#e3e5f2;">
                                        <b class="content">'.$date_pickup.'</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" style="background-color:#e3e5f2; border: solid 1px #2e489f; border-top: 0;">
                                        Tên / <i>Name :</i>
                                    </td>
                                    <td width="50%" style="background-color:#e3e5f2; border-bottom: solid 1px #2e489f;">
                                        <b class="content">'.$shipment[$i]['pickup_man'].'</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
    if($i < $n-1){
        $html .= '<pagebreak />';
    }
}
$css = '
    <style type="text/css">
        #table_parent table{
            padding: 0px;
            border-collapse: collapse;
            color: #2e489f;
        }
        #table_parent tr td {
            padding: 0px;
        }
        .numbers{
            background-color: red;
            width: 20px;
            text-align: center;
            color: #fff;
        }
        .title{
            background-color: #2e489f;
            color: #fff;
        }


        #btl_payer_account_number_and_insurance{
        }
        #btl_payer_account_number_and_insurance tr{
            border-left: solid 1px #2e489f;
        }
        #btl_payer_account_number_and_insurance tr td{
            padding: 4px;
        }


        #tbl_shipper tr td{
            padding: 4px;
            border: solid 1px #2e489f;
            margin: 0px;
        }


        #tbl_receiver{
        }
        #tbl_receiver tr{
        }
        #tbl_receiver tr td{
            padding: 4px;
            border: solid 1px #2e489f;
        }


        #tbl_special_delivery{
        }
        #tbl_special_delivery tr{
        }
        #tbl_special_delivery tr td{
            padding: 4px;
            border: solid 1px #2e489f;
        }


        #tbl_special_delivery{
        }
        #tbl_special_delivery tr{
        }
        #tbl_special_delivery tr td{
            padding: 4px;
            border: solid 1px #2e489f;
        }


        #tbl_booking_details{
        }
        #tbl_booking_details tr{
        }
        #tbl_booking_details tr td{
            padding: 0px;
            border: solid 1px #2e489f;
        }


        #tbl_number_weight tr td{
            padding: 4px;
            border: solid 1px #2e489f;
        }



        #tbl_dimension tr td{
            padding: 5px 0px;
            border: 0px;
        }


        #tbl_dimension table tr td{
            padding: 0px;
            border: 0px;
        }


        #tbl_booking_content table tr td{
            padding: 4px;
            border: solid 1px #2e489f;
        }


        #tbl_shipper_agreement tr td{
            padding: 4px;
            border: solid 1px #2e489f;
        }


        #tbl_branch_code tr td{
            padding: 4px;
            border: solid 1px #2e489f;
            height: 70px;
        }


        #tbl_product_service tr td{
            padding: 4px;
            border: solid 1px #2e489f;
        }


        #table_product_service_1 tr td{
            padding: 4px 4px;
            border: 0px;
        }


        #table_product_service_2 tr td{
            padding: 4px 4px;
            border: 0px;
        }


        #tbl_charges tr td{
            padding: 7px 6px;
            border: solid 1px #fff;
        }


        #tbl_receiver_by tr td{
            padding: 8px 6px;
            border: solid 1px #fff;
        }


        #tbl_picked_up_by tr td{
            padding: 8px 6px;
            border: solid 1px #fff;
        }

        .content{
            font-size: 14px;
        }
    </style>';

$mpdf = new \Mpdf\Mpdf(['tempDir' => $_SERVER['DOCUMENT_ROOT'].'/vendor/mpdf/mpdf/tmp', 'A4-L', 7, '', 4, 4, 4, 4, 4, 4]);
$mpdf->WriteHTML($css);
$mpdf->WriteHTML($html);
$mpdf->SetWatermarkImage('assets/images/logo_speedlink_print.png',0.1,50);
$mpdf->showWatermarkImage = true;

$file_name = $this->session->userdata('login')['id']."_ListShipment.pdf";
echo ($mpdf->Output($this->config->item('url_folder_upload').'print_shipment/'.$file_name, 'F'));

if(file_exists($this->config->item('url_folder_upload').'print_shipment/'.$file_name)){
    echo ($this->config->item('base_url_pdf').'uploads/print_shipment/'.$file_name);
} else {
    //fail;
}