<main>
    <div class="container">
        <section class="panel section-block register-section">
            <form class="form-block user-changepass-form" id="user-changepass-form" onsubmit="changePassword(); return false">
                <h3 class="form-title">
                    <?=$language['txt_user_change_pass'];?>
                </h3>
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-danger" id="error_change_password" style="display: none;"></div>
                            <div class="alert alert-success" id="success_change_password" style="display: none;"></div>
                            <div class="form-group">
                                <label class="control-label"><?=$language['txt_user_change_pass_old_pass']?> (<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="password" name="old_password" id="old_password">
                                <span class="error" id="error_old_password"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?=$language['txt_user_change_pass_new_pass']?> (<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="password" name="new_password" id="new_password">
                                <span class="error" id="error_new_password"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?=$language['txt_user_change_pass_confirmed_pass']?> (<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="password" name="confirmed_password" id="confirmed_password">
                                <span class="error" id="error_retype_password"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="action-row text-center action-group">
                                <button type="button" class="btn btn-primary btn-send-request" onclick="changePassword();"><?=$language['txt_user_change_password_change_password']?></button>
                                <button type="button" onclick="location.href='/';" class="btn btn-gray btn-close"><?=$language['txt_user_change_password_cancel']?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
</main>
<script src="/assets/js/change-password.js?v=<?php echo $this->config->item('version_random'); ?>"></script>