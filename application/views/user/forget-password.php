<main>
    <div class="container">
        <section class="panel section-block register-section">
            <form class="form-block forget-password-form" id="forget-password-form" onsubmit="sendRequestForgetPassword(); return false">
                <h3 class="form-title">
                    <?=$language['txt_user_forget_password'];?>
                </h3>
                <div class="loader"></div>
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-danger" id="error_forget_password" style="display: none;">Indicates a dangerous or potentially negative action.
                            </div>
                            <div class="alert alert-success" id="success_forget_password" style="display: none;">Indicates a dangerous or potentially negative action.
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?=$language['txt_user_forget_password_input_your_email']?> (<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="text" name="email" id="email" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="action-row text-center action-group">
                                <button type="button" class="btn btn-primary btn-send-request" onclick="sendRequestForgetPassword();"><?=$language['txt_user_forget_password_send_request']?></button>
                                <button type="button" onclick="location.href='/';" class="btn btn-gray btn-close"><?=$language['txt_user_forget_password_close']?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
</main>
<script type="text/javascript">
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src="/assets/js/forget-password.js?v=<?php echo $this->config->item('version_random'); ?>"></script>