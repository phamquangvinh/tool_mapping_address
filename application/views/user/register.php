<?php
    $lang = 'en';
    if($this->session->get_userdata()['site_lang'] == 'vietnamese') $lang = 'vi';
?>
<main>
    <div class="container">
        <section class="panel section-block register-section">
            <div class="form-block register-form">
                <h3 class="form-title">
                    <?php echo $language['txt_user_register']; ?>
                </h3>
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_full_name']; ?>(<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="text" name="name_register" id="name_register" >
                                <span class="error" id="error_name_register"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_user_name']; ?>(<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="text" name="user_name_register" id="user_name_register" >
                                <span class="error" id="error_user_name_register"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_pass_word']; ?>(<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="password" name="password_register" id="password_register" >
                                <span class="error" id="error_password_register"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_re_pass_word']; ?>(<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="password" name="re_password_register" id="re_password_register" >
                                <span class="error" id="error_re_password_register"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_email']; ?>(<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="email" name="email_register" id="email_register" >
                                <span class="error" id="error_email_register"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_mobile_phone']; ?>(<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="tel" name="mobile_phone_register" id="mobile_phone_register"  onkeypress="onlyPressNumberAndSpecialChar(event)">
                                <span class="error" id="error_mobile_phone_register"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_passport']; ?>(<span class="important-field">&ast;</span>)</label>
                                <input class="form-control" type="text" name="cmnd_register" id="cmnd_register"  onkeypress="onlyPressNumberAndSpecialChar(event)">
                                <span class="error" id="error_cmnd_register"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <button class="btn btn-add btn-show-register" data-toggle="collapse" data-target=".more-info-register">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <span><?php echo $language['txt_user_register_as_company']; ?></span>
                            </button>
                        </div>
                    </div>
                    <div class="row collapse more-info-register">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_nation']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" style="display: none;" name="country_register" id="country_register" onchange="changeCountryAddress()" lang="<?=$lang?>">
                                        <?php echo $countryOption?>
                                    </select>
                                    <!-- validate -->
                                    <!-- <label class="error">This field is required.</label> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_city']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" style="display: none;" name="city_register" id="city_register" onchange="changeCityAddress()" lang="<?=$lang?>">
                                    </select>
                                    <!-- validate -->
                                    <!-- <label class="error">This field is required.</label> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_district']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" style="display: none;" name="district_register" id="district_register" lang="<?=$lang?>">
                                    </select>
                                    <!-- validate -->
                                    <!-- <label class="error">This field is required.</label> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_address']; ?></label>
                                <input class="form-control" type="text"  name="address_register" id="address_register">
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_company_name']; ?></label>
                                <input class="form-control" type="text"  name="company_name_register" id="company_name_register">
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_department']; ?></label>
                                <input class="form-control" type="text" name="department_register" id="department_register"  >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_user_register_captcha']; ?>(<span class="important-field">&ast;</span>)</label>
                                 <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_recaptcha_key') ?>"></div>
                            </div>
                            <span class="error" id="error_captcha"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label class="checkbox-label">
                                    <input type="checkbox" name="checkRule" id="checkRule">
                                        <span class="control-label"><?php echo $language['txt_user_register_agree_with']; ?>
                                        <a class="font-weight-bold" href="#"><?php echo $language['txt_user_register_policy_services']; ?></a>
                                        <?php echo $language['txt_user_register_of_speedlink']; ?>
                                     </span>
                                </label>                                
                            </div>
                        </div>                        
                        <span class="error" id="error_check_rule"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="action-row text-center action-group">
                                <button type="button" class="btn btn-primary" onclick="RegisterSubmit();"><?php echo $language['txt_user_register_submit']; ?></button>
                                <button type="button" class="btn btn-gray" onclick="ResetFormRegister();"><?php echo $language['txt_user_register_reset']; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<script src="/assets/js/register.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript">
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
    var register_as_company = '<?php echo $language["txt_user_register_as_company"]; ?>';
</script>