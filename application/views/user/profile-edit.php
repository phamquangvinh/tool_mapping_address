<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><?php echo $language['txt_user_detail_home']; ?></a>
                </li>
                <li class="active"><?php echo $language['txt_user_edit_info_user']; ?></li>
            </ol>
        </div>
        <section class="panel section-block user-profile">
            <form action = "" method = "POST" enctype="multipart/form-data">
                <div class="form-block user-profile-info">
                    <h3 class="form-title">
                        <?php echo $language['txt_user_edit_info_user']; ?>
                    </h3>
                    <div class="group-box">
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="control-label"><?php echo $language['txt_user_edit_avatar']; ?></label>
                                <div class="kv-avatar">
                                    <input class="avatar-edit" name="avatar" id="avatar" type="file">
                                </div>
                            </div>
                        </div>
                        <?php if($dataShow['accounts_id'] != null && $dataShow['accounts_id'] != ''){ ?>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_accounts_no']; ?></label>
                                    <input class="form-control" type="text" id="account_no" name="account_no" readonly="" value="<?php echo $dataShow['account_no']; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_customer_no']; ?></label>
                                    <input class="form-control" type="text" name="customer_code" id="customer_code" readonly="" value="<?php echo $dataShow['customer_code']; ?>">
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_company']; ?></label>
                                    <input class="form-control" type="text" name="company_name" id="company_name" value="<?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_company_name'] : $dataShow['lead_company_name']); ?>">
                                    <?php if(form_error('company_name')){ ?><span class="error"><?php echo form_error('company_name'); ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_tax_code']; ?></label>
                                    <input class="form-control" type="text" name="tax_code" id="tax_code" value="<?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_tax_code'] : $dataShow['lead_tax_code']); ?>">
                                    <?php if(form_error('tax_code')){ ?><span class="error"><?php echo form_error('tax_code'); ?></span><?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_phone']; ?></label>
                                    <input class="form-control" type="text" name="phone" id="phone" value="<?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_phone'] : $dataShow['lead_phone']); ?>">
                                    <?php if(form_error('phone')){ ?><span class="error"><?php echo form_error('phone'); ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_email']; ?></label>
                                    <input class="form-control" type="email" name="email" id="email" value="<?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_email_address'] : $dataShow['lead_email_address']); ?>" disabled>
                                    <?php if(form_error('email')){ ?><span class="error"><?php echo form_error('email'); ?></span><?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_payment_method']; ?></label>
                                    <select class="form-control chosenSelect" style="display: none;" name="payment_method" id="payment_method" disabled>
<!--                                        --><?php //echo $dataShow['payment_method_option']; ?>
                                        <option value=""><?php echo $this->session->get_userdata()['site_lang']=='english' ? 'Cash' : 'Tiền Mặt'?></option>
                                    </select>
                                    <?php if(form_error('payment_method')){ ?><span class="error"><?php echo form_error('payment_method'); ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_country']; ?></label>
                                    <div class="select-box">
                                        <select class="form-control chosenSelect" style="display: none;" name="country" id="country" onchange="changeCountry();">
                                            <?php echo $dataShow['optionCountry']; ?>
                                        </select>
                                        <!-- validate -->
                                        <!-- <label class="error">This field is required.</label> -->
                                    </div>
                                    <?php if(form_error('country')){ ?><span class="error"><?php echo form_error('country'); ?></span><?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_city']; ?></label>
                                    <div class="select-box">
                                        <select class="form-control chosenSelect" style="display: none;" name="city" id="city" onchange="changeCity();">
                                            <?php echo $dataShow['optionCity']; ?>
                                        </select>
                                        <!-- validate -->
                                        <!-- <label class="error">This field is required.</label> -->
                                    </div>
                                    <?php if(form_error('city')){ ?><span class="error"><?php echo form_error('city'); ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_district']; ?></label>
                                    <div class="select-box">
                                        <select class="form-control chosenSelect" style="display: none;" name="district" id="district">
                                            <?php echo $dataShow['optionDistrict']; ?>
                                        </select>
                                        <!-- validate -->
                                        <!-- <label class="error">This field is required.</label> -->
                                    </div>
                                    <?php if(form_error('district')){ ?><span class="error"><?php echo form_error('district'); ?></span><?php } ?>
                                </div>
                            </div>
                        </div>
    <!--                    <div class="row">-->
    <!--                        <div class="col-xs-12 col-sm-6">-->
    <!--                            <div class="form-group">-->
    <!--                                <label class="control-label">--><?php //echo $language['txt_user_edit_area']; ?><!--</label>-->
    <!--                                <div class="select-box">-->
    <!--                                    <select class="form-control chosenSelect" style="display: none;">-->
    <!--                                        --><?php //echo $dataShow['optionWard']; ?>
    <!--                                    </select>-->
                                        <!-- validate -->
                                        <!-- <label class="error">This field is required.</label>
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_address']; ?></label>
                                    <input class="form-control" type="text" name="address" id="address" value="<?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_street_name'] : $dataShow['lead_street_name']); ?>">
                                    <?php if(form_error('address')){ ?><span class="error"><?php echo form_error('address'); ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_bank_name']; ?></label>
                                    <input class="form-control" type="text" name="bank_name" id="bank_name" value="<?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_bank_name'] : $dataShow['lead_bank_name']); ?>">
                                    <?php if(form_error('bank_name')){ ?><span class="error"><?php echo form_error('bank_name'); ?></span><?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_bank_account_no']; ?></label>
                                    <input class="form-control" type="text" name="bank_account_no" id="bank_account_no" value="<?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_bank_account_name'] : $dataShow['lead_bank_account_name']); ?>">
                                    <?php if(form_error('bank_account_no')){ ?><span class="error"><?php echo form_error('bank_account_no'); ?></span><?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_bank_number']; ?></label>
                                    <input class="form-control" type="text" name="bank_number" id="bank_number" value="<?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_bank_number'] : $dataShow['lead_bank_number']); ?>">
                                    <?php if(form_error('bank_number')){ ?><span class="error"><?php echo form_error('bank_number'); ?></span><?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_user_edit_description']; ?></label>
                                    <textarea class="form-control" rows="5" id="description" name="description"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_description'] : $dataShow['lead_description']); ?></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- TODO: Button save and cancel -->
                        <div class="action-row text-center action-group">
                            <button type="submit" class="btn btn-primary"><?php echo $language['btn_user_edit_save']; ?></button>
                            <button type="button" class="btn btn-gray" onclick="cancelEdit();"><?php echo $language['btn_user_edit_cancel']; ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var avatar_user = '<?php echo $dataShow['avatar_user'] ? 'uploads/avatar/'.$dataShow['avatar_user'] : 'assets/images/upload/user-avatar.png' ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/user_edit.js?v=<?php echo $this->config->item('version_random'); ?>'></script>