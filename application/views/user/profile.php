<?php
    $lang = 'en';
    if($this->session->get_userdata()['site_lang'] == 'vietnamese') $lang = 'vi';
?>
<script type="text/javascript">
    <?php $timestamp = time(); $user = $this->session->userdata("login"); ?>
    var user_id = '<?php echo $user["id"];?>';
    var timestamp = '<?php echo $timestamp;?>';
    var token = '<?php echo md5('_ecos_salt' . $timestamp);?>';
    var flagPage;    
</script>
<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><?php echo $language['txt_user_detail_home']; ?></a>
                </li>
                <li class="active"><?php echo $language['txt_user_detail_info_user']; ?></li>
            </ol>
        </div>
        <section class="panel section-block user-profile">
            <div class="form-block user-profile-info">
                <h3 class="form-title">
                    <?php echo $language['txt_user_detail_info_user']; ?>
                </h3>
                <div class="group-box">
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label"><?php echo $language['txt_user_detail_avatar']; ?></label>
                            <div class="thumbnail wrap-img">
                                <?php if (isset($dataShow['avatar']) && !empty($dataShow['avatar'])){ ?>
                                    <img class="img-responsive" src="uploads/avatar/<?php echo $dataShow['avatar'];?>" alt="Speedlink">
                                <?php }else{?>
                                    <img class="img-responsive" src="assets/images/upload/user-avatar.png" alt="Speedlink">
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <?php if($dataShow['accounts_id'] != null && $dataShow['accounts_id'] != ''){ ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_edit_accounts_no']; ?></label>
                            <span class="user-info-span"><?php echo $dataShow['account_no']; ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_edit_customer_no']; ?></label>
                            <span class="user-info-span"><?php echo $dataShow['customer_code']; ?></span>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_company_name']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_company_name'] : $dataShow['lead_company_name']); ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_tax_code']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_tax_code'] : $dataShow['lead_tax_code']); ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_phone_number']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_phone'] : $dataShow['lead_phone']); ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_email']; ?></label>
                            <span class="user-info-span"><a href="javascript:void(0)"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_email_address'] : $dataShow['lead_email_address']); ?></a></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_payment_method']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['accounts_payment_method'] : $dataShow['lead_payment_method']); ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_country']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_country_name'] : $dataShow['lead_country_name']); ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_city']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_city_name'] : $dataShow['lead_city_name']); ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_district']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_district_name'] : $dataShow['lead_district_name']); ?></span>
                        </div>
                    </div>
                    <div class="row">
<!--                        <div class="col-xs-12 col-sm-6">-->
<!--                            <label class="control-label">--><?php //echo $language['txt_user_detail_area']; ?><!--</label>-->
<!--                            <span class="user-info-span">--><?php //echo ($dataShow['accounts_id'] != null ? $dataShow['account_district_name'] : $dataShow['lead_district_name']); ?><!--</span>-->
<!--                        </div>-->
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_address']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_street_name'] : $dataShow['lead_street_name']); ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_bank_name']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_bank_name'] : $dataShow['lead_bank_name']); ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_bank_account_name']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_bank_account_name'] : $dataShow['lead_bank_account_name']); ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label class="control-label"><?php echo $language['txt_user_detail_bank_number']; ?></label>
                            <span class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_bank_number'] : $dataShow['lead_bank_number']); ?></span>
                        </div>
                    </div>
<!--                    <div class="row">-->
<!--                    </div>-->
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label"><?php echo $language['txt_user_detail_description']; ?></label>
                            <p class="user-info-span"><?php echo ($dataShow['accounts_id'] != null ? $dataShow['account_description'] : $dataShow['lead_description']); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs">
                            <li class="active" data-name="shipper">
                                <a href=".shipper-pane" data-toggle="tab"><?php echo $language['txt_user_detail_shipper_address']; ?></a>
                            </li>
                            <li data-name="consignee">
                                <a href=".consignee-pane" data-toggle="tab"><?php echo $language['txt_user_detail_consignee_address']; ?></a>
                            </li>
                            <li style="float: right">
                                <button class="btn btn-sm fa fa-print btn-default" onclick="importExcel();">
                                    Import
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="group-box">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="tab-content">
                                <div class="tab-pane fade in active shipper-pane">
                                    <div class="control-search">
                                        <input type="search" name="info-address-search" class="form-control" id="inputSearchShipper">
                                        <button class="btn-search">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <ul class="list-dilivery-contact listSlimScroll" id="list_shipper_contact">                                        
                                    </ul>
                                    <button class="btn btn-add" data-toggle="modal" data-target=".info-add-modal" id="add_address_shipper">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span><?php echo $language['txt_user_detail_add_new']; ?></span>
                                    </button>
                                </div>
                                <div class="tab-pane fade consignee-pane">
                                    <div class="control-search">
                                        <input type="search" name="info-address-search" class="form-control control-search" id="inputSearchConsignee">
                                        <button class="btn-search">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <ul class="list-dilivery-contact listSlimScroll" id="list_consignee_contact">
                                    </ul>
                                    <button class="btn btn-add" data-toggle="modal" data-target=".info-add-modal" id="add_address_consignee">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span><?php echo $language['txt_user_detail_add_new']; ?></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade info-add-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-success-modal" aria-label="Close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $language['txt_create_booking_add']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_create_booking_name']; ?></label>
                            <input type="text" class="form-control" id="add_name">
                            <span class="error" id="error_add_address_name"><?php echo $language['txt_add_address_error_receiver_name']; ?></span>
                        </div>
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_create_booking_address_shipper']; ?></label>
                            <input type="text" class="form-control" id="add_street">
                            <span class="error" id="error_add_address_street"><?php echo $language['txt_add_address_error_street']; ?></span>
                        </div>
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_create_booking_city']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="add_city" onchange="changeCityAddress();" data-placeholder="<?php echo $language['txt_create_booking_select_city']; ?>" lang="<?=$lang?>">
                                </select>
                                <span class="error" id="error_add_address_city"><?php echo $language['txt_add_address_error_city']; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_create_booking_ward']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="add_ward" data-placeholder="<?php echo $language['txt_create_booking_select_ward']; ?>" lang="<?=$lang?>">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_create_booking_mobile_phone']; ?></label>
                            <input type="tel" class="form-control" id="add_phone">
                            <span class="error" id="error_add_address_phone"><?php echo $language['txt_add_address_error_receiver_phone']; ?></span>
                            <span class="error" id="error_add_address_phone_length"><?php echo $language['txt_add_phone_number_invalid']; ?></span>
                        </div>
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_create_booking_nation']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="add_country" onchange="changeCountryAddress();" data-placeholder="<?php echo $language['txt_create_booking_select_nation']; ?>" lang="<?=$lang?>">
                                    <?php echo $countryOption ?>
                                </select>
                                <span class="error" id="error_add_address_nation"><?php echo $language['txt_add_address_error_nation']; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_create_booking_district']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="add_district" onchange="changeDistrictAddress();" data-placeholder="<?php echo $language['txt_create_booking_select_district']; ?>" lang="<?=$lang?>">
                                </select>
                                <span class="error" id="error_add_address_district"><?php echo $language['txt_add_address_error_district']; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_create_booking_zip_code']; ?></label>
                            <input type="text" class="form-control" id="add_zip_code">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-center action-group">
                    <button type="button" class="btn btn-primary" id="btn_save_address" onclick="saveAddress(this);"><?php echo $language['txt_create_booking_ok']; ?></button>
                    <button type="reset" class="btn btn-gray" onclick="resetFormAddAddress();"><?php echo $language['txt_create_booking_reset']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade info-edit-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" aria-label="Close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $language['txt_create_booking_edit']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for=""><?php echo $language['txt_create_booking_name']; ?></label>
                            <input type="text" class="form-control" id="edit_address_name">
                            <input type="hidden" class="form-control" id="edit_address_name_old">
                            <span class="error" id="error_edit_address_name"><?php echo $language['txt_add_address_error_receiver_name']; ?></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for=""><?php echo $language['txt_create_booking_mobile_phone']; ?></label>
                            <input type="tel" class="form-control" id="edit_address_phone">
                            <input type="hidden" class="form-control" id="edit_address_phone_old">
                            <span class="error" id="error_edit_address_phone"><?php echo $language['txt_add_address_error_receiver_phone']; ?></span>
                            <span class="error" id="error_edit_address_phone_length"><?php echo $language['txt_add_phone_number_invalid']; ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for=""><?php echo $language['txt_create_booking_nation']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="edit_address_country" lang="<?=$lang?>" onchange="changeCountryEditAddress();">
                                    <?php echo $countryOption ?>
                                </select>
                                <input type="hidden" class="form-control" id="edit_address_country_old">
                                <span class="error" id="error_edit_address_country"><?php echo $language['txt_add_address_error_nation']; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for=""><?php echo $language['txt_create_booking_city']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="edit_address_city" lang="<?=$lang?>" onchange="changeCityEditAddress();">
                                </select>
                                <input type="hidden" class="form-control" id="edit_address_city_old">
                                <span class="error" id="error_edit_address_city"><?php echo $language['txt_add_address_error_city']; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for=""><?php echo $language['txt_create_booking_district']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="edit_address_district" lang="<?=$lang?>" onchange="changeDistrictEditAddress();">
                                </select>
                                <input type="hidden" class="form-control" id="edit_address_district_old">
                                <span class="error" id="error_edit_address_district"><?php echo $language['txt_add_address_error_district']; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for=""><?php echo $language['txt_create_booking_ward']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="edit_address_ward" lang="<?=$lang?>"> 
                                </select>
                                <input type="hidden" class="form-control" id="edit_address_ward_old">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for=""><?php echo $language['txt_create_booking_address_shipper']; ?></label>
                            <input type="text" class="form-control" id="edit_address_street">
                            <input type="hidden" class="form-control" id="edit_address_street_old">
                            <span class="error" id="error_edit_address_street"><?php echo $language['txt_add_address_error_street']; ?></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label class="control-label" for=""><?php echo $language['txt_create_booking_zip_code']; ?></label>
                            <input type="text" class="form-control" id="edit_address_postal_code">
                            <input type="hidden" class="form-control" id="edit_address_postal_code_old">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-center action-group">
                    <button type="button" class="btn btn-primary" onclick="changeAddress(this);" id="btn_edit_address" data-type_address=""><?php echo $language['btn_user_edit_save']; ?></button>
                    <button type="reset" class="btn btn-gray" onclick="resetFormEditAddress();"><?php echo $language['btn_user_edit_reset']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="importModal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" style="width: 90%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $language['txt_import']; ?></h4>
            </div>
            <form id="popup_edit_soa" method="post" class="form-horizontal">
                <div class="modal-body">
                    <table>
                        <tr>
                            <td width="20%">
                                <div class="col-md-6"><?php echo $language['txt_import_total_record']; ?>:</div><label class="col-md-6" id="import_total_record">0</label>
                                <div class="col-md-6"><?php echo $language['txt_import_total_failed']; ?>:</div><label class="col-md-6" id="import_total_failed">0</label>
                            </td>
                            <td width="50%">
                                <center>                                        
                                    <embed width="300" height="115" id="popup_flash" type="application/x-shockwave-flash" src="example.swf" pluginspage="http://www.adobe.com/go/getflashplayer" />
                                    <h3><?php echo $language['txt_import_select_excel_file']; ?></h3>
                                    <input name="file_upload" id="file_upload" type="file"/>&nbsp;
                                    <a target="_blank" href="/assets/docs/template.xlsx"><?php echo $language['txt_import_download_sample']; ?></a>
                                    <br/>                                        
                                    <span id="img_loading"></span>
                                </center>                                    
                            </td>
                            <td width="20%"></td>
                        </tr>
                    </table>
                    <br>
                    <table class="table table-bordered" id="import" width="100%">
                        <thead>
                            <tr>
                                <th><?php echo $language['txt_import_name']; ?></th>
                                <th><?php echo $language['txt_import_phone']; ?></th>
                                <th><?php echo $language['txt_import_address']; ?></th>
                                <th><?php echo $language['txt_import_country']; ?></th>
                                <th><?php echo $language['txt_import_city']; ?></th>
                                <th><?php echo $language['txt_import_district']; ?></th>
                                <th><?php echo $language['txt_import_ward']; ?></th>
                                <th><?php echo $language['txt_import_postal_code']; ?></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="saveImport();"><?php echo $language['txt_import']; ?></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $language['txt_import_close']; ?></button>
                </div>
            </form>
        </div>

    </div>
</div>
<script type="text/javascript">
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/user_info.js?v=<?php echo $this->config->item('version_random'); ?>'></script>