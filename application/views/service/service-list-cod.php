<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="/"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_breadcrumb_list_cod']; ?></li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <div class="form-block service-cod-form">
                <h3 class="form-title">
                    <?php echo $language['txt_breadcrumb_list_cod']; ?>
                </h3>
                <div class="form-wrap">
                    <div class="search-booking-group">
                        <div class="row">
                            <form action="" method="post">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_list_booking_date_range']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" onchange="changeDateRange();" id="date_range" name="date_range">
                                                <?php echo $dataFilter['dateRangeOption']; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_list_booking_filter_date_from']; ?></label>
                                        <div class="input-group date datepicker">
                                            <input class="form-control" readonly="" name="from_date" id="from_date" type="text" value="<?php echo $dataFilter['from_date'] ?>">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_list_booking_filter_date_to']; ?></label>
                                        <div class="input-group date datepicker">
                                            <input class="form-control"  readonly="" name="to_date" id="to_date" type="text" value="<?php echo $dataFilter['to_date'] ?>">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_shipment_code']; ?></label>
                                        <input class="form-control" type="input" name="awb_tracking_no" placeholder="" value="<?php echo $dataFilter['shipment_code'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_return_cod_code']; ?></label>
                                        <input class="form-control" type="input" name="cod_code" placeholder="" value="<?php echo $dataFilter['cod_code'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_status_cod']; ?></label>
                                        <select class="form-control" id="status_cod" name="status_cod">
                                            <?php echo $dataFilter['statusCOD']; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-offset-8 col-md-4">
                                    <div class="form-group cta-block">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button type="submit" class="btn btn-primary"><?php echo $language['txt_search']; ?></button>
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" class="btn btn-primary" onclick="resetSearchCOD();"><?php echo $language['txt_reset']; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </from>
                    </div>
                    <div class="export-booking-form">
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <button class="btn btn-primary" type="button" id="export_excel_cod"><?php echo $language['txt_export_excel_cod']; ?></button>
                            </div>
                        </div>
                    </div>
                    <div class="row wrap-table">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-responsive table-hover cod-list-table" id="cod_list_table" style="width: 100%">
                                <thead>
                                    <tr role="row" style="width: 100%">
                                        <th>
                                            <input id="check_all" type="checkbox">
                                        </th>
                                        <th class="text-center">
                                            <?php echo $language['txt_shipment_no']; ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $language['txt_order_no']; ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $language['txt_delivery_date']; ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $language['txt_cod_money']; ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $language['txt_fee']; ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $language['txt_status_payment']; ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $language['txt_paid_date']; ?>
                                        </th>
                                        <th class="text-center">
                                            <?php echo $language['txt_cod_code']; ?>
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade shipment-detail-modal" id="shipment-detail-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="pull-right btn-group-action">
                    <button class="btn btn-gray btn-sm" id="print_detail_shipment" type="button">
                        <i class="fa fa-print" aria-hidden="true"></i>
                        <span>Print</span>
                    </button>
                </div>
                <h4 class="modal-title"><?php echo $language['txt_detail_shipment']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="block-panel">
                    <div class="row">
                        <div class="col-xs-12 col-md-7">
                            <h3 class="block-title" id="shipment_title">
                            </h3>
                        </div>
                        <div class="col-xs-12 col-md-5 order-nooo-label">
                            <span class="font-weight-bold"><?php echo $language['txt_order_no']; ?></span>
                            <span id="order_no"></span>
                        </div>
                    </div>
                </div>
                <div class="block-panel">
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_delivery']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_company_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="company_name"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_shipper_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="shipper_name"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_shipper_address']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="io_undelivery"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_shipper_phone']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="phone_shipper"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="block-panel">
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_receiver_info']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_receiver_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="receiver_name"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_receiver_company']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="receiver_company"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_receiver_address']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="delivery_address"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_receiver_phone']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="phone_delivery"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="block-panel">
					<span class="pull-right">Domestic</span>
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_info_shipment']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_shipment_type']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="shipment_type"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_cod_value']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="cod_value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_weight']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="weight"><span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_pcs_value']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="pcs_value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_date_entered']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="date_entered"><span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_delivery_date']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="delivery_date"><span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_payment_method']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="payment_method"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_charge_to']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="charge_to"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_info_POD']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_total_revenue']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="total_revenue"></span>
                                        <p class="noted"><?php echo $language['txt_cost_include_fee']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_req_if_fail']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_special_req']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="specical_req"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_description']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_description_product']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="description"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-right action-group">
                    <button type="submit" class="btn btn-primary" data-dismiss="modal"><?php echo $language['txt_close']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dataTable = '<?php echo $dataTable; ?>';
    var exportNull = '<?php echo $language['export_cod_null_error']; ?>';
    var printNull = '<?php echo $language['print_cod_null_error']; ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/cod_list.js?v=<?php echo $this->config->item('version_random'); ?>'></script>
