<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="#"><?php echo $language['txt_menu_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_service_cod_detail']; ?></li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <div class="form-block service-cod-detail-form">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right">
                            <div class="btn-group-sm text-right btn-group-action">
<!--                                <button class="btn btn-primary btn-sm">--><?php //echo $language['txt_service_cod_detail_confirm']; ?><!--</button>-->
                                <button class="btn btn-gray btn-sm" id="print_detail_cod" type="button" value="<?php echo $detail_cod['cod_id'] ?>">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    <span><?php echo $language['txt_print']; ?></span>
                                </button>
                            </div>
                        </div>
                        <h3 class="form-title">
                            <?php echo $language['txt_service_cod_detail']; ?>
                        </h3>
                    </div>
                </div>
                <div class="view-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_service_cod_detail_cod_code']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_cod['code']?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_from']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_cod['from_date']?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_to']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_cod['to_date']?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_total_revenue']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo number_format($detail_cod['total_amount'])." VND"?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_status_cod']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_cod['status']?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_service_cod_detail_net_off']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="checkbox-inline" id="check_ras_home_readonly">
                                        <label class="checkbox-label">
                                            <input type="checkbox"  id="net_off" name="net_off" <?php echo $detail_cod['net_off'] == 1 ? 'checked' : ''?>>
                                            <span class="control-label"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_paid_date']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_cod['paid_date']?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_note']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_cod['description']?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wrap-table">
                    <div class="col-xs-12">
                        <table class="table table-bordered table-hover cod-list-detail-table" style="width: 100%">
                            <thead>
                                <tr role="row" style="width: 100%">
                                    <th>
                                        <?php echo $language['txt_no']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_shipment_no']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_order_no']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_date_entered']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_service_cod_detail_cod_value']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_receive_place']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_cod']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_fee']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_service_cod_detail_paid']; ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <!-- <img src="assets/images/icons/success-img.png" alt="myface"> -->
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <!-- <img src="assets/images/icons/error-img.png" alt="speedlink"> -->
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dataTable = '<?php echo $dataTable; ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/cod_detail.js?v=<?php echo $this->config->item('version_random'); ?>'></script>