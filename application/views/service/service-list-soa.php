<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="/"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_breadcrumb_list_soa']; ?></li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <div class="form-block service-soa-form">
                <h3 class="form-title">
                    <?php echo $language['txt_breadcrumb_list_soa']; ?>
                </h3>
                <div class="form-wrap">
                    <form action="" method="post">
                        <div class="search-booking-group">
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_date_type']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" onchange="changeDateRange();" id="date_range" name="date_range">
                                                <?php echo $dataFilter['dateRangeOption']; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_from']; ?></label>
                                        <div class="input-group date datepicker">
                                            <input class="form-control" name="from_date" readonly="" id="from_date" type="text" value="<?php echo $dataFilter['date_from']; ?>">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_to']; ?></label>
                                        <div class="input-group date datepicker">
                                            <input class="form-control" name="to_date" readonly="" id="to_date" type="text" value="<?php echo $dataFilter['date_to']; ?>">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_soa_code']; ?></label>
                                        <input class="form-control" type="input" name="soa_code" id="soa_code" placeholder=""
                                        value="<?php echo $dataFilter['soa_code']; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_status_soa']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" id="status" name="status">
                                                <?php echo $dataFilter['statusOption'] ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-offset-6 col-sm-6 col-sm-offset-0 col-md-4">
                                    <div class="form-group cta-block">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button type="submit" class="btn btn-primary"><?php echo $language['txt_search']; ?></button>
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" class="btn btn-primary" onclick="resetFormSOA()"><?php echo $language['txt_reset']; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="export-booking-form">
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <button class="btn btn-primary" id="export_excel_soa" type="button"><?php echo $language['txt_export_excel_soa']; ?></button>
                            </div>
                        </div>
                    </div>
                    <div class="row wrap-table">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-hover soa-list-table" id="soa_list_table" style="width: 100%">
                                <thead>
                                    <tr role="row">
                                        <th><input id="check_all" type="checkbox"></th>
                                        <th><?php echo $language['txt_no']; ?></th>
                                        <th><?php echo $language['txt_soa_code']; ?></th>
                                        <th><?php echo $language['txt_from']; ?></th>
                                        <th><?php echo $language['txt_to']; ?></th>
                                        <th><?php echo $language['txt_shipment_no']; ?></th>
                                        <th><?php echo $language['txt_total_amount']; ?></th>
                                        <th><?php echo $language['txt_invoice']; ?></th>
                                        <th><?php echo $language['txt_status']; ?></th>
                                        <th><?php echo $language['txt_paid_date']; ?></th>
                                        <th><?php echo $language['txt_print']; ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dataTable = '<?php echo $dataTable; ?>';
    var exportNull = '<?php echo $language['export_soa_null_error']; ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/soa_list.js?v=<?php echo $this->config->item('version_random'); ?>'></script>
