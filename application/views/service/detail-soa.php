<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="/"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_breadcrumb_detail_soa']; ?></li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <div class="form-block service-soa-detail-form">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right">
                            <div class="btn-group-sm text-right btn-group-action">
<!--                                <button class="btn btn-primary btn-sm">--><?php //echo $language['txt_confirm']; ?><!--</button>-->
                                <button class="btn btn-gray btn-sm" id="print_detail_soa" value="<?php echo $detail_soa['soa_id']?>">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    <span><?php echo $language['txt_print']; ?></span>
                                </button>
                            </div>
                        </div>
                        <h3 class="form-title">
                            <?php echo $language['txt_breadcrumb_detail_soa']; ?>
                        </h3>
                    </div>
                </div>
                <div class="view-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_soa_code']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_soa['soa_code']?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_from']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_soa['from_date']?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_to']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_soa['to_date']?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_total_amount']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo number_format($detail_soa['total_amount'])." VND"?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_status']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_soa['status']?></span>
                                </div>
                            </div>
                        </div>                        
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_description']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_soa['description']?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_paid_date']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span><?php echo $detail_soa['paid_date']?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wrap-table ">
                    <div class="col-xs-12">
                        <table class="table table-bordered table-hover cod-list-detail-table" id="table_list_shipment_soa" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo $language['txt_no']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_date_shipment']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_shipment_no']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_send_address']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_receive_place']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_receive_address']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_service']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_weight']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_service_transport']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_bonus_fee']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_other_service']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_money']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_tax']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_tax_fee']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_total']; ?>
                                    </th>
                                    <th>
                                        <?php echo $language['txt_cod']; ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dataTable = '<?php echo $shipmentList; ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/soa_detail.js?v=<?php echo $this->config->item('version_random'); ?>'></script>