<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="/"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_breadcrumb_list_shipment']; ?></li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <div class="form-block sercvice-shipment-form">
                <h3 class="form-title"><?php echo $language['txt_breadcrumb_list_shipment']; ?></h3>
                <div class="form-wrap">
                    <div class="search-booking-group">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_shipment_code']; ?></label>
                                        <input class="form-control" type="text" name="awb_tracking_no" value="<?php echo $dataFilter['shipment_code'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_receiver_name']; ?></label>
                                        <input class="form-control" type="text" name="receiver_name" value="<?php echo $dataFilter['receiver_name'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_receiver_phone']; ?></label>
                                        <input class="form-control" type="tel" name="receiver_phone" value="<?php echo $dataFilter['receiver_phone'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="service" class="control-label"><?php echo $language['txt_service']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" id="service" name="service">
                                                <?php if ($this->session->userdata('site_lang')=='english') {
                                                    echo $dataFilter['serviceOptionEng'];
                                                }else{
                                                    echo $dataFilter['serviceOptionVn'];
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="status" class="control-label"><?php echo $language['txt_status']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" id="status" name="status">
                                                <?php echo $dataFilter['statusOption']; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="status_payment" class="control-label"><?php echo $language['txt_payment_status']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" id="status_payment" name="status_payment" disabled>
                                                <option value=""></option>
                                                <option value="paid">Paid</option>
                                                <option value="unpaid">Unpaid</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_date_created']; ?></label>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="datetimepicker-box">
                                                    <input class="form-control datepickerBox" name="date_entered_from" type="text" value="<?php echo $dataFilter['date_entered_from'] ?>" placeholder="<?php echo $language['txt_from']; ?>" readonly>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <div class="datetimepicker-box">
                                                    <input class="form-control datepickerBox" name="date_entered_to" type="text" value="<?php echo $dataFilter['date_entered_to']?>" placeholder="<?php echo $language['txt_to']; ?>" readonly>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_date_completed']; ?></label>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <div class="datetimepicker-box">
                                                    <input class="form-control datepickerBox" name="delivery_date_from" type="text" placeholder="<?php echo $language['txt_from']; ?>" readonly value="<?php echo $dataFilter['date_delivery_from'] ?>">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <div class="datetimepicker-box">
                                                    <input class="form-control datepickerBox" name="delivery_date_to" type="text" placeholder="<?php echo $language['txt_to']; ?>" readonly value="<?php echo $dataFilter['date_delivery_to'] ?>">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-offset-6 col-md-6">
                                    <div class="row form-group">
                                        <div class="col-xs-6">
                                            <button type="submit" class="btn btn-primary btn-block"><?php echo $language['txt_search']; ?></button>
                                        </div>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn btn-primary btn-block" onclick="resetSearchShipment();" ><?php echo $language['txt_reset']; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row wrap-table">
                        <div class="col-md-6">
                            <div class="row form-group action-group">
                                <div class="col-sm-6">
                                    <button class="btn btn-gray btn-block" id="print_shipment"><?php echo $language['txt_print_shipment']?></button>
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn btn-gray btn-block" id="export_excel_shipment"><?php echo $language['txt_export_excel']?></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <table class="table table-bordered table-hover shipment-list-table" id="shipment_list_table" style="width: 100%">
                                <thead style="width: 100%">
                                    <tr>
                                        <th>
                                            <input type="checkbox" value="" style="width:auto" id="check_all">
                                        </th>
                                        <th class="text-center"><?php echo $language['txt_no']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_shipment_code']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_service']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_date_created']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_date_completed']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_receiver_name']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_cod_value']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_total_revenue']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_status']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_shipment_note']; ?></th>
                                    </tr>
                                </thead>
                                <tbody style="width: 100%">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!--Table Print-->
<div id="print_table_shipment">
    <h1 class="text-center"><?php echo $language['txt_shipment_list']?></h1>
    <table class="table table-bordered table-hover listviewDatatable shipment-list-table" id="shipment_list_print" style="width: 100%">
        <thead style="width: 100%">
        <tr>
            <th class="text-center"><?php echo $language['txt_shipment_code']; ?></th>
            <th class="text-center"><?php echo $language['txt_service']; ?></th>
            <th class="text-center"><?php echo $language['txt_date_created']; ?></th>
            <th class="text-center"><?php echo $language['txt_date_completed']; ?></th>
            <th class="text-center"><?php echo $language['txt_receiver_name']; ?></th>
            <th class="text-center"><?php echo $language['txt_cod_value']; ?></th>
            <th class="text-center"><?php echo $language['txt_total_revenue']; ?></th>
            <th class="text-center"><?php echo $language['txt_status']; ?></th>
            <th class="text-center"><?php echo $language['txt_shipment_note']; ?></th>
        </tr>
        </thead>
        <tbody style="width: 100%">
        </tbody>
    </table>
</div>
<!--modal-->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade shipment-detail-modal" id="shipment-detail-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="pull-right btn-group-action">
                    <button class="btn btn-gray btn-sm" id="print_detail_shipment">
                        <i class="fa fa-print" aria-hidden="true"></i>
                        <span>Print</span>
                    </button>
                </div>
                <h4 class="modal-title"><?php echo $language['txt_detail_shipment']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="block-panel">
                    <div class="row">
                        <div class="col-xs-12 col-md-7">
                            <h3 class="block-title" id="shipment_title">
                            </h3>
                        </div>
                        <div class="col-xs-12 col-md-5 order-nooo-label">
                            <span class="font-weight-bold"><?php echo $language['txt_order_no']; ?></span>
                            <span id="order_no"></span>
                        </div>
                    </div>
                </div>
                <div class="block-panel">
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_delivery']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_company_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="company_name"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_shipper_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="shipper_name"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_shipper_address']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="io_undelivery"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_shipper_phone']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="phone_shipper"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="block-panel">
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_receiver_info']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_receiver_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="receiver_name"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_receiver_company']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="receiver_company"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_receiver_address']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="delivery_address"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_receiver_phone']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="phone_delivery"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="block-panel">
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_info_shipment']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_shipment_type']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="shipment_type"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_cod_value']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="cod_value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_weight']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="weight"><span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_pcs_value']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="pcs_value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_date_entered']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="date_entered"><span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_delivery_date']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="delivery_date"><span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_payment_method']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="payment_method"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_charge_to']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="charge_to"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_info_POD']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="pod"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_total_revenue']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="total_revenue"></span>
                                        <p class="noted"><?php echo $language['txt_cost_include_fee']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_req_if_fail']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_special_req']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="specical_req"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_description']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_description_product']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="description"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="block-panel">
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_history_shipment']; ?>
                    </h3>
                    <div class="wrap-table table-responsive">
                        <table class="table table-bordered table-hover" style="width: 100%" id="history">
                            <thead>
                            <tr>
                                <th class="text-center"><?php echo $language['txt_no']; ?></th>
                                <th class="text-center"><?php echo $language['txt_time']; ?></th>
                                <th class="text-center"><?php echo $language['txt_status']; ?></th>
                                <th class="text-center"><?php echo $language['txt_note']; ?></th>
                                <th class="text-center"><?php echo $language['txt_note_detail']; ?></th>
                            </tr>
                            </thead>
                            <tbody class="text-center">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-right action-group">
                    <button type="submit" class="btn btn-primary" data-dismiss="modal"><?php echo $language['txt_close']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dataTable = '<?php echo $dataTable; ?>';
    var exportNull = '<?php echo $language['export_shipment_null_error']; ?>';
    var printNull = '<?php echo $language['print_shipment_null_error']; ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/shipment_list.js?v=<?php echo $this->config->item('version_random'); ?>'></script>
