<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="/"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_breadcrumb_list_booking']; ?></li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <div class="form-block booking-list-form">
                <h3 class="form-title">
                    <?php echo $language['txt_breadcrumb_list_booking']; ?>
                </h3>
                <div class="form-wrap">
                    <div class="search-booking-group">
                        <form action="" method="POST">
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_list_booking_date_range']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" id="date_range" name="date_range" onchange="changeDateRange();">
                                                <?php echo $dataFilter['dateRangeOption']; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_list_booking_filter_date_from']; ?></label>
                                        <div class="input-group date datepicker">
                                            <input class="form-control" readonly="" name="from_date" id="from_date" type="text" value="<?php echo $dataFilter['dateFrom']; ?>">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_list_booking_filter_date_to']; ?></label>
                                        <div class="input-group date datepicker">
                                            <input class="form-control" readonly="" name="to_date" id="to_date" type="text" value="<?php echo $dataFilter['dateTo']; ?>">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_list_booking_filter_booking_no']; ?></label>
                                        <input class="form-control" type="input" name="booking_no" id ="booking_no" placeholder="Vd: 123456789" value="<?php echo $dataFilter['booking_no'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_list_booking_filter_status']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" id="status" name="status">
                                                <?php echo $dataFilter['statusOption']; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group cta-block">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <input type="submit" class="btn btn-primary" value="<?php echo $language['txt_list_booking_btn_search']; ?>">
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" class="btn btn-primary" onclick="resetFilterSearchBooking();"><?php echo $language['txt_list_booking_btn_reset']; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row wrap-table">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <button class="btn btn-gray" type="button" id="export_excel_booking"><?php echo $language['txt_list_booking_btn_export_excel']; ?></button>
                                <button class="btn btn-gray" type="button" id="print_booking"><?php echo $language['txt_list_booking_btn_print_booking']; ?></button>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <table class="table table-bordered table-hover booking-list-table" class="width: 100%" id="table_list_booking">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" value="" style="width:auto" id="checkAllBooking">
                                        </th>
                                        <th class="text-center"><?php echo $language['txt_list_booking_table_no']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_list_booking_table_order_no']; ?></th>
                                        <th class="col-md-2 text-center"><?php echo $language['txt_list_booking_table_pickup_address']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_list_booking_table_name_receive']; ?></th>
                                        <th class="col-md-2 text-center"><?php echo $language['txt_list_booking_table_receive_address']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_list_booking_table_receive_phone']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_list_booking_table_special_require']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_list_booking_table_pickup_date']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_list_booking_table_status']; ?></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade booking-detail-modal" id="booking-detail-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="pull-right btn-group-action">
                    <button class="btn btn-gray btn-sm" id="print_detail_booking">
                        <i class="fa fa-print" aria-hidden="true"></i>
                        <span><?php echo $language['txt_detail_booking_print']; ?></span>
                    </button>
                    <button class="btn btn-gray btn-sm" id="booking_duplicate">
                        <i class="fa fa-print" aria-hidden="true"></i>
                        <span><?php echo $language['txt_detail_booking_duplicate']; ?></span>
                    </button>
                </div>
                <h4 class="modal-title"><?php echo $language['txt_detail_booking_info']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="block-panel">
                    <div class="row">
                        <div class="col-xs-12 col-md-7">
                            <h3 class="block-title" id="booking_title">
                            </h3>
                        </div>
                        <div class="col-xs-12 col-md-5 order-nooo-label">
                            <span class="font-weight-bold"><?php echo $language['txt_order_no']; ?></span>
                            <span id="order_no"></span>
                        </div>
                    </div>
                </div>
                <div class="block-panel">
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_detail_booking_delivery_info']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_delivery_company_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="company_name"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_delivery_full_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="shipper_name"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_delivery_address']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="io_undelivery"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_delivery_phone']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="phone_shipper"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="block-panel">
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_detail_booking_receiver_info']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_receiver_full_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="receiver_name"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_receiver_company_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="receiver_company"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_receiver_address']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="delivery_address"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_receiver_phone']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="phone_delivery"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="block-panel">
                    <!-- <span class="pull-right"><?php //echo $language['txt_detail_booking_domestic']; ?></span> -->
                    <h3 class="block-title sub-title">
                        <?php echo $language['txt_detail_booking_package_info']; ?>
                    </h3>
                    <div class="block-panel-sub">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_shipment_tpye']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="shipment_type"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_cod_value']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="cod_value"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_weight']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="weight"><span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_coupon_code']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="coupon_code"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_booking_date']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="booking_date"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_pickup_date']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="pickup_date"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_address_pickup']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="pickup_address"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_special_req']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="special_req"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_payment_method']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="payment_method"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_charge_to']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="charge_to"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-5">
                                        <span class="font-weight-bold"><?php echo $language['txt_detail_booking_booking_note']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-7">
                                        <span id="booking_note"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-right action-group">
                    <button type="submit" class="btn btn-primary" data-dismiss="modal"><?php echo $language['txt_detail_booking_close']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dataTable = '<?php echo $dataTable; ?>';
    var exportNull = '<?php echo $language['export_booking_null_error']; ?>';
    var printNull = '<?php echo $language['print_booking_null_error']; ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/booking_list.js?v=<?php echo $this->config->item('version_random'); ?>'></script>