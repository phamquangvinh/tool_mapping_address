<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="#"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_breadcrumb_import_booking']; ?></li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <div class="form-block booking-import-form">
                <h3 class="form-title">
                    <?php echo $language['txt_breadcrumb_import_booking']; ?>
                </h3>
                <div class="form-wrap">
                    <input type="hidden" id="data_insert_contact">
                    <input type="hidden" id="data_insert_account_contact">
                    <input type="hidden" id="total_checkbox" value="" />
                    <table id="info_import" width="100%" cellpadding="4" cellspacing="0" border="0" style="margin-left: 10px;display: none">
                        <tr>
                            <td width="60%" style="color: blue"><b><?php echo $language['txt_import_booking_total_record']; ?></b></td>
                            <td id="total_record_import">0</td>
                        </tr>
                        <tr>
                            <td width="60%" style="color:red"><b><?php echo $language['txt_import_booking_total_failed']; ?></b></td>
                            <td id="total_failed_import">0</td>
                        </tr>
                        <tr>
                            <td width="60%" style="color: green"><b><?php echo $language['txt_import_booking_total_selected']; ?></b></td>
                            <td id="total_selected_import">0</td>
                        </tr>
                    </table>
                    <div class="import-control-group">
                        <h2 class="form-title gray-text text-center"><?php echo $language['txt_import_booking_select_excel']; ?></h2>
                        <div class="upload-group-control">
                            <form id="uploadExcel">
                                <input class="fileUpload" name="booking_excel" type="file" data-show-preview="false" data-msg-placeholder="<?php echo $language['txt_import_booking_select_file']; ?>" data-allowed-file-extensions='["xls", "xlsx"]'>
                            </form>
                            <a class="btn btn-link download-btn" href="/assets/docs/template_import_booking.xlsx" target="_blank"><?php echo $language['txt_import_booking_download_sample']; ?></a>
                            <br>

                            <span id="img_loading"></span>
                        </div>
                    </div>
                    <div class="info-import">
                    </div>
                    <div class="row wrap-table">
                        <div class="col-xs-12">
                            <div id="result">
                                <table class="table table-bordered table-hover" class="width: 100%" id="table_data_import">
                                    <thead>
                                        <tr>
                                            <th style="padding: 11px;"><input type="checkbox" id="check_all"></th>
                                            <th><?php echo $language['txt_import_booking_booking_no']; ?></th>
                                            <th><?php echo $language['txt_import_booking_service_category']; ?></th>
                                            <th><?php echo $language['txt_import_booking_non_dox']; ?></th>
                                            <th><?php echo $language['txt_import_booking_dox']; ?></th>
                                            <th><?php echo $language['txt_import_booking_cod_value']; ?></th>
                                            <th><?php echo $language['txt_import_booking_weight']; ?></th>
                                            <th><?php echo $language['txt_import_booking_pickup_country']; ?></th>
                                            <th><?php echo $language['txt_import_booking_pickup_city']; ?></th>
                                            <th><?php echo $language['txt_import_booking_pickup_district']; ?></th>
                                            <th><?php echo $language['txt_import_booking_pickup_address']; ?></th>
                                            <th><?php echo $language['txt_import_booking_shipper_name']; ?></th>
                                            <th><?php echo $language['txt_import_booking_shipper_phone']; ?></th>
                                            <th><?php echo $language['txt_import_booking_pickup_date']; ?></th>

                                            <th><?php echo $language['txt_import_booking_delivery_country']; ?></th>
                                            <th><?php echo $language['txt_import_booking_delivery_city']; ?></th>
                                            <th><?php echo $language['txt_import_booking_delivery_district']; ?></th>
                                            <th><?php echo $language['txt_import_booking_delivery_address']; ?></th>
                                            <th><?php echo $language['txt_import_booking_delivery_company_name']; ?></th>
                                            <th><?php echo $language['txt_import_booking_delivery_name']; ?></th>
                                            <th><?php echo $language['txt_import_booking_delivery_phone']; ?></th>
                                            <th><?php echo $language['txt_import_booking_payment_method']; ?></th>
                                            <th><?php echo $language['txt_import_booking_charge_to']; ?></th>
                                            <th><?php echo $language['txt_import_booking_shipment_value']; ?></th>
                                            <th><?php echo $language['txt_import_booking_special_instruction']; ?></th>
                                            <th><?php echo $language['txt_import_booking_special_request']; ?></th>
                                            <th><?php echo $language['txt_import_booking_booking_note']; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="action-row text-center action-group">
                <button class="btn btn-primary" id="btn_import_booking_bottom" type="submit" class="btn_send" style="display: none;"><?php echo $language['txt_import_booking_select_excel']; ?><?php echo $language['txt_import_booking_excel']; ?></button>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<script src='/assets/js/booking_import.js?v=<?php echo $this->config->item('version_random'); ?>'></script>