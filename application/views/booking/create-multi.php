<main>
    <?php
        $lang = 'en';
        if($this->session->get_userdata()['site_lang'] == 'vietnamese') $lang = 'vi';
    ?>
    <form class="container" id="multi_booking_data">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="/"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_breadcrumb_create_multi_booking']; ?></li>
            </ol>
        </div>
        <!-- <form action="#" method="POST"> -->
            <section class="panel section-block booking-many-section">
                <div class="form-block">
                    <h3 class="form-title">
                        <?php echo $language['txt_create_booking_shipper_name']; ?>
                    </h3>
                    <input type="hidden" id="account_id" name="account_id" value="<?php echo $this->session->userdata('login')['accounts_id'] ?>">
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-4">
                                        <span class="font-weight-bold"><?php echo $language['txt_create_booking_full_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <span id="short_name"><?php echo $dataAccount['short_name']?></span>
                                        <input type="hidden" name="short_name" value="<?php echo $dataAccount['short_name'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-4">
                                        <span class="font-weight-bold"><?php echo $language['txt_create_booking_mobile_phone']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <span id="mobile_phone"><?php echo $dataAccount['mobile_phone'] ?></span>
                                        <input type="hidden" name="mobile_phone" value="<?php echo $dataAccount['mobile_phone'] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row row-view">
                                    <div class="col-xs-12 col-sm-4">
                                        <span class="font-weight-bold"><?php echo $language['txt_create_booking_company_name']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <span id="name"><?php echo $dataAccount['name'] ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4">
                                        <span class="font-weight-bold"><?php echo $language['txt_create_booking_address_shipper']; ?></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <span id="address"><?php echo $dataAccount['address'] ?></span>
                                        <button class="btn btn-add" data-toggle="modal" data-target=".shiper-info-modal" type="button" onclick="getListAddress();">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                            <span><?php echo $language['txt_create_booking_change_address']; ?></span>
                                        </button>
                                        <span class="error" id="error_shipper_info"><?php echo $language['txt_create_booking_error_shipper_info']; ?></span>
                                        <input type="hidden" name="shipper_country" id="shipper_country" value="<?php echo $dataAccount['country_id'] ?>">
                                        <input type="hidden" name="shipper_city" id="shipper_city" value="<?php echo $dataAccount['city_id'] ?>">
                                        <input type="hidden" name="shipper_district" id="shipper_district" value="<?php echo $dataAccount['district_id'] ?>">
                                        <input type="hidden" name="shipper_ward" id="shipper_ward" value="<?php echo $dataAccount['ward_id'] ?>">
                                        <input type="hidden" name="shipper_address" id="shipper_address" value="<?php echo $dataAccount['address'] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-block order-item-block">
                    <div class="form-header clearfix">
                        <h3 class="form-title pull-left">
                            <?php echo $language['txt_booking_number_order']; ?> <span class="number_order">#1</span>
                        </h3>
                        <div class="form-title-status pull-right">
                            <div class="btn-group">
                                <!-- <button class="btn btn-secondary clone-booking-btn" type="button">
                                    <i class="fa fa-clone" aria-hidden="true"></i>
                                </button> -->
                                <button class="btn btn-secondary trash-btn" type="button">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input class="form-control" placeholder="<?php echo $language['txt_create_booking_order_no']; ?>" type="text" name="order_no[]">
                            </div>
                        </div>
                    </div>
                    <div class="form-wrap">
                        <div class="form-inner">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_receiver_name']; ?></label>
                                        <input class="form-control autoFillBooking" type="text" name="receiver_name[]">
                                        <span class="error error_receiver_name"><?php echo $language['txt_create_booking_error_receiver_name']; ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_receiver_phone']; ?></label>
                                        <input class="form-control" type="tel" name="receiver_phone[]">
                                        <span class="error error_receiver_phone"><?php echo $language['txt_create_booking_error_receiver_phone']; ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_street']; ?></label>
                                        <input class="form-control" type="text" name="receiver_address[]">
                                        <span class="error error_receiver_street"><?php echo $language['txt_create_booking_error_street']; ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_nation']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control chosenSelect" name="receiver_country[]" onchange="changeCountry($(this));" lang="<?=$lang?>">
                                                <?php echo $countryOption ?>
                                            </select>
                                            <span class="error error_receiver_country"><?php echo $language['txt_create_booking_error_nation']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_city']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control chosenSelect" lang="<?=$lang?>" name="receiver_city[]" onchange="changeCity($(this));">
                                                <?php echo $cityOption ?>
                                            </select>
                                            <span class="error error_receiver_city"><?php echo $language['txt_create_booking_error_city']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_district']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control chosenSelect" lang="<?=$lang?>" name="receiver_district[]" onchange="changeDistrict($(this));" >
                                                <option></option>
                                            </select>
                                            <span class="error error_receiver_district"><?php echo $language['txt_create_booking_error_district']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_ward']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control chosenSelect" lang="<?=$lang?>" 
                                                name="receiver_ward[]"  onchange="changeSelect($(this))">
                                                <option></option>
                                            </select>
                                            <span class="error error_receiver_ward"><?php echo $language['txt_create_booking_error_ward']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_payment_method']; ?></label>
                                        <div class="select-box">
                                            <?php $user = $this->session->userdata('login'); if ($user['accounts_id']!='' && $user['accounts_id']!=null && $user['payment_method']!='' && $user['payment_method']!=null && $user['payment_method'] == 'bank_transfer') { ?>
                                                <select class="form-control" name="payment_method[]" onchange="changeSelect($(this))">
                                                    <option value="cash"><?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                                    <option value="bank_transfer"><?php echo $language['txt_create_booking_payment_method_bank_transfer']; ?></option>
                                                </select>
                                            <?php }elseif($user['accounts_id']!='' && $user['accounts_id']!=null && $user['payment_method']!='' && $user['payment_method']!=null && $user['payment_method'] == 'other'){?>
                                                <select class="form-control" name="payment_method[]" onchange="changeSelect($(this))" readonly>
                                                    <option value="cash" selected><?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                                </select>
                                            <?php }else{ ?>
                                                <select class="form-control" name="payment_method[]" onchange="changeSelect($(this))" readonly>
                                                    <option value="cash" selected><?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                                </select>
                                            <?php } ?>
                                            <!-- validate -->
                                            <!-- <label class="error">This field is required.</label> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_charge_to']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" name="charge_to[]" onchange="changeSelect($(this))">
                                                <option value="shipper"><?php echo $language['txt_create_booking_charge_to_shipper']; ?></option>
                                                <option value="receiver"><?php echo $language['txt_create_booking_charge_to_receiver']; ?></option>
                                            </select>
                                            <!-- validate -->
                                            <!-- <label class="error">This field is required.</label> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_type_product']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" name="is_dox[]" onchange="changeSelect($(this))">
                                                <option value="non_dox"><?php echo $language['txt_create_booking_non_dox']; ?></option>
                                                <option value="dox"><?php echo $language['txt_create_booking_dox']; ?></option>
                                            </select>
                                            <!-- validate -->
                                            <!-- <label class="error">This field is required.</label> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_service']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" name="service[]" onchange="changeService($(this))">
                                                <option value="1a435d29-b822-f12d-9908-57482e79c641"><?php echo $this->session->get_userdata()['site_lang'] == 'english' ? 'Standard Express' : 'Dịch vụ chuyển phát nhanh'?></option>
                                                <option value="72d9ebd5-abd0-4778-d1af-57482e848f6b"><?php echo $this->session->get_userdata()['site_lang'] == 'english' ? 'Priority Express' : 'Dịch vụ hỏa tốc'?></option>
                                                <option value="77d6ed67-1625-d9df-7ac4-57482eac128f"><?php echo $this->session->get_userdata()['site_lang'] == 'english' ? 'Road/ Truck Service' : 'Dịch vụ đường bộ'?></option>
                                                <option value="d04ec06c-aa19-91ff-4fab-57482f5afd33"><?php echo $this->session->get_userdata()['site_lang'] == 'english' ? 'Ecommerce Next Day' : 'Giao Hàng TMĐT qua ngày '?></option>
                                            </select>
                                            <!-- validate -->
                                            <!-- <label class="error">This field is required.</label> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_weight']; ?></label>
                                        <input class="form-control" type="text" name="weight[]">
                                        <span class="error error_weight"><?php echo $language['txt_create_booking_error_weight']; ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_cod']; ?></label>
                                        <input class="form-control" type="text" name="cod_value[]">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_booking_value']; ?></label>
                                        <input class="form-control" type="text" name="shipment_value[]">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_date']; ?></label>
                                        <div class="datetimepicker-box">
                                            <input class="form-control" type="input" name="date_entered[]" id="date_entered" value="<?php echo date('d/m/Y h:i',time())?>" disabled>
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_pcs']; ?></label>
                                        <input class="form-control" type="input" name="pcs[]">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_description']; ?></label>
                                        <input class="form-control" type="input" name="description[]" placeholder="<?php echo $language['txt_placeholder_description']; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group align-middle">
                                        <div class="checkbox">
                                            <label class="checkbox-label">
                                                <input type="checkbox" name="save_address[]">
                                                <span class="control-label"><?php echo $language['txt_save_address']; ?></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-block order-item-block-hidden newBookingForm">
                    <div class="form-header clearfix">
                        <h3 class="form-title pull-left">
                            <?php echo $language['txt_booking_number_order']; ?> <span class="number_order">#1</span>
                        </h3>
                        <div class="form-title-status pull-right">
                            <div class="btn-group">
                                <!-- <button class="btn btn-secondary clone-booking-btn" type="button">
                                    <i class="fa fa-clone" aria-hidden="true"></i>
                                </button> -->
                                <button class="btn btn-secondary trash-btn" type="button">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input class="form-control" placeholder="<?php echo $language['txt_create_booking_order_no']; ?>" type="text" name="order_no[]">
                            </div>
                        </div>
                    </div>
                    <div class="form-wrap">
                        <div class="form-inner">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_receiver_name']; ?></label>
                                        <input class="form-control autoFillBooking" type="text" name="receiver_name[]">
                                        <span class="error error_receiver_name"><?php echo $language['txt_create_booking_error_receiver_name']; ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_receiver_phone']; ?></label>
                                        <input class="form-control" type="tel" name="receiver_phone[]">
                                        <span class="error error_receiver_phone"><?php echo $language['txt_create_booking_error_receiver_phone']; ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_street']; ?></label>
                                        <input class="form-control" type="text" name="receiver_address[]">
                                        <span class="error error_receiver_street"><?php echo $language['txt_create_booking_error_street']; ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_nation']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control chosenSelect3" name="receiver_country[]" onchange="changeCountry($(this));">
                                                <?php echo $countryOption ?>
                                            </select>
                                            <span class="error error_receiver_country"><?php echo $language['txt_create_booking_error_nation']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_city']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control chosenSelect3" name="receiver_city[]" onchange="changeCity($(this));">
                                                <?php echo $cityOption ?>
                                            </select>
                                            <span class="error error_receiver_city"><?php echo $language['txt_create_booking_error_city']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_district']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control chosenSelect3" name="receiver_district[]" onchange="changeDistrict($(this));" >
                                                <option></option>
                                            </select>
                                            <span class="error error_receiver_district"><?php echo $language['txt_create_booking_error_district']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_ward']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control chosenSelect3" name="receiver_ward[]"  onchange="changeSelect($(this))">
                                                <option></option>
                                            </select>
                                            <span class="error error_receiver_ward"><?php echo $language['txt_create_booking_error_ward']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_payment_method']; ?></label>
                                        <div class="select-box">
                                            <?php $user = $this->session->userdata('login'); if ($user['accounts_id']!='' && $user['accounts_id']!=null && $user['payment_method']!='' && $user['payment_method']!=null && $user['payment_method'] == 'bank_transfer') { ?>
                                                <select class="form-control" name="payment_method[]" onchange="changeSelect($(this))">
                                                    <option value="cash"><?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                                    <option value="bank_transfer"><?php echo $language['txt_create_booking_payment_method_bank_transfer']; ?></option>
                                                </select>
                                            <?php }elseif($user['accounts_id']!='' && $user['accounts_id']!=null && $user['payment_method']!='' && $user['payment_method']!=null && $user['payment_method'] == 'other'){?>
                                                <select class="form-control" name="payment_method[]" onchange="changeSelect($(this))" readonly>
                                                    <option value="cash" selected><?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                                </select>
                                            <?php }else{ ?>
                                                <select class="form-control" name="payment_method[]" onchange="changeSelect($(this))" readonly>
                                                    <option value="cash" selected><?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                                </select>
                                            <?php } ?>
                                            <!-- validate -->
                                            <!-- <label class="error">This field is required.</label> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_charge_to']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" name="charge_to[]" onchange="changeSelect($(this))">
                                                <option value="shipper"><?php echo $language['txt_create_booking_charge_to_shipper']; ?></option>
                                                <option value="receiver"><?php echo $language['txt_create_booking_charge_to_receiver']; ?></option>
                                            </select>
                                            <!-- validate -->
                                            <!-- <label class="error">This field is required.</label> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_type_product']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" name="is_dox[]" onchange="changeSelect($(this))">
                                                <option value="non_dox"><?php echo $language['txt_create_booking_non_dox']; ?></option>
                                                <option value="dox"><?php echo $language['txt_create_booking_dox']; ?></option>
                                            </select>
                                            <!-- validate -->
                                            <!-- <label class="error">This field is required.</label> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_service']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" name="service[]" onchange="changeService($(this))">
                                                <option value="1a435d29-b822-f12d-9908-57482e79c641"><?php echo $this->session->get_userdata()['site_lang'] == 'english' ? 'Standard Express' : 'Dịch vụ chuyển phát nhanh'?></option>
                                                <option value="72d9ebd5-abd0-4778-d1af-57482e848f6b"><?php echo $this->session->get_userdata()['site_lang'] == 'english' ? 'Priority Express' : 'Dịch vụ hỏa tốc'?></option>
                                                <option value="77d6ed67-1625-d9df-7ac4-57482eac128f"><?php echo $this->session->get_userdata()['site_lang'] == 'english' ? 'Road/ Truck Service' : 'Dịch vụ đường bộ'?></option>
                                                <option value="d04ec06c-aa19-91ff-4fab-57482f5afd33"><?php echo $this->session->get_userdata()['site_lang'] == 'english' ? 'Ecommerce Next Day' : 'Giao Hàng TMĐT qua ngày '?></option>
                                            </select>
                                            <!-- validate -->
                                            <!-- <label class="error">This field is required.</label> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_weight']; ?></label>
                                        <input class="form-control" type="text" name="weight[]">
                                        <span class="error error_weight"><?php echo $language['txt_create_booking_error_weight']; ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_cod']; ?></label>
                                        <input class="form-control" type="text" name="cod_value[]">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_booking_value']; ?></label>
                                        <input class="form-control" type="text" name="shipment_value[]">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_date']; ?></label>
                                        <div class="datetimepicker-box">
                                            <input class="form-control" type="input" name="date_entered[]" id="date_entered" value="<?php echo date('d/m/Y h:i',time())?>" disabled>
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_pcs']; ?></label>
                                        <input class="form-control" type="input" name="pcs[]">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_create_booking_description']; ?></label>
                                        <input class="form-control" type="input" name="description[]">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group align-middle">
                                        <div class="checkbox">
                                            <label class="checkbox-label">
                                                <input type="checkbox" name="save_address[]">
                                                <span class="control-label"><?php echo $language['txt_save_address']; ?></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row action-row">
                    <div class="col-xs-12">
                        <button class="btn btn-add add-newBooking-btn" type="button">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <span><?php echo $language['txt_create_booking_add_more']; ?></span>
                        </button>
                    </div>
                </div>
                <div class="action-row text-center action-group">
                    <button type="button" class="btn btn-primary" onclick="createMultiOrder();"><?php echo $language['txt_create_booking_create']; ?></button>
                    <button type="button" class="btn btn-gray" type="button" id="create_and_print_awb_bill" onclick="createAndPrintAWB()"><?php echo $language['txt_create_booking_create_and_print_awb']; ?></button>
                </div>
            </section>
        <!-- </form>      -->
    </form>
</main>
<!-- modal -->
<div class="modal fade shiper-info-modal" role="dialog" data-backdrop="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" aria-label="Close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $language['txt_create_booking_shipper_name']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <button class="btn btn-add" data-toggle="modal" data-target=".add-info">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <span><?php echo $language['txt_create_booking_add_address']; ?></span>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="control-search">
                                <input type="search" name="info-address-search" class="form-control" id="inputSearch">
                                <button class="btn-search">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group listSlimScroll" id="list_address_body">
                        </div>  
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-center action-group">
                    <button type="button" class="btn btn-primary" onclick="changeAddress();"><?php echo $language['txt_create_booking_ok']; ?></button>
                    <button type="reset" class="btn btn-gray" data-dismiss="modal"><?php echo $language['txt_create_booking_cancel']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="modal fade add-info" role="dialog" data-backdrop="false" id="add_address">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-success-modal" aria-label="Close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $language['txt_create_booking_add']; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_name']; ?></label>
                                <input type="text" class="form-control" id="add_name">
                                <span class="error" id="error_add_address_receiver_name"><?php echo $language['txt_add_address_error_receiver_name']; ?></span>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_address_shipper']; ?></label>
                                <input type="text" class="form-control" id="add_street">
                                <span class="error" id="error_add_address_receiver_street"><?php echo $language['txt_add_address_error_street']; ?></span>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_city']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" id="add_city" onchange="changeCityAddress();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_city']; ?>">
                                        <?php echo $cityOption?>
                                    </select>
                                    <span class="error" id="error_add_address_receiver_city"><?php echo $language['txt_add_address_error_city']; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_ward']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" id="add_ward" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_ward']; ?>">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_mobile_phone']; ?></label>
                                <input type="tel" class="form-control" id="add_phone">
                                <span class="error" id="error_add_address_receiver_phone"><?php echo $language['txt_add_address_error_receiver_phone']; ?></span>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_nation']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" id="add_country" onchange="changeCountryAddress();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_nation']; ?>">
                                        <?php echo $countryOption ?>
                                    </select>
                                    <span class="error" id="error_add_address_receiver_country"><?php echo $language['txt_add_address_error_nation']; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_district']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" id="add_district" onchange="changeDistrictAddress();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_district']; ?>">
                                    </select>
                                    <span class="error" id="error_add_address_receiver_district"><?php echo $language['txt_add_address_error_district']; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_zip_code']; ?></label>
                                <input type="text" class="form-control" id="add_zip_code">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <div class="form-group">
                                <div class="checkbox-row">
                                    <div class="checkbox-inline">
                                        <label class="checkbox-label">
                                            <input type="checkbox" name="default-name" id="default" value="1" checked>
                                            <span class="control-label"><?php echo $language['txt_create_booking_set_default']; ?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="action-row text-center action-group">
                        <button type="submit" class="btn btn-primary" onclick="saveAddress();"><?php echo $language['txt_create_booking_ok']; ?></button>
                        <button type="reset" class="btn btn-gray" onclick="resetFormAddAddress();"><?php echo $language['txt_create_booking_reset']; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal success-modal inquiry-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Your order has been successfully submitted</h2>
                <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal error-modal inquiry-error-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/error-img.png" alt="speedlink">
                </div>
                <h2 class="success-title">Your order is defective</h2>
                <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                <p class="cta-block">
                    <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var current_datetime = '<?php echo date('d/m/Y h:i',time()) ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/booking_create_multi.js?v=<?php echo $this->config->item('version_random'); ?>'></script>