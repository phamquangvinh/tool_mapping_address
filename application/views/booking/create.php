<main>
    <?php
        $lang = 'en';
        if($this->session->get_userdata()['site_lang'] == 'vietnamese') $lang = 'vi';
    ?>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="/"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_breadcrumb_create_booking']; ?></li>
            </ol>
        </div>
        <section class="panel section-block order-create-section">
            <div class="form-block">
                <h3 class="form-title">
                    <?php echo $language['txt_create_booking_shipper_name']; ?>
                </h3>
                <input type="hidden" id="account_id" name="account_id" value="<?php echo $this->session->userdata('login')['accounts_id'] ?>">
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_create_booking_full_name']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span id="short_name"><?php echo $dataAccount['short_name']?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_create_booking_mobile_phone']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span id="mobile_phone"><?php echo $dataAccount['mobile_phone'] ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row row-view">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold"><?php echo $language['txt_create_booking_company_name']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span id="name"><?php echo $dataAccount['name'] ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <span class="font-weight-bold" id="address_pickup"><?php echo $language['txt_create_booking_address_shipper']; ?></span>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <span id="address"><?php echo $dataAccount['address'] ?></span>
                                    <button class="btn btn-add" data-toggle="modal" data-target=".shiper-info-modal" type="button" onclick="getListAddress();">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                        <span><?php echo $language['txt_create_booking_change_address']; ?></span>
                                    </button>
                                    <span class="error" id="error_shipper_info"><?php echo $language['txt_create_booking_error_shipper_info']; ?></span>
                                    <input type="hidden" name="shipper_country" id="shipper_country" value="<?php echo $dataAccount['country_id'] ?>">
                                    <input type="hidden" name="shipper_city" id="shipper_city" value="<?php echo $dataAccount['city_id'] ?>">
                                    <input type="hidden" name="shipper_district" id="shipper_district" value="<?php echo $dataAccount['district_id'] ?>">
                                    <input type="hidden" name="shipper_ward" id="shipper_ward" value="<?php echo $dataAccount['ward_id'] ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-block">
                <h3 class="form-title">
                    <?php echo $language['txt_create_booking_info']; ?>
                </h3>
                <div class="form-wrap">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_type_product']; ?></label>
                                <div class="select-box">
                                    <select class="form-control" name="is_dox" id="is_dox" onchange="getListShipmentType();">
                                        <option value="non_dox" <?php if ($dataDuplicate['is_dox'] == 'non_dox'){?> selected <?php } ?> > <?php echo $language['txt_create_booking_non_dox']; ?></option>
                                        <option value="dox" <?php if ($dataDuplicate['is_dox'] == 'dox'){?> selected <?php } ?> ><?php echo $language['txt_create_booking_dox']; ?></option>
                                    </select>
                                    <!-- <label class="error">This field is required.</label> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_weight']; ?></label>
                                <input class="form-control" type="text" name="weight" id="weight" onchange="getListShipmentType();" value="<?php echo $dataDuplicate['weight'] ?>">
                                <span class="error" id="error_weight"><?php echo $language['txt_create_booking_error_weight']; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_charge_to']; ?></label>
                                <div class="select-box">
                                    <select class="form-control" name="charge_to" id="charge_to">
                                        <option value="shipper" <?php if ($dataDuplicate['charge_to'] == 'shipper'){?> selected <?php } ?> ><?php echo $language['txt_create_booking_charge_to_shipper'];?></option>
                                        <option value="receiver" <?php if ($dataDuplicate['charge_to'] == 'receiver'){?> selected <?php } ?> ><?php echo $language['txt_create_booking_charge_to_receiver']; ?></option>
                                    </select>
                                    <!-- <label class="error">This field is required.</label> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_cod']; ?></label>
                                <input class="form-control" type="text" name="cod_value" id="cod_value" onchange="getListShipmentType();" value="<?php echo $dataDuplicate['cod_value'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_payment_method']; ?></label>
                                <div class="select-box">
                                    <?php $user = $this->session->userdata('login'); if ($user['accounts_id']!='' && $user['accounts_id']!=null && $user['payment_method']!='' && $user['payment_method']!=null && $user['payment_method'] == 'bank_transfer') { ?>
                                        <select class="form-control" name="payment_method" id="payment_method">
                                            <option value="cash" <?php if ($dataDuplicate['payment_method'] == 'cash'){?> selected <?php } ?> > <?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                            <option value="bank_transfer" <?php if ($dataDuplicate['bank_transfer'] == 'cash'){?> selected <?php } ?> > <?php echo $language['txt_create_booking_payment_method_bank_transfer']; ?></option>
                                        </select>
                                    <?php }elseif($user['accounts_id']!='' && $user['accounts_id']!=null && $user['payment_method']!='' && $user['payment_method']!=null && $user['payment_method'] == 'other'){?>
                                            <select class="form-control" name="payment_method" id="payment_method" readonly="">
                                                <option value="cash" selected><?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                            </select>
                                    <?php }else{  ?>
                                        <select class="form-control" name="payment_method" id="payment_method" readonly="">
                                            <option value="cash" selected><?php echo $language['txt_create_booking_payment_method_cash']; ?></option>
                                        </select>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_pcs']; ?></label>
                                <input class="form-control" type="text" name="pcs" id="pcs" value="<?php echo $dataDuplicate['pcs_value'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_date']; ?></label>
                                <div class="date input-group">
                                    <input class="form-control" type="input" name="date_entered " id="date_entered" value="<?php echo date('d/m/Y h:i',time())?>" disabled="">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_booking_value']; ?></label>
                                <input class="form-control" type="text" name="shipment_value" id="shipment_value" value="<?php echo $dataDuplicate['shipment_value'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_tracking_no']; ?></label>
                                <input class="form-control" type="text" name="tracking_no" id="tracking_no" value="<?php echo $dataDuplicate['tracking_no'] ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $language['txt_create_booking_description']; ?></label>
                                <textarea class="form-control" type="input" name="description" id="description" rows="1" placeholder="<?php echo $language['txt_placeholder_description']; ?>"><?php echo $dataDuplicate['booking_note'] ?></textarea>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="form-block">
                <h3 class="form-title">
                    <?php echo $language['txt_create_booking_receiver_info']; ?>
                </h3>
                <div class="form-wrap">
                    <div class="form-inner receiverForm">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_create_booking_receiver_name']; ?></label>
                                    <input class="form-control autoFillBooking" type="input" name="receiver_name" id="receiver_name" value="<?php echo $dataDuplicate['receiver_name'] ?>">
                                    <span class="error" id="error_receiver_name"><?php echo $language['txt_create_booking_error_receiver_name']; ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_create_booking_receiver_phone']; ?></label>
                                    <input class="form-control" type="input" name="receiver_phone" id="receiver_phone" value="<?php echo $dataDuplicate['receiver_phone'] ?>">
                                    <span class="error" id="error_receiver_phone"><?php echo $language['txt_create_booking_error_receiver_phone']; ?></span>
                                    <span class="error" id="error_receiver_phone_length"><?php echo $language['txt_add_phone_number_invalid']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                </label>
                                <div class="form-group">
                                    <div class="form-group" >
                                        <label class="control-label"><?php echo $language['txt_create_booking_nation']; ?></label>
                                        <div class="select-box" id="r_row">
                                            <select class="form-control chosenSelect" id="receiver_country" name="receiver_country" onchange="changeCountry();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_nation']; ?>">
                                                <?php echo $countryOption ?>
                                            </select>
                                        </div>
                                        <span class="error" id="error_receiver_country"><?php echo $language['txt_create_booking_error_nation']; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_create_booking_zip_code']; ?></label>
                                    <input class="form-control" type="input" name="receiver_zip_code" id="receiver_zip_code" value="<?php echo $dataDuplicate['zip_code'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label"><?php echo $language['txt_create_booking_city']; ?></label>
                                <div class="select-box" id="city_row">
                                    <select class="form-control chosenSelect" id="receiver_city" name="receiver_city" onchange="changeCity();getListShipmentType();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_city']; ?>" >
                                        <?php echo $cityOption ?>
                                    </select>
                                </div>
                                <span class="error" id="error_receiver_city"><?php echo $language['txt_create_booking_error_city']; ?></span>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_create_booking_district']; ?></label>
                                    <div class="select-box" id="district_row">
                                        <select class="form-control chosenSelect" id="receiver_district" name="receiver_district" onchange="changeDistrict();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_district']; ?>">
                                            <?php echo $dataDuplicate['districtOption'] ?>
                                        </select>
                                    </div>
                                    <span class="error" id="error_receiver_district"><?php echo $language['txt_create_booking_error_district']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_create_booking_ward']; ?></label>
                                    <div class="select-box" id="ward_row">
                                        <select class="form-control chosenSelect" id="receiver_ward" name="receiver_ward" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_ward']; ?>">
                                            <?php echo $dataDuplicate['wardOption'] ?>
                                        </select>
                                    </div>
                                    <span class="error" id="error_receiver_ward"><?php echo $language['txt_create_booking_error_ward']; ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $language['txt_create_booking_street']; ?></label>
                                    <input class="form-control" type="input" name="receiver_address" id="receiver_address" value="<?php echo $dataDuplicate['receiver_street'] ?>">
                                    <span class="error" id="error_receiver_street"><?php echo $language['txt_create_booking_error_street']; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label class="checkbox-label">
                                            <input type="checkbox" name="save_address" id="save_address" value="0">
                                            <span class="control-label"><?php echo $language['txt_save_address']; ?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-block">
                <h3 class="form-title">
                    <?php echo $language['txt_create_booking_service']; ?>
                </h3>
                <div class="form-wrap">
                    <div class="row wrap-table">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-hover" id="list_shipment_type">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center"><?php echo $language['txt_create_booking_service']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_create_booking_price']; ?></th>
                                        <th class="text-center"><?php echo $language['txt_create_booking_description']; ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <span class="error" id="error_service"><?php echo $language['txt_create_booking_error_service']; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="action-row text-center action-group">
                <input type="hidden" class="msg_alert_empty_field" value="<?php echo $language['txt_create_booking_empty_field']; ?>">
                <button class="btn btn-primary" onclick="createOrder();"><?php echo $language['txt_create_booking_send_to_speedlink']; ?></button>
            </div>
        </section>
    </div>
</main>
<!-- modal -->
    <div class="modal fade shiper-info-modal" role="dialog" data-backdrop="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" aria-label="Close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $language['txt_create_booking_shipper_name']; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-add" data-toggle="modal" data-target=".add-info">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <span><?php echo $language['txt_create_booking_add_address']; ?></span>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="control-search">
                                    <input type="search" name="info-address-search" class="form-control" id="inputSearch">
                                    <button class="btn-search">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group listSlimScroll" id="list_address_body">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="action-row text-center action-group">
                        <button type="submit" class="btn btn-primary" onclick="changeAddress();"><?php echo $language['txt_create_booking_ok']; ?></button>
                        <button type="button" class="btn btn-gray" data-dismiss="modal"><?php echo $language['txt_create_booking_cancel']; ?></button>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade add-info" role="dialog" data-backdrop="false" id="add_address">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-success-modal" aria-label="Close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $language['txt_create_booking_add']; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_name']; ?></label>
                                <input type="text" class="form-control" id="add_name">
                                <span class="error" id="error_add_address_receiver_name"><?php echo $language['txt_add_address_error_receiver_name']; ?></span>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_address_shipper']; ?></label>
                                <input type="text" class="form-control" id="add_street">
                                <span class="error" id="error_add_address_receiver_street"><?php echo $language['txt_add_address_error_street']; ?></span>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_city']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" id="add_city" onchange="changeCityAddress();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_city']; ?>">
                                        <?php echo $cityOption?>
                                    </select>
                                    <span class="error" id="error_add_address_receiver_city"><?php echo $language['txt_add_address_error_city']; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_ward']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" id="add_ward" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_ward']; ?>">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_mobile_phone']; ?></label>
                                <input type="tel" class="form-control" id="add_phone">
                                <span class="error" id="error_add_address_receiver_phone"><?php echo $language['txt_add_address_error_receiver_phone']; ?></span>
                                <span class="error" id="error_add_address_receiver_phone_length"><?php echo $language['txt_add_phone_number_invalid']; ?></span>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_nation']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" id="add_country" onchange="changeCountryAddress();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_nation']; ?>">
                                        <?php echo $countryOption ?>
                                    </select>
                                    <span class="error" id="error_add_address_receiver_country"><?php echo $language['txt_add_address_error_nation']; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_district']; ?></label>
                                <div class="select-box">
                                    <select class="form-control chosenSelect" id="add_district" onchange="changeDistrictAddress();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_district']; ?>">
                                    </select>
                                    <span class="error" id="error_add_address_receiver_district"><?php echo $language['txt_add_address_error_district']; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo $language['txt_create_booking_zip_code']; ?></label>
                                <input type="text" class="form-control" id="add_zip_code">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <div class="form-group">
                                <div class="checkbox-row">
                                    <div class="checkbox-inline">
                                        <label class="checkbox-label">
                                            <input type="checkbox" name="default-name" id="default" value="1" checked>
                                            <span class="control-label"><?php echo $language['txt_create_booking_set_default']; ?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="action-row text-center action-group">
                        <input type="hidden" id="txt_add_address_required" value="<?=$language['txt_add_address_required']?>">
                        <button type="submit" class="btn btn-primary" onclick="saveAddress();"><?php echo $language['txt_create_booking_ok']; ?></button>
                        <button type="reset" class="btn btn-gray" onclick="resetFormAddAddress();"><?php echo $language['txt_create_booking_reset']; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal success-modal inquiry-success-modal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
                </div>
                <div class="modal-body text-center">
                    <div class="success-img">
                        <img src="/assets/images/icons/success-img.png" alt="myface">
                    </div>
                    <h2 class="success-title">Your order has been successfully submitted</h2>
                    <p class="success-detail">Customer care will contact you within 24 hours. Thank you!</p>
                    <p class="cta-block">
                        <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal error-modal inquiry-error-modal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close close-success-modal" data-dismiss="modal" aria-label="Close" type="button">X</button>
                </div>
                <div class="modal-body text-center">
                    <div class="success-img">
                        <img src="/assets/images/icons/error-img.png" alt="speedlink">
                    </div>
                    <h2 class="success-title">Your order is defective</h2>
                    <p class="success-detail">Please check again or call the hotline <a href="tel:19006411">1900 6411</a> for help</p>
                    <p class="cta-block">
                        <button class="btn btn-gray" data-dismiss="modal" aria-label="Close" type="button">OK</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
<script src='/assets/js/booking_create.js?v=<?php echo $this->config->item('version_random'); ?>'></script>
<script type="text/javascript">
    $(function() {
        loadScriptCreateOrder();
    })
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>