<!--page-container-->
<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="/"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active"><?php echo $language['txt_send_feed_back']; ?></a></li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <form action="#" method="POST">
                <div class="form-block service-soa-form">
                    <h3 class="form-title"><?php echo $language['txt_send_feed_back']; ?></h3>
                    <div class="form-wrap">
                        <div class="search-booking-group">
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_feed_back_date_type']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" id="date_range" name="date_range" onchange="changeDateRange();">
                                                <?php echo $dataFilter['dateRangeOption']; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_feed_back_from_date']; ?></label>
                                        <div class="datetimepicker-box">
                                            <input class="form-control datepicker" type="input" name="from_date" id="from_date" readonly="" value="<?php echo $dataFilter['from_date'] ?>">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_feed_back_to_date']; ?></label>
                                        <div class="datetimepicker-box">
                                            <input class="form-control datepicker" type="input" name="to_date" id="to_date" readonly="" value="<?php echo $dataFilter['to_date'] ?>">
                                            <span class="input-group-addon">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_feed_back_order_no']; ?></label>
                                        <input class="form-control" type="input" name="order_no" placeholder="" value="<?php echo $dataFilter['order_no'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $language['txt_feed_back_status']; ?></label>
                                        <div class="select-box">
                                            <select class="form-control" name="status">
                                                <?php echo $dataFilter['statusOption'] ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-offset-6 col-sm-6 col-sm-offset-0 col-md-4">
                                    <div class="form-group cta-block">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <button class="btn btn-primary"><?php echo $language['txt_feed_back_search']; ?></button>
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" class="btn btn-primary" onclick="ResetFormFeedback();"><?php echo $language['txt_feed_back_reset']; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="export-booking-form">
                            <div class="row form-group">
                                <div class="col-xs-12">
                                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target=".add-feedback-modal"><?php echo $language['txt_feed_back_create']; ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="row wrap-table">
                            <div class="col-xs-12">
                                <table class="table table-bordered table-hover dataTable" id="table_list_cases" style="width: 100%">
                                    <thead class="text-center">
                                        <tr role="row" style="width: 100%">
                                            <th><?php echo $language['txt_feed_back_stt']; ?></th>
                                            <th><?php echo $language['txt_feed_back_title']; ?></th>
                                            <th><?php echo $language['txt_feed_back_order_no']; ?></th>
                                            <th><?php echo $language['txt_feed_back_date_send']; ?></th>
                                            <th><?php echo $language['txt_feed_back_status']; ?></th>
                                            <th><?php echo $language['txt_feed_back_date_modified']; ?></th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
</main>
<!-- modal -->
<div class="modal fade add-feedback-modal" role="dialog" data-backdrop="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-success-modal" aria-label="Close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $language['txt_feed_back_create']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_feed_back_title']; ?></label>
                            <input class="form-control" id="add_feedback_title" name="add_feedback_title" type="text">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?php
                            $lang = 'en';
                            if($this->session->get_userdata()['site_lang'] == 'vietnamese') $lang = 'vi';
                        ?>
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_feed_back_order_no']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="add_feedback_order_no" name="add_feedback_order_no" data-placeholder="&nbsp;" lang="<?=$lang?>">
                                    <?php echo $dataFilter['listOrderOption'] ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_feed_back_description']; ?></label>
                            <textarea class="form-control" rows="4" id="add_feedback_description" name="add_feedback_description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-center action-group">
                    <button type="button" class="btn btn-primary" onclick="createFeedback();"><?php echo $language['txt_send_feed_back']; ?></button>
                    <button type="reset" class="btn btn-gray" data-dismiss="modal"><?php echo $language['txt_feed_back_close']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade edit-feedback-modal" role="dialog" data-backdrop="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-success-modal" aria-label="Close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $language['txt_edit_feed_back']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_feed_back_title']; ?></label>
                            <input class="form-control" id="edit_feedback_title" name="edit_feedback_title" type="text">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_feed_back_order_no']; ?></label>
                            <div class="select-box">
                                <select class="form-control chosenSelect" id="edit_feedback_order_no" name="edit_feedback_order_no" data-placeholder="&nbsp;">
                                    <?php echo $dataFilter['listOrderOption'] ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for=""><?php echo $language['txt_feed_back_description']; ?></label>
                            <textarea class="form-control" rows="4" id="edit_feedback_description" name="edit_feedback_description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-center action-group">
                    <button type="button" class="btn btn-primary" id="edit_feedback_btn"><?php echo $language['txt_edit_feed_back']; ?></button>
                    <button type="reset" class="btn btn-gray" data-dismiss="modal"><?php echo $language['txt_feed_back_close']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal feedback-detail-modal" role="dialog" data-backdrop="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-success-modal" aria-label="Close" data-dismiss="modal">&times;</button>
                <div class="pull-right modal-action" id="edit_feedback">
                    <a class="btn-add" href="javacript:void(0);" onclick="editFeedback(this)">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                    <span><?php echo $language['txt_edit_feed_back']; ?></span>
                </a>
                </div>
                <h4 class="modal-title"><?php echo $language['txt_feed_back_detail']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="row row-view">
                            <div class="col-xs-12">
                                <span class="font-weight-bold"><?php echo $language['txt_feed_back_title']; ?></span>
                            </div>
                            <div class="col-xs-12">
                                <span id="title"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="row row-view">
                            <div class="col-xs-12">
                                <span class="font-weight-bold"><?php echo $language['txt_feed_back_order_no']; ?></span>
                            </div>
                            <div class="col-xs-12">
                                <span id="order_no"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="row row-view">
                            <div class="col-xs-12">
                                <span class="font-weight-bold"><?php echo $language['txt_feed_back_description']; ?></span>
                            </div>
                            <div class="col-xs-12">
                                <span id="description"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="font-weight-bold"><?php echo $language['txt_feed_back_resolution']; ?></span>
                            </div>
                            <div class="col-xs-12">
                                <span id="resolution"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="row row-view">
                            <div class="col-xs-12">
                                <span class="font-weight-bold"><?php echo $language['txt_feed_back_status']; ?></span>
                            </div>
                            <div class="col-xs-12">
                                <span id="status"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="font-weight-bold"><?php echo $language['txt_feed_back_date_send']; ?></span>
                            </div>
                            <div class="col-xs-12">
                                <span id="date_entered"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="action-row text-center action-group">
                    <button type="reset" class="btn btn-gray" data-dismiss="modal"><?php echo $language['txt_feed_back_close']; ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var dataTable = '<?php echo $dataTable; ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/feedback.js?v=<?php echo $this->config->item('version_random'); ?>'></script>