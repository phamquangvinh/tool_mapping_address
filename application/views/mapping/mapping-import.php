<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="#"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active">Mapping Import</li>
            </ol>
        </div>
        <section class="panel section-block order-import-section">
            <div class="form-block booking-import-form">
                <h3 class="form-title">
                    IMPORT MAPPING
                </h3>
                <div class="form-wrap">
                    <div class="import-control-group">
                        <h2 class="form-title gray-text text-center"><?php echo $language['txt_import_booking_select_excel']; ?></h2>
                        <div class="upload-group-control">
                            <form id="uploadExcel">
                                <input class="fileUpload" name="mapping_excel" id="mapping_excel" type="file" data-show-preview="false" data-msg-placeholder="<?php echo $language['txt_import_booking_select_file']; ?>" data-allowed-file-extensions='["xls", "xlsx"]'>
                            </form>
                            <a class="btn btn-link download-btn" href="/assets/docs/template_import_mapping.xlsx" target="_blank">Download Sample</a>
                            <span id="img_loading"></span>
                        </div>
                    </div>
                    <div class="info-import" id="load_import">
                    </div>
                    <div class="row wrap-table">
                        <div class="col-xs-12">
                            <div id="result">
                                <table class="table table-bordered table-hover" class="width: 100%" id="table_data_import">
                                    <thead>
                                    <tr>
                                        <th id="countryText">Country Text</th>
                                        <th id="countryCode">Country Code</th>
                                        <th id="cityText">City Text</th>
                                        <th id="cityCode">City Code</th>
                                        <th id="districtText">District Text</th>
                                        <th id="districtCode">District Code</th>
                                        <th id="wardText">Ward Text</th>
                                        <th id="wardCode">Ward Code</th>
                                        <th id="partnerAddressCode">Partner Address Code</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="action-row text-center action-group">
                <button class="btn btn-primary" id="btn_import_booking_bottom" type="submit" class="btn_send" style="display: none; ">Import</button>
            </div>
        </section>
    </div>
<script type="text/javascript">
    var dataTable = '<?php echo $dataTable; ?>';
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src='/assets/js/mapping-import.js?v=<?php echo $this->config->item('version_random'); ?>'></script>


