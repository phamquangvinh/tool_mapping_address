<main>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="#"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active">Mapping Detail</li>
            </ol>
        </div>
        <section class="panel section-block shipping-section">
            <div class="form-block shipping-lookup-form" id="export_excel_data">
                <div class="form-block">
                    <h3 class="form-title pull-left">Export Data</h3>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <label for="export_partner_select">Select a partner</label>
                            <select id="export_partner_select" style="height: 30px;">
                                <?php echo $listPartner;?>
                            </select>
                            <a type="button" class="btn btn-sm btn-gray" id="export_partner">Export Mapping For Partner</a>
                        </div>
                    </div>
                    <br>
                    <div class="info-export" id="load_export">
                    </div>
                </div>
            </div>
            <div class="form-block shipping-lookup-form" id="list_mapping">
                <div class="form-block">
                    <h3 class="form-title pull-left">List View</h3>
                    <form action="" method="post">
                        <div class="form-wrap">
                            <div class="row wrap-table">
                                <div class="col-xs-12">
                                    <table class="table table-bordered table-hover mapping-list-table" class="width: 100%" id="table_mapping_list">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Partner</th>
                                            <th class="text-center">Address Code</th>
                                            <!--                                        <th class="text-center">Zip Code</th>-->
                                            <th class="text-center">Country</th>
                                            <th class="text-center">City</th>
                                            <th class="text-center">District</th>
                                            <th class="text-center">Ward</th>
                                            <th class="text-center">Branch Code</th>
                                            <th class="text-center">Hub Code</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Note</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                        <tr id = "forFilters">
                                            <th class="partnerFilter text-center"></th>
                                            <th class="text-center"></th>
                                            <th class="countryFilter text-center"></th>
                                            <th class="cityFilter text-center"></th>
                                            <th class="text-center"></th>
                                            <th class="text-center"></th>
                                            <th class="text-center"></th>
                                            <th class="text-center"></th>
                                            <th class="statusFilter text-center"></th>
                                            <th class="noteFilter text-center"></th>
                                            <th class="text-center"></th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group text-center">
                                        <button class="btn btn-primary" type="button" id="get_mapping_list">Get Mapping List</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="form-block shipping-lookup-form" id="mapping_view" style="display:none">
                <h3 class="form-title">Partner (<span id="partner"></span>)</h3>
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6" id="row_select_home_send_from">
                                            <label class="control-label">Country</label>
                                            <input type="text" class="form-control" id="country_text_ro" placeholder="" readonly/>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Country Code</label>
                                            <input type="text" class="form-control" id="country_code_ro" placeholder="" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6" id="row_select_home_send_from">
                                            <label class="control-label">City</label>
                                            <input type="text" class="form-control" id="city_text_ro" placeholder="" readonly/>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">City Code</label>
                                            <input type="text" class="form-control" id="city_code_ro" placeholder="" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6" id="row_select_home_send_from">
                                            <label class="control-label">District</label>
                                            <input type="text" class="form-control" id="district_text_ro" placeholder="" readonly/>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">District Code</label>
                                            <input type="text" class="form-control" id="district_code_ro" placeholder="" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6" id="row_select_home_send_from">
                                            <label class="control-label">Ward</label>
                                            <input type="text" class="form-control" id="ward_text_ro" placeholder="" readonly/>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Ward Code</label>
                                            <input type="text" class="form-control" id="ward_code_ro" placeholder="" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6" id="row_select_home_send_from">
                                            <label class="control-label">Address code</label>
                                            <input type="text" class="form-control" id="address_code_ro" placeholder="" readonly/>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Zip code</label>
                                            <input type="text" class="form-control" id="zip_code_ro" placeholder="" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="form-block shipping-lookup-form" id="mapping_edit" style="display:none">
                <h3 class="form-title">Speedlink</h3>
                <form id="update_form" action="" method="post">
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group">
                                        <div class="col-xs-6">
                                            <label class="control-label">Country (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box" id="country_row">
                                                <select class="form-control chosenSelect" id="country_select" onchange="changeCountry();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_nation']; ?>">
                                                    <?php echo $countryOption; ?>
                                                </select>
                                            </div>
                                            <span class="error" id="error_select_country">(*) Missing Country Field</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">City (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box" id="city_row">
                                                <select class="form-control chosenSelect" id="city_select" onchange="changeCity();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_city']; ?>">

                                                </select>
                                            </div>
                                            <span class="error" id="error_select_city">(*) Missing City Field</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6" id="row_select_home_send_from">
                                            <label class="control-label">District (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box">
                                                <select class="form-control chosenSelect" id="district_select" onchange="changeDistrict();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_district']; ?>">

                                                </select>
                                            </div>
                                            <span class="error" id="error_select_district">(*) Missing District Field</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Ward (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box" id="ward_row">
                                                <select class="form-control chosenSelect" id="ward_select" onchange="changeWard();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_ward']; ?>">

                                                </select>
                                            </div>
                                            <span class="error" id="error_select_ward">(*) Missing Ward Field</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="address_loading" style="display: none">
                            <div class="col-xs-12 text-center">
                                <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6">
                                            <label class="control-label">Branch (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box" id="branch_row">
                                                <select class="form-control chosenSelect" id="branch_select" onchange="changeBranch();" lang="<?=$lang?>" >

                                                </select>
                                            </div>
<!--                                            <input type="text" class="form-control" id="branch_select" name="branch_select" readonly/>-->
                                            <span class="error" id="error_select_branch">(*) Missing Branch Field</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Hub (<span class="important-field">&ast;</span>)</label>
                                            <input type="text" class="form-control" id="hub_select" name="hub_select" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group">
                                        <div class="col-xs-6">
                                            <label class="control-label">Address Code</label>
                                            <input type="text" class="form-control" id="suggest_code" readonly/>
                                            <div id="suggest_code_hint"></div>
                                            <div id="suggest_code_exist"></div>
                                            <div id="suggest_new_code"></div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Zip code</label>
                                            <input type="text" class="form-control" id="zipcode"/>
                                            <span class="error" id="error_missing_zipcode" style="display: none;">* Zip code missing</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="action-row text-center action-group">
                                <button type="submit" class="btn btn-primary" id="save">Save</button>
                                <button type="button" class="btn btn-gray" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
    </script>
    <script src='/assets/js/mapping-detail.js?v=<?php echo $this->config->item('version_random'); ?>'></script>


