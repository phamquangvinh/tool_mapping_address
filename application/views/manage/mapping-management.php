<main>
    <?php
    $lang = 'en';
    if($this->session->get_userdata()['site_lang'] == 'vietnamese') $lang = 'vi';
    ?>
    <div class="container">
        <div class="breadcrumb-block">
            <ol class="breadcrumb">
                <li><a href="#"><?php echo $language['txt_breadcrumb_home']; ?></a></li>
                <li class="active">Management Address</li>
            </ol>
        </div>
        <section class="panel section-block shipping-section">
            <div class="form-block shipping-lookup-form" id="export_excel_data">
                <div class="form-block">
                    <h3 class="form-title pull-left">Export Data</h3>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <a type="button" class="btn btn-gray" id="export_speedlink">Export List Address Code</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-block shipping-lookup-form" id="mapping_edit_create" >
                <h3 class="form-title">Create/Edit Address</h3>
                <form id="edit_form" action="" method="post">
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group">
                                        <div class="col-xs-6">
                                            <label class="control-label">Country (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box" id="country_row_edit">
                                                <select class="form-control chosenSelect" id="country_select_edit" onchange="changeCountryEdit();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_nation']; ?>">
                                                    <?php echo $countryOption; ?>
                                                </select>
                                            </div>
                                            <span class="error" id="error_select_country_edit">(*) Missing Country Field</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">City (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box" id="city_row_edit">
                                                <select class="form-control chosenSelect" id="city_select_edit" onchange="changeCityEdit();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_city']; ?>">

                                                </select>
                                            </div>
                                            <span class="error" id="error_select_city_edit">(*) Missing City Field</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6" id="row_select_home_send_from">
                                            <label class="control-label">District (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box">
                                                <select class="form-control chosenSelect" id="district_select_edit" onchange="changeDistrictEdit();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_district']; ?>">

                                                </select>
                                            </div>
                                            <span class="error" id="error_select_district_edit">(*) Missing District Field</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Ward (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box" id="ward_row_edit">
                                                <select class="form-control chosenSelect" id="ward_select_edit" onchange="changeWardEdit();" lang="<?=$lang?>" data-placeholder="<?php echo $language['txt_create_booking_select_ward']; ?>">

                                                </select>
                                            </div>
                                            <span class="error" id="error_select_ward_edit">(*) Missing Ward Field</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="address_loading_edit" style="display: none">
                            <div class="col-xs-12 text-center">
                                <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group" id="group_select_home_send_from">
                                        <div class="col-xs-6">
                                            <label class="control-label">Branch (<span class="important-field">&ast;</span>)</label>
                                            <div class="select-box" id="branch_row_edit">
                                                <select class="form-control chosenSelect" id="branch_select_edit" onchange="changeBranchEdit();" lang="<?=$lang?>" >

                                                </select>
                                            </div>
                                            <!--                                            <input type="text" class="form-control" id="branch_select" name="branch_select" readonly/>-->
                                            <span class="error" id="error_select_branch_edit">(*) Missing Branch Field</span>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Hub (<span class="important-field">&ast;</span>)</label>
                                            <input type="text" class="form-control" id="hub_select_edit" name="hub_select_edit" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="row size-input-group">
                                        <div class="col-xs-6">
                                            <label class="control-label">Address Code</label>
                                            <input type="text" class="form-control" id="suggest_code_edit" readonly/>
                                            <div id="suggest_code_exist_edit"></div>
                                            <div id="suggest_new_code_edit"></div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Zip code</label>
                                            <input type="text" class="form-control" id="zipcode_edit"/>
                                            <span class="error" id="error_missing_zipcode_edit" style="display: none;">* Zip code missing</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="action-row text-center action-group">
                                <button type="submit" class="btn btn-primary" id="save_edit">Save</button>
                                <button type="button" class="btn btn-gray" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
    </script>
    <script src='/assets/js/mapping-management.js?v=<?php echo $this->config->item('version_random'); ?>'></script>


