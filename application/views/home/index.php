<!--page-container-->
<?php
    $lang = 'en';
    if($this->session->get_userdata()['site_lang'] == 'vietnamese') $lang = 'vi';
?>
<main>
    <div class="container">
        <div class="col-xs-12 col-md-12 login-form">
            <div class="row" id="login-form-loading">
                <form id="login-form" action="/login" method="post">
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control" type="input" name="username" id="username" placeholder="<?php echo $language['txt_plh_email_id']; ?>" value="<?php echo $this->session->flashdata('old_username');?>">
                                <?php $error_login = $this->session->flashdata('error_login'); if (!empty($error_login['message'] && ($error_login['status'] == 1 || $error_login['status'] == 4 || $error_login['status'] == 6))){?>
                                    <span class="error">* <?php echo $error_login['message'] ?></span>
                                <?php }?>
                                <?php $fail_api = $this->session->flashdata('fail_api'); if (!empty($fail_api['message'])) { ?>
                                    <p style="display: none"><?php echo $error_login['message'] ?></p>
                                <?php } ?>
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control" type="password" name="password" id="password" placeholder="<?php echo $language['txt_plh_forgot_password']; ?>" value="<?php echo $this->session->flashdata('old_password');?>">
                                <?php $error_login = $this->session->flashdata('error_login'); if (!empty($error_login['message'] && $error_login['status'] == 5)){?>
                                    <span class="error">* <?php echo $error_login['message'] ?></span>
                                <?php }?>
                                <a class="btn btn-link" href="forget-password"><?php echo $language['txt_forgot_password']; ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 cta-block">
                        <input type="submit" class="btn btn-primary" value="<?php echo $language['txt_login']; ?>" onclick="validateLogin();">
                        <a class="btn btn-link" href="register"><?php echo $language['txt_register']; ?></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
    </script>




    <script type="text/javascript">
    var site_lang = '<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>';
</script>
<script src="/assets/js/home.js?v=<?php echo $this->config->item('version_random'); ?>"></script>