<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('_partial/_head'); ?>
<body class="">

    <?php $this->load->view('_partial/_header'); ?>

    <?php $this->load->view($module);?>

    <?php $this->load->view('_partial/_footer'); ?>
</body>
</html>