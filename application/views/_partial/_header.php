<!--header-->
<a href="#" class="overlay sidebar-close-bg"></a>
<div class="mobile-sidebar">
    <a class="sidebar-close" href="#"></a>
    <div class="sidebar-container">
        <div class="sidebar-header">
            <div class="menu-item user-profile-toggle">
                <a href="javascript:void(0);">
                    <img class="img-circle userimg" src="/assets/images/upload/avatar-default.jpg" alt="speedlink">
                    <span class="username"><?php echo $this->session->userdata('login')['last_name']; ?></span>
                </a>
                <ul class="treeview">
                    <li class="<?php echo ($menu == 'user-profile'?'active' : '' ); ?>">
                        <a href="/user-profile"><?php echo $language['txt_menu_info_user']; ?></a>
                    </li>
                    <li class="<?php echo ($menu == 'user-edit'?'active' : '' ); ?>">
                        <a href="/user-edit"><?php echo $language['txt_menu_edit_info_user']; ?></a>
                    </li>
                    <li class="<?php echo ($menu == 'change-password'?'active' : '' ); ?>">
                        <a href="/user-changepass"><?php echo $language['txt_menu_change_password']; ?></a>
                    </li>
                    <li>
                        <a href="/logout"><?php echo $language['txt_menu_exit']; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="sidebar-body">
            <ul class="list-group sidebar-menu-list">
                <li class="menu-item menu-item-toggle <?php echo ($menu == 'mapping'?'active':''); ?>">
                    <a href="javascript:void(0);"><?php echo $language['txt_menu_mapping_import']; ?>
                        <span class="order-icon pull-right">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </a>
                    <ul class="treeview">
                        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/mapping-import'?'active':''); ?>">
                            <a href="/mapping-import">Import</a>
                        </li>
                        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/mapping-detail'?'active' : '' ); ?>">
                            <a href="/mapping-detail">Detail</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item menu-item-toggle <?php echo ($menu == 'manage'?'active':''); ?>">
                    <a href="javascript:void(0);">Management
                        <span class="order-icon pull-right">
                            <i class="fa fa-angle-right"></i>
                        </span>
                    </a>
                    <ul class="treeview">
                        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/mapping-management'?'active':''); ?>">
                            <a href="/mapping-management">Management Address</a>
                        </li>
                    </ul>
                </li>x
            </ul>
            <ul class="list-group sidebar-menu-list sidebar-top-menu">
                <li class="menu-item worktime-item">
                    <a href="javascript:void(0);"><?php echo $language['txt_time_open']; ?>
                        <strong><?php echo $language['txt_time_open_value']; ?></strong>
                    </a>
                </li>
                <li class="menu-item hotline-item">
                    <a href="javascript:void(0);"><?php echo $language['txt_hotline']; ?>
                        <strong><?php echo $language['txt_hotline_value']; ?></strong>
                    </a>
                </li>
                <li class="menu-item support-item">
                    <a href="javascript:void(0);"><?php echo $language['txt_support']; ?>
                        <strong><?php echo $language['txt_support_value']; ?></strong>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--header-->
<!--mobile-menu-->
<header>
    <?php $user = $this->session->userdata('login'); if (empty($user) || !is_array($user)) { ?>
        <nav class="navbar main-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h1 class="navbar-header logo">
                            <a class="navbar-brand" href="<?php echo base_url(); ?>">
                                <img src="/assets/images/logo.png" alt="ITL">
                            </a>
                        </h1>
                    </div>
                </div>
            </div>
        </nav>
    <?php  }else{ ?>
        <nav class="navbar main-header">
            <div class="container">

                <button class="menu-mobile-toggle" type="button" title="MENU">
                    <span class="line line-top"></span>
                    <span class="line line-middle"></span>
                    <span class="line line-bottom"></span>
                </button>
                <div class="row">
                    <div class="col-md-3">
                        <h1 class="navbar-header logo">
                            <a class="navbar-brand" href="<?php echo base_url(); ?>">
                                <img src="/assets/images/logo.png" alt="ITL">
                            </a>
                        </h1>
                    </div>
                    <div class="col-md-9">
                        <div class="menu">
                            <ul class="nav navbar-nav main-menu">
                                <li class="menu-item-toggle <?php echo ($menu == 'mapping'?'active':''); ?>">
                                    <a href="javascript:;"><?php echo $language['txt_menu_mapping_import']; ?></a>
                                    <ul class="dropdown-menu menu-item-dropdown">
                                        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/mapping-import'?'active':''); ?>">
                                            <a href="/mapping-import">Import</a>
                                        </li>
                                        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/mapping-detail'?'active' : '' ); ?>">
                                            <a href="/mapping-detail">Detail</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav main-menu">
                                <li class="menu-item-toggle <?php echo ($menu == 'manage'?'active':''); ?>">
                                    <a href="javascript:;">Management</a>
                                    <ul class="dropdown-menu menu-item-dropdown">
                                        <li class="<?php echo ($_SERVER['REQUEST_URI'] == '/mapping-management'?'active':''); ?>">
                                            <a href="/mapping-management">Management Address</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="dropdown dropdown-user">
                                <a href="" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-close-others="true" title="<?php echo $user['user_name']; ?>">
                                    <img class="img-circle userimg" src="/assets/images/upload/avatar-default.jpg" alt="speedlink">
                                    <span class="username"><?php echo $this->session->userdata('login')['last_name']; ?></span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="<?php echo ($menu == 'user-profile'?'active' : '' ); ?>">
                                        <a href="/user-profile"><?php echo $language['txt_menu_info_user']; ?></a>
                                    </li>
                                    <li class="<?php echo ($menu == 'user-edit'?'active' : '' ); ?>">
                                        <a href="/user-edit"><?php echo $language['txt_menu_edit_info_user']; ?></a>
                                    </li>
                                    <li class="<?php echo ($menu == 'user-change-password'?'active' : '' ); ?>">
                                        <a href="/user-changepass"><?php echo $language['txt_menu_change_password']; ?></a>
                                    </li>
                                    <li>
                                        <a href="/logout"><?php echo $language['txt_menu_exit']; ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    <?php  } ?>
</header>
<!--Start of Tawk.to Script-->
<!--<script type="text/javascript">-->
<!--var $_Tawk_API={},$_Tawk_LoadStart=new Date();-->
<!--(function(){-->
<!--var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];-->
<!--s1.async=true;-->
<!--s1.src='https://embed.tawk.to/55adba44929a943226544b9d/default';-->
<!--s1.charset='UTF-8';-->
<!--s1.setAttribute('crossorigin','*');-->
<!--s0.parentNode.insertBefore(s1,s0);-->
<!--})();-->
<!--//DOI MAU CHO THEME TAWK CHAT-->
<!--Tawk_API = $_Tawk_API || {};-->
<!--Tawk_API.onLoad = function(){-->
<!--    $("#tawkchat-maximized-iframe-element").contents().find("#formSubmit").css('background-color', '#363693');-->
<!--    $("#tawkchat-maximized-iframe-element").contents().find("#headerBoxWrapper").css('background-color', '#363693');-->
<!--    $("#tawkchat-maximized-iframe-element").contents().find("#headerBox").css('background-color', '#363693');-->
<!--    $("#tawkchat-minified-iframe-element-rectangle").contents().find("#tawkchat-minified-container").css('background-color', '#363693');-->
<!--};-->
<!---->
<!--var site_lang = '--><?php //echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?><!--';-->
<!--//</script>-->
<!--End of Tawk.to Script-->