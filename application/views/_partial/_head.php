<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"><![endif]-->
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="robots" content="index, follow">
    <title><?php echo $language['txt_title']; ?></title>
    <!-- Styles -->
    <link href="/assets/css/bootstrap.css?v=<?php echo $this->config->item('version_random'); ?>" rel="stylesheet">
    <link href="/assets/css/font-awesome-4.7.0/css/font-awesome.min.css?v=<?php echo $this->config->item('version_random'); ?>" rel="stylesheet">
    <link href="/assets/css/fileinput.min.css?v=<?php echo $this->config->item('version_random'); ?>" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css?v=<?php echo $this->config->item('version_random'); ?>" rel="stylesheet">
    <link href="/assets/css/style.css?v=<?php echo $this->config->item('version_random'); ?>" rel="stylesheet">
    <link href="/assets/js/plugin/spinner/Spinner.css?v=<?php echo $this->config->item('version_random'); ?>" rel="stylesheet">
    <link href="/assets/js/lib/loading/loading-script.css?v=<?php echo $this->config->item('version_random'); ?>" rel="stylesheet">
    <script src="/assets/js/lib/loading/loading-script.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
    <link href="/assets/css/css.css?v=<?php echo $this->config->item('version_random'); ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/uploadify/uploadify.css?v=<?php echo $this->config->item('version_random'); ?>">
    <!-- Favicon -->
    <link rel="icon", href="/assets/images/favicon.ico", type="image/x-icon", sizes="16x16" >
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="css/ie.css">
    <![endif]-->
    <!--[if lt IE 8]><html class="ie">
    <script> window.location = "notsupport.html"; </script>
    <![endif]-->

    <?php $this->load->view('_partial/_script'); ?>
</head>