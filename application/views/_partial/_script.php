<script type="text/javascript" src="/assets/js/lib/jquery.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/lib/moment.min.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/lib/bootstrap.min.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/fileinput.min.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/jquery.slimscroll.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/bootstrap-datepicker.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/select2/select2.min.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/select2/i18n/vi.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/jquery.dataTables.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/dataTables.bootstrap.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/spinner/Spinner.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/plugin/jquery.validate.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/script.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/assets/js/utils.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src='https://www.google.com/recaptcha/api.js?v=<?php echo $this->config->item('version_random'); ?>?hl=<?php echo $this->session->get_userdata()['site_lang'] == 'vietnamese' ? 'vi' : 'en' ?>'></script>
<script type="text/javascript" src="/assets/js/plugin/jquery-ui.js?v=<?php echo $this->config->item('version_random'); ?>"></script>
<script type="text/javascript" src="/uploadify/jquery.uploadify.min.js?v=<?php echo $this->config->item('version_random'); ?>"></script>