<!--footer-->
<!--<footer>-->
<!--    <section class="footer-links-section">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-xs-12 col-sm-6 col-md-3">-->
<!--                    <div class="footer-links-col">-->
<!--                        <h4 class="list-title-item">--><?php //echo $language['txt_footer_about_us']; ?><!--</h4>-->
<!--                        <ul class="list-group footer-links-list">-->
<!--                            <li>-->
<!--                                <a class="footer-logo" href="index.html">-->
<!--                                    <img src="/assets/images/logo-footer.png" alt="speedlink">-->
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <p>-->
<!--                                    --><?php //echo $language['txt_footer_note_slogan']; ?>
<!--                                </p>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-xs-12 col-sm-6 col-md-3">-->
<!--                    <div class="footer-links-col">-->
<!--                        <h4 class="list-title-item">--><?php //echo $language['txt_footer_navigation']; ?><!--</h4>-->
<!--                        <ul class="list-group footer-links-list">-->
<!--                            <li>-->
<!--                                <a href="/">--><?php //echo $language['txt_footer_navigation_home']; ?><!--</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="/">--><?php //echo $language['txt_footer_navigation_about_us']; ?><!--</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="/">--><?php //echo $language['txt_footer_navigation_shops']; ?><!--</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="/">--><?php //echo $language['txt_footer_navigation_service']; ?><!--</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="/">--><?php //echo $language['txt_footer_navigation_news']; ?><!--</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="/">--><?php //echo $language['txt_footer_navigation_contact_us']; ?><!--</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="/">--><?php //echo $language['txt_footer_navigation_vehicles_fleet']; ?><!--</a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-xs-12 col-sm-6 col-md-3">-->
<!--                    <div class="footer-links-col">-->
<!--                        <h4 class="list-title-item">--><?php //echo $language['txt_footer_transport_office']; ?><!--</h4>-->
<!--                        <ul class="list-group list-group-has-icon footer-links-list">-->
<!--                            <li>-->
<!--                                <p>-->
<!--                                    <i class="fa fa-map-marker" aria-hidden="true"></i>--><?php //echo $language['txt_footer_transport_office_note']; ?>
<!--                                </p>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="tel:19006411">-->
<!--                                    <i class="fa fa-phone" aria-hidden="true"></i>-->
<!--                                    --><?php //echo $language['txt_footer_transport_office_tel']; ?>
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="tel:+8408080809">-->
<!--                                    <i class="fa fa-fax" aria-hidden="true"></i>-->
<!--                                    --><?php //echo $language['txt_footer_transport_office_fax']; ?>
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="mailto:salesexpress-admin@speedlink.vn">-->
<!--                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>-->
<!--                                    --><?php //echo $language['txt_footer_transport_office_email']; ?>
<!--                                </a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="https://speedlink.vn/" target="_blank">-->
<!--                                    <i class="fa fa-globe" aria-hidden="true"></i>-->
<!--                                    --><?php //echo $language['txt_footer_transport_office_website']; ?>
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-xs-12 col-sm-6 col-md-3">-->
<!--                    <div class="footer-links-col">-->
<!--                        <h4 class="list-title-item">--><?php //echo $language['txt_footer_open_time']; ?><!--</h4>-->
<!--                        <ul class="list-group footer-links-list">-->
<!--                            <li>-->
<!--                                <p>-->
<!--                                    --><?php //echo $language['txt_footer_open_time_note']; ?>
<!--                                </p>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <p>-->
<!--                                    <span>--><?php //echo $language['txt_footer_open_day_m_f']; ?><!--</span>-->
<!--                                    <span>--><?php //echo $language['txt_footer_open_day_m_f_value']; ?><!--</span>-->
<!--                                </p>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <p>-->
<!--                                    <span>--><?php //echo $language['txt_footer_open_day_s']; ?><!--</span>-->
<!--                                    <span>--><?php //echo $language['txt_footer_open_day_s_value']; ?><!--</span>-->
<!--                                </p>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <a class="back-top-btn" href="#"></a>-->
<!--</footer>-->