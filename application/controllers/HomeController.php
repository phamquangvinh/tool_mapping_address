<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller
{
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        //MODEL

        //LIBRARY
        $this->load->library('callapi');
        $this->load->library('common');
        /* Load form validation library */
        $this->load->library('form_validation');

        //LANGUAGE
        $this->lang->load('message', $this->session->userdata('site_lang') ? $this->session->userdata('site_lang') : 'english');
        $this->data['menu'] = "home";
        $this->data["language"] = array_merge(
            $this->lang->line('language'),
            $this->lang->line('login'),
            $this->lang->line('menu'),
            $this->lang->line('home'),
            $this->lang->line('footer')
        );
    }

    public function index()
    {
        $this->data['module'] = "home/index";
        $this->data['base_url'] = $this->config->base_url();
        $user = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $time = time();
        $dataApi = array(
            'userId' => $user ? $user['id'] : '',
            'timestamp' => $time,
            'country' => 'VIETNAM'
        );
        $city = $this->callapi->call($urlApi . "getCity", $dataApi);
        $optionCityValueShipper = null;
        $optionCityValueReceiver = null;
        $optionDistrictValueShipper = null;
        $optionDistrictValueReceiver = null;
        $optionWardValueShipper = null;
        $optionWardValueReceiver = null;
        $str = '<option></option>';

        if ($_POST) {
            $dataApi = array(
                'userId' => $user ? $user['id'] : '',
                'payer_id' => $user ? $user['accounts_id'] : '',
                'timestamp' => time(),
                'is_cod' => !empty($this->input->post('cod_value')) ? 1 : 0,
                'is_dox' => $this->input->post('is_dox') == 1 ? 'dox' : 'non_dox',
                'is_ras' => $this->input->post('is_ras') == 1 ? 1 : 0,
                'cod_value' => $this->input->post('cod_value'),
                'weight' => $this->input->post('weight'),
                'dest_branch_code' => $this->input->post('receiver_city'),
                'origin_branch_code' => $this->input->post('shipper_city'),
                'shipper_city' => $this->input->post('shipper_city'),
                'receiver_city' => $this->input->post('receiver_city'),
                'pickup_date' => date('Y/m/d', time())
            );
            $dataApiShipper = array(
                'userId' => $user ? $user['id'] : '',
                'timestamp' => $time,
                'city' => $this->input->post('shipper_city')
            );
            $dataApiReceiver = array(
                'userId' => $user ? $user['id'] : '',
                'timestamp' => $time,
                'city' => $this->input->post('receiver_city')
            );
            $this->data['dataFilter'] = array(
                'is_cod' => !empty($this->input->post('cod_value')) ? 1 : 0,
                'is_non_dox' => $this->input->post('is_dox') == 0 ? 1 : 0,
                'is_dox' => $this->input->post('is_dox') == 1 ? 1 : 0,
                'is_ras' => $this->input->post('is_ras') == 1 ? 1 : 0,
                'cod_value' => $this->input->post('cod_value'),
                'weight' => $this->input->post('weight'),
                'shipper_city' => $this->input->post('shipper_city'),
                'receiver_city' => $this->input->post('receiver_city'),
                'is_ward_ras' => $this->input->post('is_ward_ras'),
            );
            $districtShipper = $this->callapi->call($urlApi . "getDistrict", $dataApiShipper);
            $districtReceiver = $this->callapi->call($urlApi . "getDistrict", $dataApiReceiver);
            $dataApiShipper['district'] = $this->input->post('shipper_district');
            $dataApiReceiver['district'] = $this->input->post('receiver_district');
            $wardShipper = $this->callapi->call($urlApi . "getWards", $dataApiShipper);
            $wardReceiver = $this->callapi->call($urlApi . "getWards", $dataApiReceiver);

            $optionCityValueShipper = $this->input->post('shipper_city');
            $optionCityValueReceiver = $this->input->post('receiver_city');
            $optionDistrictValueShipper = $this->input->post('shipper_district');
            $optionDistrictValueReceiver = $this->input->post('receiver_district');
            $optionWardValueShipper = $this->input->post('shipper_ward');
            $optionWardValueReceiver = $this->input->post('receiver_ward');
            if ($wardReceiver['status'] == 1) {
                foreach ($wardReceiver['data'] as $val) {
                    $selected = $val['id'] == $optionWardValueReceiver ? 'selected' : '';
                    $str .= '<option value="' . $val['id'] . '" data-is-ras="' . $val['is_ras'] . '" ' . $selected . '>' . $val['name'] . '</option>';
                }
            }
            $checkFee = $this->callapi->call($urlApi . "checkTransportationFee", $dataApi);
            $data = $checkFee['status'] == 1 ? $checkFee['data'] : array();
            $count_success = 0;
            for ($i = 0; $i < count($data); $i++) {
                if (!empty($data[$i]['msg'])) {
                    $data[$i]['search_fail'] = true;
                } else {
                    $data[$i]['search_fail'] = false;
                    $count_success++;
                }
                $data[$i]['price_total'] = ($data[$i]['price_total'] + $data[$i]['price_ecom']) * ((100 + $data[$i]['fuel_surcharge']) / 100);
            }
            $this->data['dataCheckFee'] = $data;
            $this->data['countSuccess'] = $count_success;
        }
        $optionCityShipper = $this->common->generateSelectOption($city['status'] == 1 ? $city['data'] : array(), array(), $optionCityValueShipper, 'id', 'name');
        $optionCityReceiver = $this->common->generateSelectOption($city['status'] == 1 ? $city['data'] : array(), array(), $optionCityValueReceiver, 'id', 'name');
        $optionDistrictShipper = $this->common->generateSelectOption($districtShipper['status'] == 1 ? $districtShipper['data'] : array(), array(), $optionDistrictValueShipper, 'id', 'name');
        $optionDistrictReceiver = $this->common->generateSelectOption($districtReceiver['status'] == 1 ? $districtReceiver['data'] : array(), array(), $optionDistrictValueReceiver, 'id', 'name');
        $optionWardShipper = $this->common->generateSelectOption($wardShipper['status'] == 1 ? $wardShipper['data'] : array(), array(), $optionWardValueShipper, 'id', 'name');

        $this->data['cityOptionShipper'] = $optionCityShipper;
        $this->data['cityOptionReceiver'] = $optionCityReceiver;

        $this->data['districtOptionShipper'] = $optionDistrictShipper;
        $this->data['districtOptionReceiver'] = $optionDistrictReceiver;
        $this->data['wardOptionShipper'] = $optionWardShipper;

        $this->data['wardOptionReceiver'] = $str;
        $this->load->view('index', $this->data);
    }

    function reroute404()
    {
        header('Location: /');
        die();
    }
}
