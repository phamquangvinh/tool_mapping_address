<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MappingController extends CI_Controller
{
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        //MODEL
        $this->load->model('mapping');
        //LIBRARY

        $this->load->library('callapi');
        $this->load->library('common');
        $this->load->library('dataconvert');
        $this->load->library('convertdatetime');
        //NhÃºng file PHPExcel
        $this->load->library('PHPExcel.php');

        //LANGUAGE
        $this->lang->load('message',$this->session->userdata('site_lang'));
        $this->data['menu'] = "mapping";
        $this->data["language"] = array_merge(
            $this->lang->line('language'),
            $this->lang->line('login'),
            $this->lang->line('menu'),
            $this->lang->line('home'),
            $this->lang->line('footer')
        );
    }

    public function loginPage() {
        $this->data['module'] = "mapping/index";
        $this->data['base_url'] = $this->config->base_url();
        $user = $this->session->userdata('login');
        if($user==null) {
            $this->load->view('index', $this->data);
        } else {
            redirect('mapping-detail');
        }
    }

    // IMPORT MAPPING DATA
    public function mappingImport()
    {
        $this->data['module']= "mapping/mapping-import";
        $this->data['base_url'] = $this->config->base_url();
        $this->load->view('index', $this->data);
    }
    public function importEx() {

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));

        $dataRecord = $this->mapping->get_records_page($start, $length);
        foreach($dataRecord as $r) {

            $data[] = array(
                'country_text' => $r['country_text'],
                'country_code' => $r['country_code'],
                'city_text' => $r['city_text'],
                'city_code' => $r['city_code'],
                'district_text' => $r['district_text'],
                'district_code' => $r['district_code'],
                'ward_text' => $r['ward_text'],
                'ward_code' => $r['ward_code'],
                'partner_address_code' => $r['partner_address_code']
            );
        }
        $total_records = $this->mapping->get_total_record();
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $total_records['num'],
            "recordsFiltered" => $total_records['num'],
            "data" => $data
        );

        echo json_encode($output);
    }
    public function Excel2TempData() {
        $user = $this->session->userdata('login');
        move_uploaded_file($_FILES['mapping_excel']['tmp_name'], FCPATH.'/uploads/import/'.$user['id'].$_FILES['mapping_excel']['name']);
        $filename = FCPATH.'/uploads/import/' . $user['id'].$_FILES['mapping_excel']['name'];
        $objFile = PHPExcel_IOFactory::identify($filename);
        $objData = PHPExcel_IOFactory::createReader($objFile);
        $objData->setReadDataOnly(true);
        $objPHPExcel = $objData->load($filename);
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $data_mapping = array();
        // LAY THONG TIN TU FILE EXCEL
        $count_row = 0;
        for($i=2;$i<=$highestRow;$i++)
        {
            $country_text = trim($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
            $country_code = trim($objWorksheet->getCellByColumnAndRow(1,$i)->getValue());
            $city_text = trim($objWorksheet->getCellByColumnAndRow(2,$i)->getValue());
            $city_code = trim($objWorksheet->getCellByColumnAndRow(3,$i)->getValue());
            $district_text = trim($objWorksheet->getCellByColumnAndRow(4,$i)->getValue());
            $district_code = trim($objWorksheet->getCellByColumnAndRow(5,$i)->getValue());
            $ward_text = trim($objWorksheet->getCellByColumnAndRow(6,$i)->getValue());
            $ward_code = trim($objWorksheet->getCellByColumnAndRow(7,$i)->getValue());
            $partner_address_code = trim($objWorksheet->getCellByColumnAndRow(8,$i)->getValue());
            $count_row++;
            if($country_text != '' || $city_text != '' || $district_text != '' || $ward_text != '' || $partner_address_code != '') {
                $field_data = array(
                    'country_text'=>$country_text,
                    'country_code'=>$country_code,
                    'city_text'=>$city_text,
                    'city_code'=>$city_code,
                    'district_text'=>$district_text,
                    'district_code'=>$district_code,
                    'ward_text'=>$ward_text,
                    'ward_code'=>$ward_code,
                    'partner_address_code'=>$partner_address_code);
                array_push($data_mapping,$field_data);
            }
            if($count_row > 20 ) {
                $this->db->insert_batch('mapping_import_excel', $data_mapping);
                $count_row = 0;
                $data_mapping = array();
            }
            if($i == $highestRow) {
                $this->db->insert_batch('mapping_import_excel', $data_mapping);
                $count_row = 0;
                $data_mapping = array();
            }
        }
        unlink($filename);
    }

    public function saveData() {
        $user = $this->session->userdata('login');
        $data_import = $this->mapping->get_import_data();
        $data_insert = array();
        $count = 0;
        for ($i = 0; $i < count($data_import); $i++) {
            $data = array(
                'country_text'=>$data_import[$i]['country_text'],
                'country_code'=>$data_import[$i]['country_code'],
                'city_text'=>$data_import[$i]['city_text'],
                'city_code'=>$data_import[$i]['city_code'],
                'district_text'=>$data_import[$i]['district_text'],
                'district_code'=>$data_import[$i]['district_code'],
                'ward_text'=>$data_import[$i]['ward_text'],
                'ward_code'=>$data_import[$i]['ward_code'],
                'partner'=>$user['name']
            );
            array_push($data_insert, $data);
            $count++;
            if($count > 20 ) {
                $this->db->insert_batch('mapping_data_address_for_partner', $data_insert);
                $count = 0;
                $data_insert = array();
            }
            if($i == (count($data_import) - 1)) {
                $this->db->insert_batch('mapping_data_address_for_partner', $data_insert);
                $count = 0;
                $data_insert = array();
            }
        }
        $this->db->query("DELETE FROM `mapping_import_excel`");
    }

    // MAPPING DETAIL
    public function getMappingList() {
        $data_hubcode = $this->mapping->getMappingList();
        $count = count($data_hubcode);
        for($i = 0; $i < $count; $i++) {
            $branch_hub_code = $this->mapping->getBranchAndHubCode($data_hubcode[$i]['speedlink_address_code']);
            if ($branch_hub_code == null) {
                $data_hubcode[$i]['branch_code'] = null;
                $data_hubcode[$i]['hubcode'] = null;
            } else {
                $data_hubcode[$i]['branch_code'] = $branch_hub_code['branch_code'];
                $data_hubcode[$i]['hubcode'] = $branch_hub_code['hubcode'];
            }
        }
        $data = array('data'=>$data_hubcode);
        echo json_encode($data);
    }


    public function mappingDetail() {
        $this->data['module']= "mapping/mapping-detail";
        $this->data['base_url'] = $this->config->base_url();
        $user = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $time = time();
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => $time,
            'account_id'=>$user['accounts_id'],
            'country' => 'VIETNAM'
        );
        $account = $this->callapi->call($urlApi."getAddressDefaultForCustomerPortal",$dataApi);
        $dataAccount = $account['status'] == 1 ? $account['data'] : array();
        $this->data['dataAccount'] = $dataAccount;
        $country = $this->callapi->call($urlApi."getCountryForCreateBooking",$dataApi);
        $city = $this->callapi->call($urlApi."getCityForCreateBooking",$dataApi);
        $countryOption = $this->common->generateSelectOption($country['status'] == 1 ?$country['data']:array(),array(),null,'country_code','country');
        $cityOption = $this->common->generateSelectOption($city['status'] == 1 ?$city['data']:array(),array(),null,'province_code','city');
        $listPartner = $this->getListPartner();
        $this->data['listPartner'] = $listPartner;
        $this->data['countryOption'] = $countryOption;
        $this->data['cityOption'] = $cityOption;
        $this->load->view('index', $this->data);
    }

    public function getCode() {
        $country_text = $this->input->post('country');
        $city_text = $this->input->post('city');
        $district_text = $this->input->post('district');
        $ward_text = $this->input->post('ward');
        $partner_name = $this->input->post('partner');

        // SUGGEST CODE
        $country_part = "VN";
        $city_part = $this->mapping->suggestCityPart($country_text, $city_text);
        $district_part = $this->mapping->suggestDistrictPart($country_text, $city_text, $district_text);
        $ward_part = $this->mapping->suggestWardPart($country_text, $city_text, $district_text, $ward_text);
        $suggest_code = $country_part;
        if($city_part['status'] == 0) {
            $suggest_code .= $city_part['city_pattern'];
        } else {
            $city_part['city_pattern'] = sprintf('%1$03d', ++$city_part['city_pattern']);
            $suggest_code .= $city_part['city_pattern'];
        }
        if($district_part['status'] == 0) {
            $suggest_code .= $district_part['district_pattern'];
        } else {
            $district_part['district_pattern'] = sprintf('%1$03d', ++$district_part['district_pattern']);
            $suggest_code .= $district_part['district_pattern'];
        }
        if($ward_part['status'] == 0) {
            $suggest_code .= $ward_part['ward_pattern'];
        } else {
            $ward_part['ward_pattern'] = sprintf('%1$03d', ++$ward_part['ward_pattern']);
            $suggest_code .= $ward_part['ward_pattern'];
        }
        // CHECK EXIST
        $check = $this->mapping->checkExistCodePartner($partner_name, $suggest_code);
        if($check == false) {
            $hint = 1;
        } else {
            $hint = 0;
        }
    
        // BRANCH AND HUB CODE
        $branch_and_hub = $this->mapping->getBranchAndHubCode($suggest_code);
        $branch_code = $branch_and_hub['branch_code'];
        $hub_code = $branch_and_hub['hubcode'];
        // ZIP CODE
        $zip_code = $this->mapping->suggestZipCode($suggest_code);
        // CHECK IF NEW DATA
        if($zip_code != null) {
            $new_data = 1;
        } else {
            $new_data = 0;
        }
        $data = array('address_code' => $suggest_code, 'hint' => $hint, 'branch_code' => $branch_code, 'hub_code' => $hub_code, 'zip_code'=>$zip_code['zip_code'], 'new_data'=> $new_data);
        echo json_encode($data);
    }

    public function getFieldData(){
        $user = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $time = time();
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => $time,
            'account_id'=>$user['accounts_id'],
            'country' => 'VIETNAM'
        );
        $data = $this->input->post('parameter');
        $data_code = $this->mapping->getPartnerField($data[2], $data[3], $data[4], $data[5]);
        $suggest_code = '';

        if(empty($data_code) || $data_code[0]['code'] == null) {
            // SUGGEST SPEEDLINK ADDRESS CODE
            $country_part = "VN";
            $city_part = $this->mapping->suggestCityPart($data[2], $data[3]);
            $district_part = $this->mapping->suggestDistrictPart($data[2], $data[3], $data[4]);
            $ward_part = $this->mapping->suggestWardPart($data[2], $data[3], $data[4], $data[5]);
            $suggest_code .= $country_part;
            // CITY PATTERN
            if($city_part['status'] == 0) {
                $suggest_code .= $city_part['city_pattern'];
            } else {
                $city_part['city_pattern'] = sprintf('%1$03d', ++$city_part['city_pattern']);
                $suggest_code .= $city_part['city_pattern'];
            }
            // DISTRICT PATTERN
            if($district_part['status'] == 0) {
                $suggest_code .= $district_part['district_pattern'];
            } else {
                $district_part['district_pattern'] = sprintf('%1$03d', ++$district_part['district_pattern']);
                $suggest_code .= $district_part['district_pattern'];
            }
            // WARD PATTERN
            if($ward_part['status'] == 0) {
                $suggest_code .= $ward_part['ward_pattern'];
            } else {
                $ward_part['ward_pattern'] = sprintf('%1$03d', ++$ward_part['ward_pattern']);
                $suggest_code .= $ward_part['ward_pattern'];
            }
            $data_code[0]['code'] = $suggest_code;
        }
        echo json_encode($data_code);
    }

    public function updatedMapping()
    {
        $country_text = $this->input->post('country');
        $city_text = $this->input->post('city');
        $district_text = $this->input->post('district');
        $ward_text = $this->input->post('ward');
        $address_code = $this->input->post('address_code');
        $partner_name = $this->input->post('partner');

        $country_check = $this->input->post('country_select');
        $city_check = $this->input->post('city_select');
        $district_check = $this->input->post('district_select');
        $ward_check = $this->input->post('ward_select');
        $status = 'match';

        $update_query = "UPDATE `mapping_data_address_for_partner` SET country_text = '".$country_text."', city_text = '".$city_text."', district_text = '".$district_text."', ward_text = '".$ward_text."', speedlink_address_code = '".$address_code."', note = NULL, status = '".$status."'
        WHERE country_text LIKE '%".$country_check."' AND city_text LIKE '%".$city_check."' AND `district_text` LIKE '%".$district_check."' AND  ward_text LIKE '%".$ward_check."' AND partner = '".$partner_name."'";
        $this->db->query($update_query);
        echo json_encode("Success");
    }

    public function saveEditAddress()
    {
        // INPUT DATA
        $country_text = $this->input->post('country');
        $city_text = $this->input->post('city');
        $district_text = $this->input->post('district');
        $ward_text = $this->input->post('ward');
        $address_code = $this->input->post('address_code');
        $branch = $this->input->post('branch_code');
        $zipcode = $this->input->post('zip_code');
        $hub = $this->input->post('hub_code');

        // CHECK IF NEW DATA INPUT
        $check = $this->mapping->suggestZipCode($address_code);
        if($check != null) {
            $new_data = 0;
        } else {
            $new_data = 1;
        }
        // GET COUNTRY_CODE
        $country_code = ($this->mapping->getCountryCode($country_text))['id'];
        // CITY_CODE
        $city_code = ($this->mapping->getProvinceCode($city_text))['id'];
        // DISTRICT_CODE
        $district_code = ($this->mapping->getDistrictCode($city_code, $district_text))['id'];
        // WARD_CODE
        $ward_code = ($this->mapping->getWardCode($district_code, $ward_text))['id'];
        // BRANCH_CODE
        $branch_code = ($this->mapping->getBranchCode($branch))['id'];
        // HUB_CODE
        $hub_code = ($this->mapping->getHubCode($hub))['id'];
        if($new_data == 0)
        {
            $update_query = "UPDATE `speedlink_address_code_copy` SET zip_code = '".$zipcode."', branch_code = '".$branch_code."', hub_code = '".$hub_code."'
                            WHERE `code` = '".$address_code."'";
            $this->db->query($update_query);
            echo json_encode("Edit Success!");
        } else {
            $insert_data = array('code'=>$address_code,
                'zip_code'=>$zipcode,
                'country'=>$country_text,
                'province'=>$city_text,
                'district'=>$district_text,
                'ward'=>$ward_text,
                'country_code'=>$country_code,
                'province_code'=>$city_code,
                'district_code'=>$district_code,
                'ward_code'=>$ward_code,
                'branch_code'=>$branch_code,
                'hub_code'=>$hub_code
            );
            $this->db->insert('speedlink_address_code_copy',$insert_data);
            echo json_encode("Create Success!");
        }

    }
    public function getCodeEdit()
    {
        $country_text = $this->input->post('country');
        $city_text = $this->input->post('city');
        $district_text = $this->input->post('district');
        $ward_text = $this->input->post('ward');

        // SUGGEST CODE
        $country_part = "VN";
        $city_part = $this->mapping->suggestCityPart($country_text, $city_text);
        $district_part = $this->mapping->suggestDistrictPart($country_text, $city_text, $district_text);
        $ward_part = $this->mapping->suggestWardPart($country_text, $city_text, $district_text, $ward_text);
        $suggest_code = $country_part;
        if($city_part['status'] == 0) {
            $suggest_code .= $city_part['city_pattern'];
        } else {
            $city_part['city_pattern'] = sprintf('%1$03d', ++$city_part['city_pattern']);
            $suggest_code .= $city_part['city_pattern'];
        }
        if($district_part['status'] == 0) {
            $suggest_code .= $district_part['district_pattern'];
        } else {
            $district_part['district_pattern'] = sprintf('%1$03d', ++$district_part['district_pattern']);
            $suggest_code .= $district_part['district_pattern'];
        }
        if($ward_part['status'] == 0) {
            $suggest_code .= $ward_part['ward_pattern'];
        } else {
            $ward_part['ward_pattern'] = sprintf('%1$03d', ++$ward_part['ward_pattern']);
            $suggest_code .= $ward_part['ward_pattern'];
        }
        // BRANCH AND HUB CODE
        $branch_and_hub = $this->mapping->getBranchAndHubCode($suggest_code);
        $branch_code = $branch_and_hub['branch_code'];
        $hub_code = $branch_and_hub['hubcode'];
        // ZIP CODE
        $zip_code = $this->mapping->suggestZipCode($suggest_code);
        // CHECK IF NEW DATA
        if($zip_code != null) {
            $new_data = 0;
        } else {
            $new_data = 1;
        }
        $data = array('address_code' => $suggest_code, 'branch_code' => $branch_code, 'hub_code' => $hub_code, 'zip_code'=>$zip_code['zip_code'], 'new_data'=> $new_data);
        echo json_encode($data);
    }

    public function getListBranch()
    {
        $list = $this->mapping->getListBranch();
        $optionBranch = $this->common->generateSelectOption($list,array(),null,'id','branch_code');
        $result = array(
            'status' => true,
            'message' => 'Success',
            'data'=>$optionBranch
        );
        echo json_encode($result);die();
    }

    public function getHubByBranch() {
        $branch_code = $this->input->post('branch');
        $hub_code = $this->mapping->getHubByBranch($branch_code);
        echo json_encode($hub_code);
    }

    public function getListPartner() {
        $result = '<option></option>';
        $listPartner = $this->mapping->getListPartner();
        if(count($listPartner) > 0) {
            foreach($listPartner as $item) {
                $select = $item['partner'];
                $result .= '<option value="'.$select.'">'.$select.'</option>';
            }
        }
        return $result;
    }

    public function createXLSXForPartner($partner) {
            // create file name
            $fileName = $partner.'-MappingData';
            // load excel library
            $empInfo = $this->mapping->mappingListPartner($partner);
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            // set Header
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Country Text');
            $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Country Code');
            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'City Text');
            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'City Code');
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'District Text');
            $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'District Code');
            $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Ward Text');
            $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Ward Code');
            $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Speedlink Address Code');
            $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Note');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Status');
        $rowCount = 2;
            foreach ($empInfo as $element) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['country_text']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['country_code']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['city_text']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['city_code']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['district_text']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['district_code']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['ward_text']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['ward_code']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['speedlink_address_code']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['note']);
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['status']);
                $rowCount++;
            }
            $currentRow = $rowCount;
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=".$fileName.".xlsx");  //File name extension was wrong
            header('Cache-Control: max-age=0');
            $objWriter->save(  'php://output');
    }

    // MAPPING MANAGEMENT
    public function mappingManagement(){
        $this->data['menu'] = "manage";
        $this->data['module']= "manage/mapping-management";
        $this->data['base_url'] = $this->config->base_url();

        $user = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $time = time();
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => $time,
            'account_id'=>$user['accounts_id'],
            'country' => 'VIETNAM'
        );
        $account = $this->callapi->call($urlApi."getAddressDefaultForCustomerPortal",$dataApi);
        $dataAccount = $account['status'] == 1 ? $account['data'] : array();
        $this->data['dataAccount'] = $dataAccount;
        $country = $this->callapi->call($urlApi."getCountryForCreateBooking",$dataApi);
        $city = $this->callapi->call($urlApi."getCityForCreateBooking",$dataApi);
        $countryOption = $this->common->generateSelectOption($country['status'] == 1 ?$country['data']:array(),array(),null,'country_code','country');
        $cityOption = $this->common->generateSelectOption($city['status'] == 1 ?$city['data']:array(),array(),null,'province_code','city');
        $this->data['countryOption'] = $countryOption;
        $this->data['cityOption'] = $cityOption;
        $this->load->view('index', $this->data);
    }

    public function createXLSXForSpeedlink() {
        $list = $this->mapping->speedlinkCodeList();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Code');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Zipcode');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Country');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'City');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'District');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Ward');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Country Code');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'City Code');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'District Code');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Ward Code');
        $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Branch Code');
        $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Hub Code');

        $rowCount = 2;
        foreach ($list as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['zip_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['country']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['province']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['district']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['ward']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['country_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['province_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['district_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['ward_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['branch_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['hub_code']);
            $rowCount++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=SpeedlinkCode.xlsx");
        header('Cache-Control: max-age=0');
        $objWriter->save(  'php://output');
    }

}
