<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//Load composer's autoloader
require 'vendor/autoload.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class FeedBackController extends CI_Controller {
	private $data = array();
	public function __construct(){
		parent::__construct();
		//MODEL

		//LIBRARY
		$this->load->library('callapi');		
		$this->load->library('common');
		$this->load->library('convertdatetime');
		$this->load->library('dataconvert');

		//LANGUAGE
		$this->lang->load('message',$this->session->userdata('site_lang'));
		$this->data['menu'] = "feed-back";
		$this->data["language"] = array_merge(
			$this->lang->line('language'),
			$this->lang->line('login'),
			$this->lang->line('menu'),
			$this->lang->line('home'),
			$this->lang->line('footer'),
			$this->lang->line('feed-back')
		);
	}
	public function index(){
		$this->data['module'] = "feedback/feed-back-list";
		$this->data['base_url'] = $this->config->base_url();
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'accounts_id' => $user?$user['accounts_id']:'',
			'timestamp' => time()
		);
		$array_status = array(
			array(
				'status' => 'Pending Input',
				'value' => 'Pending Input'
			),
			array(
				'status' => 'New',
				'value' => 'New'
			),
			array(
				'status' => 'Closed',
				'value' => 'Closed'
			),
		);

		if ($_POST) {
			$dateRangeValue = $this->input->post('date_range');
			$dataApi['status'] = $this->input->post('status');
			$dataApi['order_no'] = $this->input->post('order_no');
			$dataApi['from_date'] = date('Y-m-d 00:00:01', strtotime($this->dataconvert->convertDateDB($this->input->post('from_date'))));
			$dataApi['to_date'] = date('Y-m-d 23:59:50', strtotime($this->dataconvert->convertDateDB($this->input->post('to_date'))));

			$this->data['dataFilter']['order_no'] = $this->input->post('order_no');
			$this->data['dataFilter']['from_date'] = $this->input->post('from_date');
			$this->data['dataFilter']['to_date'] = $this->input->post('to_date');
			$statusValue = $this->input->post('status');
		}else{
			$statusValue = null;
			$dateRangeValue = 'Last_7_Days';
			$dataApi['from_date'] = date('Y-m-d 00:00:01', strtotime('-8 days',time()));
			$dataApi['to_date'] = date('Y-m-d 23:59:50',strtotime('-1 days',time()));
		}
		
		$data = $this->callapi->call($urlApi."getListCase",$dataApi);
		$dataTable = $data['status'] == 1 ? $data['data'] : array();
		for($i = 0 ; $i < count($dataTable) ; $i++){
			$dataTable[$i]['no'] = $i + 1;
			$dataTable[$i]['title'] = '<a href="#" data-toggle="modal" data-target=".feedback-detail-modal" data-detail_feedback="'.$dataTable[$i]['id'].'">'.$dataTable[$i]['name'].'</a>';
			$dataTable[$i]['order_no'] = $dataTable[$i]['order_id'];
			$dataTable[$i]['date_entered'] = ConvertDateTime::convertDateTimeDisplay($dataTable[$i]['date_entered']);
			$dataTable[$i]['date_modified'] = ConvertDateTime::convertDateTimeDisplay($dataTable[$i]['date_modified']);
			if ($dataTable[$i]['status'] == 'New') {
				$dataTable[$i]['button'] = '<a class="btn-add" href="javascript:void(0);" onclick="deleteFeedback(this)"  data-id="'.$dataTable[$i]['id'].'"><i class="fa fa-trash-o"></i></a>';
			}else{
				$dataTable[$i]['button'] = '';
			}
		}


		$list_order = $this->callapi->call($urlApi."getListOrderOfAccount",$dataApi);
		$result_list_order = $list_order['status'] == 1 ? $list_order['data'] : array();

		$this->data['dataFilter']['listOrderOption'] = $this->common->generateSelectOption(
			$result_list_order, array(), null, 'order_id', 'order_id'
		);
		$this->data['dataFilter']['statusOption'] = $this->common->generateSelectOption(
			$array_status, array(), $statusValue, 'value', 'status'
		);
		$this->data['dataFilter']['dateRangeOption'] = $this->common->generateOptionRangeDatetimeSearch($dateRangeValue,$this->session->get_userdata()['site_lang']);
		$this->data['dataTable'] = base64_encode(json_encode($dataTable));
		$this->load->view('index', $this->data);
	}
	public function getDetailFeedBack(){
		$user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'feedback_id' => $this->input->post('feedback_id'),            
			'accounts_id' => $user?$user['accounts_id']:'',
        );
        $detail_feedback = $this->callapi->call($urlApi."getListCase",$dataApi);
        $detail_feedback_data = $detail_feedback['status'] == 1 ? $detail_feedback['data'][0] : array();

		//current date
	    $timestamp = strtotime($detail_feedback_data['date_entered']);
    	$tNew = $timestamp + 7*60*60;
        $detail_feedback_data['date_entered'] = date('d/m/Y h:i A', $tNew);
        // $detail_feedback_data['date_entered'] = date('d/m/Y h:i A',strtotime($detail_feedback_data['date_entered']));
        
        echo json_encode($detail_feedback_data);
	}
	public function deleteFeedback(){
		$user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'feedback_id' => $this->input->post('feedback_id'),            
			'accounts_id' => $user?$user['accounts_id']:'',
        );
        $result = $this->callapi->call($urlApi."deleteFeedback",$dataApi);
        if ($result['status'] == 1) {
        	echo json_encode('Deleted Success.');
        }else{
        	echo json_encode('Can not deleted.');
        }
	}
	public function CreateFeedback(){
		$user   = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		
		if($this->input->post('title') == '') {
			die(json_encode(['err' => true, 'msg' =>  $this->data['language']['txt_feed_back_create_title_required']]));
		}else if($this->input->post('order_no') == '') {
			die(json_encode(['err' => true, 'msg' =>  $this->data['language']['txt_feed_back_create_orderno_required']]));
		}else if($this->input->post('description') == '') {
			die(json_encode(['err' => true, 'msg' =>  $this->data['language']['txt_feed_back_create_description_required']]));
		}
		//current date
		$time = date('Y-m-d H:i:s');
	    $timestamp = strtotime($time);
    	$tNew = $timestamp - 7*60*60;
        $parameters = array(        	
            'name' 				=> $this->input->post('title'),
            'order_id' 			=> $this->input->post('order_no'),
            'description' 		=> $this->input->post('description'),
            'date_entered' 		=> date('Y-m-d H:i:s', $tNew),
            'date_modified' 	=> date('Y-m-d H:i:s', $tNew),
            'account_id' 		=> $user?$user['accounts_id']:'',
            'created_by' 		=> $user?$user['accounts_id']:'',
            'status' 			=> 'New',
            'priority' 			=> 'P1',
            'assigned_user_id' 	=> 'cc3bfa5e-5671-236b-7f76-5445a0f93471',
            'deleted' 			=> 0
        );        
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),         
			'accounts_id' => $user?$user['accounts_id']:'',
			'parameters' => $parameters
        );	
        $resultApi = $this->callapi->call($urlApi."createFeedback",$dataApi);
		if($resultApi['status'] != 1) {
			die(json_encode(['err' => true, 'msg' =>  $this->data['language']['txt_feed_back_create_fail']]));
		} 
		else{
			if ($this->config->item('enviroment_staging')) {
				$dataEmails = array(
					array(
						'name' => 'Sjn',
						'email' => 'dieter.tuan@itlvn.com',
					)
    			);
			}else{
				$dataEmails = array(
					array(
						'name' => 'Csexpress HCM',
						'email' => 'csexpress-hcm@speedlink.vn',
					)
    			);	
			}
        	
        	//Get customer (name in accounts table)
        	$customer = '';
        	if($user && $user['accounts_name'] != '') {
        		$customer = $user['accounts_name'];
        	}else {
        		$customer = $user['first_name'].$user['last_name'];
        	}
			foreach($dataEmails as $e) {
	        	$dataApiSendEmailCS = array(
					'userId' => $user?$user['id']:'',
					'timestamp' => time(),
		    		'subject' => 'New Feedback',
		    		'email' => $e,
		    		'customer' => $customer,
		    		'order_no' => $this->input->post('order_no'),
		    		'title' => $this->input->post('title'),
		    		'description' => $this->input->post('description')
				);
				$ts = $this->callapi->call($urlApi."sendEmailNewFeedBack",$dataApiSendEmailCS);	
			}
		}
		die(json_encode(['err' => false, 'msg' => $this->data['language']['txt_feed_back_create_success']]));
	}
	public function updateFeedback(){
		$user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $parameters = array(        	
            'name' 				=> $this->input->post('title'),
            'order_id' 			=> $this->input->post('order_no'),
            'description' 		=> $this->input->post('description'),
            'date_modified' 	=> date('Y-m-d h:i:s')
        );        
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),         
			'accounts_id' => $user?$user['accounts_id']:'',
			'parameters' => $parameters,
			'feedback_id' => $this->input->post('feedback_id') 
        );
        $resultApi = $this->callapi->call($urlApi."updateFeedback",$dataApi);
        if($resultApi['status'] != 1) {
            die(json_encode($this->data['language']['txt_feed_back_update_fail']));
        }
        else{
            if ($this->config->item('enviroment_staging')) {
				$dataEmails = array(
					array(
						'name' => 'Sjn',
						'email' => 'dieter.tuan@itlvn.com',
					)
    			);
			}else{
				$dataEmails = array(
					array(
						'name' => 'Csexpress HCM',
						'email' => 'csexpress-hcm@speedlink.vn',
					)
    			);	
			}
            //Get customer (name in accounts table)
            $customer = '';
            if($user && $user['accounts_name'] != '') {
                $customer = $user['accounts_name'];
            }else {
                $customer = $user['first_name'].$user['last_name'];
            }
            foreach($dataEmails as $e) {
                $dataApiSendEmailCS = array(
                    'userId' => $user?$user['id']:'',
                    'timestamp' => time(),
                    'subject' => 'Update Feedback',
                    'email' => $e,
                    'customer' => $customer,
                    'order_no' => $this->input->post('order_no'),
                    'title' => $this->input->post('title'),
                    'description' => $this->input->post('description')

                );
                $ts = $this->callapi->call($urlApi."sendEmailNewFeedBack",$dataApiSendEmailCS);
            }
        }
    	echo json_encode($resultApi['message']);
	}
}
