<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require 'vendor/autoload.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {
	private $data = array();
	public function __construct(){
		parent::__construct();
		//MODEL

		//LIBRARY
		$this->load->library('callapi');
		$this->load->library('common');
		/* Load form validation library */
		$this->load->library('form_validation');
		$this->load->library('convertdatetime');
		$this->load->library('PHPExcel');

		//LANGUAGE
		$this->lang->load('message',$this->session->userdata('site_lang'));
		$this->data["language"] = array_merge(
			$this->lang->line('language'),
			$this->lang->line('login'),
			$this->lang->line('menu'),
			$this->lang->line('home'),
			$this->lang->line('footer'),
			$this->lang->line('booking'),
			$this->lang->line('user')
		);
	}
	public function profile(){
		$this->data['module'] = "user/profile";
		$this->data['menu'] = "user-profile";
		$this->data['base_url'] = $this->config->base_url();
		$language = $this->session->userdata('site_lang');
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'accountId' => $user?$user['accounts_id']:'',
			'timestamp' => $time
		);
		$user = $this->callapi->call($urlApi."getUserProfile",$dataApi);
		$this->data['dataShow'] = $user['status'] == 1 ?$user['data'][0]:array();
		$this->data['dataShow']['accounts_payment_method'] = $this->common->paymentMethod[$this->data['dataShow']['accounts_payment_method']][$language == 'vietnamese'?'name_vn':'name'];
		$this->data['dataShow']['lead_payment_method'] = $this->common->paymentMethod[$this->data['dataShow']['lead_payment_method']][$language == 'vietnamese'?'name_vn':'name'];
		// $userSub = $this->callapi->call($urlApi."getUserAddress",$dataApi);
		// $this->data['dataShowSub'] = $userSub['status'] == 1 ?$userSub['data']:array();

		$country = $this->callapi->call($urlApi."getCountryForCreateBooking",$dataApi);
		$countryOption = $this->common->generateSelectOption($country['status'] == 1 ?$country['data']:array(),array(),null,'country_code','country');
		$this->data['countryOption'] = $countryOption;

		$this->load->view('index', $this->data);
	}
	public function profileEdit(){
		$this->data['module'] = "user/profile-edit";
		$this->data['menu'] = "user-edit";
		$this->data['base_url'] = $this->config->base_url();

		/* Set validation rule for name field in the form */
		$this->form_validation->set_rules('company_name', $this->data['language']['txt_user_edit_company'], 'required');
		$this->form_validation->set_rules('tax_code', $this->data["language"]['txt_user_edit_tax_code'], 'required');
		$this->form_validation->set_rules('phone', $this->data["language"]['txt_user_edit_phone'], 'required|min_length[10]|max_length[11]');
		//$this->form_validation->set_rules('email', $this->data["language"]['txt_user_edit_email'], 'required');
//		$this->form_validation->set_rules('payment_method', $this->data["language"]['txt_user_edit_payment_method'], 'required');
		$this->form_validation->set_rules('country', $this->data["language"]['txt_user_edit_country'], 'required');
		$this->form_validation->set_rules('city', $this->data["language"]['txt_user_edit_city'], 'required');
		//$this->form_validation->set_rules('district', $this->data["language"]['txt_user_edit_district'], 'required');
		$this->form_validation->set_rules('address', $this->data["language"]['txt_user_edit_address'], 'required');
		$this->form_validation->set_rules('bank_name', $this->data["language"]['txt_user_edit_bank_name'], 'required');
		$this->form_validation->set_rules('bank_account_no', $this->data["language"]['txt_user_edit_bank_account_no'], 'required');
		$this->form_validation->set_rules('bank_number', $this->data["language"]['txt_user_edit_bank_number'], 'required');

        $this->form_validation->set_message('min_length',  $this->data['language']['field_require_phone_length']);
        $this->form_validation->set_message('max_length',  $this->data['language']['field_require_phone_length']);
		$this->form_validation->set_message('required',  $this->data['language']['field_require']);

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'accountId' => $user?$user['accounts_id']:'',
			'timestamp' => $time
		);
		$user = $this->callapi->call($urlApi."getUserProfile",$dataApi);
		$dataUser = $user['status'] == 1 ?$user['data'][0]:array();
		$call_back = function() use ($dataUser,$time,$urlApi) {
			$this->data['dataShow'] = $dataUser;
			if($_POST){
				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_company_name' : 'lead_company_name'] = $this->input->post('company_name');
				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_tax_code' : 'lead_tax_code'] = $this->input->post('tax_code');
				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_phone' : 'lead_phone'] = $this->input->post('phone');
//				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_email_address' : 'lead_email_address'] = $this->input->post('email');
				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_street_name' : 'lead_street_name'] = $this->input->post('address');
				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_bank_name' : 'lead_bank_name'] = $this->input->post('bank_name');
				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_bank_account_name' : 'lead_bank_account_name'] = $this->input->post('bank_account_no');
				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_bank_number' : 'lead_bank_number'] = $this->input->post('bank_number');
				$this->data['dataShow'][$dataUser['accounts_id'] ? 'account_description' : 'lead_description'] = $this->input->post('description');
				$this->data['dataShow']['avatar'] = $this->input->post('avatar');

//				$paymentMethod = $this->input->post('payment_method');
				$countryId = $this->input->post('country');
				$cityId = $this->input->post('city');
				$districtId = $this->input->post('district');
			}else {
//				$paymentMethod = $dataUser['accounts_id'] ? $dataUser['accounts_payment_method'] : $dataUser['lead_payment_method'];
				$countryId = $dataUser ? ($dataUser['accounts_id'] ? $dataUser['account_country_id'] : $dataUser['lead_country_id']) : '';
				$cityId = $dataUser ? ($dataUser['accounts_id'] ? $dataUser['account_city_id'] : $dataUser['lead_city_id']) : '';
				$districtId = $dataUser ? ($dataUser['accounts_id'] ? $dataUser['account_district_id'] : $dataUser['lead_district_id']) : '';
                $this->data['dataShow']['avatar_user'] = $dataUser['avatar'];
			}
			//payment method
//			$this->data['dataShow']['payment_method_option'] = $this->common->generateSelectOptionPaymentMethod(
//				$paymentMethod, $this->session->userdata('site_lang')
//			);

			$dataApi = array(
				'userId' => $dataUser ? $dataUser['id'] : '',
				'timestamp' => $time
			);
			//quoc gia
			$country = $this->callapi->call($urlApi . "getCountry", $dataApi);
			$dataCountry = $country['status'] == 1 ? $country['data'] : array();

			$this->data['dataShow']['optionCountry'] = $this->common->generateSelectOption(
				$dataCountry, array(), $countryId, 'id', ($this->session->userdata('site_lang') == 'vietnamese' ? 'name_vn' : 'name')
			);
			//thanh pho
			$dataApi['country'] = $countryId;
			$city = $this->callapi->call($urlApi . "getCity", $dataApi);
			$dataCity = $city['status'] == 1 ? $city['data'] : array();

			$this->data['dataShow']['optionCity'] = $this->common->generateSelectOption(
				$dataCity, array(), $cityId, 'id', 'name'
			);
			//quan huyen
			$dataApi['city'] = $cityId;
			$district = $this->callapi->call($urlApi . "getDistrict", $dataApi);
			$dataDistrict = $district['status'] == 1 ? $district['data'] : array();
			$this->data['dataShow']['optionDistrict'] = $this->common->generateSelectOption(
				$dataDistrict, array(), $districtId, 'id', 'name'
			);
			//phuong xa
//			$dataApi['district'] = $districtId;
//			$ward = $this->callapi->call($urlApi . "getWards", $dataApi);
//			$dataWard = $ward['status'] == 1 ? $ward['data'] : array();
//
//			$wardId = $dataUser?($dataUser['accounts_id']?$dataUser['account_district_id']:$dataUser['lead_district_id']):'';
//			$this->data['dataShow']['optionWard'] = $this->common->generateSelectOption(
//				$dataDistrict, array(), null, 'id', 'name'
//			);

			$this->load->view('index', $this->data);
		};
		if ($this->form_validation->run() == FALSE) {
			$call_back();
		}else {
            $file_ext = substr($_FILES["avatar"]["name"], strripos($_FILES["avatar"]["name"], '.')); // get file name
            $target_file = 'uploads/avatar/' . $dataUser['id'].$file_ext;
            move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file);

			$arrUpdate = array(
				'userId'=>$dataUser['id'],
				'accountId'=>$dataUser['accounts_id'],
				'leadId'=>$dataUser['leads_id'],
				'company_name'=>$this->input->post('company_name'),
				'tax_code'=>$this->input->post('tax_code'),
				($dataUser['accounts_id']?'phone_office':'phone_home')=>$this->input->post('phone'),
				'email_address'=>$this->input->post('email'),
				'payment_method'=>$this->input->post('payment_method'),
				($dataUser['accounts_id']?'billing_address_country':'primary_address_country')=>$this->input->post('country'),
				($dataUser['accounts_id']?'billing_address_city':'primary_address_city')=>$this->input->post('city'),
				($dataUser['accounts_id']?'billing_address_district':'primary_address_district')=>$this->input->post('district'),
				($dataUser['accounts_id']?'billing_address_street':'primary_address_street')=>$this->input->post('address'),
				'bank_name'=>$this->input->post('bank_name'),
				'bank_number'=>$this->input->post('bank_number'),
				'bank_account_name'=>$this->input->post('bank_account_no'),
				'description'=>$this->input->post('description'),
				'timestamp'=>$time,
                'avatar'=>!empty($file_ext) ? $dataUser['id'].$file_ext : ''
			);
			$update = $this->callapi->call($urlApi . "saveInfoUser", $arrUpdate);
			if($update['status'] == 1) {
				redirect($this->config->item('base_url') . "user-profile");
			}else{
				$this->form_validation->set_rules('custom', '', 'callback_custom');
				$this->form_validation->set_message('custom',  $this->data['language']['field_require_custom']);
				$this->form_validation->run();
				$call_back();
			}
		}
	}
	function custom($str){
		return FALSE;
	}
	public function createUser(){		
		$this->data['module'] = "user/register";
		$this->data['menu'] = "user-profile";
		$this->data['base_url'] = $this->config->base_url();
		$user 	= $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');		
        if($user) return redirect(base_url());

		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time()
		);

		$country = $this->callapi->call($urlApi."getCountryForRegister",$dataApi);		
		$countryData = $country['status'] == 1 ? $country['data'] : array();
		$countryOption = $this->common->getSelectOptionsCountry(json_encode($countryData), 'VIETNAM');
		$this->data['countryOption'] = $countryOption;

		$city = $this->callapi->call($urlApi."getCityForRegister",$dataApi);		
		$cityData = $city['status'] == 1 ? $city['data'] : array();
		$this->data['list_city'] = $cityData;

		$list_district = $this->callapi->call($urlApi."getDistrictForRegister",$dataApi);
		$list_district_data = $list_district['status'] == 1 ? $list_district['data'] : array();
		$this->data['list_district'] = base64_encode(json_encode($list_district_data));
		
		$this->load->view('index', $this->data);
	}
	public function registerUser(){
		$user 	= $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
        $lang_register = $this->data['language'];

        $validate = $this->validateRegister();
        if ($validate['check']) {
            $register_data = $this->input->post('list_insert');
            $dataApi = array(
                'userId'                    => $user?$user['id']:'',
                'timestamp'                 => time(),
                'date_entered' 				=> $this->convertdatetime->convertTimeToTimeDBCRM(date('Y-m-d H:i:s')),
                'user_name' 				=> $register_data['user_name'],
                'password_register' 		=> $register_data['password_register'],
                'account_name' 				=> $register_data['account_name'],
                'phone_mobile' 				=> $register_data['mobile_register'],
                'company_name' 				=> $register_data['company_name'],
                'department' 				=> $register_data['department'],
                'email' 					=> $register_data['email'],
                'primary_address_country' 	=> $register_data['primary_address_country'],
                'primary_address_city' 		=> $register_data['primary_address_city'],
                'primary_address_district' 	=> $register_data['receiver_district_1'],
                'primary_address_street' 	=> $register_data['primary_address_street'],
                'deleted' 					=> '0',
                'status' 					=> 'Draft',
                'cmnd'	 					=> $register_data['cmnd_register']
            );

            $register_lead = $this->callapi->call($urlApi."RegisterLeadCRM",$dataApi);
            if ($register_lead['status'] == 1) {
            	$mail = new PHPMailer();
			    try {
				    //Server settings
				    $mail->SMTPOptions = array(
						'ssl' => array(
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						)
					);
				    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
				    $mail->isSMTP();                                      // Set mailer to use SMTP
				    $mail->Host = 'smtp.gmail.com';  				// Specify main and backup SMTP servers
				    $mail->SMTPAuth = true;                               // Enable SMTP authentication
				    $mail->Username = 'customer.portal.speedlink@gmail.com';                 // SMTP username
				    $mail->Password = 'Itlvn123';                           // SMTP password
				    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
				    $mail->Port = 465;                                    // TCP port to connect to

				    //Recipients				    
		    		$mail->setFrom('no.reply@speedlink.vn', 'Speedlink');

				    if ($this->config->item('enviroment_staging')) {
				    	$mail->addAddress('dieter.tuan@itlvn.com');  
				    }else{
				    	$mail->addAddress('csexpress-hcm@speedlink.vn');  
				    }   // Add a recipient
                    $mail->addBCC('dieter.tuan@itlvn.com');
                    $mail->addBCC('tuantruong.it205@gmail.com');

				    //Content
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->Subject = 'Have new register portal need approve';
				    $mail->Body    = 'Dear all <br><br> Have new register portal need approve, link: <a href="'.$this->config->item('url_crm').'index.php?module=Leads&action=DetailView&record='.$register_lead['data'].'">Click here</a> <br><br> Thanks & Best Regards';
				    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

				    // thuc thi lenh gui mail 
				    $mail->send();
                	echo json_encode(array('result'=>'success','message'=>$lang_register['register_success']));die();
				} catch (Exception $e) {
				    echo 'Message could not be sent.';
				    echo 'Mailer Error: ' . $mail->ErrorInfo;
				}
            }
        }else{
            echo json_encode($validate);die();
        }
	}
	public function validateRegister()
    {
        $lang_register = $this->data['language'];
        $register_data = $this->input->post('list_insert');
        $result = array(
            'check' => true,
            'result'=>''
        );
        $user 	= $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');

		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time()
		);
        //ACCOUNT NAME
        if ($register_data['account_name'] == '') {
            $result['check'] = false;
            $error[] = array('name'=>'account_name','message'=>'* '.$lang_register['error_account_name']);
        }
        //USER NAME
        if ($register_data['user_name'] == '') {
            $result['check'] = false;
            $error[] = array('name'=>'user_name','message'=>'* '.$lang_register['error_user_name']);
        } else {
            if (!preg_match('/^[a-zA-Z0-9]{0,}$/', $register_data['user_name'])) {
                $result['check'] = false;
            	$error[] = array('name'=>'user_name','message'=>'* '.$lang_register['error_user_name_char']);
            }

            if (strlen($register_data['user_name']) < 5 || strlen($register_data['user_name']) > 30) {
                $result['check'] = false;
            	$error[] = array('name'=>'user_name','message'=>'* '.$lang_register['error_user_name_5char']);
            }
            $dataApi['user_name'] = $register_data['user_name'];
            $checkExistUserName = $this->callapi->call($urlApi."checkExistUserName",$dataApi);		
			$checkExistUserNameData = $checkExistUserName['status'] == 1 ? $checkExistUserName['data'] : array();
            if (count($checkExistUserNameData)>0) {
                $result['check'] = false;
            	$error[] = array('name'=>'user_name','message'=>'* '.$lang_register['error_exists']);
            }
        }

        //PASSWORD
        if ($register_data['password_register'] == '') {
            $result['check'] = false;
        	$error[] = array('name'=>'password_register','message'=>'* '.$lang_register['error_password']);
        } else {
            if ((strlen($register_data['password_register']) < 4) || (strlen($register_data['password_register']) > 20)) {
                $result['check'] = false;
        		$error[] = array('name'=>'password_register','message'=>'* '.$lang_register['error_password_char']);
            } else {
                if ($register_data['re_password_register'] == '') {
                    $result['check'] = false;
        			$error[] = array('name'=>'re_password_register','message'=>'* '.$lang_register['error_repeat_password']);
                }
                if ($register_data['password_register'] != $register_data['re_password_register']) {
                    $result['check'] = false;
        			$error[] = array('name'=>'re_password_register','message'=>'* '.$lang_register['error_equal_password']);
                }
            }
        }

        //EMAIL
        if ($register_data['email'] == '' || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $register_data['email'])) {
            $result['check'] = false;
			$error[] = array('name'=>'email','message'=>'* '.$lang_register['error_email']);
        } else {        	
            $dataApi['email'] = $register_data['email'];
            $checkExistEmail = $this->callapi->call($urlApi."checkExistEmail",$dataApi);		
			$checkExistEmailData = $checkExistEmail['status'] == 1 ? $checkExistEmail['data'] : array();
            if (count($checkExistEmailData) > 0 && $checkExistEmailData[0]['email_id'] != '' && ($checkExistEmailData[0]['lead_id'] != '' || $checkExistEmailData[0]['account_id'] != '')) {
                $result['check'] = false;
				$error[] = array('name'=>'email','message'=>'* '.$lang_register['error_email_exist']);
            }
        }

        //MOBILE
        if ($register_data['mobile_register'] == '' || (strlen($register_data['mobile_register']) < 10) || (strlen($register_data['mobile_register']) > 11)) {
            $result['check'] = false;
			$error[] = array('name'=>'mobile','message'=>'* '.$lang_register['error_mobile']);
        }else{
        	$dataApi['mobile_register'] = $register_data['mobile_register'];
            $checkExistMobile = $this->callapi->call($urlApi."checkExistMobile",$dataApi);		
			$checkExistMobileData = $checkExistMobile['status'] == 1 ? $checkExistMobile['data'] : array();
            if($checkExistMobileData > 0 && $checkExistMobileData != null){
                $result['check'] = false;
				$error[] = array('name'=>'mobile','message'=>'* '.$lang_register['error_mobile_exist']);
            }
        }

        //CNMD
        if ($register_data['cmnd_register'] == ''){
            $result['check'] = false;
			$error[] = array('name'=>'cmnd_register','message'=>'* '.$lang_register['error_cmnd']);
        }else{
        	$dataApi['cmnd_register'] = $register_data['cmnd_register'];
            $checkExistCMND = $this->callapi->call($urlApi."checkExistCMND",$dataApi);		
			$checkExistCMNDData = $checkExistCMND['status'] == 1 ? $checkExistCMND['data'] : array();
            if($checkExistCMNDData > 0 && $checkExistCMNDData != null){
                $result['check'] = false;
				$error[] = array('name'=>'cmnd_register','message'=>'* '.$lang_register['error_cmnd_exist']);
            }
        }

        //CAPTCHA
       if (!empty($register_data['captcha'])){
           $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lcp80MUAAAAAB2wgrljPKx54QLZjsix1vKfT9cL&response=".$register_data['captcha']."&remoteip=".$_SERVER['REMOTE_ADDR']);
           if($response.success == false){
				$error[] = array('name'=>'captcha','message'=>'* '.$lang_register['error_captcha']);
           }
       }else{
           $result['check'] = false;
			$error[] = array('name'=>'captcha','message'=>'* '.$lang_register['missing_captcha']);
       }

        //RULE
        if ($register_data['rule'] == 0) {
            $result['check'] = false;
			$error[] = array('name'=>'rule','message'=>'* '.$lang_register['error_checkRule']);
        }

        //MSG ERRORS
        $result['error'] = $error;
        if ($result['check'] == false) {
        	$result['result_check'] = $lang_register['error_register_data'];
        }
        return $result;
    }
    /**
     * Hàm show page reset mật khẩu
     * @return [html] [description]
     */
    public function forgetPassword() {
        $user 	= $this->session->userdata('login');
        if($user) return redirect(base_url());
    	$this->data['module'] = "user/forget-password";
		$this->data['menu'] = "user-forget-password";
		$this->data['base_url'] = $this->config->base_url();
		$this->load->view('index', $this->data);
    }
    /**
     * Hàm thực hiện reset mật khẩu
     * @return [json] [description]
     */
    public function resetPassword() {
    	$post = $this->input->post();
    	$urlApi = $this->config->item('url_api_portal');
        $user 	= $this->session->userdata('login');

    	$errs = array();
    	if($post['email'] === '') {
    		die(json_encode(array('err' => true, 'msg' => $this->data['language']['txt_user_forget_password_enter_email'])));
    	}else if(!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
    		die(json_encode(array('err' => true, 'msg' => $this->data['language']['txt_user_forget_password_email_invalid'])));
    	}else{
	    	$dataApiUser = array(
				'userId' => $user?$user['id']:'',
				'timestamp' => time(),
	    		'email' => $post['email'],
			);
    		//check email exits
    		$userCheck = $this->callapi->call($urlApi."checkExistEmail",$dataApiUser);
    		if($userCheck['status'] == 0) {
	    		die(json_encode(array('err' => true, 'msg' => $this->data['language']['txt_user_forget_password_email_not_found'])));
    		}else{
    			$userGet = null;
    			if($userCheck['data'][0]['lead_id']) {
    				$userApiData = array(
    					'userId' => $user?$user['id']:'',
						'timestamp' => time(),
			    		'lead_id' => $userCheck['data'][0]['lead_id'],
    				);
    				$userGet = $this->callapi->call($urlApi."getUserByLeadId",$userApiData);
    			}else if($userCheck['data'][0]['account_id']){
    				$userApiData = array(
    					'userId' => $user?$user['id']:'',
						'timestamp' => time(),
			    		'account_id' => $userCheck['data'][0]['account_id'],
    				);
    				$userGet = $this->callapi->call($urlApi."getUserByAccountId",$userApiData);
    			}
    			if(is_null($userGet)) {
		    		die(json_encode(array('err' => true, 'msg' => $this->data['language']['txt_user_forget_password_user_not_found'])));
    			}else{
    				//reset password
    				$newPassword = '0123456';
    				$dataApiChangePassword = array(
						'userId' => $userGet['data'][0]['id'],
						'timestamp' => time(),
			    		'user_hash' => md5($newPassword)
					);

		    		//check email exits
		    		$changePasswordStatus = $this->callapi->call($urlApi."changePasswordUserById",$dataApiChangePassword);
		    		//Send email password
		    		$fullName = $userGet['data'][0]['first_name'].$userGet['data'][0]['last_name'];
		    		$dataApiSendEmailResetPassword = array(
						'userId' => $userGet['data'][0]['id'],
						'timestamp' => time(),
			    		'subject' => $this->data['language']['txt_user_forget_password_subject_email'],
			    		'user_email' => $post['email'],
			    		'name' => $fullName,
			    		'email_text_hello' => $this->data['language']['txt_user_forget_password_email_text_hello'],
			    		'email_text_username' => $this->data['language']['txt_user_forget_password_email_text_username'],
			    		'email_text_password' => $this->data['language']['txt_user_forget_password_email_text_password'],
			    		'user_name' => $userGet['data'][0]['user_name'],
			    		'password' => $newPassword,
			    		'text_1' => $this->data['language']['txt_user_forget_password_email_text_1'],
			    		'text_2' => $this->data['language']['txt_user_forget_password_email_text_2'],
			    		'text_3' => $this->data['language']['txt_user_forget_password_email_text_3'],
					);
		    		$this->callapi->call($urlApi."sendEmailResetPassword",$dataApiSendEmailResetPassword);
    			}
    			
    		}
    	}
		die(json_encode(array('err' => false, 'msg' => $this->data['language']['txt_user_forget_password_reset_password_success'])));
    }
    /**
     * Hàm show page thay đổi mật khẩu
     * @return [html] [description]
     */
    public function userChangePass() {
        // $user 	= $this->session->userdata('login');
        // if(!$user) return redirect(base_url());
    	$this->data['module'] = "user/user-changepass";
		$this->data['menu'] = "user-change-password";
		$this->data['base_url'] = $this->config->base_url();
		$this->load->view('index', $this->data);
    }
    /**
     * Hàm thay đổi mật khẩu
     * @return [json] [description]
     */
    public function changePassword() {
        $post = $this->input->post();
    	$urlApi = $this->config->item('url_api_portal');
        $user 	= $this->session->userdata('login');
        $lang = $this->data['language'];

        $validate = $this->validateChangePassword();
        if ($validate['check']){
            $dataApiChangePassword = array(
                'userId' => $user['id'],
                'timestamp' => time(),
                'user_hash' => md5($post['new_password'])
            );

            $changePasswordStatus = $this->callapi->call($urlApi."changePasswordUserById",$dataApiChangePassword);
            if ($changePasswordStatus['status'] == 1){
                echo json_encode(array('result'=>'success','message'=>$lang['change_password_success']));die();
            }else{
                echo json_encode(array('result'=>'failed','message'=>$lang['change_password_failed']));die();
            }
        }else{
            echo json_encode($validate);die();
        }
    }
	public function getDataImport(){
		$file = $this->input->post('file');
		$user = $this->session->userdata('login');
		$filename = $_SERVER['DOCUMENT_ROOT'].'/uploads/import/' . $user['id'].'--'. date('Y-M-d-H-i'). $file['type'];
		$objPHPExcel = PHPExcel_IOFactory::load($filename);
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow = $objWorksheet->getHighestRow();

		//LAY THONG TIN TU FILE EXCEL
		$array_data_get_from_file = array();
		for ($i = 2; $i <= $highestRow; $i++) {
            $array_data_get_from_file['name'][] = trim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue());
            $array_data_get_from_file['phone'][] = trim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue());
            $array_data_get_from_file['address'][] = trim($objWorksheet->getCellByColumnAndRow(2, $i)->getValue());
            $array_data_get_from_file['country'][] = trim($objWorksheet->getCellByColumnAndRow(3, $i)->getValue());
            $array_data_get_from_file['city'][] = trim($objWorksheet->getCellByColumnAndRow(4, $i)->getValue());
            $array_data_get_from_file['district'][] = trim($objWorksheet->getCellByColumnAndRow(5, $i)->getValue());
            $array_data_get_from_file['ward'][] = trim($objWorksheet->getCellByColumnAndRow(6, $i)->getValue());
            $array_data_get_from_file['postal_code'][] = trim($objWorksheet->getCellByColumnAndRow(7, $i)->getValue());
		}
		//XU LY THONG TIN HIEN THI VA IMPORT
		if ($highestRow > 1 && $highestRow <= 2001) {
			$arrData = array();
			$arrTotal = array(
				'total_record' => 0,
				'total_failed' => 0,
				'total_selected' => 0,
			);
			for ($i = 0; $i < ($highestRow - 1); $i++) {
				//setdata save
				if ($array_data_get_from_file['name'][$i] && $array_data_get_from_file['address'][$i] && $array_data_get_from_file['country'][$i] && $array_data_get_from_file['city'][$i] && $array_data_get_from_file['district'][$i] && (strlen($array_data_get_from_file['phone'][$i])==10 || strlen($array_data_get_from_file['phone'][$i])==11) && is_numeric($array_data_get_from_file['phone'][$i]) && $array_data_get_from_file['phone'][$i]) {
					$is_record_failed = false;
				}else{
					$is_record_failed = true;
					$arrTotal['total_failed']++;
				}

				if(!$array_data_get_from_file['phone'][$i]){
					$arrData[$i]['phone'] = '<span style="color:red">* '.$this->data['language']['txt_required_field'].'</span>';
				}elseif (strlen($array_data_get_from_file['phone'][$i])<10 || strlen($array_data_get_from_file['phone'][$i])>11){
					$arrData[$i]['phone'] = '<span style="color:red">* '.$this->data['language']['txt_import_mobile_length'].'</span>';
				}elseif(!is_numeric($array_data_get_from_file['phone'][$i])){
					$arrData[$i]['phone'] = '<span style="color:red">* '.$this->data['language']['txt_import_mobile_not_number'].'</span>';
				}else{
					$arrData[$i]['phone'] = $array_data_get_from_file['phone'][$i];
				}
				$arrData[$i]['name'] = $array_data_get_from_file['name'][$i] ? $array_data_get_from_file['name'][$i] : '<span style="color:red">* '.$this->data['language']['txt_required_field'].'</span>';
				
				$arrData[$i]['address'] = $array_data_get_from_file['address'][$i] ? $array_data_get_from_file['address'][$i] : '<span style="color:red">* '.$this->data['language']['txt_required_field'].'</span>';
				$arrData[$i]['country'] = $array_data_get_from_file['country'][$i] ? $array_data_get_from_file['country'][$i] : '<span style="color:red">* '.$this->data['language']['txt_required_field'].'</span>';
				$arrData[$i]['city'] = $array_data_get_from_file['city'][$i] ? $array_data_get_from_file['city'][$i] : '<span style="color:red">* '.$this->data['language']['txt_required_field'].'</span>';
				$arrData[$i]['district'] = $array_data_get_from_file['district'][$i] ? $array_data_get_from_file['district'][$i] : '<span style="color:red">* '.$this->data['language']['txt_required_field'].'</span>';
				$arrData[$i]['ward'] = $array_data_get_from_file['ward'][$i];
				$arrData[$i]['postal_code'] = $array_data_get_from_file['postal_code'][$i];
				$arrTotal['total_record']++;
			}
			// $arrTotal['total_record'] = $arrTotal['total_selected'] + $arrTotal['total_failed'];
			echo json_encode(array(
				'status' => true,
				'data' => $arrData,
				'total' => array(
					'total_record'=>$arrTotal['total_record'],
					'total_failed'=>$arrTotal['total_failed'],
					'total_selected'=>$arrTotal['total_selected'],
				)
			));
		} else {
			echo json_encode(array(
				'status' => false,
				'data'=>$highestRow
			));;
		}
	}
	public function saveImportCredit(){
		$dataParam = $this->input->post('parameters');
		$param = json_decode(base64_decode($dataParam[0]),true);
		$invoice = json_decode(base64_decode($dataParam[1]),true);

		$result = array(
			'status' => true,
			'message' => 'Success',
		);
		if($param != '' && $param != null) {
			try {
				$arrUpdate = array();
				$arrUpdateVatInvoice = array();
				$listInvoice = $this->mvatinvoice->getListByPairData($param);
				$dataInvoice = array();
				$flagStatusPaid = true;
				if(count($listInvoice)>0){
					foreach($listInvoice AS $index => $item){
						$dataInvoice[$item['soa_code'].$item['invoice_no']] = $item;
						if($item != 'paid'){
							$flagStatusPaid = false;
						}
					}
				}
				$arrInvoiceNo = array();
				$arrDataPaymentSession = array();
				for ($i = 0; $i < count($param); $i++) {
					$arrUpdate[0] = array(
						'id'=>$param[$i]['id'],
						'paid_amount'=>( (count($arrUpdate)>0)?($arrUpdate[0]['paid_amount'] + $param[$i]['paid_amount']):$param[$i]['paid_amount'])
					);
					if($flagStatusPaid){
						$arrUpdate[0]['status'] = 'paid';
					}
					$arrUpdateVatInvoice[$i] = array(
						'id'=>$dataInvoice[$param[$i]['code'].$param[$i]['invoice_no']]['id'],
						'status'=>'paid',
						//'soa_amount'=>$param[$i]['paid_amount'],
						'paid_date'=>$param[$i]['paid_date'],
						'invoice_note'=>$param[$i]['note_view'],
					);
					$this->rabbit->emit('CalculateCreditAccount', $listInvoiceId);
					$arrInvoiceNo[] = array('invoice_no'=>$param[$i]['invoice_no'],'soa_code'=>$param[$i]['code']);
				}
				if(count($arrUpdate)>0){
					$this->msoa->updateBatchSoa($arrUpdate,'id');
				}
				if(count($arrUpdateVatInvoice)>0){
					$this->mvatinvoice->updateBatchVatInvoice($arrUpdateVatInvoice,'id');
				}
				if(count($invoice)>0){
					$this->mvatinvoice->insertBatchPaymentSession($invoice);
				}
				if(count($arrInvoiceNo)>0){
					$dataInvoiceUpdateBalanceAmount = $this->mvatinvoice->getListPaymentSessionByCustomWhere(array('custom'=>$arrInvoiceNo));
					$arrUpdatePaymentSession = array();
					foreach($dataInvoiceUpdateBalanceAmount AS $index => $item){
						if(array_key_exists($item['soa_code'].$item['invoice_no'],$arrDataPaymentSession)) {
							$arrDataPaymentSession[$item['soa_code'] . $item['invoice_no']] -= $item['paid_amount'];
						}else{
							$arrDataPaymentSession[$item['soa_code'] . $item['invoice_no']] = $item['invoice_amount'] - $item['paid_amount'];
						}
						$arrUpdatePaymentSession[] = array(
							'id'=>$item['id'],
							'balance_amount'=>$arrDataPaymentSession[$item['soa_code'] . $item['invoice_no']]
						);
					}
				}
				if(count($arrUpdatePaymentSession)>0){
					$this->mvatinvoice->updateBatchPaymentSession($arrUpdatePaymentSession,'id');
				}
			}catch(Exception $e){
				$result['status'] = false;
				$result['message'] = $e->getMessage();
			}
		}else{
			$result = array(
				'status' => false,
				'message' => 'Data null'
			);
		}
		echo json_encode($result);
	}
	public function saveAddressShipper(){
		$user 						= $this->session->userdata('login');
		$urlApi 					= $this->config->item('url_api_portal');

		$parameters 				=  json_decode(base64_decode($this->input->post('parameters'),true),true);
		$data = array();
		foreach($parameters AS $index=>$item){
			if (is_int($index)) {
				$data[] = array(
					'user_name'     => $item['name'],
					'user_phone'    => $item['phone'],
					'user_street'   => $item['address'],
					'user_country'  => $item['country'],
					'user_city'     => $item['city'],
					'user_district' => $item['district'],
					'user_ward'     => $item['ward'],
					'postal_code'   => $item['postal_code'],
					'is_default'    => 0,
					'deleted'       => 0,
					'user_id'		=> $user['id'],
					'created_by'	=> $user['id'],
					'date_entered'	=> date('Y-m-d h:i:s')
				);
			}			
		}
		if (count($data)>0) {
			$dataApi = array(
				'userId' 		=> $user?$user['id']:'',
				'timestamp' 	=> time(),
				'parameters' 	=> base64_encode(json_encode($data)),
				'account_id'	=> $user['accounts_id']
			);
			$result = $this->callapi->call($urlApi."saveAddressShipperImport",$dataApi);
			echo json_encode($result);die();
		}else{
			echo json_encode([
				'status'=>($result?1:501),
				'message'=>$this->data["language"]["txt_import_address_null"]
			]);
			die();			
		}		
	}
	public function saveAddressConsignee(){
		$user 						= $this->session->userdata('login');
		$urlApi 					= $this->config->item('url_api_portal');

		$parameters 				= json_decode(base64_decode($this->input->post('parameters'),true),true);

		$data = array();
		foreach($parameters AS $index=>$item){
			if (is_int($index)) {
				$data[] = array(
					'user_name'     => $item['name'],
					'user_phone'    => $item['phone'],
					'user_street'   => $item['address'],
					'user_country'  => $item['country'],
					'user_city'     => $item['city'],
					'user_district' => $item['district'],
					'user_ward'     => $item['ward'],
					'postal_code'   => $item['postal_code'],
					'is_default'    => 0,
					'deleted'       => 0,
					'user_id'		=> $user['id'],
					'created_by'	=> $user['id'],
					'date_entered'	=> date('Y-m-d h:i:s')
				);
			}
		}
		if (count($data)>0) {
			$dataApi = array(
				'userId' 		=> $user?$user['id']:'',
				'timestamp' 	=> time(),
				'parameters' 	=> base64_encode(json_encode($data)),
				'account_id'	=> $user['accounts_id'],
			);
			$data = $this->callapi->call($urlApi."saveAddressConsigneeImport",$dataApi);
			echo json_encode($data);die();
		}else{
			echo json_encode([
				'status'=>($result?1:501),
				'message'=>$this->data["language"]["txt_import_address_null"]
			]);
			die();			
		}		
	}
    public function validateChangePassword()
    {
        $lang = $this->data['language'];
        $post = $this->input->post();
        $result = array(
            'check' => true,
            'result'=>''
        );
        $user 	= $this->session->userdata('login');

        if(!$user) {
            $result['check'] = false;
            $result['error'] = array('name'=>'user_login','message'=>$lang['error_user_login']);
            return $result;
        }


        //Old Password
        if ($post['old_password'] == '') {
            $result['check'] = false;
            $error[] = array('name'=>'old_password','message'=>$lang['error_old_password']);
        }else{
            if ($user['password'] != md5($post['old_password'])){
                $result['check'] = false;
                $error[] = array('name'=>'old_password','message'=>$lang['error_old_password_not_match']);
            }
        }
        //New Password
        if ($post['new_password'] == '') {
            $result['check'] = false;
            $error[] = array('name'=>'new_password','message'=>$lang['error_new_password']);
        }else{
            if (strlen($post['new_password']) < 4){
                $result['check'] = false;
                $error[] = array('name'=>'new_password','message'=>$lang['error_new_password_too_short']);
            }elseif($post['new_password'] == $post['old_password']){
                $result['check'] = false;
                $error[] = array('name'=>'new_password','message'=>$lang['error_new_password_same_old_password']);
            }
        }
        //Retype Password
        if ($post['confirmed_password'] == '') {
            $result['check'] = false;
            $error[] = array('name'=>'confirmed_password','message'=>$lang['error_retype_password']);
        }else{
            if ($post['new_password'] != $post['confirmed_password']){
                $result['check'] = false;
                $error[] = array('name'=>'confirmed_password','message'=>$lang['error_retype_password_not_match']);
            }
        }

        //MSG ERRORS
        $result['error'] = $error;
        if ($result['check'] == false) {
            $result['result_check'] = $lang['validate_change_password_failed'];
        }
        return $result;
    }
}
