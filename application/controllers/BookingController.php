<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BookingController extends CI_Controller {
	private $data = array();
	public function __construct(){
		parent::__construct();
		//MODEL

		//LIBRARY
		$this->load->library('callapi');
		$this->load->library('common');
		$this->load->library('dataconvert');
		$this->load->library('convertdatetime');
		//NhÃºng file PHPExcel
		$this->load->library('PHPExcel.php');

		//LANGUAGE
		$this->lang->load('message',$this->session->userdata('site_lang'));
		$this->data['menu'] = "booking";
		$this->data["language"] = array_merge(
			$this->lang->line('language'),
			$this->lang->line('login'),
			$this->lang->line('menu'),
			$this->lang->line('home'),
			$this->lang->line('footer'),
			$this->lang->line('booking')
		);
	}

	public function create(){
		$this->data['module'] = "booking/create";
		$this->data['base_url'] = $this->config->base_url();

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => $time,
			'account_id'=>$user['accounts_id'],
			'country' => 'VIETNAM'
		);
		$account = $this->callapi->call($urlApi."getAddressDefaultForCustomerPortal",$dataApi);
		$dataAccount = $account['status'] == 1 ? $account['data'] : array();
		$this->data['dataAccount'] = $dataAccount;

		$country = $this->callapi->call($urlApi."getCountryForCreateBooking",$dataApi);
		$city = $this->callapi->call($urlApi."getCityForCreateBooking",$dataApi);
		$countryOption = $this->common->generateSelectOption($country['status'] == 1 ?$country['data']:array(),array(),'VIETNAM','country_code','country');
		$cityOption = $this->common->generateSelectOption($city['status'] == 1 ?$city['data']:array(),array(),null,'province_code','city');
		$this->data['countryOption'] = $countryOption;
		$this->data['cityOption'] = $cityOption;

		$this->load->view('index', $this->data);
	}
	public function import(){

		$this->data['module'] = "booking/import";
		$this->data['base_url'] = $this->config->base_url();
		$this->load->view('index', $this->data);
	}
	public function listView(){

		$this->data['module'] = "booking/list";
		$this->data['base_url'] = $this->config->base_url();

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();

		$dataApi = array(
			'userId' 		=> $user?$user['id']:'',
			'timestamp' 	=> $time,
			'accounts_id' 	=> $user['accounts_id'],
			'leads_id' 		=> $user['leads_id']
		);

		$dataStatus = array(
			array(
				'status' => 'Processing'
			),
			array(
				'status' => 'Picked Up'
			),
			array(
				'status' => 'Reschedule'
			),
			array(
				'status' => 'Canceled'
			),
			array(
				'status' => 'Delayed'
			)
		);

		if($_POST){
			$dateRangeValue = $this->input->post('date_range');
			$dataApi['status'] = $this->input->post('status');
			$dataApi['booking_no'] = $this->input->post('booking_no');
			$dataApi['from_date'] = $this->dataconvert->convertDateDB($this->input->post('from_date'));
			$dataApi['to_date'] = $this->dataconvert->convertDateDB($this->input->post('to_date'));
			$dataApi['isAdmin'] = 1;
			$data = $this->callapi->call($urlApi."getListBooking",$dataApi);
			$dataTable = $data['status'] == 1 ? $data['data'] : array();
			for($i = 0 ; $i < count($dataTable) ; $i++){
				$dataTable[$i]['check_box'] = '<input type="checkbox" class="check_box" value="'.$dataTable[$i]['id'].'">';
				$dataTable[$i]['no'] = $i + 1;
				$dataTable[$i]['booking_no'] = '<a href="#" data-toggle="modal" data-target="#booking-detail-modal" data-detail_booking="'.$dataTable[$i]['id'].'">'.$dataTable[$i]['booking_no'].'</a>';
				$dataTable[$i]['pickup_date_view'] = ConvertDateTime::convertDateTimeDisplay($dataTable[$i]['pickup_date']);
			}
			$statusValue = $this->input->post('status');

            $this->data['dataFilter']['booking_no'] = $this->input->post('booking_no');
            $this->data['dataFilter']['dateFrom'] = $this->input->post('from_date');
            $this->data['dataFilter']['dateTo'] = $this->input->post('to_date');
		}else{
			$dataApi['status'] = '';
			$dataApi['booking_no'] = '';
			$dataApi['from_date'] = date('Y-m-d 00:00:01', strtotime('-6 days',time()));
			$dataApi['to_date'] = date('Y-m-d 23:59:50', time());
			$dataApi['isAdmin'] = 1;
			$data = $this->callapi->call($urlApi."getListBooking",$dataApi);
			$dataTable = $data['status'] == 1 ? $data['data'] : array();
			for($i = 0 ; $i < count($dataTable) ; $i++){
				$dataTable[$i]['check_box'] = '<input type="checkbox" class="check_box" value="'.$dataTable[$i]['id'].'">';
				$dataTable[$i]['no'] = $i + 1;
				$dataTable[$i]['booking_no'] = '<a href="#" data-toggle="modal" data-target="#booking-detail-modal" data-detail_booking="'.$dataTable[$i]['id'].'">'.$dataTable[$i]['booking_no'].'</a>';
				$dataTable[$i]['pickup_date_view'] =ConvertDateTime::convertDateTimeDisplay($dataTable[$i]['pickup_date']);
			}
			$dateRangeValue = 'Last_7_Days';
			$statusValue = null;
		}
		$this->data['dataFilter']['dateRangeOption'] = $this->common->generateOptionRangeDatetimeSearch($dateRangeValue,$this->session->get_userdata()['site_lang']);
		$this->data['dataFilter']['statusOption'] = $this->common->generateSelectOption(
			$dataStatus, array(), $statusValue, 'status', 'status'
		);
		$this->data['dataTable'] = base64_encode(json_encode($dataTable));
		$this->load->view('index', $this->data);
	}

	public function createMulti(){
		$this->data['module'] = "booking/create-multi";
		$this->data['base_url'] = $this->config->base_url();

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => $time,
			'account_id'=>$user['accounts_id'],
			'country' => 'VIETNAM'
		);
		$account = $this->callapi->call($urlApi."getAddressDefaultForCustomerPortal",$dataApi);
		$dataAccount = $account['status'] == 1 ? $account['data'] : array();
		$this->data['dataAccount'] = $dataAccount;

		$country = $this->callapi->call($urlApi."getCountryForCreateBooking",$dataApi);
		$city = $this->callapi->call($urlApi."getCityForCreateBooking",$dataApi);
		$countryOption = $this->common->generateSelectOption($country['status'] == 1 ?$country['data']:array(),array(),'VIETNAM','country_code','country');
		$cityOption = $this->common->generateSelectOption($city['status'] == 1 ?$city['data']:array(),array(),'VIETNAM','province_code','city');
		$this->data['countryOption'] = $countryOption;
		$this->data['cityOption'] = $cityOption;

		// $service = $this->callapi->call($urlApi."getServiceListForCustomerPortal",$dataApi);
		// $serviceOption = $this->common->generateSelectOption($service['status'] == 1 ?$service['data']:array(),array(),null,'id','name');
		// $this->data['serviceOption'] = $serviceOption;

		$this->load->view('index', $this->data);
	}
	public function saveAddressShipper(){
		$user 						= $this->session->userdata('login');
		$urlApi 					= $this->config->item('url_api_portal');

		$parameters 				= $this->input->post('parameters');
		$parameters['user_id'] 		= $user['id'];
		$parameters['created_by'] 	= $user['id'];
		$parameters['date_entered'] = date('Y-m-d h:i:s');
		$dataApi = array(
			'userId' 		=> $user?$user['id']:'',
			'timestamp' 	=> time(),
			'parameters' 	=> $parameters,
			'account_id'	=> $user['accounts_id'],
		);
		$data = $this->callapi->call($urlApi."saveAddressShipper",$dataApi);
		if($data['status']!=1) {
			$data['message'] = $this->data['language']['txt_add_address_success'];
			die(json_encode(["err" => false, "msg" => $this->data['language']['txt_add_address_fail']]));
		}else{
            die(json_encode(["err" => true, "msg" => $this->data['language']['txt_add_address_success']]));
        }
	}
	public function saveAddressConsignee(){
		$user 						= $this->session->userdata('login');
		$urlApi 					= $this->config->item('url_api_portal');

		$parameters 				= $this->input->post('parameters');
		$parameters['user_id'] 		= $user['id'];
		$parameters['created_by'] 	= $user['id'];
		$parameters['date_entered'] = date('Y-m-d h:i:s');

		$dataApi = array(
			'userId' 		=> $user?$user['id']:'',
			'timestamp' 	=> time(),
			'parameters' 	=> $parameters,
			'account_id'	=> $user['accounts_id'],
		);
		$data = $this->callapi->call($urlApi."saveAddressConsignee",$dataApi);
		if($data['status']!=1) {
			$data['message'] = $this->data['language']['txt_add_address_success'];
			die(json_encode(["err" => false, "msg" => $this->data['language']['txt_add_address_fail']]));
		}else{
            die(json_encode(["err" => true, "msg" => $this->data['language']['txt_add_address_success']]));
        }
	}
	public function getListShipmentType(){
		$user 						= $this->session->userdata('login');
		$urlApi 					= $this->config->item('url_api_portal');

		$parameters 				= $this->input->post('parameters');
		$parameters['is_ras'] 		= 0;
		$parameters['pickup_date'] 	= date('Y/m/d',time());

		$dataApi = array(
			'userId' 				=> $user?$user['id']:'',
            'payer_id' 				=> $user?$user['accounts_id']:'',
			'timestamp' 			=> time(),
			'is_cod'				=> $parameters['is_cod'],
            'is_dox'				=> $parameters['is_dox'],
            'is_ras' 				=> 0,
            'cod_value'				=> $parameters['cod_value'],
            'weight'				=> $parameters['weight'],
            'dest_branch_code' 		=> $parameters['dest_branch_code'],
            'origin_branch_code' 	=> $parameters['origin_branch_code'],
            'shipper_city' 			=> $parameters['shipper_city'],
            'receiver_city' 		=> $parameters['receiver_city'],
            'pickup_date' 			=> date('Y/m/d',time())
		);

		$resultApi = $this->callapi->call($urlApi."checkTransportationFee",$dataApi);
		$result = $resultApi['status'] == 1 ? $resultApi['data'] : array();
		$data = array();
		for ($i=0; $i < count($result); $i++) {
            if (empty($result[$i]['msg'])) {
                $data[$i]['check_box'] = '<input type="radio" name="shipment_type" value="'.$result[$i]['shipment_type_id'].'">';
                $data[$i]['service'] = $this->session->userdata('site_lang')=='english' ? $result[$i]['service'] : $result[$i]['service_vn'];
                $data[$i]['price'] = number_format(($result[$i]['price_total'] + $result[$i]['price_ecom'])*((100+$result[$i]['fuel_surcharge'])/100)) . " VND";
                $data[$i]['description'] = $result[$i]['description'];
            }
		}
		echo json_encode($data);die();
	}
	public function createOrder(){
		$user 	= $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$language = $this->session->userdata('site_lang');

		$parameters = $this->input->post('parameters');
		$parameters_decode = json_decode(base64_decode($parameters),true);
		
		if ($parameters_decode['receiver_name']=="") {
			echo json_encode(array('status' => 2,'message'=> $this->data['language']['txt_create_booking_receiver_name_required'] ));die();
		}
		if ($parameters_decode['receiver_phone']=="") {
			echo json_encode(array('status' => 3,'message'=> $this->data['language']['txt_create_booking_receiver_phone_required']  ));die();
		}else if(strlen($parameters_decode['receiver_phone']) < 10 || strlen($parameters_decode['receiver_phone']) > 11) {
			echo json_encode(array('status' => 2,'message'=> $this->data['language']['txt_create_booking_phone_number_invalid'] ));die();
		}

		if ($parameters_decode['receiver_country']=="") {
			echo json_encode(array('status' => 4,'message'=> $this->data['language']['txt_create_booking_receiver_country_required'] ));die();
		}
		if ($parameters_decode['receiver_city']=="") {
			echo json_encode(array('status' => 5,'message'=> $this->data['language']['txt_create_booking_receiver_city_required'] ));die();
		}
		if ($parameters_decode['receiver_district']=="") {
			echo json_encode(array('status' => 6,'message'=> $this->data['language']['txt_create_booking_receiver_district_required'] ));die();
		}
		if ($parameters_decode['receiver_street']=="") {
			echo json_encode(array('status' => 7,'message'=> $this->data['language']['txt_create_booking_receiver_street_required'] ));die();
		}		
		if (!isset($parameters_decode['service_code']) || $parameters_decode['service_code']=="") {
			echo json_encode(array('status' => 9,'message'=> $this->data['language']['txt_create_booking_service_required'] ));die();
		}
		$dataApi = array(
			'userId' 			 => $user?$user['id']:'',
			'origin_branch_code' => $user?$user['branch_code']:'',
			'origin_hub_code' 	 => $user?$user['hub_code']:'',
			'timestamp' 		 => time(),
			'parameters'		 => $parameters,
			'account_id'		 => $user['accounts_id'],
            'leads_id'           => $user['leads_id']
		);
		$resultApi = $this->callapi->call($urlApi."createOrderForCustomerPortal",$dataApi);
		if($resultApi['status'] == 1) {
			$resultApi['message'] = $this->data['language']['txt_create_booking_success'];
		}elseif($resultApi['status'] == 2) {
			$resultApi['message'] = $this->data['language']['txt_create_booking_parameter_wrong'];
		}elseif($resultApi['status'] == 3) {
			$resultApi['message'] = $this->data['language']['txt_create_booking_parameter_missing'];
		}elseif($resultApi['status'] == 4) {
			$resultApi['message'] = $this->data['language']['txt_create_booking_parameter_failed'];
		}elseif($resultApi['status'] == 5) {
			$resultApi['message'] = $this->data['language']['txt_create_booking_parameter_address_wrong'];
		}elseif($resultApi['status'] == -1) {
            $resultApi['message'] = $this->data['language']['txt_create_booking_duplicate_tracking_no'];
        }
		if ($parameters_decode['save_address'] == 1) {
			$dataConsigneeAddress = array(
				'user_id'		=> $user['id'],
				'created_by' 	=> $user['id'],
				'date_entered' 	=> date('Y-m-d h:i:s'),
				'user_name'     => $parameters_decode['receiver_name'],
		        'user_phone'    => $parameters_decode['receiver_phone'],
		        'user_street'   => $parameters_decode['receiver_street'],
		        'user_country'  => $parameters_decode['receiver_country'],
		        'user_city'     => $parameters_decode['receiver_city'],
		        'user_district' => $parameters_decode['receiver_district'],
		        'user_ward'     => $parameters_decode['receiver_ward'],
		        'postal_code'   => $parameters_decode['receiver_zip_code'],
		        'is_default'    => 0,
		        'deleted'       => 0
			);
			$dataApi['parameters'] = $dataConsigneeAddress;
			$this->callapi->call($urlApi."saveAddressConsignee",$dataApi);
		}
		echo json_encode($resultApi);die();
	}
	public function createMultiOrder(){
		$user 	= $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');

		$para = $this->input->post();
		$data = array();
        $dataConsigneeAddress = array();
        $j = 0;
		for ($i=0; $i < count($para['order_no']) ; $i++) {
			if (!empty($para['receiver_name'][$i])) {
				$data[$j]['account_id'] 			 = $user['accounts_id'];
				$data[$j]['leads_id'] 			 	 = $user['leads_id'];
				$data[$j]['shipper_contact_name']  	 = $para['short_name'];
				$data[$j]['phone_shipper'] 		 	 = $para['mobile_phone'];
				$data[$j]['pickup_address'] 		 = $para['shipper_address'];
				$data[$j]['pickup_ward'] 	 		 = $para['shipper_ward'];			
				$data[$j]['pickup_district'] 	 	 = $para['shipper_district'];			
				$data[$j]['pickup_city'] 	  		 = $para['shipper_city'];			
				$data[$j]['pickup_country'] 	 	 = $para['shipper_country'];			
				$data[$j]['order_no'] 				 = $para['order_no'][$i];
				$data[$j]['weight'] 				 = $para['weight'][$i];
				$data[$j]['charge_to'] 			 	 = $para['charge_to'][$i];
				$data[$j]['cod_value'] 			 	 = $para['cod_value'][$i];
				$data[$j]['payment_method'] 		 = $para['payment_method'][$i];
				$data[$j]['shipment_value'] 		 = $para['shipment_value'][$i];
				$data[$j]['pcs'] 		             = $para['pcs'][$i];
				$data[$j]['description'] 			 = $para['description'][$i];
				$data[$j]['receiver_name'] 		 	 = $para['receiver_name'][$i];
				$data[$j]['receiver_phone'] 		 = $para['receiver_phone'][$i];			
				$data[$j]['receiver_country'] 	 	 = $para['receiver_country'][$i];
				// $data[$j]['receiver_country'] 	 	 = 'VIETNAM';
				$data[$j]['receiver_city'] 		 	 = $para['receiver_city'][$i];
				$data[$j]['receiver_district'] 	 	 = $para['receiver_district'][$i];
				$data[$j]['receiver_ward'] 	 		 = $para['receiver_ward'][$i];
				$data[$j]['receiver_street'] 		 = $para['receiver_address'][$i];
				$data[$j]['service'] 		 	 	 = $para['service'][$i];
				$data[$j]['is_dox'] 		 	 	 = $para['is_dox'][$i];
				$data[$j]['is_cod'] 		 	 	 = $para['cod_value'][$i] != "" && $para['cod_value'][$i] != 0 ? 1 : 0;
				if ($para['save_address'][$i] == 1) {
					$dataConsigneeAddress[] = array(
						'user_id'		=> $user['id'],
						'created_by' 	=> $user['id'],
						'date_entered' 	=> date('Y-m-d h:i:s'),
						'user_name'     => $para['receiver_name'][$i],
				        'user_phone'    => $para['receiver_phone'][$i],
				        'user_street'   => $para['receiver_address'][$i],
				        'user_country' 	=> $para['receiver_country'][$i],
				        // 'user_country'  => 'VIETNAM',
				        'user_city'     => $para['receiver_city'][$i],
				        'user_district' => $para['receiver_district'][$i],
				        'user_ward'     => $para['receiver_ward'][$i],
				        'is_default'    => 0,
				        'deleted'       => 0
					);
				}
				$j++;
			}
		}
		$dataApi = array(
			'userId' 			 => $user?$user['id']:'',
			'origin_branch_code' => $user?$user['branch_code']:'',
			'origin_hub_code' 	 => $user?$user['hub_code']:'',
			'timestamp' 		 => time(),
			'data'		 		 => base64_encode(json_encode($data)),
			'account_id'		 => $user['accounts_id'],
            'leads_id'           => $user['leads_id']
		);
        $dataApi['parameters'] = $dataConsigneeAddress;
		$resultApi = $this->callapi->call($urlApi."createMultiOrderForCustomerPortal",$dataApi);
		if ($resultApi['status'] == 1 && isset($dataConsigneeAddress) && count($dataConsigneeAddress) >0) {
			for ($i = 0;$i<count($dataConsigneeAddress);$i++){
                $dataApi['parameters'] = $dataConsigneeAddress[$i];
                $this->callapi->call($urlApi."saveAddressConsignee",$dataApi);
            }
            $resultApi['message'] = $this->data['language']['txt_create_booking_success'];
		}elseif($resultApi['status'] == -1) {
            $resultApi['message'] = $this->data['language']['error_duplicate_tracking_no_1'].' "'.$resultApi['data'].'" '.$this->data['language']['error_duplicate_tracking_no_2'];
        }
		echo json_encode($resultApi);die();
	}	
	public function importExcel(){
		$user 	= $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');

		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time()
		);

		$file = $_FILES['booking_excel'];
		move_uploaded_file($_FILES['booking_excel']['tmp_name'], APPPATH.'/uploads/'.$user['id'].$_FILES['booking_excel']['name']);

        $array_html = array();
        $array_insert = array();

        $filename = APPPATH.'/uploads/' . $user['id'].$_FILES['booking_excel']['name'];
        $objPHPExcel = PHPExcel_IOFactory::load($filename);

		$objFile = PHPExcel_IOFactory::identify($filename);
		$objData = PHPExcel_IOFactory::createReader($objFile);
		$objData->setReadDataOnly(true);
		$objPHPExcel = $objData->load($filename);


        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        //$highestColumn = $objWorksheet->getHighestColumn();
        //$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        //LAY THONG TIN TU FILE EXCEL

        $count_row = 0;
        $arr_data_from_file = array();
        for ($i = 2; $i <= $highestRow; $i++) {
            if(trim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue()) != '') {
                $arr_data_from_file['service_category'][] = trim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue());
                $arr_data_from_file['dox'][]              = trim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue());
                $arr_data_from_file['is_cod'][]           = trim($objWorksheet->getCellByColumnAndRow(2, $i)->getValue());
                $arr_data_from_file['cod_value'][]        = trim($objWorksheet->getCellByColumnAndRow(3, $i)->getValue());
                $arr_data_from_file['weight'][]           = trim($objWorksheet->getCellByColumnAndRow(4, $i)->getValue());
                $arr_data_from_file['pickup_country'][]   = trim($objWorksheet->getCellByColumnAndRow(5, $i)->getValue());
                $arr_data_from_file['pickup_city'][]      = trim($objWorksheet->getCellByColumnAndRow(6, $i)->getValue());
                $arr_data_from_file['pickup_district'][]  = trim($objWorksheet->getCellByColumnAndRow(7, $i)->getValue());
                $arr_data_from_file['pickup_address'][]   = trim($objWorksheet->getCellByColumnAndRow(8, $i)->getValue());
                $arr_data_from_file['shipper_contact_name'][] = trim($objWorksheet->getCellByColumnAndRow(9, $i)->getValue());
                $arr_data_from_file['shipper_phone'][]    = trim($objWorksheet->getCellByColumnAndRow(10, $i)->getValue());
                $arr_data_from_file['pickup_date'][]      = PHPExcel_Style_NumberFormat::toFormattedString(trim($objWorksheet->getCellByColumnAndRow(11, $i)->getCalculatedValue()), 'yyyy-mm-dd hh:mm:ss');
                $arr_data_from_file['delivery_country'][] = trim($objWorksheet->getCellByColumnAndRow(12, $i)->getValue());
                $arr_data_from_file['delivery_city'][]    = trim($objWorksheet->getCellByColumnAndRow(13, $i)->getValue());
                $arr_data_from_file['delivery_district'][]= trim($objWorksheet->getCellByColumnAndRow(14, $i)->getValue());
                $arr_data_from_file['delivery_address'][] = trim($objWorksheet->getCellByColumnAndRow(15, $i)->getValue());
                $arr_data_from_file['delivery_company_name'][]= trim($objWorksheet->getCellByColumnAndRow(16, $i)->getValue());
                $arr_data_from_file['delivery_contact_name'][]= trim($objWorksheet->getCellByColumnAndRow(17, $i)->getValue());
                $arr_data_from_file['delivery_contact_phone'][]= trim($objWorksheet->getCellByColumnAndRow(18, $i)->getValue());
                $arr_data_from_file['payment_method'][]   = trim($objWorksheet->getCellByColumnAndRow(19, $i)->getValue());
                $arr_data_from_file['charge_to'][]        = trim($objWorksheet->getCellByColumnAndRow(20, $i)->getValue());
                $arr_data_from_file['shipment_value'][]   = trim($objWorksheet->getCellByColumnAndRow(21, $i)->getValue());
                $arr_data_from_file['special_instruction'][] = trim($objWorksheet->getCellByColumnAndRow(22, $i)->getValue());
                $arr_data_from_file['specical_req'][]        = trim($objWorksheet->getCellByColumnAndRow(23, $i)->getValue());
                $arr_data_from_file['booking_note'][]        = trim($objWorksheet->getCellByColumnAndRow(24, $i)->getValue());
                $count_row++;
            }
        }
        //echo json_encode($arr_data_from_file);die();
        if($count_row <= 2000) {
            //XU LY THONG TIN HIEN THI VA IMPORT
            if ($count_row > 0) {
                // $lead_db_user = $this->maccount->getLeadById($user['lead_id']);

				$servicecategory_db_api = $this->callapi->call($urlApi."getAllShipmentTypeCategoryImportBooking",$dataApi);
				$servicecategory_db = $servicecategory_db_api['status'] == 1 ? $servicecategory_db_api['data'] : array();

				$shipment_type_api = $this->callapi->call($urlApi."getAllShipmentTypeImportBooking",$dataApi);
				$shipmenttype_db = $shipment_type_api['status'] == 1 ? $shipment_type_api['data'] : array();

                //LAY DANH SACH HUB
				$array_hub_api = $this->callapi->call($urlApi."getAllMailHub",$dataApi);
				$array_hub_db = $array_hub_api['status'] == 1 ? $array_hub_api['data'] : array();

                //LAY DANH SACH COUNTRY
                $array_country_api = $this->callapi->call($urlApi."getCountryActive",$dataApi);
				$array_country_db = $array_country_api['status'] == 1 ? $array_country_api['data'] : array();

                //LAY DANH SACH PROVICE
                $array_province_api = $this->callapi->call($urlApi."getAllProvice",$dataApi);
				$array_province_db = $array_province_api['status'] == 1 ? $array_province_api['data'] : array();

                //LAY DANH SACH DISTRICT
                $array_district_api = $this->callapi->call($urlApi."getDistrictForImportBooking",$dataApi);
				$array_district_db = $array_district_api['status'] == 1 ? $array_district_api['data'] : array();

                //LAY DANH SACH SHIPMENT TYPE
                $array_shipment_type_api = $this->callapi->call($urlApi."getAllShipmentType",$dataApi);
				$array_shipment_type_db = $array_shipment_type_api['status'] == 1 ? $array_shipment_type_api['data'] : array();

                //GENERATE DANH SACH BOOKING NO
                $dataApi['number_code'] = $count_row;
                $array_booking_no_api = $this->callapi->call($urlApi."generateListBookingNo",$dataApi);
				$array_booking_no = $array_booking_no_api['status'] == 1 ? $array_booking_no_api['data'] : array();
                //LAY DANH SACH DATA HIEN THI VA INSERT
                for ($i = 0; $i < ($count_row); $i++) {
                    $check_insert = 1;
                    //GENERATE
                    $booking_id = Common::generateID('spl');
                    $array_temp_insert = array(
                        'id' => $booking_id,
                        'name' => '',
                        'company_id' => '',
                        'parent_shipper_options' => '',
                        // 'contact_shipper_name' => '',
                        'phone_shipper' => '',
                        'country_shipper' => '',
                        'city_shipper' => '',
                        'district_shipper' => '',
                        'address_shipper' => '',
                        'parent_payer_options' => '',
                        'company_delivery' => '',
                        'contact_delivery_name' => '',
                        'phone_delivery' => '',
                        'city_delivery' => '',
                        'country_delivery' => '',
                        'district_delivery' => '',
                        'address_delivery' => '',
                        'origin_branch_code' => '',
                        'origin_hub_code' => '',
                        'dest_branch_code' => '',
                        'dest_hub_code' => '',
                        'payer_id' => '',
                        'acc_no' => '',
                        'customer_code' => '',
                        'shipment_type_internal' => '',
                        'shipment_type' => '',
                        'cod_value' => '',
                        'weight' => '',
                        'pickup_date' => '',
                        'specical_instruction' => '',
                        'io_undelivery' => '',
                        'specical_req' => '',
                        'shipment_value' => '',
                        'booking_note' => '',
                        'payment_method' => '',
                        'charge_to' => '',
                    );
                    $array_temp_html = array(
                        // 'id' => $booking_id,
                        'checkbox' => '<img src="/assets/images/icon-not-ok.png" style="height: 20px;"/>',
                        'booking_no' => '',
                        'service_category' => '',
                        'dox' => '',
                        'is_cod' => '',
                        'cod_value' => '',
                        'weight' => '',
                        'pickup_city' => '',
                        'io_undelivery' => '',
                        'pickup_date' => '',
                        'delivery_city' => '',
                        'delivery_district' => '',
                        'delivery_address' => '',
                        'delivery_company_name' => '',
                        'delivery_contact_name' => '',
                        'delivery_contact_phone' => '',
                        'payment_method' => '',
                        'charge_to' => '',
                        'shipment_value' => '',
                        'special_instruction' => '',
                        'specical_req' => '',
                        'booking_note' => '',
                    );


                    //SHIPMENT TYPE
                    if(!empty($arr_data_from_file['service_category'][$i])){
                        $key = array_search($arr_data_from_file['service_category'][$i], $this->common->get_field_data($servicecategory_db, 'name'));
                        $array_temp_html['service_category'] = $arr_data_from_file['service_category'][$i];
                        $array_temp_html['dox'] = $arr_data_from_file['dox'][$i];
                        $array_temp_html['is_cod'] = $arr_data_from_file['is_cod'][$i];
                        $array_temp_html['cod_value'] = $arr_data_from_file['cod_value'][$i];
                        if($key !== false){
                            if($arr_data_from_file['dox'][$i] == 'Yes'){
                                if($arr_data_from_file['is_cod'][$i] == 'Yes'){
                                    //Dox, cÃ³ COD vÃ  COD Value khÃ´ng rá»—ng
                                    if(!empty($arr_data_from_file['cod_value'][$i])){
                                        //cod value pháº£i lÃ  sá»‘
                                        if(is_numeric($arr_data_from_file['cod_value'][$i])){
                                            foreach ($shipmenttype_db as $value) {
                                                if($value['parent_service'] == $servicecategory_db[$key]['id'] && $value['service'] == 'dox' && $value['is_cod'] == 1){
                                                    //insert
                                                    $array_temp_insert['shipment_type_internal'] = $value['id'];
                                                    $array_temp_insert['shipment_type'] = $value['name'];
                                                    $array_temp_insert['cod_value'] = $arr_data_from_file['cod_value'][$i];
                                                }
                                            }
                                        }else{
                                            //cod value Sai(khÃ´ng pháº£i lÃ  sá»‘)
                                            $check_insert = 0;
                                            $array_temp_html['cod_value'] = '<span style="color:red">COD Value is wrong.</span>';
                                        }
                                    //Dox, cÃ³ COD nhÆ°ng COD Value rá»—ng
                                    }else{
                                        //cod value Sai(rá»—ng)
                                        $check_insert = 0;
                                        $array_temp_html['cod_value'] = '<span style="color:red">COD Value can not null</span>';
                                    }
                                }else if($arr_data_from_file['is_cod'][$i] == 'No'){
                                    foreach ($shipmenttype_db as $value) {
                                        if($value['parent_service'] == $servicecategory_db[$key]['id'] && $value['service'] == 'dox' && $value['is_cod'] == 0){
                                            //insert
                                            $array_temp_insert['shipment_type_internal'] = $value['id'];
                                            $array_temp_insert['shipment_type'] = $value['name'];
                                            $array_temp_insert['cod_value'] = 0;
                                        }
                                    }
                                    if(empty($arr_data_from_file['cod_value'][$i])){
                                        //html
                                        $array_temp_html['cod_value'] = 0;
                                    }else{
                                        //html
                                        $array_temp_html['cod_value'] = '<span style="color:red">COD Value is null.</span>';
                                    }
                                }else{
                                    //is_cod Sai (chá»‰ nháº­n giÃ¡ trá»‹ Yes hoáº·c No)
                                    $check_insert = 0;
                                    $array_temp_html['is_cod'] = '<span style="color:red">Is COD is wrong</span>';
                                }
                            //Non Dox
                            }else if($arr_data_from_file['dox'][$i] == 'No'){
                                //Non Dox, CÃ³ COD
                                if($arr_data_from_file['is_cod'][$i] == 'Yes'){
                                    //Non Dox, cÃ³ COD vÃ  COD_Value khÃ´ng rá»—ng (True)
                                    if(!empty($arr_data_from_file['cod_value'][$i])){
                                        //cod value pháº£i lÃ  sá»‘ (True)
                                        if(is_numeric($arr_data_from_file['cod_value'][$i])){
                                            foreach ($shipmenttype_db as $value) {
                                                if($value['parent_service'] == $servicecategory_db[$key]['id'] && $value['service'] == 'non_dox' && $value['is_cod'] == 1){
                                                    //insert
                                                    $array_temp_insert['shipment_type_internal'] = $value['id'];
                                                    $array_temp_insert['shipment_type'] = $value['name'];
                                                    $array_temp_insert['cod_value'] = $arr_data_from_file['cod_value'][$i];
                                                }
                                            }
                                        }else{
                                            //cod_value KhÃ´ng pháº£i lÃ  sá»‘ (False)
                                            $check_insert = 0;
                                            $array_temp_html['cod_value'] = '<span style="color:red">COD Value is wrong.</span>';
                                        }
                                    //Non Dox, cÃ³ COD nhÆ°ng COD Value rá»—ng (False)
                                    }else{
                                        $check_insert = 0;
                                        $array_temp_html['cod_value'] = '<span style="color:red">COD Value can not null</span>';
                                    }
                                //Non Dox, KhÃ´ng cÃ³ COD
                                }else if($arr_data_from_file['is_cod'][$i] == 'No'){
                                    foreach ($shipmenttype_db as $value) {
                                        if($value['parent_service'] == $servicecategory_db[$key]['id'] && $value['service'] == 'non_dox' && $value['is_cod'] == 0){
                                            //insert
                                            $array_temp_insert['shipment_type_internal'] = $value['id'];
                                            $array_temp_insert['shipment_type'] = $value['name'];
                                            $array_temp_insert['cod_value'] = 0;
                                        }
                                    }
                                    if(empty($arr_data_from_file['cod_value'][$i])){
                                        //html
                                        $array_temp_html['cod_value'] = 0;
                                    }else{
                                        //html
                                        $array_temp_html['cod_value'] = '<span style="color:red">COD Value is null.</span>';
                                    }
                                }else{
                                    //is cod Sai (chá»‰ nháº­n giÃ¡ trá»‹ Yes hoáº·c No)
                                    $check_insert = 0;
                                    $array_temp_html['is_cod'] = '<span style="color:red">Is COD is wrong (True value is Yes or No)</span>';
                                }
                            }else{
                                //Dox Sai (chá»‰ nháº­n giÃ¡ trá»‹ Yes hoáº·c No)
                                $check_insert = 0;
                                $array_temp_html['dox'] = '<span style="color:red">Dox is wrong (True value is Yes or No)</span>';
                            }
                        }else{
                            //Service Category Sai
                            $check_insert = 0;
                            $array_temp_html['service_category'] = '<span style="color:red">Service Category is wrong</span>';
                        }
                    }else{
                        //Service Category KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['service_category'] = '<span style="color:red">Service Category can not null</span>';
                    }

                    //WEIGHT
                    if(!empty($arr_data_from_file['weight'][$i])){
                        if(is_numeric($arr_data_from_file['weight'][$i])){
                            //insert
                            $array_temp_insert['weight'] = $arr_data_from_file['weight'][$i];
                            //html
                            $array_temp_html['weight'] = $arr_data_from_file['weight'][$i];
                        }else{
                            //KhÃ´ng pháº£i lÃ  sá»‘ (False)
                            $check_insert = 0;
                            $array_temp_html['weight'] = '<span style="color:red">Weight is wrong.</span>';
                        }
                    }else{
                        //WEIGHT KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['weight'] = '<span style="color:red">Weight can not null.</span>';
                    }

                    //PICKUP COUNTRY
                    $country_id_shipper = '';
                    $array_temp_html['pickup_country'] = $arr_data_from_file['pickup_country'][$i];
                    if(!empty($arr_data_from_file['pickup_country'][$i])){
                        $key = array_search($arr_data_from_file['pickup_country'][$i], $this->common->get_field_data($array_country_db, 'name'));
                        if($key !== false){
                            $country_id_shipper = $array_country_db[$key]['id'];
                            $array_temp_insert['country_shipper'] = $array_country_db[$key]['id'];
                        }else{
                            //pickup_country KHONG ÄÃºng (False)
                            $check_insert = 0;
                            $array_temp_html['pickup_country'] = '<span style="color:red">Pickup Country is wrong.</span>';
                        }
                    }else{
                        //pickup_country KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['pickup_country'] = '<span style="color:red">Pickup Country can not null.</span>';
                    }
                    //PICKUP CITY
                    $city_pickup_id = '';
                    $array_temp_html['pickup_city'] = $arr_data_from_file['pickup_city'][$i];
                    if(!empty($arr_data_from_file['pickup_city'][$i])){
                        $arr_province_of_shipper_country = array();
                        for ($k = 0; $k < count($array_province_db); $k++) {
                            if ($array_province_db[$k]['country_id'] == $country_id_shipper) {
                                $arr_province_of_shipper_country[] = $array_province_db[$k];
                            }
                        }
                        $key = array_search($arr_data_from_file['pickup_city'][$i], $this->common->get_field_data($arr_province_of_shipper_country, 'branch_name'));
                        if($key !== false){
                            $city_pickup_id = $arr_province_of_shipper_country[$key]['id'];
                            $array_temp_insert['city_shipper'] = $arr_province_of_shipper_country[$key]['id'];
                            $array_temp_insert['origin_branch_code'] = $arr_province_of_shipper_country[$key]['id'];
                            $array_temp_insert['origin_hub_code'] = $arr_province_of_shipper_country[$key]['hub_id'];
                        }else{
                            //pickup_city KHONG ÄÃºng (False)
                            $check_insert = 0;
                            $array_temp_html['pickup_city'] = '<span style="color:red">Pickup City is wrong.</span>';
                        }
                    }else{
                        //pickup_city KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['pickup_city'] = '<span style="color:red">Pickup City can not null.</span>';
                    }


                    //SHIPPER DISTRICT
                    $array_temp_html['pickup_district'] = $arr_data_from_file['pickup_district'][$i];
                    if ($arr_data_from_file['pickup_district'][$i] != '') {
                        $arr_district_of_receiver_city = array();
                        for ($k = 0; $k < count($array_district_db); $k++) {
                            if ($array_district_db[$k]['province'] == $city_pickup_id) {
                                $arr_district_of_receiver_city[] = $array_district_db[$k];
                            }
                        }
                        $key_district = array_search(trim($arr_data_from_file['pickup_district'][$i]), $this->common->get_field_data_trim($arr_district_of_receiver_city, 'name'));
                        if ($key_district !== false) {
                            if ($arr_district_of_receiver_city[$key_district]['province'] == $city_pickup_id) {
                                //HTML
                                $array_temp_html['pickup_district'] = $arr_data_from_file['pickup_district'][$i];
                                //INSERT
                                $array_temp_insert['district_shipper'] = $arr_district_of_receiver_city[$key_district]['id'];
                            } else {
                                $check_insert = 0;
                                $array_temp_html['pickup_district'] = '<span style="color:red">pickup_district is not in this Shipper City</span>';
                            }
                        } else {
                            $check_insert = 0;
                            $array_temp_html['pickup_district'] = '<span style="color:red">Shipper District is wrong</span>';
                        }
                    } else {
                        $check_insert = 0;
                        $array_temp_html['pickup_district'] = '<span style="color:red">Shipper District can not null</span>';
                    }

                    //PICKUP ADDRESS
                    if(!empty($arr_data_from_file['pickup_address'][$i])){
                        //insert
                        $array_temp_insert['address_shipper'] = $arr_data_from_file['pickup_address'][$i];
                        //html
                        $array_temp_html['pickup_address'] = $arr_data_from_file['pickup_address'][$i];
                    }else{
                        //pickup_address KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['pickup_address'] = '<span style="color:red">Pickup Address can not null.</span>';
                    }
                    $array_temp_insert['pickup_address'] = $arr_data_from_file['pickup_address'][$i].', '.$arr_data_from_file['pickup_district'][$i].', '.$arr_data_from_file['pickup_city'][$i].', '.$arr_data_from_file['pickup_country'][$i];

                    //SHIPPER CONTACT NAME
                    if(!empty($arr_data_from_file['shipper_contact_name'][$i])){
                        //insert
                        $array_temp_insert['shipper_contact_name'] = $arr_data_from_file['shipper_contact_name'][$i];
                        //html
                        $array_temp_html['shipper_contact_name'] = $arr_data_from_file['shipper_contact_name'][$i];
                    }else{
                        //shipper_contact_name KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['shipper_contact_name'] = '<span style="color:red">Shipper Contact Name can not null.</span>';
                    }

                    //SHIPPER PHONE
                    if(!empty($arr_data_from_file['shipper_phone'][$i])){
                        //insert
                        $array_temp_insert['phone_shipper'] = $arr_data_from_file['shipper_phone'][$i];
                        //html
                        $array_temp_html['shipper_phone'] = $arr_data_from_file['shipper_phone'][$i];
                    }else{
                        //shipper_phone KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['shipper_phone'] = '<span style="color:red">Shipper Phone can not null.</span>';
                    }

                    //PICKUP DATE
                    $array_temp_insert['pickup_date'] = ConvertDateTime::convertTimeToTimeDBCRM($arr_data_from_file['pickup_date'][$i], true);
                    $array_temp_html['pickup_date'] = ConvertDateTime::convertDateTimeDisplayNonePlusForTracking($arr_data_from_file['pickup_date'][$i], true);


                    //DELIVERY COUNTRY
                    $array_temp_html['delivery_country'] = $arr_data_from_file['delivery_country'][$i];
                    if(!empty($arr_data_from_file['delivery_country'][$i])){
                        $key = array_search($arr_data_from_file['delivery_country'][$i], $this->common->get_field_data($array_country_db, 'name'));
                        if($key !== false){
                            $array_temp_insert['country_delivery'] = $array_country_db[$key]['id'];
                        }else{
                            //delivery_country KHONG ÄÃºng (False)
                            $check_insert = 0;
                            $array_temp_html['delivery_country'] = '<span style="color:red">Delivery Country is wrong.</span>';
                        }
                    }else{
                        //delivery_country KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['delivery_country'] = '<span style="color:red">Delivery Country can not null.</span>';
                    }
                    //DELIVERY CITY
                    //DELIVERY DISTRICT
                    $array_temp_html['delivery_city'] = $arr_data_from_file['delivery_city'][$i];
                    $array_temp_html['delivery_district'] = $arr_data_from_file['delivery_district'][$i];
                    if(!empty($arr_data_from_file['delivery_city'][$i])){
                        $key = array_search($arr_data_from_file['delivery_city'][$i], $this->common->get_field_data($array_province_db, 'branch_name'));
                        if($key !== false){
                            $array_temp_insert['city_delivery'] = $array_province_db[$key]['id'];
                            $array_temp_insert['dest_branch_code'] = $array_province_db[$key]['id'];
                            $array_temp_insert['dest_hub_code'] = $array_province_db[$key]['hub_id'];

                            //DELIVERY DISTRICT
                            $id_city_delivery = $array_province_db[$key]['id'];
                            if(!empty($arr_data_from_file['delivery_district'][$i])){
                                for ($j=0; $j < count($array_district_db); $j++) { 
                                    if(($array_district_db[$j]['province'] == $id_city_delivery) && ($array_district_db[$j]['name'] == $arr_data_from_file['delivery_district'][$i])){
                                        //insert
                                        $array_temp_insert['district_delivery'] = $array_district_db[$j]['id'];
                                    }
                                }
                                //DELIVERY DISTRICT KHONG ÄÃºng (False)
                                if(empty($array_temp_insert['district_delivery'])){
                                    $check_insert = 0;
                                    $array_temp_html['delivery_district'] = '<span style="color:red">Delivery District is wrong.</span>';
                                }
                            }
                        }else{
                            //DELIVERY CITY KHONG ÄÃºng (False)
                            $check_insert = 0;
                            $array_temp_html['delivery_city'] = '<span style="color:red">Delivery City is wrong.</span>';
                        }
                    }else{
                        //delivery_city KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['delivery_city'] = '<span style="color:red">Delivery City can not null.</span>';
                    }

                    //DELIVERY ADDRESS
                    if(!empty($arr_data_from_file['delivery_address'][$i])){
                        //insert
                        $array_temp_insert['address_delivery'] = $arr_data_from_file['delivery_address'][$i];
                        //html
                        $array_temp_html['delivery_address'] = $arr_data_from_file['delivery_address'][$i];
                    }else{
                        //DELIVERY ADDRESS KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['delivery_address'] = '<span style="color:red">Delivery Address can not null.</span>';
                    }
                    $array_temp_insert['delivery_address'] = $arr_data_from_file['delivery_address'][$i].', '.$arr_data_from_file['delivery_district'][$i].', '.$arr_data_from_file['delivery_city'][$i].', '.$arr_data_from_file['delivery_country'][$i];
                	$array_temp_insert['io_undelivery'] = $array_temp_insert['delivery_address'];

                    //DELIVERY COMPANY NAME
                    $array_temp_insert['company_delivery'] = $arr_data_from_file['delivery_company_name'][$i];
                    $array_temp_html['delivery_company_name'] = $arr_data_from_file['delivery_company_name'][$i];

                    //DELIVERY CONTACT NAME
                    if(!empty($arr_data_from_file['delivery_contact_name'][$i])){
                        $array_temp_insert['contact_delivery_name'] = $arr_data_from_file['delivery_contact_name'][$i];
                        $array_temp_html['delivery_contact_name'] = $arr_data_from_file['delivery_contact_name'][$i];
                    }else{
                        //DELIVERY CONTACT NAME KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['delivery_contact_name'] = '<span style="color:red">Delivery Contact Name can not null.</span>';
                    }

                    //DELIVERY CONTACT PHONE
                    if(!empty($arr_data_from_file['delivery_contact_phone'][$i])){
                        $array_temp_insert['phone_delivery'] = $arr_data_from_file['delivery_contact_phone'][$i];
                        $array_temp_html['delivery_contact_phone'] = $arr_data_from_file['delivery_contact_phone'][$i];
                    }else{
                        //DELIVERY CONTACT PHONE KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['delivery_contact_phone'] = '<span style="color:red">Delivery Contact Phone can not null.</span>';
                    }

                    //PAYMENT METHOD
                    //CHARGE TO
                    $array_temp_html['payment_method'] = $arr_data_from_file['payment_method'][$i];
                    $array_temp_html['charge_to'] = $arr_data_from_file['charge_to'][$i];
                    if(!empty($arr_data_from_file['payment_method'][$i])){
                        if(!empty($arr_data_from_file['charge_to'][$i])){
                            switch ($arr_data_from_file['charge_to'][$i]) {
                                case 'Shipper':
                                    $array_temp_insert['charge_to'] = 'shipper';
                                    if($arr_data_from_file['payment_method'][$i] == 'Cash'){
                                        $array_temp_insert['payment_method'] = 'cash';
                                    }else if($arr_data_from_file['payment_method'][$i] == 'Bank Transfer'){
                                        $array_temp_insert['payment_method'] = 'bank_transfer';
                                    }else if($arr_data_from_file['payment_method'][$i] == 'Other'){
                                        $array_temp_insert['payment_method'] = 'other';
                                    }else{
                                        $check_insert = 0;
                                        $array_temp_html['payment_method'] = '<span style="color:red">Payment Method is wrong.</span>';
                                    }
                                    break;
                                case 'Receiver':
                                    $array_temp_insert['charge_to'] = 'receiver';
                                    if($arr_data_from_file['payment_method'][$i] == 'Cash'){
                                        $array_temp_insert['payment_method'] = 'cash';
                                    }else if($arr_data_from_file['payment_method'][$i] == 'Bank Transfer'){
                                        $array_temp_insert['payment_method'] = 'bank_transfer';
                                    }else{
                                        $check_insert = 0;
                                        $array_temp_html['payment_method'] = '<span style="color:red">Payment Method is wrong.</span>';
                                    }
                                    break;
                                case 'Third Party':
                                    $array_temp_insert['charge_to'] = 'receiver';
                                    if($arr_data_from_file['payment_method'][$i] == 'Bank Transfer'){
                                        $array_temp_insert['payment_method'] = 'bank_transfer';
                                    }else{
                                        $check_insert = 0;
                                        $array_temp_html['payment_method'] = '<span style="color:red">Payment Method is wrong.</span>';
                                    }
                                    break;
                                default:
                                    $check_insert = 0;
                                    $array_temp_html['charge_to'] = '<span style="color:red">Charge To is wrong.</span>';
                                    break;
                            }
                        }else{
                            //CHARGE TO NAME KHONG DC RONG~
                            $check_insert = 0;
                            $array_temp_html['charge_to'] = '<span style="color:red">Charge To can not null.</span>';
                        }
                    }else{
                        //PAYMENT METHOD NAME KHONG DC RONG~
                        $check_insert = 0;
                        $array_temp_html['payment_method'] = '<span style="color:red">Payment Method can not null.</span>';
                    }

                    //SHIPMENT VALUE
                    if(is_numeric($arr_data_from_file['shipment_value'][$i])){
                        $array_temp_insert['shipment_value'] = $arr_data_from_file['shipment_value'][$i];
                        $array_temp_html['shipment_value'] = number_format($arr_data_from_file['shipment_value'][$i]);
                    }else{
                        $check_insert = 0;
                        $array_temp_html['shipment_value'] = '<span style="color:red">Shipment Value is number.</span>';
                    }

                    //SPECIAL INSTRUCTION
                    $array_temp_insert['specical_instruction'] = $arr_data_from_file['special_instruction'][$i];
                    $array_temp_html['special_instruction'] = $arr_data_from_file['special_instruction'][$i];

                    //SPECICAL REQ
                    $array_temp_insert['specical_req'] = $arr_data_from_file['specical_req'][$i];
                    $array_temp_html['specical_req'] = $arr_data_from_file['specical_req'][$i];

                    //BOOKING NOTE
                    $array_temp_insert['booking_note'] = $arr_data_from_file['booking_note'][$i];
                    $array_temp_html['booking_note'] = $arr_data_from_file['booking_note'][$i];

                    if ($check_insert == 1) {
                        $array_temp_html['is_check'] = 1;
                        $array_temp_html['is_enable'] = 1;
                        $array_temp_html['booking_no'] = $array_booking_no[$i];
                        $array_temp_insert['name'] = $array_booking_no[$i];
                        $array_temp_html['checkbox'] = '<input type="checkbox" checked="checked" onclick="checkCheckbox(this)"/>';
                        $array_insert[] = $array_temp_insert;
                    } else {
                        $array_temp_html['is_check'] = 0;
                        $array_temp_html['is_enable'] = 0;
                        $array_temp_html['booking_no'] = '<span style="color:red">Record can not import</span>';
                        $array_temp_html['checkbox'] = '<img src="/assets/images/icon-not-ok.png" style="height: 20px;"/>';
                    }
                    $array_html[] = $array_temp_html;
                }


                //LAY ACCOUNT SHIPPER
                // var_dump($user);
            	$dataApi['account_id'] = $user['account_id'];
                $account_db_user_api = $this->callapi->call($urlApi."getAccountByID",$dataApi);
				$account_db_user = $account_db_user_api['status'] == 1 ? $account_db_user_api['data'] : array();

				$dataApi['lead_id'] = $user['lead_id'];
                $lead_db_user_api = $this->callapi->call($urlApi."getLeadById",$dataApi);
				$lead_db_user = $lead_db_user_api['status'] == 1 ? $lead_db_user_api['data'] : array();

                $data_insert_contact = array();
                $data_insert_account_contact = array();
                // var_dump($account_db_user);die;
                if(!empty($account_db_user)){
                    for ($i=0; $i < count($array_insert); $i++) { 
                        $array_insert[$i]['company_id'] = $account_db_user[0]['id'];
                        //SHIPPER
                        $array_insert[$i]['parent_shipper_options'] = 'Accounts';
                        $array_insert[$i]['shipper_company_name'] = $account_db_user[0]['name'];

                        //PAYER
                        $array_insert[$i]['parent_payer_options']= 'Accounts';
                        $array_insert[$i]['payer_id'] = $account_db_user[0]['id'];
                        $array_insert[$i]['acc_no'] = $account_db_user[0]['account_no'];
                        $array_insert[$i]['customer_code'] = $account_db_user[0]['customer_code'];
                    }
                }else if(!empty($lead_db_user)){
                    for ($i=0; $i < count($array_insert); $i++) { 
                        //SHIPPER
                        $array_insert[$i]['parent_shipper_options'] = 'Leads';
                        $array_insert[$i]['shipper_company_name'] = $lead_db_user[0]['account_name'];
                        //PAYER
                        $array_insert[$i]['parent_payer_options']= 'Leads';
                        $array_insert[$i]['lead_payer_id'] = $lead_db_user[0]['id'];
                        $array_insert[$i]['acc_no'] = '';//Leads thÃ¬ Ä‘á»ƒ rá»—ng
                        $array_insert[$i]['customer_code'] = '';//Leads thÃ¬ Ä‘á»ƒ rá»—ng
                    }
                }
            }
            echo json_encode(array(
                'status' => true,
                'html' => $array_html,
                'insert' => $array_insert,
                'insert_contact' => $data_insert_contact,
                'insert_account_contact' => $data_insert_account_contact,
            ));
        }else{
            echo json_encode(array(
                'status' => false,
                'message' => 'Data too large(Maximum 2000 records)',
            ));
        }
	}
	public function insertBooking(){		
        $login = $this->session->userdata('login');
        $user 	= $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');

		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time()
		);

		$data_insert = json_decode(base64_decode($this->input->post('data_insert')),true);
        // $data_list_id = json_decode(base64_decode($this->input->post('data_list_id')),true);
        // $data_insert_contact = json_decode(base64_decode($this->input->post('data_insert_contact')), true);
        // $data_insert_account_contact = json_decode(base64_decode($this->input->post('data_insert_account_contact')), true);
        
        $date_now = ConvertDateTime::convertTimeToTimeDBCRM(mdate('%Y-%m-%d %H:%i:%s', now()));
        $count = count($data_insert);
        if($count > 0) {
            for ($i = 0; $i < $count; $i++) {
	        	$name_api = $this->callapi->call($urlApi."generateBookingNo",$dataApi);
				$name = $name_api['status'] == 1 ? $name_api['data'] : array();
                $data_insert[$i]['name'] = $name;
                $data_insert[$i]['status'] = 'Processing';
                $data_insert[$i]['status_reason'] = '1100';
                $data_insert[$i]['date_entered'] = $date_now;
                $data_insert[$i]['date_modified'] = $date_now;
                $data_insert[$i]['booking_date'] = $date_now;
                $data_insert[$i]['modified_user_id'] = $login['id'];
                $data_insert[$i]['created_by'] = $login['id'];
                $data_insert[$i]['assigned_user_id'] = $login['id'];
                $data_insert[$i]['deleted'] = 0;
            }
            $dataApi['array_insert'] = $data_insert;
            $result_insert = $this->callapi->call($urlApi."insertBooking",$dataApi);
			// $name = $name_api['status'] == 1 ? $name_api['data'] : array();
            if ($result_insert['status'] == 1) {
                for ($i=0; $i < count($data_insert); $i++) { 
                	$dataApi['booking_id'] = $data_insert[$i]['id'];
                	$update_duplicate = $this->callapi->call($urlApi."checkDuplicate",$dataApi);
                }
                echo 1;
            } else {
                echo 0;
            }
        }else{
            echo 'Data is empty';
        }
	}
	public function getDetailBooking(){
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'booking_id' => $this->input->post('booking_id')
        );
        $detail_booking = $this->callapi->call($urlApi."getDetailBooking",$dataApi);
        $detail_booking_data = $detail_booking['status'] == 1 ? $detail_booking['data'] : array();
    	$detail_booking_data['booking_date'] = date('d/m/Y',strtotime($detail_booking_data['booking_date']));
    	$detail_booking_data['pickup_date'] = date('d/m/Y',strtotime($detail_booking_data['pickup_date']));
    	$detail_booking_data['weight'] = $detail_booking_data['weight']." kg";
    	switch ($detail_booking_data['payment_method']) {
    		case 'cash':
    			$detail_booking_data['payment_method'] = 'Cash';
    			break;
    		case 'bank_transfer':
    			$detail_booking_data['payment_method'] = 'Bank Transfer';
    			break;
			case 'other':
    			$detail_booking_data['payment_method'] = 'Other';
    			break;
    		default:
    			break;
    	}
    	switch ($detail_booking_data['charge_to']) {
    		case 'shipper':
    			$detail_booking_data['charge_to'] = 'Shipper';
    			break;
    		case 'receiver':
    			$detail_booking_data['charge_to'] = 'Receiver';
    			break;
			case '3rd':
    			$detail_booking_data['charge_to'] = '3rd Party';
    			break;
    		default:
    			break;
    	}
    	if ($this->session->get_userdata()['site_lang']=='vietnamese') {
    		$detail_booking_data['shipment_type'] = $detail_booking_data['shipment_type_vn'];
    	}
        echo json_encode($detail_booking_data);
    }
    public function ExportBookingToExcel(){
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'arr_id' => $this->input->post('arr_id')
        );
        $time = time();
        $dataTable = $this->callapi->call($urlApi."getListBookingForExportExcel",$dataApi);
        $dataTable = $dataTable['data'];

        $objPHPExcel = new PHPExcel();
        $objSheet = $objPHPExcel->getSheet(0);

        #region STYLE
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '0C0D0E')
                )
            )
        );
        $style_header = array(
            'font' => array('size' => 11,'color' => array('rgb' => '000000')),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '92D050')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        $objSheet->getColumnDimension('A')->setAutoSize(false);
        $objSheet->getColumnDimension('B')->setAutoSize(false);
        $objSheet->getColumnDimension('C')->setAutoSize(false);
        $objSheet->getColumnDimension('D')->setAutoSize(false);
        $objSheet->getColumnDimension('E')->setAutoSize(false);
        $objSheet->getColumnDimension('F')->setAutoSize(false);
        $objSheet->getColumnDimension('G')->setAutoSize(false);
        $objSheet->getColumnDimension('H')->setAutoSize(false);
        $objSheet->getColumnDimension('I')->setAutoSize(false);

        $objSheet->getColumnDimension('A')->setWidth(8.71);
        $objSheet->getColumnDimension('B')->setWidth(18.71);
        $objSheet->getColumnDimension('C')->setWidth(35.71);
        $objSheet->getColumnDimension('D')->setWidth(13.71);
        $objSheet->getColumnDimension('E')->setWidth(35.71);
        $objSheet->getColumnDimension('F')->setWidth(12.71);
        $objSheet->getColumnDimension('G')->setWidth(30.71);
        $objSheet->getColumnDimension('H')->setWidth(16.71);
        $objSheet->getColumnDimension('I')->setWidth(13.71);
        #endregion STYLE

        // Set properties
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'Order No')
            ->setCellValue('C1', 'Pickup Address')
            ->setCellValue('D1', 'Receiver Name')
            ->setCellValue('E1', 'Receiver Address')
            ->setCellValue('F1', 'Receiver Phone')
            ->setCellValue('G1', 'Special Require')
            ->setCellValue('H1', 'Pickup Date')
            ->setCellValue('I1', 'Status');

        $current_row =1;
        for ($i=1; $i<=count($dataTable);$i++){
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.($i+1), $i)
                ->setCellValue('B'.($i+1), $dataTable[$i-1]['name'])
                ->setCellValue('C'.($i+1), $dataTable[$i-1]['pickup_address'])
                ->setCellValue('D'.($i+1), $dataTable[$i-1]['receiver_name'])
                ->setCellValue('E'.($i+1), $dataTable[$i-1]['receiver_address'])
                ->setCellValue('F'.($i+1), $dataTable[$i-1]['receiver_phone'])
                ->setCellValue('G'.($i+1), $dataTable[$i-1]['specical_req'])
                ->setCellValue('H'.($i+1), ConvertDateTime::convertDateTimeDisplay($dataTable[$i-1]['pickup_date']))
                ->setCellValue('I'.($i+1), $dataTable[$i-1]['status']);
            $current_row = $i+2;
        }

        $objSheet->getStyle('A1:I1')->applyFromArray($style_header);
        $objSheet->getStyle('A1:I'.$current_row)->applyFromArray($style_border);

        $objPHPExcel->getActiveSheet()->setTitle('Booking');
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($this->config->item('url_folder_upload').'ListBooking_'.$time.'.xlsx');

        $response = array(
            'success' => true,
            'url' => '/uploads/ListBooking_'.$time.'.xlsx'
        );

        header('Content-type: application/json');

        // and in the end you respond back to javascript the file location
        echo json_encode($response);
    }
    public function printBooking(){
    	$urlApi = $this->config->item('url_api_portal');
    	$dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'arr_id' => $this->input->post('arr_id')
        );
        $list_booking = $this->callapi->call($urlApi."getBookingByListID",$dataApi);
        $data['booking'] = $list_booking['status'] == 1 ? $list_booking['data'] : array();
        for($i = 0;$i < count($data['booking']);$i++){
            if($data['booking'][$i]['awb_online'] != null && $data['booking'][$i]['awb_online'] != ''){

            }else{
                $awb_online = 'ISPL' . date('ymdHi') . rand(0, 200) ;
                $data['booking'][$i]['awb_online'] = $awb_online;
                $array_update = array(
                	'booking_id'=>$data['booking'][$i]['id'],
                	'awb_online'=>$awb_online,
                	'timestamp'	=>time()
                );
                $this->callapi->call($urlApi."updateAwbOnlineBooking",$array_update);
            }
        }
        $this->load->view('/print/template_booking_airway_bill.php', $data);
    }
    public function createMultiOrderAndPrintAWB(){
		$user 	= $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');

		$para = $this->input->post();

		$data = array();
		for ($i=0; $i < count($para['order_no']) ; $i++) {
            $data[$i]['account_id'] 			 = $user['accounts_id'];
            $data[$i]['leads_id'] 			 	 = $user['leads_id'];
            $data[$i]['shipper_contact_name']  	 = $para['short_name'];
            $data[$i]['phone_shipper'] 		 	 = $para['mobile_phone'];
            $data[$i]['pickup_address'] 		 = $para['shipper_address'];
            $data[$i]['pickup_ward'] 	 		 = $para['shipper_ward'];
            $data[$i]['pickup_district'] 	 	 = $para['shipper_district'];
            $data[$i]['pickup_city'] 	  		 = $para['shipper_city'];
            $data[$i]['pickup_country'] 	 	 = $para['shipper_country'];
            $data[$i]['order_no'] 				 = $para['order_no'][$i];
            $data[$i]['weight'] 				 = $para['weight'][$i];
            $data[$i]['charge_to'] 			 	 = $para['charge_to'][$i];
            $data[$i]['cod_value'] 			 	 = $para['cod_value'][$i];
            $data[$i]['payment_method'] 		 = $para['payment_method'][$i];
            $data[$i]['shipment_value'] 		 = $para['shipment_value'][$i];
            $data[$i]['pcs'] 		             = $para['pcs'][$i];
            $data[$i]['description'] 			 = $para['description'][$i];
            $data[$i]['receiver_name'] 		 	 = $para['receiver_name'][$i];
            $data[$i]['receiver_phone'] 		 = $para['receiver_phone'][$i];
            // $data[$i]['receiver_country'] 	 	 = $para['receiver_country'][$i];
            $data[$i]['receiver_country'] 	 	 = 'VIETNAM';
            $data[$i]['receiver_city'] 		 	 = $para['receiver_city'][$i];
            $data[$i]['receiver_district'] 	 	 = $para['receiver_district'][$i];
            $data[$i]['receiver_ward'] 	 		 = $para['receiver_ward'][$i];
            $data[$i]['receiver_street'] 		 = $para['receiver_address'][$i];
            $data[$i]['service'] 		 	 	 = $para['service'][$i];
            $data[$i]['is_dox'] 		 	 	 = $para['is_dox'][$i];
            $data[$i]['is_cod'] 		 	 	 = $para['cod_value'][$i] != "" && $para['cod_value'][$i] != 0 ? 1 : 0;
            $data[$i]['save_address'] 		 	 = $para['save_address'][$i];
			if ($para['save_address'][$i] = 1) {
				$dataConsigneeAddress[] = array(
					'user_id'		=> $user['id'],
					'created_by' 	=> $user['id'],
					'date_entered' 	=> date('Y-m-d h:i:s'),
					'user_name'     => $para['receiver_name'][$i],
			        'user_phone'    => $para['receiver_phone'][$i],
			        'user_street'   => $para['receiver_address'][$i],
			        'user_country'  => $para['receiver_country'][$i],
			        'user_city'     => $para['receiver_city'][$i],
			        'user_district' => $para['receiver_district'][$i],
			        'user_ward'     => $para['receiver_ward'][$i],
			        'is_default'    => 0,
			        'deleted'       => 0
				);
			}
		}
		$dataApi = array(
			'userId' 			 => $user?$user['id']:'',
			'origin_branch_code' => $user?$user['branch_code']:'',
			'origin_hub_code' 	 => $user?$user['hub_code']:'',
			'timestamp' 		 => time(),
			'data'		 		 => base64_encode(json_encode($data)),
			'account_id'		 => $user['accounts_id'],
            'leads_id'           => $user['leads_id']
		);
		$resultApi = $this->callapi->call($urlApi."createMultiOrderForCustomerPortal",$dataApi);
		if ($resultApi['status'] == 1 && isset($dataConsigneeAddress) && count($dataConsigneeAddress) >0) {
			$dataApi['parameters'] = $dataConsigneeAddress;
			$this->callapi->call($urlApi."saveAddressConsignee",$dataApi);
		}elseif($resultApi['status'] == -1) {
            echo $this->data['language']['error_duplicate_tracking_no_1'].' "'.$resultApi['data'].'" '.$this->data['language']['error_duplicate_tracking_no_2']; die();
        }
		$resultData = $resultApi['status'] == 1 ? $resultApi['data'] : array();
		if (count($resultData)>0) {
			$dataApi['arr_id'] = $resultData;
			$list_booking = $this->callapi->call($urlApi."getBookingByListID",$dataApi);
        	$data['booking'] = $list_booking['status'] == 1 ? $list_booking['data'] : array();
        	for($i = 0;$i < count($data['booking']);$i++){
	            if($data['booking'][$i]['awb_online'] != null && $data['booking'][$i]['awb_online'] != ''){

	            }else{
	                $awb_online = 'ISPL' . date('ymdHi') . rand(0, 200) ;
	                $data['booking'][$i]['awb_online'] = $awb_online;
	                $array_update = array(
	                	'booking_id'=>$data['booking'][$i]['id'],
	                	'awb_online'=>$awb_online,
	                	'timestamp'	=>time()
	                );
	                $this->callapi->call($urlApi."updateAwbOnlineBooking",$array_update);
	            }
	        }
        	$this->load->view('/print/template_booking_airway_bill.php', $data);
		}
	}
	public function duplicateOrder($id){
		$this->data['module'] = "booking/create";
		$this->data['base_url'] = $this->config->base_url();

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => $time,
			'account_id'=> $user['accounts_id'],
			'booking_id' => $id,
			'country' => 'VIETNAM'
		);
		$account = $this->callapi->call($urlApi."getAddressDefaultForCustomerPortal",$dataApi);
		$dataAccount = $account['status'] == 1 ? $account['data'] : array();
		$booking_api = $this->callapi->call($urlApi."getBookingDataForDuplicate",$dataApi);
		$dataBooking = $booking_api['status'] == 1 ? $booking_api['data'] : array();

		$this->data['dataDuplicate'] = array(
			'is_dox' => $dataBooking['service'],
			'weight' => $dataBooking['weight'],
			'charge_to' => $dataBooking['charge_to'],
			'cod_value' => $dataBooking['cod_value'],
			'payment_method' => $dataBooking['payment_method'],
			'pcs_value' => $dataBooking['pcs_value'],
			'shipment_value' => $dataBooking['shipment_value'],
			'tracking_no' => $dataBooking['tracking_no'],
			'booking_note' => $dataBooking['booking_note'],
			'receiver_name' => $dataBooking['contact_delivery_name'],
			'receiver_phone' => $dataBooking['phone_delivery'],
			'country' => $dataBooking['country_delivery'],
			'city' => $dataBooking['city_delivery'],
			'district' => $dataBooking['district_delivery'],
			'ward' => $dataBooking['ward_delivery'],
			'zip_code' => $dataBooking['phone_delivery'],
			'receiver_street' => $dataBooking['delivery_address']
		);
		$dataApi['city'] = $dataBooking['city_delivery'];
		$dataApi['district'] = $dataBooking['district_delivery'];

		$country = $this->callapi->call($urlApi."getCountryForCreateBooking",$dataApi);
		$city = $this->callapi->call($urlApi."getCityForCreateBooking",$dataApi);
		$district = $this->callapi->call($urlApi."getDistrictForCreateBooking",$dataApi);
		$ward = $this->callapi->call($urlApi."getWardForCreateBooking",$dataApi);

		$optionCityValue = $dataBooking['city_delivery'];
		$optionDistrictValue = $dataBooking['district_delivery'];
		$optionWardValue = $dataBooking['ward_delivery'];

		$countryOption = $this->common->generateSelectOption($country['status'] == 1 ?$country['data']:array(),array(),'VIETNAM','country_code','country');
		$cityOption = $this->common->generateSelectOption($city['status'] == 1 ?$city['data']:array(),array(),$optionCityValue,'province_code','city');
		$districtOption = $this->common->generateSelectOption($district['status'] == 1 ?$district['data']:array(),array(),$optionDistrictValue,'district_code','district');
		$wardOption = $this->common->generateSelectOption($ward['status'] == 1 ?$ward['data']:array(),array(),$optionWardValue,'code','ward');

		$this->data['countryOption'] = $countryOption;
		$this->data['cityOption'] = $cityOption;
		$this->data['dataDuplicate']['districtOption'] = $districtOption;
		$this->data['dataDuplicate']['wardOption'] = $wardOption;
		$this->data['dataAccount'] = $dataAccount;
		$this->load->view('index', $this->data);
	}
    public function createInternational(){
        $this->data['module'] = "booking/create-international";
        $this->data['base_url'] = $this->config->base_url();

        $user = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $time = time();
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => $time,
            'account_id'=>$user['accounts_id'],
            'country' => 'VIETNAM'
        );
        $account = $this->callapi->call($urlApi."getAddressDefaultForCustomerPortal",$dataApi);
        $dataAccount = $account['status'] == 1 ? $account['data'] : array();
        $this->data['dataAccount'] = $dataAccount;

        $country = $this->callapi->call($urlApi."getCountryForCreateBooking",$dataApi);
        $city = $this->callapi->call($urlApi."getCityForCreateBooking",$dataApi);
        $countryOption = $this->common->generateSelectOption($country['status'] == 1 ?$country['data']:array(),array(),'VIETNAM','country_code','country');
        $cityOption = $this->common->generateSelectOption($city['status'] == 1 ?$city['data']:array(),array(),null,'province_code','city');
        $this->data['countryOption'] = $countryOption;
        $this->data['cityOption'] = $cityOption;

        $this->load->view('index', $this->data);
    }
    public function createOrderInternational(){
        $user 	= $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $language = $this->session->userdata('site_lang');

        $parameters = $this->input->post('parameters');
        $parameters_decode = json_decode(base64_decode($parameters),true);

        if ($parameters_decode['receiver_name']=="") {
            echo json_encode(array('status' => 2,'message'=> $this->data['language']['txt_create_booking_receiver_name_required'] ));die();
        }
        if ($parameters_decode['receiver_phone']=="") {
            echo json_encode(array('status' => 3,'message'=> $this->data['language']['txt_create_booking_receiver_phone_required']  ));die();
        }else if(strlen($parameters_decode['receiver_phone']) < 10 || strlen($parameters_decode['receiver_phone']) > 11) {
            echo json_encode(array('status' => 2,'message'=> $this->data['language']['txt_create_booking_phone_number_invalid'] ));die();
        }

        if ($parameters_decode['receiver_country']=="") {
            echo json_encode(array('status' => 4,'message'=> $this->data['language']['txt_create_booking_receiver_country_required'] ));die();
        }
        if ($parameters_decode['receiver_city']=="") {
            echo json_encode(array('status' => 5,'message'=> $this->data['language']['txt_create_booking_receiver_city_required'] ));die();
        }
        if ($parameters_decode['receiver_street']=="") {
            echo json_encode(array('status' => 7,'message'=> $this->data['language']['txt_create_booking_receiver_street_required'] ));die();
        }
        if (!isset($parameters_decode['service_code']) || $parameters_decode['service_code']=="") {
            echo json_encode(array('status' => 9,'message'=> $this->data['language']['txt_create_booking_service_required'] ));die();
        }
        $dataApi = array(
            'userId' 			 => $user?$user['id']:'',
            'origin_branch_code' => $user?$user['branch_code']:'',
            'origin_hub_code' 	 => $user?$user['hub_code']:'',
            'timestamp' 		 => time(),
            'parameters'		 => $parameters,
            'account_id'		 => $user['accounts_id'],
            'leads_id'           => $user['leads_id']
        );
        $resultApi = $this->callapi->call($urlApi."createOrderInternationalForCustomerPortal",$dataApi);
        echo json_encode($resultApi);die;
        if($resultApi['status'] == 1) {
            $resultApi['message'] = $this->data['language']['txt_create_booking_success'];
        }elseif($resultApi['status'] == 2) {
            $resultApi['message'] = $this->data['language']['txt_create_booking_parameter_wrong'];
        }elseif($resultApi['status'] == 3) {
            $resultApi['message'] = $this->data['language']['txt_create_booking_parameter_missing'];
        }elseif($resultApi['status'] == 4) {
            $resultApi['message'] = $this->data['language']['txt_create_booking_parameter_failed'];
        }elseif($resultApi['status'] == 5) {
            $resultApi['message'] = $this->data['language']['txt_create_booking_parameter_address_wrong'];
        }elseif($resultApi['status'] == -1) {
            $resultApi['message'] = $this->data['language']['txt_create_booking_duplicate_tracking_no'];
        }
        if ($parameters_decode['save_address'] == 1) {
            $dataConsigneeAddress = array(
                'user_id'		=> $user['id'],
                'created_by' 	=> $user['id'],
                'date_entered' 	=> date('Y-m-d h:i:s'),
                'user_name'     => $parameters_decode['receiver_name'],
                'user_phone'    => $parameters_decode['receiver_phone'],
                'user_street'   => $parameters_decode['receiver_street'],
                'user_country'  => $parameters_decode['receiver_country'],
                'user_city'     => $parameters_decode['receiver_city'],
                'user_district' => $parameters_decode['receiver_district'],
                'user_ward'     => $parameters_decode['receiver_ward'],
                'postal_code'   => $parameters_decode['receiver_zip_code'],
                'is_default'    => 0,
                'deleted'       => 0
            );
            $dataApi['parameters'] = $dataConsigneeAddress;
            $this->callapi->call($urlApi."saveAddressConsignee",$dataApi);
        }
        echo json_encode($resultApi);die();
    }
}