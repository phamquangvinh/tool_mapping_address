<?php
/**
 * Created by PhpStorm.
 * User: dieter.tuan
 * Date: 1/8/2018
 * Time: 4:07 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //MODEL

        //LIBRARY
        $this->load->library('callapi');
        $this->load->library('dataconvert');
        $this->load->library('convertdatetime');
        $this->load->library('common');
        $this->load->library('PHPExcel');

        //LANGUAGE
        $this->lang->load('message',$this->session->userdata('site_lang'));
        $this->data['menu'] = "service";
        $this->data["language"] = array_merge(
            $this->lang->line('language'),
            $this->lang->line('login'),
            $this->lang->line('menu'),
            $this->lang->line('home'),
            $this->lang->line('service'),
            $this->lang->line('booking'),
            $this->lang->line('footer')
        );
    }
    public function listShipment(){
        $this->data['module']   = "service/service-list-shipment";
        $this->data['base_url'] = $this->config->base_url();

        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'user_id'           => $user?$user['id']:'',
            'userId'            => $user?$user['id']:'',
            'accounts_id'       => $user['accounts_id'],
            'leads_id'          => $user['leads_id'],
            'timestamp'         => time()
        );

        $status         = $this->callapi->call($urlApi."getStatus",$dataApi);
        $dataStatus     = $status['status'] == 1 ? $status['data'] : array();
        $service        = $this->callapi->call($urlApi."getServiceName",$dataApi);
        $dataService    = $service['status'] == 1 ? $service['data'] : array();

        if($_POST) {
            $date_entered_from  = $this->input->post('date_entered_from');
            if (!empty($date_entered_from)){
                $date_entered_from = date('Y-m-d 00:00:01', strtotime(str_replace('/', '-', $date_entered_from)));
            }
            $date_entered_to    = $this->input->post('date_entered_to');
            if (!empty($date_entered_to)){
                $date_entered_to = date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $date_entered_to)));
            }
            $date_delivery_from = $this->input->post('delivery_date_from');
            if (!empty($date_delivery_from)){
                $date_delivery_from = date('Y-m-d 00:00:01', strtotime(str_replace('/', '-', $date_delivery_from)));
            }
            $date_delivery_to   = $this->input->post('delivery_date_to');
            if (!empty($date_delivery_to)){
                $date_delivery_to = date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $date_delivery_to)));
            }
            $dataApi['awb_tracking_no']   = $this->input->post('awb_tracking_no');
            $dataApi['receiver_name']     = $this->input->post('receiver_name');
            $dataApi['receiver_phone']    = $this->input->post('receiver_phone');
            $dataApi['service_id']        = $this->input->post('service');
            $dataApi['status']            = $this->input->post('status');
            $dataApi['status_payment']    = $this->input->post('status_payment');
            $dataApi['date_entered_from'] = $date_entered_from;
            $dataApi['date_entered_to']   = $date_entered_to;
            $dataApi['date_delivery_from']= $date_delivery_from;
            $dataApi['date_delivery_to']  = $date_delivery_to;

            $this->data['dataFilter']['shipment_code']      = $this->input->post('awb_tracking_no');
            $this->data['dataFilter']['receiver_name']      = $this->input->post('receiver_name');
            $this->data['dataFilter']['receiver_phone']     = $this->input->post('receiver_phone');
            $this->data['dataFilter']['date_entered_from']  = $this->input->post('date_entered_from');
            $this->data['dataFilter']['date_entered_to']    = $this->input->post('date_entered_to');
            $this->data['dataFilter']['date_delivery_from'] = $this->input->post('delivery_date_from');
            $this->data['dataFilter']['date_delivery_to']   = $this->input->post('delivery_date_to');

            $serviceValue = $this->input->post('service');
            $statusValue = $this->input->post('status');

            $data_api = $this->callapi->call($urlApi."getListShipment",$dataApi);
            $dataTable = $data_api['status'] == 1 ? $data_api['data'] : array();
            for($i = 0 ; $i < count($dataTable) ; $i++){
                $dataTable[$i]['check_box']         = '<input type="checkbox" name="check_box[]" value='.$dataTable[$i]['shipment_id'].'>';
                $dataTable[$i]['no']                = $i+1;
                $dataTable[$i]['awb_tracking_no']   = '<a href="#" data-toggle="modal" data-target="#shipment-detail-modal" data-detail_shipment="'.$dataTable[$i]['shipment_id'].'">'.$dataTable[$i]['awb_tracking_no'].'</a>';
                $dataTable[$i]['services']          = $this->session->userdata('site_lang')=='english' ? $dataTable[$i]['services'] : $dataTable[$i]['services_vn'];
                $dataTable[$i]['date_entered']      = $dataTable[$i]['date_entered'] && $dataTable[$i]['date_entered']!= null && $dataTable[$i]['date_entered'] != '0000-00-00 00:00:00'? date('d/m/Y h:i:s A',strtotime('+7 hours',strtotime($dataTable[$i]['date_entered']))) : '';
                $dataTable[$i]['delivery_date']     = $dataTable[$i]['delivery_date'] && $dataTable[$i]['delivery_date']!= null && $dataTable[$i]['delivery_date'] != '0000-00-00 00:00:00' ? date('d/m/Y h:i:s A',strtotime('+7 hours',strtotime($dataTable[$i]['delivery_date']))) : '';
                $dataTable[$i]['cod_value']         = $dataTable[$i]['cod_value']? number_format($dataTable[$i]['cod_value'])." VND" : '';
                $dataTable[$i]['total_revenue']     = $dataTable[$i]['total_revenue'] ? number_format($dataTable[$i]['total_revenue'])." VND" : '';
            }
        }else{
            $dataApi['awb_tracking_no']   = '';
            $dataApi['receiver_name']     = '';
            $dataApi['receiver_phone']    = '';
            $dataApi['service_id']        = '';
            $dataApi['status']            = '';
            $dataApi['status_payment']    = '';
            $dataApi['date_entered_from'] = date('Y-m-d 00:00:01',strtotime('-7 days',time()));
            $dataApi['date_entered_to']   = date('Y-m-d 23:59:50');
            $dataApi['date_delivery_from']= '';
            $dataApi['date_delivery_to']  = '';

            $serviceValue = null;
            $statusValue = null;
            $this->data['dataFilter']['date_entered_from']  = date('d/m/Y' ,strtotime('-7 days',time()));
            $this->data['dataFilter']['date_entered_to']    = date('d/m/Y');

            $data_api = $this->callapi->call($urlApi."getListShipment",$dataApi);
            $dataTable = $data_api['status'] == 1 ? $data_api['data'] : array();
            for($i = 0 ; $i < count($dataTable) ; $i++){
                $dataTable[$i]['check_box']         = '<input type="checkbox" name="check_box[]" value='.$dataTable[$i]['shipment_id'].'>';
                $dataTable[$i]['no']                = $i+1;
                $dataTable[$i]['awb_tracking_no']   = '<a href="#" data-toggle="modal" data-target="#shipment-detail-modal" data-detail_shipment="'.$dataTable[$i]['shipment_id'].'">'.$dataTable[$i]['awb_tracking_no'].'</a>';
                $dataTable[$i]['services']          = $this->session->userdata('site_lang')=='english' ? $dataTable[$i]['services'] : $dataTable[$i]['services_vn'];
                $dataTable[$i]['date_entered']      = $dataTable[$i]['date_entered'] && $dataTable[$i]['date_entered']!= null && $dataTable[$i]['date_entered'] != '0000-00-00 00:00:00'? date('d/m/Y h:i:s A',strtotime('+7 hours',strtotime($dataTable[$i]['date_entered']))) : '';
                $dataTable[$i]['delivery_date']     = $dataTable[$i]['delivery_date'] && $dataTable[$i]['delivery_date']!= null && $dataTable[$i]['delivery_date'] != '0000-00-00 00:00:00' ? date('d/m/Y h:i:s A',strtotime('+7 hours',strtotime($dataTable[$i]['delivery_date']))) : '';
                $dataTable[$i]['cod_value']         = $dataTable[$i]['cod_value']? number_format($dataTable[$i]['cod_value'])." VND" : '';
                $dataTable[$i]['total_revenue']     = $dataTable[$i]['total_revenue'] ? number_format($dataTable[$i]['total_revenue'])." VND" : '';
            }
        }

        $this->data['dataFilter']['statusOption'] = $this->common->generateSelectOption(
            $dataStatus, array(), $statusValue, 'status', 'status'
        );
        $this->data['dataFilter']['serviceOptionEng'] = $this->common->generateSelectOption(
            $dataService, array(), $serviceValue, 'id', 'name'
        );
        $this->data['dataFilter']['serviceOptionVn'] = $this->common->generateSelectOption(
            $dataService, array(), $serviceValue, 'id', 'name_vn'
        );
        $this->data['dataTable'] = base64_encode(json_encode($dataTable));
        $this->load->view('index', $this->data);
    }
    public function listSOA(){
        $this->data['module']   = "service/service-list-soa";
        $this->data['base_url'] = $this->config->base_url();
        $lang = $this->data["language"];

        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'user_id'              => $user?$user['id']:'',
            'userId'               => $user?$user['id']:'',
            'accounts_id'          => $user['accounts_id'],
            'timestamp'            => time()
        );

        $array_status = array(
            array(
                'value' => 'sent',
                'text'  => $this->session->userdata('site_lang')=='english' ? 'Sent' : 'Đã gửi'
            ),
            array(
                'value' => 'confirmed',
                'text'  => $this->session->userdata('site_lang')=='english' ? 'Confirmed' : 'Đã xác nhận'
            ),
            array(
                'value' => 'approved',
                'text'  => $this->session->userdata('site_lang')=='english' ? 'Aprroved' : 'Đã duyệt'
            ),
            array(
                'value' => 'paid',
                'text'  => $this->session->userdata('site_lang')=='english' ? 'Paid' : 'Đã thanh toán'
            ),
        );

        if($_POST) {
            $from_date              = $this->input->post('from_date');
            $to_date                = $this->input->post('to_date');
            $dataApi['soa_code']    = $this->input->post('soa_code');
            $dataApi['status']      = $this->input->post('status');
            $dataApi['from_date']   = !empty($from_date) ? $this->dataconvert->convertDateDB($from_date) : '';
            $dataApi['to_date']     = !empty($to_date) ? $this->dataconvert->convertDateDB($to_date) : '';

            $dateRangeValue = $this->input->post('date_range');
            $statusValue = $this->input->post('status');
            $this->data['dataFilter']['soa_code'] = $this->input->post('soa_code');
            $this->data['dataFilter']['dateRangeOption'] = $dateRangeValue;
            $this->data['dataFilter']['date_from'] = $this->input->post('from_date');
            $this->data['dataFilter']['date_to'] = $this->input->post('to_date');

            $data_api = $this->callapi->call($urlApi."getListSOA",$dataApi);
            $dataTable = $data_api['status'] == 1 ? $data_api['data'] : array();

            for($i = 0 ; $i < count($dataTable) ; $i++){
                $dataTable[$i]['check_box']     = $dataTable[$i]['id'];
                $dataTable[$i]['no']            = $i+1;
                $dataTable[$i]['soa_code']      = '<a href="/detail-soa/'.$dataTable[$i]['id'].'">'.$dataTable[$i]['soa_code'].'</a>';
                $dataTable[$i]['from_date']     = $dataTable[$i]['from_date'] && $dataTable[$i]['from_date']!='0000-00-00' ? date('d/m/Y',strtotime($dataTable[$i]['from_date'])) : '';
                $dataTable[$i]['to_date']       = $dataTable[$i]['to_date'] && $dataTable[$i]['to_date']!='0000-00-00' ? date('d/m/Y',strtotime($dataTable[$i]['to_date'])) : '';
                $dataTable[$i]['paid_date']     = $dataTable[$i]['paid_date'] && $dataTable[$i]['paid_date']!='0000-00-00' ? date('d/m/Y',strtotime($dataTable[$i]['paid_date'])) : '';
                $dataTable[$i]['total_amount']  = $dataTable[$i]['total_amount'] ? number_format($dataTable[$i]['total_amount'])." VND" : '';
                $dataTable[$i]['shipment_no']   = $dataTable[$i]['total_shipment'];
                $dataTable[$i]['print']         = '<button class="btn btn-add" id="print_detail_soa" value="'.$dataTable[$i]['id'].'" type="button" onclick="printDetailSOA(this)"><i class="fa fa-print" aria-hidden="true"></i><span>'.$lang['txt_print'].'</span></button>';
            }
        }else{
            $dataApi['soa_code']  = '';
            $dataApi['status']    = '';
            $dataApi['from_date'] = date('Y-m-d 00:00:01',strtotime('-1 month',time()));
            $dataApi['to_date']   = date('Y-m-d 23:59:50');

            $dateRangeValue = 'Last_30_Days';
            $statusValue = null;

            $data_api = $this->callapi->call($urlApi."getListSOA",$dataApi);
            $dataTable = $data_api['status'] == 1 ? $data_api['data'] : array();

            for($i = 0 ; $i < count($dataTable) ; $i++){
                $dataTable[$i]['check_box']     = $dataTable[$i]['id'];
                $dataTable[$i]['no']            = $i+1;
                $dataTable[$i]['soa_code']      = '<a href="/detail-soa/'.$dataTable[$i]['id'].'">'.$dataTable[$i]['soa_code'].'</a>';
                $dataTable[$i]['from_date']     = $dataTable[$i]['from_date'] && $dataTable[$i]['from_date']!='0000-00-00' ? date('d/m/Y',strtotime($dataTable[$i]['from_date'])) : '';
                $dataTable[$i]['to_date']       = $dataTable[$i]['to_date'] && $dataTable[$i]['to_date']!='0000-00-00' ? date('d/m/Y',strtotime($dataTable[$i]['to_date'])) : '';
                $dataTable[$i]['paid_date']     = $dataTable[$i]['paid_date'] && $dataTable[$i]['paid_date']!='0000-00-00' ? date('d/m/Y',strtotime($dataTable[$i]['paid_date'])) : '';
                $dataTable[$i]['total_amount']  = $dataTable[$i]['total_amount'] ? number_format($dataTable[$i]['total_amount'])." VND" : '';
                $dataTable[$i]['shipment_no']   = $dataTable[$i]['total_shipment'];
                $dataTable[$i]['print']         = '<button class="btn btn-add" id="print_detail_soa" value="'.$dataTable[$i]['id'].'" type="button" onclick="printDetailSOA(this)"><i class="fa fa-print" aria-hidden="true"></i><span>'.$lang['txt_print'].'</span></button>';
            }
        }
        $this->data['dataFilter']['dateRangeOption'] = $this->common->generateOptionRangeDatetimeSearch($dateRangeValue,$this->session->get_userdata()['site_lang']);
        $this->data['dataFilter']['statusOption'] = $this->common->generateSelectOption(
            $array_status, array(), $statusValue, 'value', 'text'
        );
        $this->data['dataTable'] = base64_encode(json_encode($dataTable));
        $this->load->view('index', $this->data);
    }
    public function ExportSOAToExcel(){
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'user_id'   => $this->session->userdata('login')['accounts_id'],
            'accounts_id'   => $this->session->userdata('login')['accounts_id'],
            'timestamp' => time(),
            'arr_id' => $this->input->post('arr_id')
        );
        $time = time();
        $dataTable = $this->callapi->call($urlApi."getListSOA",$dataApi);
        $dataTable = $dataTable['data'];
        $objPHPExcel = new PHPExcel();
        $objSheet = $objPHPExcel->getSheet(0);

        #region STYLE
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '0C0D0E')
                )
            )
        );
        $style_header = array(
            'font' => array('size' => 11,'color' => array('rgb' => '000000')),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '92D050')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        $objSheet->getColumnDimension('A')->setAutoSize(false);
        $objSheet->getColumnDimension('B')->setAutoSize(false);
        $objSheet->getColumnDimension('C')->setAutoSize(false);
        $objSheet->getColumnDimension('D')->setAutoSize(false);
        $objSheet->getColumnDimension('E')->setAutoSize(false);
        $objSheet->getColumnDimension('F')->setAutoSize(false);
        $objSheet->getColumnDimension('G')->setAutoSize(false);
        $objSheet->getColumnDimension('H')->setAutoSize(false);
        $objSheet->getColumnDimension('I')->setAutoSize(false);

        $objSheet->getColumnDimension('A')->setWidth(8.71);
        $objSheet->getColumnDimension('B')->setWidth(15.71);
        $objSheet->getColumnDimension('C')->setWidth(15.71);
        $objSheet->getColumnDimension('D')->setWidth(23.71);
        $objSheet->getColumnDimension('E')->setWidth(23.71);
        $objSheet->getColumnDimension('F')->setWidth(15.71);
        $objSheet->getColumnDimension('G')->setWidth(18.71);
        $objSheet->getColumnDimension('H')->setWidth(12.71);
        $objSheet->getColumnDimension('I')->setWidth(23.71);
        #endregion STYLE
        // Set properties
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'SOA Code')
            ->setCellValue('C1', 'From')
            ->setCellValue('D1', 'To')
            ->setCellValue('E1', 'Shipment No')
            ->setCellValue('F1', 'Total Amount')
            ->setCellValue('G1', 'Invoice No')
            ->setCellValue('H1', 'Status')
            ->setCellValue('I1', 'Paid Date');

        $current_row =1;
        for ($i=1; $i<=count($dataTable);$i++){
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.($i+1), $i)
                ->setCellValue('B'.($i+1), $dataTable[$i-1]['soa_code'])
                ->setCellValue('C'.($i+1), $this->dataconvert->convertDateTimeDisplay($dataTable[$i-1]['from_date']))
                ->setCellValue('D'.($i+1), $this->dataconvert->convertDateTimeDisplay($dataTable[$i-1]['to_date']))
                ->setCellValue('E'.($i+1), $dataTable[$i-1]['total_shipment'])
                ->setCellValue('F'.($i+1), $dataTable[$i-1]['sum_total_amount'])
                ->setCellValue('G'.($i+1), $dataTable[$i-1]['vat_invoice'])
                ->setCellValue('H'.($i+1), $dataTable[$i-1]['status'])
                ->setCellValue('I'.($i+1), $this->dataconvert->convertDateTimeDisplay($dataTable[$i-1]['paid_date']));
            $current_row = $i+2;
        }

        $objSheet->getStyle('A1:I1')->applyFromArray($style_header);
        $objSheet->getStyle('A1:I'.$current_row)->applyFromArray($style_border);

        $objPHPExcel->getActiveSheet()->setTitle('SOA');
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($this->config->item('url_folder_upload').'ListSOA_'.$time.'.xlsx');

        $response = array(
            'success' => true,
            'url' => '/uploads/ListSOA_'.$time.'.xlsx'
        );

        header('Content-type: application/json');

        // and in the end you respond back to javascript the file location
        echo json_encode($response);
    }
    public function getDetailShipment(){
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'shipment_id' => $this->input->post('shipment_id')
        );
        $detail_shipment = $this->callapi->call($urlApi."getDetailShipment",$dataApi);
        $detail_shipment_data = $detail_shipment['status'] == 1 ? $detail_shipment['data'] : array();

        if ($detail_shipment_data['acc_company_name'] == null || $detail_shipment_data['acc_company_name'] == ""){
//            $detail_shipment_data['company_name'] = $detail_shipment_data['ld_company_name'];
            $detail_shipment_data['company_name'] = '';
        }else{
            $detail_shipment_data['company_name'] = $detail_shipment_data['acc_company_name'];
        }

        switch ($detail_shipment_data['charge_to']) {
            case 'shipper':
                $detail_shipment_data['charge_to'] = 'Shipper';
                break;
            case 'receiver':
                $detail_shipment_data['charge_to'] = 'Receiver';
                break;
            case '3rd':
                $detail_shipment_data['charge_to'] = '3rd Party';
                break;
            default:
                break;
        }

        switch ($detail_shipment_data['payment_method']) {
            case 'cash':
                $detail_shipment_data['payment_method'] = 'Cash';
                break;
            case 'bank_transfer':
                $detail_shipment_data['payment_method'] = 'Bank Transfer';
                break;
            case 'other':
                $detail_shipment_data['payment_method'] = 'Other';
                break;
            default:
                break;
        }

        if ($detail_shipment_data['date_entered'] != null && $detail_shipment_data['date_entered'] != "" && $detail_shipment_data['date_entered'] != "0000-00-00 00:00:00" && $detail_shipment_data['date_entered'] != "0000-00-00"){
            $detail_shipment_data['date_entered'] = date('d/m/Y h:i',strtotime($detail_shipment_data['date_entered']));
        }else{
            $detail_shipment_data['date_entered'] = '';
        }

        if ($detail_shipment_data['delivery_date'] != null && $detail_shipment_data['delivery_date'] != "" && $detail_shipment_data['delivery_date'] != "0000-00-00 00:00:00" && $detail_shipment_data['delivery_date'] != "0000-00-00"){
            $detail_shipment_data['delivery_date'] = date('d/m/Y h:i',strtotime($detail_shipment_data['delivery_date']));
        }else{
            $detail_shipment_data['delivery_date'] = '';
        }

        //HISTORY SHIPMENT
        $destinationBranchQT = array(
            "167e3b69-6370-0895-60eb-56e690642489",
            "4aae681c-6a13-5e8a-6415-56e68ff1e689",
            "d16a0fff-b264-d096-3972-56e68f7ce7d8",
            "37483a51-b6e3-c090-0b7c-56dffb145714",
            "a2823e81-288b-031b-f42e-56e68f3cb1f3"
        );
        $result_array = array(
            'code' => $detail_shipment_data['awb_tracking_no'],
            'international' => 0,
            'check' => false,
            'errors' => array(),
            'errors_msg' => array(),
            'international_data' => array(
                'history' => array()
            ),
            'domestic_data' => array(
                'history' => array()
            )
        );
        $dataApi['awb_tracking_no'] = $detail_shipment_data['awb_tracking_no'];
        $booking_api = $this->callapi->call($urlApi."getBookingInfoTrackAndTrace",$dataApi);
        $booking = $booking_api['data'] ? $booking_api['data'] : array();
        $shipment_api = $this->callapi->call($urlApi."getShipmentInfoTrackAndTrace",$dataApi);
        $shipment = $shipment_api['data'] ? $shipment_api['data'] : array();        
        if ($shipment != false && count($shipment) > 0) {
            if ($shipment['delivery_date'] != '' && $shipment['delivery_date'] != '0000-00-00 00:00:00' && $shipment['delivery_date'] != '0000-00-00 00:00:00') {
                $shipment['delivery_date'] = $this->convertdatetime->convertDateTimeDisplayPlusForTracking($shipment['delivery_date']);
            } else {
                $shipment['delivery_date'] = '';
                $shipment['delivery_person'] = '';
            }
            $shipment['booking_date'] = $this->convertdatetime->convertDateTimeDisplayPlusForTracking($shipment['booking_date']);
            if($shipment['status'] == 'Handovered'){
                $shipment['status'] = 'In Transit';
            }
            if (in_array($shipment['destination_branch'], $destinationBranchQT) !== false) {
                $result_array['international'] = 1;
                $shipmentTracking = $this->callapi->call($urlApi."getListTrackingInternational",$dataApi);
                if (count($shipmentTracking) > 0 && $shipmentTracking != false) {
                    echo json_encode($shipmentTracking);die();
                    $result = array();
                    for ($i = 0; $i < count($shipmentTracking); $i++) {
                        $result[] = array(
                            'datetime' => $this->convertdatetime->convertDateTimeDisplayNonePlusForTracking($shipmentTracking[$i]['date_entered']),
                            'location' => $shipmentTracking[$i]['location'],
                            'activity' => $shipmentTracking[$i]['activity']
                        );
                    }
                    $result_array['international_data']['history'] = $result;
                }else {
                    $result_array['domestic_data']['history'] = $this->historyTrackingDomesticShipment($shipment['id']);
                    }
                unset($shipment['id']);
                unset($shipment['booking_date_entered']);
            }
            $result_array['international'] = 0;
            $result_array['check'] = true;
        }else {
            $result_array['check'] = false;
            $result_array['errors'][] = 1;
            $result_array['errors_msg'][] = 'Đơn hàng không tồn tại.';
        }
        if (count($booking) > 0 && $booking != false) {
            if (in_array($booking['destination_branch'], $destinationBranchQT) !== false) {
            } else {
                $result_array['domestic_data']['history'] = $this->historyTrackingDomestic($booking['id']);
            }
        }
        $detail_shipment_data['history'] = $result_array;

        echo json_encode($detail_shipment_data);die();
    }
    public function ExportShipmentToExcel(){
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'arr_id' => $this->input->post('arr_id')
        );
        $time = time();
        $dataTable = $this->callapi->call($urlApi."getListShipment",$dataApi);
        $dataTable = $dataTable['data'];

        $objPHPExcel = new PHPExcel();
        $objSheet = $objPHPExcel->getSheet(0);

        #region STYLE
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '0C0D0E')
                )
            )
        );
        $style_header = array(
            'font' => array('size' => 11,'color' => array('rgb' => '000000')),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '92D050')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        $objSheet->getColumnDimension('A')->setAutoSize(false);
        $objSheet->getColumnDimension('B')->setAutoSize(false);
        $objSheet->getColumnDimension('C')->setAutoSize(false);
        $objSheet->getColumnDimension('D')->setAutoSize(false);
        $objSheet->getColumnDimension('E')->setAutoSize(false);
        $objSheet->getColumnDimension('F')->setAutoSize(false);
        $objSheet->getColumnDimension('G')->setAutoSize(false);
        $objSheet->getColumnDimension('H')->setAutoSize(false);
        $objSheet->getColumnDimension('I')->setAutoSize(false);
        $objSheet->getColumnDimension('J')->setAutoSize(false);

        $objSheet->getColumnDimension('A')->setWidth(8.71);
        $objSheet->getColumnDimension('B')->setWidth(8.71);
        $objSheet->getColumnDimension('C')->setWidth(15.71);
        $objSheet->getColumnDimension('D')->setWidth(23.71);
        $objSheet->getColumnDimension('E')->setWidth(23.71);
        $objSheet->getColumnDimension('F')->setWidth(15.71);
        $objSheet->getColumnDimension('G')->setWidth(12.71);
        $objSheet->getColumnDimension('H')->setWidth(12.71);
        $objSheet->getColumnDimension('I')->setWidth(15.71);
        $objSheet->getColumnDimension('J')->setWidth(20.71);
        #endregion STYLE

        // Set properties
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'Shipment Code')
            ->setCellValue('C1', 'Service')
            ->setCellValue('D1', 'Date Created')
            ->setCellValue('E1', 'Date Completed')
            ->setCellValue('F1', 'Receiver Name')
            ->setCellValue('G1', 'COD Value')
            ->setCellValue('H1', 'Total Revenue')
            ->setCellValue('I1', 'Status')
            ->setCellValue('J1', 'Shipment Note');

        $current_row =1;
        for ($i=1; $i<=count($dataTable);$i++){
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.($i+1), $i)
                ->setCellValue('B'.($i+1), $dataTable[$i-1]['awb_tracking_no'])
                ->setCellValue('C'.($i+1), $dataTable[$i-1]['services'])
                ->setCellValue('D'.($i+1), $this->dataconvert->convertDateTimeDisplay($dataTable[$i-1]['date_entered']))
                ->setCellValue('E'.($i+1), $this->dataconvert->convertDateTimeDisplay($dataTable[$i-1]['delivery_date']))
                ->setCellValue('F'.($i+1), $dataTable[$i-1]['receiver_name'])
                ->setCellValue('G'.($i+1), $dataTable[$i-1]['cod_value'])
                ->setCellValue('H'.($i+1), $dataTable[$i-1]['total_revenue'])
                ->setCellValue('I'.($i+1), $dataTable[$i-1]['status'])
                ->setCellValue('J'.($i+1), $dataTable[$i-1]['shipment_note']);
            $current_row = $i+2;
        }

        $objSheet->getStyle('A1:J1')->applyFromArray($style_header);
        $objSheet->getStyle('A1:J'.$current_row)->applyFromArray($style_border);

        $objPHPExcel->getActiveSheet()->setTitle('Shipment');
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($this->config->item('url_folder_upload').'ListShipment_'.$time.'.xlsx');
        //$objWriter->save('uploads/ListShipment_'.$time.'.xlsx');

        $response = array(
            'success' => true,
            'url' => '/uploads/ListShipment_'.$time.'.xlsx'
        );

        header('Content-type: application/json');

        // and in the end you respond back to javascript the file location
        echo json_encode($response);
    }
    function printShipment()
    {
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'arr_id' => $this->input->post('arr_id')
        );
        $list_shipment = $this->callapi->call($urlApi."getShipmentByListID",$dataApi);
        $data['shipment'] = $list_shipment['status'] == 1 ? $list_shipment['data'] : array();
        $this->load->view('/print/template_shipment_airway_bill', $data);
    }
    public function detailSOA($id){
        $this->data['module'] = "service/detail-soa";
        $this->data['base_url'] = $this->config->base_url();

        $user = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $time = time();
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => $time,
            'soa_code'=>$id
        );
        $detailAPI = $this->callapi->call($urlApi."getDetailSOA",$dataApi);
        $detail_soa = $detailAPI['status'] == 1 ? $detailAPI['data'] : array();
        $detail_soa['from_date'] = $detail_soa['from_date'] ? date('d/m/Y',strtotime($detail_soa['from_date'])) : '';
        $detail_soa['to_date'] = $detail_soa['to_date'] ? date('d/m/Y',strtotime($detail_soa['to_date'])) : '';
        $detail_soa['paid_date'] = $detail_soa['paid_date'] ? date('d/m/Y',strtotime($detail_soa['paid_date'])) : '';
        $detail_soa['soa_id'] = $id;
        switch ($detail_soa['status']){
            case "recheck":
                $detail_soa['status'] = $this->data['language']['txt_soa_status_recheck'];
                break;
            case "approved":
                $detail_soa['status'] = $this->data['language']['txt_soa_status_approved'];
                break;
            case "new":
                $detail_soa['status'] = $this->data['language']['txt_soa_status_new'];
                break;
            case "sent":
                $detail_soa['status'] = $this->data['language']['txt_soa_status_sent'];
                break;
            case "paid":
                $detail_soa['status'] = $this->data['language']['txt_soa_status_paid'];
                break;
            case "confirmed":
                $detail_soa['status'] = $this->data['language']['txt_soa_status_confirmed'];
                break;
            default:
                break;
        }
        $this->data['detail_soa'] = $detail_soa;

        $detail_shipment_of_soa = $this->callapi->call($urlApi."getShipmentListBySOA",$dataApi);
        $detail_shipment_of_soa_result = $detail_shipment_of_soa['status'] == 1 ? $detail_shipment_of_soa['data'] : array();

        $dataTable = array();
        for($i = 0 ; $i < count($detail_shipment_of_soa_result) ; $i++){
            $dataTable[$i]['no'] = $i+1;
            $dataTable[$i]['date_entered'] = $detail_shipment_of_soa_result[$i]['date_entered'] ? $this->dataconvert->convertDateTimeDisplay($detail_shipment_of_soa_result[$i]['date_entered']) : '';
            $dataTable[$i]['awb_tracking_no'] = $detail_shipment_of_soa_result[$i]['awb_tracking_no'];
            $dataTable[$i]['send_address'] = $detail_shipment_of_soa_result[$i]['delivery_place'];
            $dataTable[$i]['receive_place'] = $detail_shipment_of_soa_result[$i]['receive_place'];
            $dataTable[$i]['receive_address'] = $detail_shipment_of_soa_result[$i]['address_delivery'];
            $dataTable[$i]['service'] = $detail_shipment_of_soa_result[$i]['description'];
            $dataTable[$i]['weight'] = $detail_shipment_of_soa_result[$i]['weight'];
            $dataTable[$i]['transport_service'] = $detail_shipment_of_soa_result[$i]['net_revenue'] ? number_format($detail_shipment_of_soa_result[$i]['net_revenue'],2) : '';
            $dataTable[$i]['bonus_fee'] = $detail_shipment_of_soa_result[$i]['total_surcharge'] ? $detail_shipment_of_soa_result[$i]['total_surcharge'] : '';
            $dataTable[$i]['other_fee'] = $detail_shipment_of_soa_result[$i]['total_selling_vas'] ? $detail_shipment_of_soa_result[$i]['total_selling_vas'] : '';
            $dataTable[$i]['total'] = $detail_shipment_of_soa_result[$i]['total_selling'] ? number_format($detail_shipment_of_soa_result[$i]['total_selling']) : '';
            $dataTable[$i]['tax'] = $detail_shipment_of_soa_result[$i]['tax_rate']/100;
            $dataTable[$i]['tax_fee'] = $detail_shipment_of_soa_result[$i]['tax_value'] ? number_format($detail_shipment_of_soa_result[$i]['tax_value']) : '';
            $dataTable[$i]['cash'] = $detail_shipment_of_soa_result[$i]['amount'] ? $detail_shipment_of_soa_result[$i]['amount'] : '';
            $dataTable[$i]['cod'] = $detail_shipment_of_soa_result[$i]['cod_value'] ? number_format($detail_shipment_of_soa_result[$i]['cod_value']) : '';
        }
        $this->data['shipmentList'] = base64_encode(json_encode($dataTable));

        $this->load->view('index', $this->data);
    }
    public function ExportCODToExcel(){
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $time = time();
        $dataApi = array(
            'userId'        => $user?$user['id']:'',
            'user_id'       => $this->session->userdata('login')['accounts_id'],
            'accounts_id'   => $this->session->userdata('login')['accounts_id'],
            'leads_id'      => $this->session->userdata('login')['leads_id'],
            // 'user_id'    => '626c81a0-550f-2c86-426a-58f4621c8898',
            'timestamp'     => $time,
            'arr_id'        => $this->input->post('arr_id')
        );
        $dataTable = $this->callapi->call($urlApi."getListCOD",$dataApi);
        $dataTable = $dataTable['data'];

        $objPHPExcel = new PHPExcel();
        $objSheet = $objPHPExcel->getSheet(0);

        #region STYLE
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '0C0D0E')
                )
            )
        );
        $style_header = array(
            'font' => array('size' => 11,'color' => array('rgb' => '000000')),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '92D050')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        $objSheet->getColumnDimension('A')->setAutoSize(false);
        $objSheet->getColumnDimension('B')->setAutoSize(false);
        $objSheet->getColumnDimension('C')->setAutoSize(false);
        $objSheet->getColumnDimension('D')->setAutoSize(false);
        $objSheet->getColumnDimension('E')->setAutoSize(false);
        $objSheet->getColumnDimension('F')->setAutoSize(false);
        $objSheet->getColumnDimension('G')->setAutoSize(false);
        $objSheet->getColumnDimension('H')->setAutoSize(false);
        $objSheet->getColumnDimension('I')->setAutoSize(false);

        $objSheet->getColumnDimension('A')->setWidth(8.71);
        $objSheet->getColumnDimension('B')->setWidth(15.71);
        $objSheet->getColumnDimension('C')->setWidth(23.71);
        $objSheet->getColumnDimension('D')->setWidth(15.71);
        $objSheet->getColumnDimension('E')->setWidth(15.71);
        $objSheet->getColumnDimension('F')->setWidth(15.71);
        $objSheet->getColumnDimension('G')->setWidth(18.71);
        $objSheet->getColumnDimension('H')->setWidth(23.71);
        $objSheet->getColumnDimension('I')->setWidth(15.71);
        #endregion STYLE

        // Set properties
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'Shipment Code')
            ->setCellValue('C1', 'Order No')
            ->setCellValue('D1', 'Delivery Date')
            ->setCellValue('E1', 'Cod Value')
            ->setCellValue('F1', 'Cash')
            ->setCellValue('G1', 'Payment Status')
            ->setCellValue('H1', 'Paid Date')
            ->setCellValue('I1', 'Invoice No');

        $current_row =1;
        for ($i=1; $i<=count($dataTable);$i++){
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.($i+1), $i)
                ->setCellValue('B'.($i+1), $dataTable[$i-1]['awb_tracking_no'])
                ->setCellValue('C'.($i+1), $dataTable[$i-1]['awb_tracking_no'])
                ->setCellValue('D'.($i+1), $this->dataconvert->convertDateTimeDisplay($dataTable[$i-1]['delivery_date']))
                ->setCellValue('E'.($i+1), $dataTable[$i-1]['cod_value'])
                ->setCellValue('F'.($i+1), $dataTable[$i-1]['cash'])
                ->setCellValue('G'.($i+1), $dataTable[$i-1]['status'])
                ->setCellValue('H'.($i+1), $this->dataconvert->convertDateTimeDisplay($dataTable[$i-1]['paid_date']))
                ->setCellValue('I'.($i+1), $dataTable[$i-1]['cod_code']);
            $current_row = $i+2;
        }

        $objSheet->getStyle('A1:I1')->applyFromArray($style_header);
        $objSheet->getStyle('A1:I'.$current_row)->applyFromArray($style_border);

        $objPHPExcel->getActiveSheet()->setTitle('COD');
        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($this->config->item('url_folder_upload').'ListCOD_'.$time.'.xlsx');

        $response = array(
            'success' => true,
            'url' => '/uploads/ListCOD_'.$time.'.xlsx'
        );

        header('Content-type: application/json');

        // and in the end you respond back to javascript the file location
        echo json_encode($response);
    }
    public function listCOD(){
        $this->data['module']   = "service/service-list-cod";
        $this->data['base_url'] = $this->config->base_url();

        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'user_id'           => $user?$user['id']:'',
            'accounts_id'       => $user['accounts_id'],
            'leads_id'          => $user['leads_id'],
            'timestamp'         => time()
        );

        $dataStatus = array(
            array(
                'value' => 'paid',
                'text'  => $this->session->userdata('site_lang')=='english' ? 'Paid' : 'Đã thanh toán'
            ),
            array(
                'value' => 'notpaid',
                'text'  => $this->session->userdata('site_lang')=='english' ? 'Not Paid' : 'Chưa thanh toán'
            )
        );

        if($_POST) {
            $from_date                  = $this->input->post('from_date');
            $to_date                    = $this->input->post('to_date');
            $dataApi['awb_tracking_no'] = $this->input->post('awb_tracking_no');
            $dataApi['status']          = $this->input->post('status_cod');
            $dataApi['from_date']       = !empty($from_date) ? $this->dataconvert->convertDateDB($from_date) : '';
            $dataApi['to_date']         = !empty($to_date) ? $this->dataconvert->convertDateDB($to_date) : '';
            $dataApi['cod_code']        = $this->input->post('cod_code');

            $dateRangeValue                                 = $this->input->post('date_range');
            $statusValue                                    = $this->input->post('status_cod');
            $this->data['dataFilter']['cod_code']           = $this->input->post('cod_code');
            $this->data['dataFilter']['shipment_code']      = $this->input->post('awb_tracking_no');
            $this->data['dataFilter']['from_date']          = $from_date;
            $this->data['dataFilter']['to_date']            = $to_date;

            $data_api = $this->callapi->call($urlApi."getListCOD",$dataApi);
            $dataTable = $data_api['status'] == 1 ? $data_api['data'] : array();
            for($i = 0 ; $i < count($dataTable) ; $i++){
                $dataTable[$i]['check_box']         = '<input type="checkbox" name="check_box[]" value="'.$dataTable[$i]['shipment_id'].'"">';
                $dataTable[$i]['awb_tracking_no']   = '<a href="#" data-toggle="modal" data-target="#shipment-detail-modal" data-detail_shipment="'.$dataTable[$i]['shipment_id'].'">'.$dataTable[$i]['awb_tracking_no'].'</a>';
                $dataTable[$i]['order_no']          = $dataTable[$i]['awb_tracking_no'];
                $dataTable[$i]['delivery_date']     = $this->dataconvert->convertDateTimeDisplay($dataTable[$i]['delivery_date']);
                $dataTable[$i]['cod_value']         = $dataTable[$i]['cod_value'] ? number_format($dataTable[$i]['cod_value'])." VND" : '';
                $dataTable[$i]['total_fee']         = $dataTable[$i]['total_fee'] ? number_format($dataTable[$i]['total_fee'])." VND" : '';
                $dataTable[$i]['paid_date']         = $dataTable[$i]['paid_date'] && $dataTable[$i]['paid_date']!='0000-00-00' ? date('d/m/Y', strtotime($dataTable[$i]['paid_date'])) : '';
                $dataTable[$i]['bangke_no']         = $dataTable[$i]['cod_code'];
                $dataTable[$i]['xem']               = $dataTable[$i]['cod_id'] ? '<a href="/detail-cod/'.$dataTable[$i]['cod_id'].'" target="_blank">'.$this->data['language']['txt_detail_cod'].'</a>' : '';
            }
        }else{
            $dataApi['awb_tracking_no'] = '';
            $dataApi['status']          = '';
            $dataApi['from_date']       = date('Y-m-d 00:00:01',strtotime('-1 month',time()));
            $dataApi['to_date']         = date('Y-m-d 23:59:50');
            $dataApi['cod_code']        = '';

            $dateRangeValue = 'Last_30_Days';
            $statusValue = null;

            $data_api = $this->callapi->call($urlApi."getListCOD",$dataApi);
            $dataTable = $data_api['status'] == 1 ? $data_api['data'] : array();
            for($i = 0 ; $i < count($dataTable) ; $i++){
                $dataTable[$i]['check_box']         = '<input type="checkbox" name="check_box[]" value="'.$dataTable[$i]['shipment_id'].'"">';
                $dataTable[$i]['awb_tracking_no']   = '<a href="#" data-toggle="modal" data-target="#shipment-detail-modal" data-detail_shipment="'.$dataTable[$i]['shipment_id'].'">'.$dataTable[$i]['awb_tracking_no'].'</a>';
                $dataTable[$i]['order_no']          = $dataTable[$i]['awb_tracking_no'];
                $dataTable[$i]['delivery_date']     = $this->dataconvert->convertDateTimeDisplay($dataTable[$i]['delivery_date']);
                $dataTable[$i]['cod_value']         = $dataTable[$i]['cod_value'] ? number_format($dataTable[$i]['cod_value'])." VND" : '';
                $dataTable[$i]['total_fee']         = $dataTable[$i]['total_fee'] ? number_format($dataTable[$i]['total_fee'])." VND" : '';
                $dataTable[$i]['paid_date']         = $dataTable[$i]['paid_date'] && $dataTable[$i]['paid_date']!='0000-00-00' ? date('d/m/Y', strtotime($dataTable[$i]['paid_date'])) : '';
                $dataTable[$i]['bangke_no']         = $dataTable[$i]['cod_code'];
                $dataTable[$i]['xem']               = $dataTable[$i]['cod_id'] ? '<a href="/detail-cod/'.$dataTable[$i]['cod_id'].'" target="_blank">'.$this->data['language']['txt_detail_cod'].'</a>' : '';
            }
        }

        $this->data['dataFilter']['statusCOD'] = $this->common->generateSelectOption(
            $dataStatus, array(), $statusValue, 'value', 'text'
        );
        $this->data['dataFilter']['dateRangeOption'] = $this->common->generateOptionRangeDatetimeSearch($dateRangeValue,$this->session->get_userdata()['site_lang']);
        $this->data['dataTable'] = base64_encode(json_encode($dataTable));

        $this->load->view('index', $this->data);
    }
    public function detailCOD($id){
        $this->data['module'] = "service/detail-cod";
        $this->data['base_url'] = $this->config->base_url();

        $user = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $time = time();
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => $time,
            'cod_id'=>$id
        );
        $detailAPI = $this->callapi->call($urlApi."getDetailCOD",$dataApi);
        $detail_cod = $detailAPI['status'] == 1 ? $detailAPI['data'] : array();
        $detail_cod['from_date'] = $detail_cod['from_date'] ? date('d/m/Y',strtotime($detail_cod['from_date'])) : '';
        $detail_cod['to_date']   = $detail_cod['to_date'] ? date('d/m/Y', strtotime($detail_cod['to_date'])) : '';
        $detail_cod['paid_date'] = $detail_cod['paid_date'] ? date('d/m/Y', strtotime($detail_cod['paid_date'])) : '';

        $data_api = $this->callapi->call($urlApi."getListShipmentByCOD",$dataApi);
        $data_shipment = $data_api['status'] == 1 ? $data_api['data'] : array();
        $dataTable = array();
        for($i = 0 ; $i < count($data_shipment) ; $i++){
            $dataTable[$i]['stt']               = $i+1;
            $dataTable[$i]['awb_tracking_no']   = $data_shipment[$i]['awb_tracking_no'];
            $dataTable[$i]['order_no']          = $data_shipment[$i]['awb_tracking_no'];
            $dataTable[$i]['booking_date']      = $this->dataconvert->convertDateTimeDisplay($data_shipment[$i]['booking_date']);
            $dataTable[$i]['cod_value']         = $data_shipment[$i]['cod_value'] ? number_format($data_shipment[$i]['cod_value'])." VND" : '';
            $dataTable[$i]['address_delivery']  = $data_shipment[$i]['address_delivery'];
            $dataTable[$i]['cod']               = $data_shipment[$i]['cod_value'] ? number_format($data_shipment[$i]['cod_value'])." VND" : '';
            $dataTable[$i]['cash']              = $data_shipment[$i]['cash'] ? number_format($data_shipment[$i]['cash'])." VND" : '';
            $dataTable[$i]['paid']              = $data_shipment[$i]['amount'] ? number_format($data_shipment[$i]['amount'])." VND" : '';
        }
        $this->data['detail_cod'] = $detail_cod;
        $this->data['dataTable'] = base64_encode(json_encode($dataTable));
        $this->load->view('index', $this->data);
    }
    function printSOA(){
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');

        $dataApi = array(
            'userId'        => $user?$user['id']:'',
            'user_id'       => $this->session->userdata('login')['accounts_id'],
            'accounts_id'   => $this->session->userdata('login')['accounts_id'],
            // 'user_id'           => '626c81a0-550f-2c86-426a-58f4621c8898',
            'timestamp' => time(),
            'soa_id' => $this->input->post('soa_id')
        );

        $soa = $this->callapi->call($urlApi."getStatementOfAccountByIdForExport",$dataApi);
        $arr_statement = $soa['status'] == 1 ? $soa['data'] : array();
        $detail_soa = $this->callapi->call($urlApi."getShipmentByIdSOAForExport",$dataApi);
        $arr_shipment = $detail_soa['status'] == 1 ? $detail_soa['data'] : array();

        $objPHPExcel = new PHPExcel();
        $objSheet = $objPHPExcel->getSheet(0);
        $objSheet->setTitle('STATEMENT OF ACCOUNT');
        $border_style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        );
        $style_company_name = array(
            'font' => array('bold' => true, 'size' => 14,),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        $style_company_info = array(
            'font' => array('bold' => true, 'size' => 14,),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        $style_header = array(
            'font' => array('bold' => true, 'size' => 12,),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        $style_title = array(
            'font' => array('bold' => true, 'size' => 18,),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        $currencyFormat = '#,##0;(#,##0)';
        $weightFormat = '#,##0.00';
        $taxFormat = '0';


        // LOGO
        $logo = 'assets/images/logo-speedlink.png';
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName("logo_sci");
        $objDrawing->setDescription("logo_sci");
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('A1');
        // $objDrawing->setHeight(120);
        $objDrawing->setWidth(260);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        // END LOGO


        $objSheet->setCellValueExplicit('D1', 'CTY TNHH SPEEDLINK VIỆT NAM');
        $objSheet->setCellValueExplicit('G1', $arr_statement['code']);

        $objSheet->mergeCells('D1:F1');
        $objSheet->getStyle('D1:G1')->applyFromArray($style_company_name);

        $objSheet->mergeCells('A3:I3');
        $objSheet->setCellValueExplicit('A3', 'BẢNG KÊ CƯỚC PHÍ CHUYỂN PHÁT NHANH');
        $objSheet->getStyle('A3')->applyFromArray($style_title);

        $objSheet->setCellValueExplicit('D4', 'From Date:');
        $objSheet->setCellValueExplicit('E4', $arr_statement['from_date']);
        $objSheet->setCellValueExplicit('F4', 'To Date:');
        $objSheet->setCellValueExplicit('G4', $arr_statement['to_date']);
        $objSheet->getStyle('D4:G4')->applyFromArray($style_company_info);

        $objSheet->setCellValueExplicit('C5', 'Mã KH:');
        $objSheet->setCellValueExplicit('D5', $arr_statement['account_no']);
        $objSheet->getStyle('C5:D5')->applyFromArray($style_company_info);

        $objSheet->setCellValueExplicit('C6', 'Tên Cty:');
        $objSheet->mergeCells('D6:I6');
        $objSheet->setCellValueExplicit('D6', $arr_statement['customer_name']);
        $objSheet->getStyle('C6:D6')->applyFromArray($style_company_info);

        $objSheet->setCellValueExplicit('C7', 'Địa chỉ:');
        $objSheet->mergeCells('D7:I7');
        $objSheet->setCellValueExplicit('D7', $arr_statement['address']);
        $objSheet->getStyle('C7:D7')->applyFromArray($style_company_info);


        $end_col = 'M';
        $objSheet->setCellValueExplicit('A8', 'STT');
        $objSheet->setCellValueExplicit('B8', 'Ngày vận đơn');
        $objSheet->setCellValueExplicit('C8', 'Số vận đơn');
        $objSheet->setCellValueExplicit('D8', 'Nơi gửi');
        $objSheet->setCellValueExplicit('E8', 'Nơi nhận');
        $objSheet->setCellValueExplicit('F8', 'Địa chỉ người nhận');
        $objSheet->setCellValueExplicit('G8', 'Dịch vụ ');
        $objSheet->setCellValueExplicit('H8', 'Weight:');
        $objSheet->setCellValueExplicit('I8', 'Dịch vụ chuyển phát');
        $objSheet->setCellValueExplicit('J8', 'Phụ phí');
        $objSheet->setCellValueExplicit('K8', 'Dịch vụ khác');
        $objSheet->setCellValueExplicit('L8', 'Thành tiền');
        $objSheet->setCellValueExplicit('M8', 'Thu hộ');
        //$objSheet->setCellValueExplicit('M8', 'Thuế suất');
        //$objSheet->setCellValueExplicit('N8', 'Thuế');
        //$objSheet->setCellValueExplicit('O8', 'Total:');
        //$objSheet->setCellValueExplicit('P8', 'Thu hộ');
        //$objSheet->setCellValueExplicit('Q8', 'Ghi chú');

        $objSheet->getStyle('A8:L8')->getAlignment()->setWrapText(true);
        $objSheet->getStyle('A8:L8')->applyFromArray($style_header);
        $objSheet->getStyle('A8:L8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objSheet->getStyle('A8:L8')->getFill()->getStartColor()->setARGB('9BDEE7');


        $total_weight = 0;
        $total_net_revenue = 0;
        $total_surcharge = 0;
        $total_other_charge = 0;
        $total_net_amount = 0;
        $total_vat_amount = 0;
        $total_amount = 0;
        $total_cod_value = 0;

        $count_row = 9;
        $no = 0;
        for ($row = 0; $row < count($arr_shipment); $row++) {
            $no ++;
            // $col = 'A';
            $objSheet->setCellValueExplicit('A' . ($row + 9), $no);
            $objSheet->setCellValueExplicit('B' . ($row + 9), $this->dataconvert->convertDateTimeDisplay($arr_shipment[$row]['real_pickup_date']));
            $objSheet->setCellValueExplicit('C' . ($row + 9), $arr_shipment[$row]['awb_tracking_no']);
            $objSheet->setCellValueExplicit('D' . ($row + 9), $arr_shipment[$row]['branch_name']);
            $objSheet->setCellValueExplicit('E' . ($row + 9), $arr_shipment[$row]['city_name']);
            $objSheet->setCellValueExplicit('F' . ($row + 9), $arr_shipment[$row]['address_delivery']);
            $objSheet->setCellValueExplicit('G' . ($row + 9), $arr_shipment[$row]['description']);
            $objSheet->setCellValue('H' . ($row + 9), $arr_shipment[$row]['consignment_weight']);
            $objSheet->setCellValue('I' . ($row + 9), $arr_shipment[$row]['tranport_fee']);
            $objSheet->setCellValue('J' . ($row + 9), $arr_shipment[$row]['total_surcharge']);
            $objSheet->setCellValue('K' . ($row + 9), $arr_shipment[$row]['other_charge']);
            $objSheet->setCellValue('L' . ($row + 9), $arr_shipment[$row]['total_net_amount']);
            $objSheet->setCellValue('M' . ($row + 9), $arr_shipment[$row]['cod_value']);
            /*$objSheet->setCellValue('M' . ($row + 9), $arr_shipment[$row]['tax_rate']);
            $objSheet->setCellValue('N' . ($row + 9), $arr_shipment[$row]['vat_amount']);
            $objSheet->setCellValue('O' . ($row + 9), $arr_shipment[$row]['amount']);
            $objSheet->setCellValue('P' . ($row + 9), $arr_shipment[$row]['cod_value']);*/

            $objSheet->getStyle('H' . ($row + 9))->getNumberFormat()->setFormatCode($weightFormat);
            $objSheet->getStyle('I' . ($row + 9))->getNumberFormat()->setFormatCode($currencyFormat);
            $objSheet->getStyle('J' . ($row + 9))->getNumberFormat()->setFormatCode($currencyFormat);
            $objSheet->getStyle('K' . ($row + 9))->getNumberFormat()->setFormatCode($currencyFormat);
            $objSheet->getStyle('L' . ($row + 9))->getNumberFormat()->setFormatCode($currencyFormat);
            $objSheet->getStyle('M' . ($row + 9))->getNumberFormat()->setFormatCode($currencyFormat);
            /*$objSheet->getStyle('M' . ($row + 9))->getNumberFormat()->setFormatCode($taxFormat);
            $objSheet->getStyle('N' . ($row + 9))->getNumberFormat()->setFormatCode($currencyFormat);
            $objSheet->getStyle('O' . ($row + 9))->getNumberFormat()->setFormatCode($currencyFormat);
            $objSheet->getStyle('P' . ($row + 9))->getNumberFormat()->setFormatCode($currencyFormat);
            $objSheet->setCellValueExplicit('Q' . ($row + 9), '');*/

            $total_weight += $arr_shipment[$row]['consignment_weight'];
            $total_net_revenue += $arr_shipment[$row]['tranport_fee'];
            $total_surcharge += $arr_shipment[$row]['total_surcharge'];
            $total_other_charge += $arr_shipment[$row]['other_charge'];
            $total_net_amount += $arr_shipment[$row]['total_net_amount'];
            $total_vat_amount += $arr_shipment[$row]['vat_amount'];
            $total_amount += $arr_shipment[$row]['amount'];
            $total_cod_value += $arr_shipment[$row]['cod_value'];
            $count_row++;
        }
        $objSheet->mergeCells('A' . $count_row . ':G' . $count_row);
        $objSheet->setCellValueExplicit('A' . $count_row, 'Tổng Tiền');
        $objSheet->getStyle('A' . $count_row)->applyFromArray($style_header);
        $objSheet->getStyle('H' . $count_row . ':S' . $count_row)->applyFromArray($style_header);

        $objSheet->setCellValue('H' . $count_row, $total_weight);
        $objSheet->getStyle('H' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->setCellValue('I' . $count_row, $total_net_revenue);
        $objSheet->getStyle('I' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->setCellValue('J' . $count_row, $total_surcharge);
        $objSheet->getStyle('J' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->setCellValue('K' . $count_row, $total_other_charge);
        $objSheet->getStyle('K' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->setCellValue('L' . $count_row, $total_net_amount);
        $objSheet->getStyle('L' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->setCellValue('M' . $count_row, $total_cod_value);
        $objSheet->getStyle('M' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
        /*$objSheet->setCellValue('N' . $count_row, $total_vat_amount);
        $objSheet->getStyle('N' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->setCellValue('O' . $count_row, $total_amount);
        $objSheet->getStyle('O' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->setCellValue('P' . $count_row, $total_cod_value);
        $objSheet->getStyle('P' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);*/

        $objSheet->setCellValueExplicit('K'.($count_row + 3), 'Thuế suất');
        $objSheet->setCellValueExplicit('L'.($count_row + 3), '10%');
        $objSheet->getStyle('K'.($count_row + 3))->applyFromArray($style_header);

        $objSheet->setCellValueExplicit('K'.($count_row + 4), 'Tiền thuế');
        $objSheet->setCellValue('L'.($count_row + 4), ($total_net_amount * 0.1));
        $objSheet->getStyle('L' . ($count_row + 4))->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->getStyle('K'.($count_row + 4))->applyFromArray($style_header);

        $objSheet->setCellValueExplicit('K'.($count_row + 5), 'Tổng cộng');
        $objSheet->setCellValue('L'.($count_row + 5), (($total_net_amount * 0.1) + $total_net_amount ));
        $objSheet->getStyle('L' . ($count_row + 5))->getNumberFormat()->setFormatCode($currencyFormat);
        $objSheet->getStyle('K'.($count_row + 5))->applyFromArray($style_header);


        for ($col = 'A'; $col !== 'N'; $col++) {
            $objSheet->getColumnDimension($col)->setAutoSize(true);
        }

        $file_name = $arr_statement['code'].'.xlsx';
        $folder = $this->config->item('url_folder_upload').'statement_of_account';
        $highest_row = $objSheet->getHighestRow();
        $objSheet->getStyle("A8:" . $end_col . $count_row)->applyFromArray($border_style);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($folder."/".$file_name);

        $response = "/uploads/statement_of_account/".$file_name;

        header('Content-type: application/json');

        // and in the end you respond back to javascript the file location
        echo json_encode($response);
    }
    public function printCOD(){
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');

        $dataApi = array(
            'userId'        => $user?$user['id']:'',
            'user_id'       => $this->session->userdata('login')['accounts_id'],
            'accounts_id'   => $this->session->userdata('login')['accounts_id'],
            'leads'         => $this->session->userdata('login')['leads_id'],
            'timestamp' => time(),
            'cod_id' => $this->input->post('cod_id')
        );

        $statement_api = $this->callapi->call($urlApi."getStatementCODById",$dataApi);
        $statement = $statement_api['status'] == 1 ? $statement_api['data'] : array();
        $shipment_api = $this->callapi->call($urlApi."getStatementCODDetailByStatementId",$dataApi);
        $shipment = $shipment_api['status'] == 1 ? $shipment_api['data'] : array();
        $statement['total_collected_cod_value'] = 0;
        $statement['total_cod_value'] = 0;
        $statement['total_fee'] = 0;

        $objPHPExcel = new PHPExcel();
            $objSheet = $objPHPExcel->getSheet(0);
            $objSheet->setTitle('STATEMENT OF COD EXPRESS');
            $border_style = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    )
                )
            );
            $style_company_name = array(
                'font' => array('bold' => true, 'size' => 14,),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
            );
            $style_company_info = array(
                'font' => array('bold' => true, 'size' => 14,),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
            );
            $style_header = array(
                'font' => array('bold' => true, 'size' => 12,),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
            );
            $style_title = array(
                'font' => array('bold' => true, 'size' => 18,),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
            );
            $currencyFormat = '#,##0_);(#,##0)';


            // LOGO
            $logo = 'assets/images/logo-speedlink.png';
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName("logo_sci");
            $objDrawing->setDescription("logo_sci");
            $objDrawing->setPath($logo);
            $objDrawing->setCoordinates('A1');
            // $objDrawing->setHeight(120);
            $objDrawing->setWidth(240);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            // END LOGO


            $objSheet->setCellValueExplicit('D1', 'CTY TNHH SPEEDLINK VIỆT NAM');
            $objSheet->mergeCells('D1:F1');
            $objSheet->getStyle('D1')->applyFromArray($style_company_name);

            $objSheet->mergeCells('A3:I3');
            $objSheet->setCellValueExplicit('A3', 'STATEMENT OF COD EXPRESS');
            $objSheet->getStyle('A3')->applyFromArray($style_title);

            $objSheet->setCellValueExplicit('D4', 'From Date:');
            $objSheet->setCellValueExplicit('E4', $this->convertDateDisplay($statement['from_date'], false));
            $objSheet->setCellValueExplicit('F4', 'To Date:');
            $objSheet->setCellValueExplicit('G4', $this->convertDateDisplay($statement['to_date'], false));
            $objSheet->getStyle('D4:G4')->applyFromArray($style_company_info);

            $objSheet->setCellValueExplicit('C5', 'Mã KH:');
            $objSheet->setCellValueExplicit('D5', $statement['customer_no']);
            $objSheet->getStyle('C5:D5')->applyFromArray($style_company_info);

            $objSheet->setCellValueExplicit('C6', 'Tên Cty:');
            $objSheet->mergeCells('D6:I6');
            $objSheet->setCellValueExplicit('D6', $statement['customer_name']);
            $objSheet->getStyle('C6:D6')->applyFromArray($style_company_info);

            $objSheet->setCellValueExplicit('C7', 'Địa chỉ:');
            $objSheet->mergeCells('D7:I7');
            $objSheet->setCellValueExplicit('D7', $statement['customer_address']);
            $objSheet->getStyle('C7:D7')->applyFromArray($style_company_info);


            $end_col = 'H';
            $objSheet->setCellValueExplicit('A8', 'SỐ TT');
            $objSheet->setCellValueExplicit('B8', 'Tracking Number');
            $objSheet->setCellValueExplicit('C8', 'Order / Tracking No');
            $objSheet->setCellValueExplicit('D8', 'Order Date');
            $objSheet->setCellValueExplicit('E8', 'Destination Hub');
            $objSheet->setCellValueExplicit('F8', 'Customer Name');
            $objSheet->setCellValueExplicit('G8', 'Sales');
            $objSheet->setCellValueExplicit('H8', 'COD');
            if ($statement['payment_method'] == 'other') {
                $objSheet->setCellValueExplicit('I8', 'Số tiền đã chuyển vào TK Cty');
                $objSheet->setCellValueExplicit('J8', 'Tiền cước');
                $end_col = 'J';
            }
            $objSheet->getStyle('A8:K8')->getAlignment()->setWrapText(true);
            $objSheet->getStyle('A8:K8')->applyFromArray($style_header);
            $objSheet->getStyle('A8:K8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $objSheet->getStyle('A8:K8')->getFill()->getStartColor()->setARGB('9BDEE7');
            for ($col = 'A'; $col !== 'L'; $col++) {
                $objSheet->getColumnDimension($col)->setAutoSize(true);
            }

            $total_cod = 0;
            $total_amount = 0;
            $total_fee = 0;
            $count_row = 9;

        if(!empty($shipment) && count($statement)>0) {
            $total_cod_value = 0;
            for ($i = 0; $i < count($shipment); $i++) {
                $shipment[$i]['no'] = ($i + 1);
                // $shipment[$i]['amount'] = $shipment[$i]['cod_value'] - $shipment[$i]['total_revenue'];
                $statement['total_collected_cod_value'] += $shipment[$i]['collected_cod_value'];
                $statement['total_cod_value'] += $shipment[$i]['cod_value'];
                $statement['total_fee'] += $shipment[$i]['total_revenue'];

                $total_revenue = $shipment[$i]['total_revenue'];
                if($statement['net_off'] == 1){
                    if($shipment[$i]['payment_method'] == 'cash'){
                        $total_revenue = 0;
                    }
                }
                $amount = $shipment[$i]['cod_value'] - $total_revenue;
                $objSheet->setCellValue('A' . ($i + 9), $shipment[$i]['no']);
                $objSheet->setCellValueExplicit('B' . ($i + 9), $shipment[$i]['awb_tracking_no']);
                $objSheet->setCellValueExplicit('C' . ($i + 9), $shipment[$i]['order_tracking_no']);
                $objSheet->setCellValueExplicit('D' . ($i + 9), $this->dataconvert->convertDateTimeDisplay($shipment[$i]['real_pickup_date']));
                $objSheet->setCellValueExplicit('E' . ($i + 9), $shipment[$i]['dest_hub_code']);
                $objSheet->setCellValueExplicit('F' . ($i + 9), $shipment[$i]['payer_name']);
                $objSheet->setCellValueExplicit('G' . ($i + 9), $shipment[$i]['assigned_user_name']);
                $objSheet->setCellValue('H' . ($i + 9), $shipment[$i]['cod_value']);
                $objSheet->getStyle('H' . ($i + 9))->getNumberFormat()->setFormatCode($currencyFormat);
                if ($statement['payment_method'] == 'other') {
                    $objSheet->setCellValue('I' . ($i + 9), $amount);
                    $objSheet->getStyle('I' . ($i + 9))->getNumberFormat()->setFormatCode($currencyFormat);
                    $objSheet->setCellValue('J' . ($i + 9), $total_revenue);
                    $objSheet->getStyle('J' . ($i + 9))->getNumberFormat()->setFormatCode($currencyFormat);
                }
                $objSheet->getStyle('H' . ($i + 9))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objSheet->getStyle('I' . ($i + 9))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objSheet->getStyle('J' . ($i + 9))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $total_cod += $shipment[$i]['cod_value'];
                $total_amount += $amount;
                $total_fee += $total_revenue;
                $count_row++;
            }
            $objSheet->setCellValue('H' . $count_row, $total_cod);
            $objSheet->getStyle('H' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objSheet->getStyle('H' . $count_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            if ($statement['payment_method'] == 'other') {
                $objSheet->setCellValue('I' . $count_row, $total_amount);
                $objSheet->setCellValue('J' . $count_row, $total_fee);
                $objSheet->getStyle('I' . $count_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objSheet->getStyle('J' . $count_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objSheet->getStyle('I' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
                $objSheet->getStyle('J' . $count_row)->getNumberFormat()->setFormatCode($currencyFormat);
            }


            $objSheet->mergeCells('A' . $count_row . ':G' . $count_row);
            $objSheet->setCellValueExplicit('A' . $count_row, 'Tổng Cộng');
            $objSheet->getStyle('A' . $count_row)->applyFromArray($style_header);
            $objSheet->getStyle('H' . $count_row . ':K' . $count_row)->applyFromArray($style_header);

            if ($statement['payment_method'] == 'other') {
                $objSheet->setCellValue('J' . ($count_row + 1), $total_cod - $total_fee);
                $objSheet->getStyle('J' . ($count_row + 1))->getNumberFormat()->setFormatCode($currencyFormat);
                $objSheet->getStyle('J' . ($count_row + 1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objSheet->getStyle('J' . ($count_row + 1))->getFill()->getStartColor()->setARGB('FFFF00');
                $objSheet->getStyle('J' . ($count_row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objSheet->getStyle('J' . ($count_row + 1))->applyFromArray(
                    array(
                        'font' => array('bold' => true, 'size' => 18, 'color' => array('rgb' => 'FF0000'),),
                    )
                );
            } else {
                $objSheet->setCellValue('H' . ($count_row + 1), $total_cod);
                $objSheet->getStyle('H' . ($count_row + 1))->getNumberFormat()->setFormatCode($currencyFormat);
                $objSheet->getStyle('H' . ($count_row + 1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objSheet->getStyle('H' . ($count_row + 1))->getFill()->getStartColor()->setARGB('FFFF00');
                $objSheet->getStyle('H' . ($count_row + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objSheet->getStyle('H' . ($count_row + 1))->applyFromArray(
                    array(
                        'font' => array('bold' => true, 'size' => 18, 'color' => array('rgb' => 'FF0000'),),
                    )
                );
            }

            $highest_row = $objSheet->getHighestRow();
            $objSheet->getStyle("A8:" . $end_col . ($highest_row - 1))->applyFromArray($border_style);

            $file_name = $statement['code'].'.xlsx';
            $folder = $this->config->item('url_folder_upload').'statement_of_cod';

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($folder."/".$file_name);

            $response = "/uploads/statement_of_cod/".$file_name;

            header('Content-type: application/json');

            // and in the end you respond back to javascript the file location
            echo json_encode($response);
        }
    }
    public static function convertDateDisplay($date_time, $bool = true)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {
            if ($bool == true) {
                $value = strtotime($date_time) - (60 * 60 * 7);
            } else {
                $value = strtotime($date_time);
            }
            return date('d/m/Y', $value);
        }
    }
    public function historyTrackingDomestic($booking_id = '')
    {
        $user   = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'booking_id' => $booking_id
        );
        $result = array();
        if ($booking_id != '' && $booking_id != null) {
            $booking_api = $this->callapi->call($urlApi."getBookingByIDForTrackAndTrace",$dataApi);
            $booking = $booking_api['status'] == 1 ? $booking_api['data'][0] : array();
            if (count($booking) > 0) {
                //LOG BOOKING
                $log_confirm_booking = array(
                    'date_time' => $this->convertdatetime->convertDateTimeDisplayPlusForTracking($booking['date_entered']),
                    'date_time_not_convert' => $this->convertdatetime->convertDateTimeDBPlusForPortal($booking['date_entered']),
                    'description' => $booking['description'],
                    'branch_name' => $booking['origin_branch'],
                    'hub_name' => $booking['origin_hub_code'],
                    'status' => 'Processing',
                    'status_vn' => 'Đang xử lý',
                    'reason_eng' => 'Confirmed booking',
                    'reason_vn' => 'Đã nhận được yêu cầu lấy hàng',
                );
                $log_booking_api = $this->callapi->call($urlApi."getAuditLogBooking",$dataApi);
                $log_booking = $log_booking_api['status'] == 1 ? $log_booking_api['data'] : array();
                for ($i = 0; $i < count($log_booking); $i++) {
                    if ($log_booking[$i]['status'] == 'Picked Up' || $log_booking[$i]['status'] == 'Picked up') {
                        unset($log_booking[$i]);
                    }
                }
                //END LOG BOOKING
                //LOG SHIPMENT
                $booking_shipment_api = $this->callapi->call($urlApi."getShipmentIdByBookingId",$dataApi);
                $booking_shipment = $booking_shipment_api['status'] == 1 ? $booking_shipment_api['data'] : array();
                $log_shipment = array();
                if (!empty($booking_shipment)) {
                    $dataApi['shipment_id'] = $booking_shipment[0]['shipment_id'];
                    $log_shipment_api = $this->callapi->call($urlApi."getAuditLogShipment",$dataApi);
                    $log_shipment = $log_shipment_api['status'] == 1 ? $log_shipment_api['data'] : array();
                    $masterbill_detail_api = $this->callapi->call($urlApi."getMasterBillIdByShipmentId",$dataApi);
                    $masterbill_detail = $masterbill_detail_api['status'] == 1 ? $masterbill_detail_api['data'] : array();
                }
                $arr_log_shipment = array();
                foreach ($log_shipment as $key => $value) {
                    //step 1 to 2
                    if ($value['step_from'] == 1 && $value['step_to'] == 2) {
                        if ($value['parent_type'] == 'Shipment' && ($value['status'] == 'Picked Up' || $value['status'] == 'Picked up')) {
                            $arr_log_shipment[] = $value;
                        }
                    }
                    //step 7 to 8
                    if ($value['step_from'] == 7 && $value['step_to'] == 8) {
                        if ($value['status'] == 'On Delivery') {
                            $arr_log_shipment[] = $value;
                        }
                    }
                    //step 8 to 9
                    if ($value['step_from'] == 8 && $value['step_to'] == 9) {
                        if ($value['status'] == 'Detained') {
                            $arr_log_shipment[] = $value;
                        }
                        if ($value['status'] == 'On Delivery') {
                            $arr_log_shipment[] = $value;
                        }
                    }
                    //step 9 to 10
                    if ($value['step_from'] == 9 && $value['step_to'] == 10) {
                        if ($value['status'] == 'Delivered') {
                            $arr_log_shipment[] = $value;
                        }
                        if ($value['status'] == 'Detained') {
                            $arr_log_shipment[] = $value;
                        }
                        if ($value['status'] == 'Reschedule') {
                            $arr_log_shipment[] = $value;
                        }
                        if ($value['status'] == 'Canceled') {
                            $arr_log_shipment[] = $value;
                        }
                    }
                }
                //END LOG SHIPMENT

                //LOG MASTERBILL
                $log_masterbill = array();
                if (!empty($masterbill_detail)) {
                    $dataApi['masterbill_id'] = $masterbill_detail['masterbill_id'];
                    $log_masterbill_api = $this->callapi->call($urlApi."getAuditLogShipmentMasterbill",$dataApi);
                    $log_masterbill = $log_masterbill_api['status'] == 1 ? $log_masterbill_api['data'] : array();
                    for ($i = 1; $i < (count($log_masterbill) - 2); $i++) {
                        $log_masterbill[$i]['status'] = 'In Transit';
                    }
                }
                $arr_log_masterbill = array();
                foreach ($log_masterbill as $key => $value) {
                    //step to 3
                    if ($value['step_to'] == 3) {
                        $value['status'] = 'Bagging';
                    }
                    if ($value['step_to'] == 4 || $value['step_to'] == 5 || $value['step_to'] == 6) {
                        $value['status'] = 'In Transit';
                    }
                    //step to 7
                    if ($value['step_to'] == 7) {
                        if ($value['parent_type'] == 'MasterBill') {
                            $value['status'] = 'Bagging';
                        }
                    }
                    $arr_log_masterbill[] = $value;
                }
                foreach ($arr_log_masterbill as $key => $value) {
                    if (($value['step_from'] == 3 && $value['step_to'] == 3) || ($value['step_from'] == 7 && $value['step_to'] == 7) || ($value['step_from'] == 7 && $value['step_to'] == 8)) {
                        unset($arr_log_masterbill[$key]);
                    }
                }
                //END LOG MASTERBILL

                $arr_log = array_merge($log_booking, $arr_log_shipment, $arr_log_masterbill);

                //SORT BY DATE_ENTERED
                for ($i = 0; $i < count($arr_log); $i++) {
                    for ($j = 0; $j < count($arr_log); $j++) {
                        if (strtotime($arr_log[$i]['date_entered']) < strtotime($arr_log[$j]['date_entered'])) {
                            $temp = $arr_log[$i];
                            $arr_log[$i] = $arr_log[$j];
                            $arr_log[$j] = $temp;
                        } else if (strtotime($arr_log[$i]['date_entered']) == strtotime($arr_log[$j]['date_entered'])) {
                            //SORT BY STEP (STATUS)
                            if ($arr_log[$i]['step_from'] < $arr_log[$j]['step_from']) {
                                $temp = $arr_log[$i];
                                $arr_log[$i] = $arr_log[$j];
                                $arr_log[$j] = $temp;
                            }
                        }
                    }
                }

                //CONVERT
                for ($i = 0; $i < count($arr_log); $i++) {
                    if ($arr_log[$i]['reason'] == 4100) {
                        $arr_log[$i]['reason_eng'] = 'Sorting';
                        $arr_log[$i]['reason_vn'] = 'Soạn hàng chuẩn bị đi giao';
                    }

                    switch ($arr_log[$i]['status']) {
                        case 'Processing':
                            $arr_log[$i]['status_vn'] = 'Đang xử lý';
                            break;
                        case 'Reschedule':
                            $arr_log[$i]['status_vn'] = 'Chưa lấy được hàng';
                            break;
                        case 'Picked Up':
                            $arr_log[$i]['status_vn'] = 'Đã nhận hàng';
                            break;
                        case 'Bagging':
                            $arr_log[$i]['status_vn'] = 'Đóng gói kiện hàng';
                            break;
                        case 'In Transit':
                            $arr_log[$i]['status_vn'] = 'Đang vận chuyển';
                            break;
                        case 'Un Bagging':
                            $arr_log[$i]['status_vn'] = 'Mở kiện hàng';
                            break;
                        case 'On Delivery':
                            $arr_log[$i]['status_vn'] = 'Đang giao hàng';
                            break;
                        case 'Detained':
                            $arr_log[$i]['status_vn'] = 'Giữ lại hàng';
                            break;
                        case 'Canceled':
                            $arr_log[$i]['status_vn'] = 'Hủy đơn hàng';
                            break;
                        case 'Delivered':
                            $arr_log[$i]['status_vn'] = 'Đã phát thành công';
                            break;
                        default:
                            $arr_log[$i]['status_vn'] = '';
                            break;
                    }
                    $temp = array(
                        'date_time' => $this->convertdatetime->convertDateTimeDisplayPlusForTracking($arr_log[$i]['date_entered']),
                        'date_time_not_convert' => $this->convertdatetime->convertDateTimeDBPlusForPortal($arr_log[$i]['date_entered']),
                        'description' => $arr_log[$i]['description'],
                        'branch_name' => $arr_log[$i]['branch_name'],
                        'hub_name' => $arr_log[$i]['hub_name'],
                        'status' => $arr_log[$i]['status'],
                        'status_vn' => $arr_log[$i]['status_vn'],
                        'reason_eng' => $arr_log[$i]['reason_eng'],
                        'reason_vn' => $arr_log[$i]['reason_vn'],

                        // 'step_from'     => $arr_log[$i]['step_from'],
                        // 'step_to'     => $arr_log[$i]['step_to'],
                        // 'parent_type'     => $arr_log[$i]['parent_type'],
                    );
                    $result[] = $temp;
                }
                array_unshift($result, $log_confirm_booking);
            }
        }
        return $result;
    }
    public function historyTrackingDomesticShipment($shipment_id = '')
    {
        $result = array();
        if ($shipment_id != '' && $shipment_id != null) {
            $shipment = $this->Shipment->getShipmentByArrayAccountId(array($shipment_id));
            $log_confirm_booking = array();
            if (count($shipment) > 0) {
                if($shipment[0]['booking_id'] != '' && $shipment[0]['booking_id'] != null) {
                    $booking = $this->Booking->getBookingByIDForTrackAndTrace($shipment[0]['booking_id']);
                    //LOG BOOKING
                    $log_confirm_booking = array(
                        'date_time' => Convertdatetime::convertDateTimeDisplayPlusForTracking($booking[0]['date_entered']),
                        'date_time_not_convert' => Convertdatetime::convertDateTimeDBPlusForPortal($booking[0]['date_entered']),
                        'description' => $booking[0]['description'],
                        'branch_name' => $booking[0]['origin_branch'],
                        'hub_name' => $booking[0]['origin_hub_code'],
                        'status' => 'Processing',
                        'status_vn' => 'Đang xử lý',
                        'reason_eng' => 'Confirmed booking',
                        'reason_vn' => 'Đã nhận được yêu cầu lấy hàng',
                    );
                    $log_booking = $this->Auditlog->getAuditLogBooking($shipment[0]['booking_id']);
                    for ($i = 0; $i < count($log_booking); $i++) {
                        if ($log_booking[$i]['status'] == 'Picked Up' || $log_booking[$i]['status'] == 'Picked up') {
                            unset($log_booking[$i]);
                        }
                    }
                    //END LOG BOOKING
                }
                $log_shipment = $this->Auditlog->getAuditLogShipment($shipment[0]['id']);
                $masterbill_detail = $this->Masterbilldetail->getMasterBillIdByShipmentId($shipment[0]['id']);

                $arr_log_shipment = array();
                foreach ($log_shipment as $key => $value) {
                    //step 1 to 2
                    if ($value['step_from'] == 1 && $value['step_to'] == 2) {
                        if ($value['parent_type'] == 'Shipment' && ($value['status'] == 'Picked Up' || $value['status'] == 'Picked up')) {
                            $arr_log_shipment[] = $value;
                        }
                    }
                    //step 7 to 8
                    if ($value['step_from'] == 7 && $value['step_to'] == 8) {
                        if ($value['status'] == 'On Delivery') {
                            $arr_log_shipment[] = $value;
                        }
                    }
                    //step 8 to 9
                    if ($value['step_from'] == 8 && $value['step_to'] == 9) {
                        if ($value['status'] == 'Detained') {
                            $arr_log_shipment[] = $value;
                        }
                        if ($value['status'] == 'On Delivery') {
                            $arr_log_shipment[] = $value;
                        }
                    }
                    //step 9 to 10
                    if ($value['step_from'] == 9 && $value['step_to'] == 10) {
                        if ($value['status'] == 'Delivered') {
                            $arr_log_shipment[] = $value;
                        }
                        if ($value['status'] == 'Detained') {
                            $arr_log_shipment[] = $value;
                        }
                        if ($value['status'] == 'Reschedule') {
                            $arr_log_shipment[] = $value;
                        }
                        if ($value['status'] == 'Canceled') {
                            $arr_log_shipment[] = $value;
                        }
                    }
                }
                //END LOG SHIPMENT
                //LOG MASTERBILL
                $log_masterbill = array();
                if (!empty($masterbill_detail)) {
                    $log_masterbill = $this->Auditlog->getAuditLogShipmentMasterbill($masterbill_detail[0]['masterbill_id']);
                    for ($i = 1; $i < (count($log_masterbill) - 2); $i++) {
                        $log_masterbill[$i]['status'] = 'In Transit';
                    }
                }
                $arr_log_masterbill = array();
                foreach ($log_masterbill as $key => $value) {
                    //step to 3
                    if ($value['step_to'] == 3) {
                        $value['status'] = 'Bagging';
                    }
                    if ($value['step_to'] == 4 || $value['step_to'] == 5 || $value['step_to'] == 6) {
                        $value['status'] = 'In Transit';
                    }
                    //step to 7
                    if ($value['step_to'] == 7) {
                        if ($value['parent_type'] == 'MasterBill') {
                            $value['status'] = 'Bagging';
                        }
                    }
                    $arr_log_masterbill[] = $value;
                }
                foreach ($arr_log_masterbill as $key => $value) {
                    if (($value['step_from'] == 3 && $value['step_to'] == 3) || ($value['step_from'] == 7 && $value['step_to'] == 7) || ($value['step_from'] == 7 && $value['step_to'] == 8)) {
                        unset($arr_log_masterbill[$key]);
                    }
                }
                //END LOG MASTERBILL

                $arr_log = array_merge($arr_log_shipment, $arr_log_masterbill);

                //SORT BY DATE_ENTERED
                for ($i = 0; $i < count($arr_log); $i++) {
                    for ($j = 0; $j < count($arr_log); $j++) {
                        if (strtotime($arr_log[$i]['date_entered']) < strtotime($arr_log[$j]['date_entered'])) {
                            $temp = $arr_log[$i];
                            $arr_log[$i] = $arr_log[$j];
                            $arr_log[$j] = $temp;
                        } else if (strtotime($arr_log[$i]['date_entered']) == strtotime($arr_log[$j]['date_entered'])) {
                            //SORT BY STEP (STATUS)
                            if ($arr_log[$i]['step_from'] < $arr_log[$j]['step_from']) {
                                $temp = $arr_log[$i];
                                $arr_log[$i] = $arr_log[$j];
                                $arr_log[$j] = $temp;
                            }
                        }
                    }
                }

                //CONVERT
                for ($i = 0; $i < count($arr_log); $i++) {
                    if ($arr_log[$i]['reason'] == 4100) {
                        $arr_log[$i]['reason_eng'] = 'Sorting';
                        $arr_log[$i]['reason_vn'] = 'Soạn hàng chuẩn bị đi giao';
                    }

                    switch ($arr_log[$i]['status']) {
                        case 'Processing':
                            $arr_log[$i]['status_vn'] = 'Đang xử lý';
                            break;
                        case 'Reschedule':
                            $arr_log[$i]['status_vn'] = 'Chưa lấy được hàng';
                            break;
                        case 'Picked Up':
                            $arr_log[$i]['status_vn'] = 'Đã nhận hàng';
                            break;
                        case 'Picked up':
                            $arr_log[$i]['status_vn'] = 'Đã nhận hàng';
                            break;
                        case 'Bagging':
                            $arr_log[$i]['status_vn'] = 'Đóng gói kiện hàng';
                            break;
                        case 'In Transit':
                            $arr_log[$i]['status_vn'] = 'Đang vận chuyển';
                            break;
                        case 'Un Bagging':
                            $arr_log[$i]['status_vn'] = 'Mở kiện hàng';
                            break;
                        case 'On Delivery':
                            $arr_log[$i]['status_vn'] = 'Đang giao hàng';
                            break;
                        case 'Detained':
                            $arr_log[$i]['status_vn'] = 'Giữ lại hàng';
                            break;
                        case 'Canceled':
                            $arr_log[$i]['status_vn'] = 'Hủy đơn hàng';
                            break;
                        case 'Delivered':
                            $arr_log[$i]['status_vn'] = 'Đã phát thành công';
                            break;
                        default:
                            $arr_log[$i]['status_vn'] = '';
                            break;
                    }
                    $temp = array(
                        'date_time' => Convertdatetime::convertDateTimeDisplayPlusForTracking($arr_log[$i]['date_entered']),
                        'date_time_not_convert' => Convertdatetime::convertDateTimeDBPlusForPortal($arr_log[$i]['date_entered']),
                        'description' => $arr_log[$i]['description'],
                        'branch_name' => $arr_log[$i]['branch_name'],
                        'hub_name' => $arr_log[$i]['hub_name'],
                        'status' => $arr_log[$i]['status'],
                        'status_vn' => $arr_log[$i]['status_vn'],
                        'reason_eng' => $arr_log[$i]['reason_eng'],
                        'reason_vn' => $arr_log[$i]['reason_vn'],
                    );
                    $result[] = $temp;
                }
                if($shipment[0]['booking_id'] != '' && $shipment[0]['booking_id'] != null) {
                    array_unshift($result, $log_confirm_booking);
                }
            }
        }
        return $result;
    }
}