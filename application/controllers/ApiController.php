<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiController extends CI_Controller {
	private $data = array();
	public function __construct(){
		parent::__construct();
		//MODEL

		//LIBRARY
		$this->load->library('callapi');
		$this->load->library('common');

		//LANGUAGE

	}
	public function getDistrict(){
		$dataParam = $this->input->post('parameters');
		$param = json_decode(base64_decode($dataParam),true);

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => $time,
			'city'=>$param
		);

		$district = $this->callapi->call($urlApi."getDistrict",$dataApi);
		if($district['status']){
			$optionDistrict = $this->common->generateSelectOption($district['status'] == 1 ?$district['data']:array(),array(),null,'id','name');
			$result = array(
				'status' => true,
				'message' => 'Success',
				'data'=>$optionDistrict
			);
		}else{
			$result = array(
				'status' => 0,
				'message' => $district['description'],
				'data'=>array()
			);
		}
		echo json_encode($result);die();
	}
	public function getWard(){
		$dataParam = $this->input->post('parameters');
		$param = json_decode(base64_decode($dataParam),true);

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => $time,
			'district'=> $param
		);

		$ward = $this->callapi->call($urlApi."getWards",$dataApi);
		if($ward['status']){
			$str = '<option></option>';
			foreach($ward['data'] as $val) {
				$str.= '<option value="'.$val['id'].'" data-is-ras="'.$val['is_ras'].'">'.$val['name'].'</option>';
			}
			$result = array(
				'status' => true,
				'message' => 'Success',
				'data'=>$str
			);
		}else{
			$result = array(
				'status' => 0,
				'message' => $ward['description'],
				'data'=>array()
			);
		}
		echo json_encode($result);die();
	}
	public function getCityForCreateBooking(){
		$country = $this->input->post('country');
		$country_decode = json_decode(base64_decode($country),true);

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => $time,
			'country'=>$country_decode
		);

		$city = $this->callapi->call($urlApi."getCityForCreateBooking",$dataApi);
		if($city['status']){
			$optionDistrict = $this->common->generateSelectOption($city['status'] == 1 ?$city['data']:array(),array(),null,'province_code','city');
			$result = array(
				'status' => true,
				'message' => 'Success',
				'data'=>$optionDistrict
			);
		}else{
			$result = array(
				'status' => 0,
				'message' => $city['description'],
				'data'=>array()
			);
		}
		echo json_encode($result);die();
	}
	public function getDistrictForCreateBooking(){
		$city = $this->input->post('city');
		$country = $this->input->post('country');
		$city_decode = json_decode(base64_decode($city),true);
		$country_decode = json_decode(base64_decode($country),true);

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => $time,
			'city'=>$city_decode,
			'country'=>$country_decode
		);

		$district = $this->callapi->call($urlApi."getDistrictForCreateBooking",$dataApi);
		if($district['status']){
			$optionDistrict = $this->common->generateSelectOption($district['status'] == 1 ?$district['data']:array(),array(),null,'district_code','district');
			$result = array(
				'status' => true,
				'message' => 'Success',
				'data'=>$optionDistrict
			);
		}else{
			$result = array(
				'status' => 0,
				'message' => $district['description'],
				'data'=>array()
			);
		}
		echo json_encode($result);die();
	}
	public function getWardForCreateBooking(){
		$district = $this->input->post('district');
		$city = $this->input->post('city');
		$country = $this->input->post('country');
		$district_decode = json_decode(base64_decode($district),true);
		$city_decode = json_decode(base64_decode($city),true);
		$country_decode = json_decode(base64_decode($country),true);

		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$time = time();
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => $time,
			'district'=> $district_decode,
			'city'=>$city_decode,
			'country'=>$country_decode
		);

		$ward = $this->callapi->call($urlApi."getWardForCreateBooking",$dataApi);
		if($ward['status']){
			$optionDistrict = $this->common->generateSelectOption($ward['status'] == 1 ?$ward['data']:array(),array(),null,'code','ward');
			$result = array(
				'status' => true,
				'message' => 'Success',
				'data'=>$optionDistrict
			);
		}else{
			$result = array(
				'status' => 0,
				'message' => $ward['description'],
				'data'=>array()
			);
		}
		echo json_encode($result);die();
	}
	public function getListAddressForBooking()
	{
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time()
		);
		$addressList = $this->callapi->call($urlApi."getListAddressByUser",$dataApi);

		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'account_id'=>$user['accounts_id'],
			'country' => 'VIETNAM'
		);
		$account = $this->callapi->call($urlApi."getAddressDefaultForCustomerPortal",$dataApi);
		$dataAccount = $account['status'] == 1 ? $account['data'] : array();
		if($addressList['status']){
			$dataAddress = $addressList['data'];
			$currentAddr = 0;
			$data = '';
			for ($i=0; $i < count($dataAddress); $i++) {
				$dataAddress[$i]['address'] = $dataAddress[$i]['street'].', '.$dataAddress[$i]['ward'].', '.$dataAddress[$i]['district'].', '.$dataAddress[$i]['city'].', '.$dataAddress[$i]['country'];
				$data .= '<div class="row contact-info-list resultRow" data-addr-id="'.$dataAddress[$i]['addr_id'].'">
                    <div class="col-xs-12 col-sm-1">
                        <div class="contact-choosen text-center">
                            <input type="radio" name="address_choosen" value="'.base64_encode(json_encode($dataAddress[$i])).'" '.($dataAddress[$i]['addr_id']==$dataAccount['addr_id']? 'checked':'').'>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-10">
                        <p class="contact-info-name pull-left">
                            <i class="fa fa-user"></i><span class="resultName">'.$dataAddress[$i]['name'].'</span>
                        </p>
                        <p class="contact-info-phone pull-right">
                            <i class="fa fa-phone"></i><span class="resultPhone">'.$dataAddress[$i]['phone'].'</span>
                        </p>
                        <div class="clearfix"></div>
                        <p class="contact-info-address">
                            <i class="fa fa-home"></i> '.$dataAddress[$i]['address'].'
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-1">
                        <div class="contact-info-action text-center">
	                        <a href="javascript:void(0);" onclick="deleteAddress(this)" data-type="shipper" data-id="'.$dataAddress[$i]['addr_id'].'">	                            
                            	<i class="fa fa-trash-o"></i>
                        	</a>
                        </div>
                    </div>
                </div>';
			}
			echo $data;
		}
	}
	public function getListAddressByUser(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time()
		);
		$addressList = $this->callapi->call($urlApi."getListAddressByUser",$dataApi);
		if($addressList['status']){
			$dataAddress = $addressList['data'];
			for ($i=0; $i < count($dataAddress); $i++) {
				$dataAddress[$i]['address'] = $dataAddress[$i]['street'].', '.$dataAddress[$i]['ward'].', '.$dataAddress[$i]['district'].', '.$dataAddress[$i]['city'].', '.$dataAddress[$i]['country'];
				$data .= '<li class="group-box resultRowShipper">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <ul>
                                <li>
                                	<i class="fa fa-user fa-2x"></i><span class="resultNameShipper">' .$dataAddress[$i]['name'].'</span>
                            	</li>
                                <li><i class="fa fa-home fa-2x"></i>' .$dataAddress[$i]['address'].'</li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <ul>
                                <li>
                                	<i class="fa fa-phone fa-2x"></i><span class="resultPhoneShipper">' .$dataAddress[$i]['phone'].'</span>
                            	</li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-1">
                            <div class="row">
                                <div class="col-xs-3 col-sm-12">
                                    <div>
                                    	<a href="#" data-toggle="modal" data-target=".info-edit-modal" data-detail_address="'.$dataAddress[$i]['addr_id'].'" data-type_address="shipper">
                                    		<i class="fa fa-pencil-square-o fa-2x"></i>
                                		</a>
                            		</div>
                                </div>
                                <div class="col-xs-3 col-sm-12">
                                    <div>
                                    	<a href="javascript:void(0);" onclick="deleteAddress(this)" data-type="shipper" data-id="'.$dataAddress[$i]['addr_id'].'">
                                    		<i class="fa fa-trash-o fa-2x"></i>
                                    	</a>
                                	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>';
			}
		echo $data;
		}
	}
	public function getListConsigneeByUser(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time()
		);
		$addressList = $this->callapi->call($urlApi."getListConsigneeByUser",$dataApi);
		if($addressList['status']){
			$dataAddress = $addressList['data'];
			for ($i=0; $i < count($dataAddress); $i++) {
				$dataAddress[$i]['address'] = $dataAddress[$i]['street'].', '.$dataAddress[$i]['ward'].', '.$dataAddress[$i]['district'].', '.$dataAddress[$i]['city'].', '.$dataAddress[$i]['country'];
				$data .= '<li class="group-box resultRowConsignee">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <ul>
                                <li><i class="fa fa-user fa-2x"></i><span class="resultNameConsignee">' .$dataAddress[$i]['name'].'</span></li>
                                <li><i class="fa fa-home fa-2x"></i>' .$dataAddress[$i]['address'].'</li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <ul>
                                <li><i class="fa fa-phone fa-2x"></i><span class="resultPhoneConsignee">' .$dataAddress[$i]['phone'].'</span></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-1">
                            <div class="row">
                                <div class="col-xs-3 col-sm-12">
                                    <div><a href="#" data-toggle="modal" data-target=".info-edit-modal" data-detail_address="'.$dataAddress[$i]['addr_id'].'" data-type_address="consignee"><i class="fa fa-pencil-square-o fa-2x"></i></a></div>
                                </div>
                                <div class="col-xs-3 col-sm-12">
                                    <div><a href="javascript:void(0);" onclick="deleteAddress(this)" data-type="consignee" data-id="'.$dataAddress[$i]['addr_id'].'">
                                    		<i class="fa fa-trash-o fa-2x"></i>
                                    	</a>
                                	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>';
			}
		echo $data;
		}
	}
	public function getDetailAddressByUser(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'addr_id' => $this->input->post('addr_id')
		);
		$addressList = $this->callapi->call($urlApi."getListAddressByUser",$dataApi);
		$addressListData = $addressList['status']? $addressList['data'][0] : array();

		echo json_encode($addressListData);
	}
	public function getDetailAddressConsignee(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'addr_id' => $this->input->post('addr_id')
		);
		$addressList = $this->callapi->call($urlApi."getListConsigneeByUser",$dataApi);
		$addressListData = $addressList['status']? $addressList['data'][0] : array();

		echo json_encode($addressListData);
	}
	public function getCityForEditAddress(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'country' => $this->input->post('country_id')
		);
		$listCity = $this->callapi->call($urlApi."getCity",$dataApi);
		$listCityData = $listCity['status']? $listCity['data'] : array();

		echo json_encode($listCityData);
	}
	public function getDistrictForEditAddress(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'city' => $this->input->post('city_id')
		);
		$listDistrict = $this->callapi->call($urlApi."getDistrict",$dataApi);
		$listDistrictData = $listDistrict['status']? $listDistrict['data'] : array();

		echo json_encode($listDistrictData);
	}
	public function getWardForEditAddress(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'district' => $this->input->post('district_id')
		);
		$listWard = $this->callapi->call($urlApi."getWards",$dataApi);
		$listWardData = $listWard['status']? $listWard['data'] : array();

		echo json_encode($listWardData);
	}
	public function updateAddressShipper(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');

		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'parameters' => $this->input->post('parameters'),
			'id' => $this->input->post('id'),
		);
		$result_api = $this->callapi->call($urlApi."updateAddressShipper",$dataApi);
		$result_data = $result_api['status']? $result_api['data'] : array();

		echo json_encode($result_data);
	}
	public function deleteAddressShipper(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');

		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'id' => $this->input->post('id'),
		);
		$result_api = $this->callapi->call($urlApi."deleteAddressShipper",$dataApi);
		$result_data = $result_api['status']? $result_api['data'] : array();

		echo json_encode($result_data);
	}
	public function deleteAddressConsignee(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');

		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'id' => $this->input->post('id'),
		);
		$result_api = $this->callapi->call($urlApi."deleteAddressConsignee",$dataApi);
		$result_data = $result_api['status']? $result_api['data'] : array();

		echo json_encode($result_data);
	}
	public function getListAddressForAutocomplete(){
		$user = $this->session->userdata('login');
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'userId' => $user?$user['id']:'',
			'timestamp' => time(),
			'query' => $this->input->get('query')
		);
		$addressList = $this->callapi->call($urlApi."getListConsigneeByUser",$dataApi);
		if($addressList['status']){
			$dataAddress = $addressList['data'];
		}else{
			$dataAddress = array();
		}
		echo json_encode($dataAddress);
	}
	public function updateDefaultAddress(){
        $user = $this->session->userdata('login');
        $urlApi = $this->config->item('url_api_portal');
        $dataApi = array(
            'userId' => $user?$user['id']:'',
            'timestamp' => time(),
            'addr_id' => $this->input->post('addr_id')
        );
        $updateDefault = $this->callapi->call($urlApi."updateDefaultAddress",$dataApi);
        echo json_encode($updateDefault);
    }
}
