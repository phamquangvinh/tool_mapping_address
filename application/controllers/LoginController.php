<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {
	private $data = array();
	public function __construct(){
		parent::__construct();
		//MODEL

		//LIBRARY
		$this->load->library('callapi');

		//LANGUAGE
		$this->lang->load('message',$this->session->userdata('site_lang'));
		$this->data["language"] = array_merge(
			$this->lang->line('language'),
			$this->lang->line('login')
		);
	}
	public function login(){
		$urlApi = $this->config->item('url_api_portal');
		$dataApi = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'timestamp' => time(),
		);
		$data = $this->callapi->call($urlApi."checkUserLogin",$dataApi);
		if($data['status'] === 1 && $data['message'] != ''){
			$this->session->set_userdata('login', $data['data']);
			$result = array(
				'status'=>true,
				'message'=>"Success",
				'data'=>'mapping-detail',
			);
            redirect('mapping-detail');
		}else{
			$result = array(
				'status'=>false,
				'message'=>$this->data["language"]["txt_error_login"],
				'data'=>array(),
			);
			if ($this->session->userdata('site_lang')=='english'){
				if ($data['status'] == 1){
			        $data['message'] = 'Can not connect to system';
                }
			    if ($data['status'] == 4){
			        $data['message'] = 'Email/Username is not exist';
                }
                if ($data['status'] == 5){
                    $data['message'] = 'Password is incorrect';
                }
                if ($data['status'] == 6){
                    $data['message'] = 'Accounts dont have permission';
                }
            }else{
				if ($data['status'] == 1){
			        $data['message'] = 'Không thể kết nối với hệ thống';
                }
                if ($data['status'] == 4){
                    $data['message'] = 'Email/ Tên đăng nhập không tồn tại';
                }
                if ($data['status'] == 5){
                    $data['message'] = 'Mật khẩu không chính xác';
                }
                if ($data['status'] == 6){
                    $data['message'] = 'Tài khoản không có quyền truy cập';
                }
            }
            $this->session->set_flashdata('error_login',array('status'=>$data['status'],'message'=>$data['message']));
            $this->session->set_flashdata('old_username',$this->input->post('username'));
            $this->session->set_flashdata('old_password',$this->input->post('password'));
            redirect('MappingController/loginPage');
		}
		echo json_encode($result);
	}
	public function logout(){
		$this->session->unset_userdata('login');
		redirect('MappingController/loginPage');
	}
}
