/**
 * [Hàm hiển thị màn hình loading]
 * status = true =>  Hiển thị màn hình loading với nội dung là msg
 * status = false => ẩn màn hình loading
 * @param  {Boolean} status [description]
 * @param  {String}  msg    [description]
 */
function loadingScreenJS(status = true, msg = '') {
    if(status && $('.waiting-screen').length == 0) {
        $('body').append('<div class="waiting-screen"><div class="content"><i class="fa fa-spinner fa-spin"></i><p class="msg">'+msg+'</p></div></div>');    
    }else{
        $('.waiting-screen').remove();
    }
}