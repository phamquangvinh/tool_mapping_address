$(document).ready(function(){
    $('.error').hide();
    //EXPORT EXCEL
    $('#export_speedlink').on('click', function() {
        var r = confirm('Export Speedlink Address Data?');
        if(r == true) {
            $('#export_speedlink').attr('href', 'MappingController/createXLSXForSpeedlink');
            $('#export_speedlink').attr('target', '_blank');
        }
    });
    // VALIDATION CREATE/EDIT ADDRESS FORM
    $("#edit_form").submit(function(e){
        if($('#select2-country_select_edit-container')[0].title == "") {
            $('#error_select_country_edit').show();
            $('#error_select_city_edit').show();
            $('#error_select_district_edit').show();
            $('#error_select_ward_edit').show();
            $('#error_select_branch_edit').show();
            alert('Missing Field!');
        } else if($('#select2-city_select_edit-container')[0].title == "") {
            $('#error_select_country_edit').hide();
            $('#error_select_city_edit').show();
            $('#error_select_district_edit').show();
            $('#error_select_ward_edit').show();
            $('#error_select_branch_edit').show();
            alert('Missing Field!');
        } else if($('#select2-district_select_edit-container')[0].title == "") {
            $('#error_select_country_edit').hide();
            $('#error_select_city_edit').hide();
            $('#error_select_district_edit').show();
            $('#error_select_ward_edit').show();
            $('#error_select_branch_edit').show();
            alert('Missing Field!');
        } else if($('#select2-ward_select_edit-container')[0].title == "") {
            $('#error_select_country_edit').hide();
            $('#error_select_city_edit').hide();
            $('#error_select_district_edit').hide();
            $('#error_select_ward_edit').show();
            $('#error_select_branch_edit').show();
            alert('Missing Field!');
        } else if($('#select2-branch_select_edit-container')[0].title == "") {
            $('#error_select_country_edit').hide();
            $('#error_select_city_edit').hide();
            $('#error_select_district_edit').hide();
            $('#error_select_ward_edit').hide();
            $('#error_select_branch_edit').show();
            alert('Missing Field!');
        }
        else {
            var r = confirm('Save data?');
            if (r == true) {
                // GET PLACEHOLDER ZIPCODE IF DON'T HAVE ANY INPUT
                if($('#zipcode_edit').val() == '') {
                    var zipcode = $('#zipcode_edit').attr('placeholder');
                } else {
                    var zipcode = $('#zipcode_edit').val();
                }
                $.ajax({
                    url: "/MappingController/saveEditAddress",
                    type: "POST",
                    data: {
                        'ward': $('#select2-ward_select_edit-container')[0].title,
                        'district': $('#select2-district_select_edit-container')[0].title,
                        'city': $('#select2-city_select_edit-container')[0].title,
                        'country': $('#select2-country_select_edit-container')[0].title,
                        'branch_code': $('#select2-branch_select_edit-container')[0].title,
                        'hub_code': $('#hub_select_edit').attr('placeholder'),
                        'address_code': $('#suggest_code_edit').attr('placeholder'),
                        'zip_code': zipcode,
                    },
                    dataType: 'json',
                    success: function (msg) {
                        alert(msg);
                        // CLEAR INPUT FIELD
                        // $('#select2-country_select_edit-container').html('');
                        $('#select2-city_select_edit-container').html('');
                        $('#select2-district_select_edit-container').html('');
                        $('#select2-ward_select_edit-container').html('');
                        $('#suggest_code_edit').removeAttr('placeholder');
                        $('#select2-branch_select_edit-container').html('');
                        $('#hub_select_edit').removeAttr('placeholder');
                        $('#suggest_code_exist_edit').hide();
                        $('#suggest_new_code_edit').hide();
                        $('#error_missing_zipcode_edit').hide();
                        $('#zipcode_edit').removeAttr('placeholder');
                        $('#zipcode_edit').val('');
                    },
                    beforeSend: function () {
                        if (!$('#zipcode_edit').val()) {
                            $('#error_missing_zipcode_edit').show();
                            $('#error_select_branch_edit').hide();
                        }
                    }
                });
            }
        }
        e.preventDefault();
    });
});


function changeCountryEdit() {
    $('#address_loading_edit').show();
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify($('#country_select_edit :selected').val()))))
        },
        success: function(data) {
            $('#address_loading_edit').hide();
            if(data.status) {
                $('#city_select_edit').html(data.data);
                $('#city_select_edit').trigger('chosen:updated');
                $('#city_select_edit').prop("disabled", false);
            }
            else {
                alert(data.message);
            }
        },
        beforeSend: function() {
            $('#city_select_edit').html("");
            $('#city_select_edit').prop("disabled", true);
            $('#district_select_edit').html("");
            $('#district_select_edit').prop("disabled", true);
            $('#ward_select_edit').html("");
            $('#ward_select_edit').prop("disabled", true);
            $('#branch_select_edit').html("");
            $('#branch_select_edit').prop("disabled", true);
            $("#hub_select_edit").removeAttr("placeholder");
            $('#suggest_code_edit').removeAttr("placeholder");
            $('#zipcode_edit').removeAttr("placeholder");
            $('#zipcode_edit').text("");
            $('#suggest_code_exist_edit').hide();
            $('#suggest_new_code_edit').hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#address_loading_edit').hide();
            alert(xhr.status+"<br>"+thrownError);
        }
    })
}
function changeCityEdit() {
    $('#address_loading_edit').show();
    if($('#city_select_edit :selected').val() != '') {
        $.ajax({
            url: "/apiController/getDistrictForCreateBooking",
            dataType: "json",
            type: 'post',
            data: {
                'city': btoa(unescape(encodeURIComponent(JSON.stringify($('#city_select_edit :selected').val())))),
                'country': btoa(unescape(encodeURIComponent(JSON.stringify($('#country_select_edit :selected').val()))))
            },
            success: function(data) {
                $('#address_loading_edit').hide();
                if(data.status) {
                    $('#district_select_edit').html(data.data);
                    $('#district_select_edit').trigger('chosen:updated');
                    $('#district_select_edit').prop("disabled", false);
                }
                else {
                    alert(data.message);
                }
            },
            beforeSend: function(){
                $('#district_select_edit').html("");
                $('#district_select_edit').trigger('chosen:updated');
                $('#district_select_edit').prop("disabled", true);

                $('#ward_select_edit').html("");
                $('#ward_select_edit').trigger('chosen:updated');
                $('#ward_select_edit').prop("disabled", true);
                $("#hub_select_edit").removeAttr("placeholder");
                $('#suggest_code_edit').removeAttr("placeholder");
                $('#zipcode_edit').removeAttr("placeholder");
                $('#suggest_code_exist_edit').hide();
                $('#suggest_new_code_edit').hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#address_loading_edit').hide();
                alert(xhr.status+"<br>"+thrownError);
            }
        });
    } else {
        $('#district_select_edit').html("");
        $('#district_select_edit').trigger('chosen:updated');
        $('#district_select_edit').prop("disabled", true);

        $('#ward_select_edit').html("");
        $('#ward_select_edit').trigger('chosen:updated');
        $('#ward_select_edit').prop("disabled", true);

        $('#address_loading_edit').hide();
    }
}
function changeDistrictEdit() {
    $('#address_loading_edit').show();
    if($('#district_select_edit :selected').val()!='') {
        $.ajax({
            url: "/apiController/getWardForCreateBooking",
            dataType: "json",
            type: 'post',
            data: {
                'district': btoa(unescape(encodeURIComponent(JSON.stringify($('#district_select_edit :selected').val())))),
                'city': btoa(unescape(encodeURIComponent(JSON.stringify($('#city_select_edit :selected').val())))),
                'country': btoa(unescape(encodeURIComponent(JSON.stringify($('#country_select_edit :selected').val())))),
            },
            success: function(data) {
                $('#address_loading_edit').hide();
                if(data.status) {
                    $('#ward_select_edit').html(data.data);
                    $('#ward_select_edit').trigger("chosen: updated");
                    $('#ward_select_edit').prop("disabled", false);
                }
                else {
                    alert(data.message);
                }
            },
            beforeSend: function() {
                $('#ward_select_edit').html("");
                $('#ward_select_edit').trigger('chosen:updated');
                $('#ward_select_edit').prop("disabled", true);
                $("#hub_select_edit").removeAttr("placeholder");
                $('#suggest_code_edit').removeAttr("placeholder");
                $('#zipcode_edit').removeAttr("placeholder");
                $('#suggest_code_exist_edit').hide();
                $('#suggest_new_code_edit').hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#address_loading').hide();
                alert(xhr.status+"<br>"+thrownError);
            }
        });
    }
    else {
        $('#ward_select_edit').html("");
        $('#ward_select_edit').trigger('chosen:updated');
        $('#ward_select_edit').prop("disabled", true);

        $('#address_loading_edit').hide();
    }
}
function changeWardEdit() {
    $('#address_loading_edit').show();
    if($('#district_select_edit :selected').val()!='') {
        $.ajax({
            url: "/MappingController/getCodeEdit",
            type: 'post',
            data: {
                'ward': $('#select2-ward_select_edit-container')[0].title,
                'district': $('#select2-district_select_edit-container')[0].title,
                'city': $('#select2-city_select_edit-container')[0].title,
                'country': $('#select2-country_select_edit-container')[0].title,
            },
            dataType: 'json',
            success: function (data) {
                $('#address_loading_edit').hide();
                $('#suggest_new_code_edit').html('');
                $("#suggest_code_edit").attr("placeholder", data['address_code']);
                $('#zipcode_edit').attr("placeholder", data['zip_code']);
                if (data['new_data'] == 0) {
                    $('#suggest_code_exist_edit').html('<span style="color: red">* Existing Code!</span>');
                    $('#suggest_code_exist_edit').show();
                    $('#suggest_new_code_edit').html('');
                    $('#suggest_new_code_edit').hide();
                }
                else {
                    $('#suggest_new_code_edit').html('<span style="color:green">* Code Suggesting!</span>');
                    $('#suggest_new_code_edit').show();
                    $('#suggest_code_exist_edit').html('');
                    $('#suggest_code_exist_edit').hide();
                }
                $.ajax({
                    url: "/MappingController/getListBranch",
                    dataType: 'json',
                    success: function (data) {
                        if(data.status) {
                            $('#branch_select_edit').html(data.data);
                            $('#branch_select_edit').trigger("chosen: updated");
                            $('#branch_select_edit').prop("disabled", false);
                            $('#hub_select_edit').removeAttr("placeholder");
                        }
                        else {
                            alert(data.message);
                        }
                    }
                });
            }
        });
    } else {
        $('#address_loading_edit').hide();
    }
}
function changeBranchEdit() {
    $('#address_loading_edit').show();
    if($('#branch_select_edit :selected').val()!='') {
        $.ajax({
            url: "/MappingController/getHubByBranch",
            dataType:'json',
            type:'post',
            data: {
                'branch': $('#select2-branch_select_edit-container')[0].title,
            },
            success: function(data) {
                $('#address_loading_edit').hide();
                $('#hub_select_edit').attr("placeholder", data['hubcode']);
            }
        });
    }
}
