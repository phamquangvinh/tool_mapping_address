$(document).ready(function() {
    $('.error').hide();
});
function changePassword() {
    loadingScreenJS(true);
    $('.error').hide();
	$('#user-changepass-form #error_change_password').hide();
	$('#user-changepass-form #success_change_password').hide();
	var form = $('form').get();
    var fData = new FormData(form);
    fData.append('old_password', $('#user-changepass-form #old_password').val());
    fData.append('new_password', $('#user-changepass-form #new_password').val());
    fData.append('confirmed_password', $('#user-changepass-form #confirmed_password').val());
    $.ajax({
        url: '/UserController/changePassword',
        type: 'POST',
        data: fData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
            console.log(data);
            loadingScreenJS(false);
            if(data.result === "failed"){
                alert(data.message);
            }else{
                if(data.result === 'success'){
                    alert(data.message);
                    window.location.href = "/logout";
                }else{
                    alert(data.result_check);
                    var error = data.error;
                    showErrorValidateForm(error);
                }
            }

        },
        error: function(xhr, ajaxOption, throwError) {
            loadingScreenJS(false);
        }
    });
}
function showErrorValidateForm(error){
    for (var i = 0; i < error.length; i++){
        if (error[i].name == "user_login") {
            alert(error[i].message);
        }
        if (error[i].name == "old_password") {
            $('#error_old_password').html(error[i].message);
            $('#error_old_password').show();
        }
        if (error[i].name == "new_password") {
            $('#error_new_password').html(error[i].message);
            $('#error_new_password').show();
        }
        if (error[i].name == "confirmed_password") {
            $('#error_retype_password').html(error[i].message);
            $('#error_retype_password').show();
        }
    }
}