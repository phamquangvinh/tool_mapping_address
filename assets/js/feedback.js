var spinnerFrom,spinnerTo,spinnerLogin;
var from_date = $("#from_date").datepicker({
    ignoreReadonly: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true
});

var to_date = $("#to_date").datepicker({
    ignoreReadonly: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true
});

if (site_lang == 'vi') {
    var language = {
        "sEmptyTable":   "Không có dữ liệu phù hợp",
        "sProcessing":   "Đang xử lý...",
        "sLengthMenu":   "Xem _MENU_ mục",
        "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
        "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
        "sInfoFiltered": "(được lọc từ _MAX_ mục)",
        "sInfoPostFix":  "",
        "sSearch":       "Tìm:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Đầu",
            "sPrevious": "Trước",
            "sNext":     "Tiếp",
            "sLast":     "Cuối"
        }
    };
}else{
    var language = {
        "sEmptyTable":     "No data available in table",
        "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
        "sInfoFiltered":   "(filtered from _MAX_ total entries)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "Show _MENU_ entries",
        "sLoadingRecords": "Loading...",
        "sProcessing":     "Processing...",
        "sSearch":         "Search:",
        "sZeroRecords":    "No matching records found",
        "oPaginate": {
            "sFirst":    "First",
            "sLast":     "Last",
            "sNext":     "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending":  ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    };
}
$(document).ready(function() {
    var data = JSON.parse(atob(dataTable));
    var table = $('#table_list_cases').DataTable({
        "columns": [
            {'data': 'no', 'width': '5%'},
            {'data': 'title','width': '10%',},
            {'data': 'order_no', 'width': '15%'},
            {'data': 'date_entered', 'width': '10%'},
            {'data': 'status', 'width': '10%'},
            {'data': 'date_modified', 'width': '10%'},
            {'data': 'button', 'width': '7%'}
        ],
        "language": language,
        "scrollX": true
    });
    $('#table_list_cases').DataTable().clear().draw();
    $('#table_list_cases').DataTable().rows.add(data).draw();

    $('.feedback-detail-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var feedback_id = button.data('detail_feedback'); // Extract info from data-* attributes
        var modal = $(this);
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            url: "FeedBackController/getDetailFeedBack",
            type: "POST",
            dataType:"json",
            data: {
                'feedback_id' : feedback_id
            },
            success: function (data, status, jqXHR) {
                modal.find('#title').text(data.name);
                modal.find('#order_no').text(data.order_id);
                modal.find('#description').text(data.description);
                modal.find('#resolution').text(data.resolution);
                modal.find('#status').text(data.status);
                modal.find('#date_entered').text(data.date_entered);
                modal.find('#edit_feedback').attr("data-id", data.id);
                if (data.status == 'New') {
                    $('#edit_feedback').show();
                }else{
                    $('#edit_feedback').hide();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });

    if (!$('#from_date').val() && !$('#to_date').val()) {
        changeDateRange();
    }
});
function changeDateRange(){
    from_date.datepicker('setDate', $('#date_range :selected').attr('from'));
    to_date.datepicker('setDate', $('#date_range :selected').attr('to'));
}
function deleteFeedback(_this){
    var id = $(_this).data('id');
    if (confirm("Are you sure for delete Feedback?")) {
        $.ajax({
            url: "/FeedBackController/deleteFeedback",
            dataType: "json",
            type: 'post',
            data: {
                'feedback_id' : id
            },
            //async: false,
            success: function (data) {
                alert(data);
                loadingScreenJS(false);
                location.reload();
            },
            beforeSend: function(){
                loadingScreenJS(true);
            }
        });
    }
    return false;
}
function createFeedback(){
    var title = $('#add_feedback_title').val();
    var order_no = $('#add_feedback_order_no').val();
    var description = $('#add_feedback_description').val();    
    $.ajax({
        url: "/FeedBackController/CreateFeedback",
        dataType: "json",
        type: 'post',
        data: {
            'title'         : title,
            'order_no'      : order_no,
            'description'   : description
        },
        //async: false,
        success: function (data) {
            if(data.err) {
                loadingScreenJS(false);
                alert(data.msg);
            } else{
                $('.add-feedback-modal').modal('hide');
                alert(data.msg);
                clearDataCreateFeedback();
                loadingScreenJS(false);
                location.reload();
            }
        },
        beforeSend: function(){
            loadingScreenJS(true);
        }
    });
}
function clearDataCreateFeedback(){
    $('#add_feedback_title').val('');
    $('#add_feedback_description').val('');
    $('#add_feedback_order_no').val(null).trigger("change");
}
function clearDataEditFeedback(){
    $('#edit_feedback_title').val('');
    $('#edit_feedback_description').val('');
    $('#edit_feedback_order_no').val(null).trigger("change");
}
function editFeedback(_this){
    var modal = $(_this).closest('.feedback-detail-modal');
    var old_title = modal.find('#title').text();
    var old_description = modal.find('#description').text();
    var old_order_no = modal.find('#order_no').text();
    var feedback_id = modal.find('#edit_feedback').data('id');
    $('.modal').modal('hide');
    $('.edit-feedback-modal').modal('show');
    $('.edit-feedback-modal').find('#edit_feedback_title').val(old_title);
    $('.edit-feedback-modal').find('#edit_feedback_order_no').val(old_order_no).trigger("change").select2("close");
    $('.edit-feedback-modal').find('#edit_feedback_description').val(old_description);

    $('#edit_feedback_btn').click(function() {        
        var new_title = $('.edit-feedback-modal').find('#edit_feedback_title').val();
        var new_order_no = $('.edit-feedback-modal').find('#edit_feedback_order_no').val();
        var new_description = $('.edit-feedback-modal').find('#edit_feedback_description').val();
        $.ajax({
            url: "/FeedBackController/updateFeedback",
            dataType: "json",
            type: 'post',
            data: {
                'feedback_id'   : feedback_id,
                'title'         : new_title,
                'order_no'      : new_order_no,
                'description'   : new_description
            },
            //async: false,
            success: function (data) {
                alert(data);
                $('.edit-feedback-modal').modal('hide');
                clearDataEditFeedback();                
                loadingScreenJS(false);
                location.reload();
            },
            beforeSend: function(){            
                loadingScreenJS(true);
            },
        });
    });
}
function ResetFormFeedback() {
    $('#date_range').val('Last_7_Days');
    changeDateRange();
    $('input[name="order_no"]').val('');
    $('select[name="status"]').val('');
}
