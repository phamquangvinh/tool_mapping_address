var spinnerFrom,spinnerTo,spinnerLogin;
var from_date = $("#from_date").datepicker({
    ignoreReadonly: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true
});

var to_date = $("#to_date").datepicker({
    ignoreReadonly: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true
});
if (site_lang == 'vi') {
    var language = {
        "sEmptyTable":   "Không có dữ liệu phù hợp",
        "sProcessing":   "Đang xử lý...",
        "sLengthMenu":   "Xem _MENU_ mục",
        "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
        "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
        "sInfoFiltered": "(được lọc từ _MAX_ mục)",
        "sInfoPostFix":  "",
        "sSearch":       "Tìm:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Đầu",
            "sPrevious": "Trước",
            "sNext":     "Tiếp",
            "sLast":     "Cuối"
        }
    };
}else{
    var language = {
        "sEmptyTable":     "No data available in table",
        "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
        "sInfoFiltered":   "(filtered from _MAX_ total entries)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "Show _MENU_ entries",
        "sLoadingRecords": "Loading...",
        "sProcessing":     "Processing...",
        "sSearch":         "Search:",
        "sZeroRecords":    "No matching records found",
        "oPaginate": {
            "sFirst":    "First",
            "sLast":     "Last",
            "sNext":     "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending":  ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    };
}
$(document).ready(function() {
    if (!$('#from_date').val() && !$('#to_date').val()) {
        changeDateRange();
    }
    var data = JSON.parse(atob(dataTable));
    var table = $('#table_list_booking').DataTable({
        "columns": [
            {'data': 'check_box', 'width': '4%'},
            {'data': 'no', 'width': '5%'},
            {'data': 'booking_no','width': '10%',},
            {'data': 'pickup_address', 'width': '15%'},
            {'data': 'contact_delivery_name', 'width': '10%'},
            {'data': 'delivery_address', 'width': '10%'},
            {'data': 'phone_delivery', 'width': '10%'},
            {'data': 'specical_req', 'width': '7%'},
            {'data': 'pickup_date_view', 'width': '7%'},
            {'data': 'status', 'width': '10%'}
        ],
        "scrollX": true,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "language": language,
        "order": [[1, 'asc']]
    });
    $('#table_list_booking').DataTable().clear().draw();
    $('#table_list_booking').DataTable().rows.add(data).draw();

    // Handle click on "Select all" control
    $('#checkAllBooking').on('click', function(){
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#table_list_booking tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#checkAllBooking').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    $('#booking-detail-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var booking_id = button.data('detail_booking'); // Extract info from data-* attributes
        var modal = $(this);
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            url: "BookingController/getDetailBooking",
            type: "POST",
            dataType:"json",
            data: {
                'booking_id' : booking_id
            },
            success: function (data, status, jqXHR) {
                modal.find('#print_detail_booking').val(booking_id);
                modal.find('#booking_duplicate').val(booking_id);
                modal.find('#booking_title').text(data.name);
                modal.find('#order_no').text(data.tracking_no);
                modal.find('#company_name').text(data.company_name);
                modal.find('#shipper_name').text(data.shipper_contact_name);
                modal.find('#io_undelivery').text(data.io_undelivery);
                modal.find('#phone_shipper').text(data.phone_shipper);
                modal.find('#receiver_name').text(data.receiver_name);
                modal.find('#receiver_company').text(data.receiver_company);
                modal.find('#delivery_address').text(data.address_delivery);
                modal.find('#phone_delivery').text(data.phone_delivery);
                modal.find('#shipment_type').text(data.shipment_type);
                modal.find('#cod_value').text(data.cod_value);
                modal.find('#weight').text(data.weight);
                modal.find('#coupon_code').text(data.coupon_code);
                modal.find('#booking_date').text(data.booking_date);
                modal.find('#pickup_date').text(data.pickup_date);
                modal.find('#pickup_address').text(data.pickup_address);
                modal.find('#specical_req').text(data.specical_req);
                modal.find('#payment_method').text(data.payment_method);
                modal.find('#charge_to').text(data.charge_to);
                modal.find('#booking_note').text(data.booking_note);
                $('.btn-group-action').show();
            },
            beforeSend: function(){
                $('.btn-group-action').hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });

    $('#export_excel_booking').click(function(){
        var arr_id = [];
        table.$('input[type=checkbox]:checked').each(function(i,e){
            arr_id.push($(this).val());
        });
        if (arr_id.length>0){
            $.ajax({
                url: "/BookingController/ExportBookingToExcel",
                type: 'post',
                data: {
                    'arr_id' : arr_id
                },
                //async: false,
                success: function (response) {
                    loadingScreenJS(false);
                    window.location.href = response.url;
                    //window.open(response.url,'_blank');
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                }
            });
        }else{
            alert(exportNull);
        }
    });

    $('#print_booking').click(function(){
        var arr_id = [];
        table.$('input[type=checkbox]:checked').each(function(i,e){
            arr_id.push($(this).val());
        });
        if (arr_id.length>0){
            $.ajax({
                url: "/BookingController/printBooking",
                type: 'post',
                data: {
                    'arr_id' : arr_id
                },
                //async: false,
                success: function (data) {
                    loadingScreenJS(false);
                    //window.location.href = response.url;
                    window.open(data,'_blank');
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                }
            });
        }else{
            alert(printNull);
        }
        //window.open('BookingController/print_booking/'+arr_id, '_blank');
    });

    $('#print_detail_booking').click(function(){
        var arr_id = [$('#print_detail_booking').val()];
        $.ajax({
            url: "/BookingController/printBooking",
            type: 'post',
            data: {
                'arr_id' : arr_id
            },
            //async: false,
            success: function (data) {   
                loadingScreenJS(false);             
                window.open(data,'_blank');
            },
            beforeSend: function(){
                loadingScreenJS(true);
            }
        });
    });

    $('#booking_duplicate').click(function(){
        $('.booking-detail-modal').modal('hide');
        loadingScreenJS(true);
        var id = $('#booking_duplicate').val();
        location.href = '/booking-duplicate/'+id;
    });

    if (!$('#from_date').val() && !$('#to_date').val()) {
        changeDateRange();
    }
});
function changeDateRange(){
    from_date.datepicker('setDate', $('#date_range :selected').attr('from'));
    to_date.datepicker('setDate', $('#date_range :selected').attr('to'));
}
function resetFilterSearchBooking(){
    $('#date_range').val('Last_7_Days');
    changeDateRange();
    $('#booking_no').val('');
    $('#status').val('');
}