$(document).ready(function() {    
    $('.error').hide();
    if (site_lang == 'vi') {
        var language = {
            "sEmptyTable":   "Không có dữ liệu phù hợp",
            "sProcessing":   "Đang xử lý...",
            "sLengthMenu":   "Xem _MENU_ mục",
            "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
            "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix":  "",
            "sSearch":       "Tìm:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Đầu",
                "sPrevious": "Trước",
                "sNext":     "Tiếp",
                "sLast":     "Cuối"
            }
        };
    }else{
        var language = {
            "sEmptyTable":     "No data available in table",
            "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
            "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
            "sInfoFiltered":   "(filtered from _MAX_ total entries)",
            "sInfoPostFix":    "",
            "sInfoThousands":  ",",
            "sLengthMenu":     "Show _MENU_ entries",
            "sLoadingRecords": "Loading...",
            "sProcessing":     "Processing...",
            "sSearch":         "Search:",
            "sZeroRecords":    "No matching records found",
            "oPaginate": {
                "sFirst":    "First",
                "sLast":     "Last",
                "sNext":     "Next",
                "sPrevious": "Previous"
            },
            "oAria": {
                "sSortAscending":  ": activate to sort column ascending",
                "sSortDescending": ": activate to sort column descending"
            }
        };
    }
    getListAddress();
    spinnerCountry  = new Spinner('country_row');
    spinnerCity     = new Spinner('city_row');
    spinnerDistrict = new Spinner('district_row');
    
    $('.chosenSelect2').select2({
        language : site_lang,
        width: '100%',
        dropdownParent: $(this).parent()
    });

    $("#default").on('change', function() {
        if ($(this).is(':checked')) {
        $(this).attr('value', '1');
        } else {
        $(this).attr('value', '0');
        }
    });

    $("#save_address").on('change', function() {
        if ($(this).is(':checked')) {
        $(this).attr('value', '1');
        } else {
        $(this).attr('value', '0');
        }
    }); 

    $('#list_shipment_type').DataTable({
        "paging": false,
        "searching": false,
        "info":     false,
        "language": language,
        "columns": [
            {'data': 'check_box','width': '10%','className': "text-center"},
            {'data': 'service', 'width': '30%','className': "text-center"},
            {'data': 'price', 'width': '30%','className': "text-center"},
            {'data': 'description', 'width': '30%','className': "text-center"}
        ]
    });

    $('.autoFillBooking').autocomplete({
        source: function (request, response) {
            jQuery.get("/apiController/getListAddressForAutocomplete", {
                query: request.term
               
            }, function (data) {
                response(jQuery.parseJSON(data));
            });
        },
        //minLength: 2,
        select: function(event, ui) {
            // console.log(ui.item.city_id);
            //event.preventDefault();
            $('.autoFillBooking').val(ui.item.name);
            // $('#receiver_name').val(ui.item.name);
            $('#receiver_phone').val(ui.item.phone);
            $('#receiver_country').val(ui.item.country_id).trigger("change").select2("close");
            $('#receiver_zip_code').val(ui.item.zipcode);
            
            $.when(changeCountryAsync(ui.item.country_id), changeCityAsync(ui.item.city_id), changeDistrictAsync(ui.item.district_id)).done(function(country, city, district){
                if(country[0].status) {
                    $('#receiver_city').html(country[0].data);
                    $('#receiver_city').val(ui.item.city_id);
                    $('#receiver_city').trigger('chosen:updated');
                    $('#receiver_city').prop("disabled", false);
                }
                if(city[0].status) {
                    $('#receiver_district').html(city[0].data);
                    $('#receiver_district').val(ui.item.district_id);
                    $('#receiver_district').trigger('chosen:updated');
                    $('#receiver_district').prop("disabled", false);
                }
                if(district[0].status) {
                    $('#receiver_ward').html(district[0].data);
                    $('#receiver_ward').val(ui.item.ward_id);
                    $('#receiver_ward').trigger('chosen:updated');
                    $('#receiver_ward').prop("disabled", false);
                }
                spinnerCountry.hide();
                spinnerCity.hide();
                spinnerDistrict.hide();
                getListShipmentType();
            });
            $('#receiver_address').val(ui.item.street);
            return false;
        }
    }).data('ui-autocomplete')._renderItem = function(ul, item) {
        return $('<li>').addClass('info-item')
        .append('<div class = "row info-inner">' + '<div class = "col-xs-6 info-name">' +item.name+ '</div>' + '<div class = "col-xs-6 info-phone text-right">' +item.phone+ '</div>' + '</div>')
        .appendTo(ul);
    };

    $('#inputSearch').keyup(function() {
        liveSearch();
    });
    getListShipmentType();

    //validatorweight
    $('.add-info.modal #add_phone').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, F5, space
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 32) {
            e.preventDefault();
        }
    });
    $('.add-info.modal #add_phone').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });

    //validatorweight
    $('.order-create-section #weight').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, . .(number pad)and F5
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 110, 116]) !== -1 ||
            // Allow: Comma
            (e.keyCode === 188) ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('.order-create-section #weight').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });

     //COD 
    $('.order-create-section #cod_value, .order-create-section #cod, .order-create-section #shipment_value, .order-create-section #pcs').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and F5
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('.order-create-section #cod_value, .order-create-section #cod, .order-create-section #shipment_value, .order-create-section #pcs').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });

    //validatorweight
    $('.order-create-section #receiver_phone').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, F5, space
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 32) {
            e.preventDefault();
        }
    });
    $('.order-create-section #receiver_phone').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });
});
function changeCountryAsync(country) {
    spinnerCountry.show("Processing");
    return $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( country ))))
        },
        async: true
    });
}
function changeCityAsync(city) {
    spinnerCity.show("Processing");
    return $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( city )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_country :selected').val() ))))
        },
        async: true});
}
function changeDistrictAsync(district) {
    spinnerDistrict.show("Processing");
    return $.ajax({
        url: "/apiController/getWardForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'district': btoa(unescape(encodeURIComponent(JSON.stringify( district )))),
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_country :selected').val() ))))
        },
        async: true});
}
function changeCountry(){
    spinnerCountry.show("Processing");
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_country :selected').val() ))))
        },
        async: true,
        success: function (data) {
            spinnerCountry.hide();
            if(data.status) {
                $('#receiver_city').html(data.data);
                $('#receiver_city').trigger('chosen:updated');
                $('#receiver_city').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#receiver_city").html("");
            $("#receiver_city").prop('disabled', true);
            $("#receiver_district").html("");
            $("#receiver_district").prop('disabled', true);
            $("#receiver_ward").html("");
            $("#receiver_ward").prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            spinnerCountry.hide();
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeCity(){
    spinnerCity.show("Processing");
    if($('#city_row').parent().children('.error').length > 0) $('#city_row').parent().children('.error').hide();
    if($('#receiver_city :selected').val() != '') {
        $.ajax({
            url: "/apiController/getDistrictForCreateBooking",
            dataType: "json",
            type: 'post',
            data: {
                'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_city :selected').val() )))),
                'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_country :selected').val() ))))
            },
            async: true,
            success: function (data) {
                spinnerCity.hide();
                if(data.status) {
                    $('#receiver_district').html(data.data);
                    $('#receiver_district').trigger('chosen:updated');
                    $('#receiver_district').prop("disabled", false);
                }else{
                    alert(data.message);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                spinnerCity.hide();
                alert(xhr.status+"<br>"+thrownError);
            }
        });
    }else{
        $('#receiver_district').html('');
        $('#receiver_district').trigger('chosen:updated');
        $('#receiver_district').prop('disabled', true);

        $('#receiver_ward').html('');
        $('#receiver_ward').trigger('chosen:updated');
        $('#receiver_ward').prop('disabled', true);
        spinnerCity.hide();
    }
    
}
function changeDistrict(){
    spinnerDistrict.show("Processing");
    if($('#district_row').parent().children('.error').length > 0) $('#district_row').parent().children('.error').hide();
    if($('#receiver_district :selected').val()){
        $.ajax({
            url: "/apiController/getWardForCreateBooking",
            dataType: "json",
            type: 'post',
            data: {
                'district': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_district :selected').val() )))),
                'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_city :selected').val() )))),
                'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_country :selected').val() ))))
            },
            async: true,
            success: function (data) {
                spinnerDistrict.hide();
                if(data.status) {
                    $('#receiver_ward').html(data.data);
                    $('#receiver_ward').trigger('chosen:updated');
                    $('#receiver_ward').prop("disabled", false);
                }else{
                    alert(data.message);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                spinnerDistrict.hide();
                alert(xhr.status+"<br>"+thrownError);
            }
        });
    }else{
        $('#receiver_ward').html('');
        $('#receiver_ward').trigger('chosen:updated');
        $('#receiver_ward').prop('disabled', true);
        spinnerDistrict.hide();
    }
   
}
function getListAddress(hasLoad = false){
    $.ajax({
        url: "/apiController/getListAddressForBooking",
        type: 'post',
        //async: false,
        success: function (data) {
            $('#list_address_body').html(data);
            if(hasLoad) {
                var address = $("#list_address_body input[name=address_choosen]:checked").val();
                var add = JSON.parse(atob(address));
                $('#short_name').html(add['name']);
                $('#mobile_phone').html(add['phone']);
                $('#address').html(add['address']);
                console.log(add);
                $('#shipper_city').val(add['city_id']);
                $('#shipper_country').val(add['country_id']);
                $('#shipper_district').val(add['district_id']);
                $('#shipper_ward').val(add['ward_id']);
                $('#shipper_address').val(add['address']);
            }
        }
    });
}
function changeCountryAddress(){
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#add_city').html(data.data);
                $('#add_city').trigger('chosen:updated');
                $('#add_city').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_city").html("");
            $("#add_city").prop('disabled', true);
            $("#add_district").html("");
            $("#add_district").prop('disabled', true);
            $("#add_ward").html("");
            $("#add_ward").prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeCityAddress(){
    $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            spinnerCity.hide();
            if(data.status) {
                $('#add_district').html(data.data);
                $('#add_district').trigger('chosen:updated');
                $('#add_district').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_district").html("");
            $("#add_district").prop('disabled', true);
            $("#add_ward").html("");
            $("#add_ward").prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeDistrictAddress(){
    $.ajax({
        url: "/apiController/getWardForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'district': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_district :selected').val() )))),
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            spinnerDistrict.hide();
            if(data.status) {
                $('#add_ward').html(data.data);
                $('#add_ward').trigger('chosen:updated');
                $('#add_ward').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_ward").html("");
            $("#add_ward").prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function saveAddress(){
    if (validateFormAddAddress()) {
        var parameters = {
            'user_name'     : $('#add_name').val(),
            'user_phone'    : $('#add_phone').val(),
            'user_street'   : $('#add_street').val(),
            'user_country'  : $('#add_country').val(),
            'user_city'     : $('#add_city').val(),
            'user_district' : $('#add_district').val(),
            'user_ward'     : $('#add_ward').val(),
            'postal_code'   : $('#add_zip_code').val(),
            'is_default'    : $('#default').val(),
            'deleted'       : 0        
        };
        $.ajax({
            url: "/BookingController/saveAddressShipper",
            dataType: "json",
            type: 'post',
            data: {
                'parameters': parameters
            },
            async: true,
            success: function (data) {
                if (data.err) {
                    alert(data.msg);
                    $('.add-info').modal('hide');
                    getListAddress(true);
                    resetFormAddAddress();
                    loadingScreenJS(false);
                }else{
                    alert(data.msg);
                }
                loadingScreenJS(false);
            },
            beforeSend: function(){
                loadingScreenJS(true);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status+"<br>"+thrownError);
                loadingScreenJS(false);
            }
        });

    }else{
        alert($('.add-info #txt_add_address_required').val());
    }
}
function changeAddress(){
    var address = $("input[name=address_choosen]:checked").val();
    if (address) {        
        var add = JSON.parse(atob(address));
        $('.shiper-info-modal').modal('hide');
        $('#short_name').html(add['name']);
        $('#mobile_phone').html(add['phone']);
        $('#address').html(add['address']);
        $('#shipper_city').val(add['city_id']);
        $('#shipper_country').val(add['country_id']);
        $('#shipper_district').val(add['district_id']);
        $('#shipper_ward').val(add['ward_id']);
        $('#shipper_address').val(add['address']);
        getListShipmentType();
        $.ajax({
            url: "/ApiController/updateDefaultAddress",
            dataType: "json",
            type: 'post',
            data: {
                'addr_id': add['addr_id']
            },
            //async: false,
            success: function (data) {

            },
            beforeSend: function(){
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status+"<br>"+thrownError);
            }
        });
    }else{
        alert('Vui long chon dia chi');
    }
}
function resetFormAddAddress(){
    $('#add_name').val('');
    $('#add_phone').val('');
    $('#add_street').val('');
    $('#add_zip_code').val('');
    $('#add_country').prop('selectedIndex',0);
    $('#select2-add_country-container').text('');
    $('#add_city').prop('selectedIndex',0);
    $('#select2-add_city-container').text('');
    $("#add_ward").html("");
    $("#add_district").html("");
}
function getListShipmentType(){
    var cod_value = $('#cod_value').val();
    if (cod_value && cod_value!=0) {
        is_cod = 1;
    }else{
        is_cod = 0;
    }
    var parameters = {
        'is_cod'                : is_cod,
        'is_dox'                : $('#is_dox').val(),
        'cod_value'             : cod_value,
        'weight'                : $('#weight').val(),
        'origin_branch_code'    : $('#shipper_city').val(),
        'shipper_city'          : $('#shipper_city').val(),
        'receiver_city'         : $('#receiver_city').val(),
        'dest_branch_code'      : $('#receiver_city').val()
    };
    if (parameters['is_dox'] && parameters['shipper_city'] && parameters['weight'] && parameters['receiver_city'] ) {
        $.ajax({
            url: "/BookingController/getListShipmentType",
            dataType: "json",
            type: 'post',
            data: {
                'parameters': parameters
            },
            //async: false,
            success: function (data) {
                $('#list_shipment_type').DataTable().clear().draw();
                $('#list_shipment_type').DataTable().rows.add(data).draw();

                //Hide error
                $('#list_shipment_type input[type=radio]').change(function(e) {
                    $('#list_shipment_type_wrapper').parent().children('.error').hide();
                });
            },
            beforeSend: function(){            
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status+"<br>"+thrownError);
            }
        });
    }
}
/**
 * Load script create order form
 * @return {[type]} [description]
 */
function loadScriptCreateOrder() {
    $('input[type=input], input[type=number]').blur(function(e) {
        if($(this).val() != '' && $(this).parent().children('.error').length > 0) $(this).parent().children('.error').hide();
    })
}
function createOrder(){
    if (validateForm()) {
        var parameters = {       
            "is_dox"                : $("#is_dox").val(),
            "weight"                : $("#weight").val() ? $("#weight").val() : 0,
            "charge_to"             : $("#charge_to").val(),
            "cod_value"             : $("#cod_value").val() ? $("#cod_value").val() : 0,
            "payment_method"        : $("#payment_method").val(),
            "pcs"                   : $("#pcs").val() ? $("#pcs").val() : 0,
            "shipment_value"        : $("#shipment_value").val() ? $("#shipment_value").val() : 0,
            "description"           : $("#description").val(),
            "receiver_name"         : $("#receiver_name").val(),
            "receiver_phone"        : $("#receiver_phone").val(),
            "receiver_country"      : $("#receiver_country").val(),
            "receiver_country_text" : $("#select2-receiver_country-container").attr('title'),
            "receiver_zip_code"     : $("#receiver_zip_code").val(),
            "receiver_city"         : $("#receiver_city").val(),
            "receiver_city_text"    : $("#select2-receiver_city-container").attr('title'),
            "receiver_district"     : $("#receiver_district").val(),
            "receiver_district_text": $("#select2-receiver_district-container").attr('title'),
            "receiver_ward"         : $("#receiver_ward").val(),
            "receiver_ward_text"    : $("#select2-receiver_ward-container").attr('title')?$("#select2-receiver_ward-container").attr('title'):'',
            "receiver_street"       : $("#receiver_address").val(),
            "service_code"          : $("input[name=shipment_type]:checked").val(),
            "shipper_contact_name"  : $("#short_name").html(),
            "phone_shipper"         : $("#mobile_phone").html(),
            "pickup_address"        : $("#address").html(),
            "pickup_ward"           : $("#shipper_ward").val(),
            "pickup_district"       : $("#shipper_district").val(),
            "pickup_city"           : $("#shipper_city").val(),
            "pickup_country"        : $("#shipper_country").val(),
            "order_no"              : $("#tracking_no").val(),
            "save_address"          : $("#save_address").val()
        };
        $.ajax({
            url: "/BookingController/createOrder",
            dataType: "json",
            type: 'post',
            data: {
                'parameters': btoa(unescape(encodeURIComponent(JSON.stringify( parameters ))))
            },
            //async: false,
            success: function (data) {
                if (data.status == 1) {
                    alert(data.message);
                    location.href = '/booking-list';
                }else{
                    alert(data.message);
                }
                loadingScreenJS(false);
            },
            beforeSend: function(){            
                loadingScreenJS(true);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status+"<br>"+thrownError);
                loadingScreenJS(false);
            }
        });
    }else{
        alert($('.order-create-section .msg_alert_empty_field').val());
    }
}
function validateForm(){
    $('.error').hide();
    var check = true;
    var receiver_name = $("#receiver_name").val();
    var receiver_country = $("#receiver_country").val();
    var receiver_city = $("#receiver_city").val();
    var receiver_district = $("#receiver_district").val();
    var shipper_country = $("#shipper_country").val();
    var shipper_city = $("#shipper_city").val();
    var shipper_district = $("#shipper_district").val();
    var shipper_address = $("#address").html();
    //var receiver_ward = $("#receiver_ward").val();
    var receiver_address = $("#receiver_address").val();
    var receiver_phone = $("#receiver_phone").val();
    var weight = $("#weight").val();

    if(!receiver_name){
        $('#error_receiver_name').show();
        check = false;
    }
    if(!receiver_country){
        $('#error_receiver_country').show();
        check = false;
    }
    if(!receiver_city){
        $('#error_receiver_city').show();
        check = false;
    }
    if(!receiver_district){
        $('#error_receiver_district').show();
        check = false;
    }
    if(!shipper_country || !shipper_city || !shipper_district || !shipper_address ){
        $('#error_shipper_info').show();
        $('#error_shipper_info').css('position','inherit');
        check = false;
    }
    // if(!receiver_ward){
    //     $('#error_receiver_ward').show();
    //     check = false;
    // }
    if(!receiver_address){
        $('#error_receiver_street').show();
        check = false;
    }
    if(!receiver_phone){
        $('#error_receiver_phone').show();
        check = false;
    }else {
        if (receiver_phone.length < 10 || receiver_phone.length > 11) {
            $('#error_receiver_phone_length').show();
            check = false;
        }
    }
    if(!$("input[name=shipment_type]:checked").val()){
        $('#error_service').show();
        $('#error_service').css('position','inherit');
        check = false;
    }
    if(!weight){
        $('#error_weight').show();
        check = false;
    }
    return check;
}
function validateFormAddAddress(){
    $('.error').hide();
    var check = true;
    var add_name = $("#add_name").val();
    var add_country = $("#add_country").val();
    var add_city = $("#add_city").val();
    var add_district = $("#add_district").val();
    //var receiver_ward = $("#receiver_ward").val();
    var add_street = $("#add_street").val();
    var add_phone = $("#add_phone").val();

    if(!add_name){
        $('#error_add_address_receiver_name').show();
        check = false;
    }
    if(!add_country){
        $('#error_add_address_receiver_country').show();
        check = false;
    }
    if(!add_city){
        $('#error_add_address_receiver_city').show();
        check = false;
    }
    if(!add_district){
        $('#error_add_address_receiver_district').show();
        check = false;
    }
    // if(!receiver_ward){
    //     $('#error_receiver_ward').show();
    //     check = false;
    // }
    if(!add_street){
        $('#error_add_address_receiver_street').show();
        check = false;
    }
    if(!add_phone){
        $('#error_add_address_receiver_phone').show();
        check = false;
    }else {
        if (add_phone.length < 10 || add_phone.length > 11) {
            $('#error_add_address_receiver_phone_length').show();
            check = false;
        }
    }
    return check;
}
function deleteAddress(_this){
    var id = $(_this).data('id');
    if (site_lang === 'en' ? confirm("Are you sure for delete address?") : confirm("Bạn có chắc muốn xóa địa chỉ này?")) {
        $.ajax({
            url: "/apiController/deleteAddressShipper",
            dataType: "json",
            type: 'post',
            data: {
                'id' : id
            },
            //async: false,
            success: function (data) {
                if (site_lang === 'en'){
                    alert('Deleted success');
                }else{
                    alert('Xóa địa chỉ thành công');
                }
                getListAddress();
            }
        });
    }
    return false;
}
function liveSearch() {
    var input, filter, row, name, phone, i;
    input = document.getElementById('inputSearch');
    filter = input.value.toUpperCase();
    row = $('.resultRow');
    $(row).each(function() {
        name = $(this).find('.resultName');
        phone =  $(this).find('.resultPhone');
        if (name || phone) {
            if (name.text().toUpperCase().indexOf(filter) > -1 || phone.text().toUpperCase().indexOf(filter) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        }
    });
}