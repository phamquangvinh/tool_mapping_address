var ITL = ITL || {};
ITL.portal = ITL.portal || {};
jQuery.extend(ITL.portal, {
    getParam: function (type) {
        var param = {};
        switch (type){
            case 'login':
                param['username'] = $('#username').val();
                param['password'] = $('#password').val();
                break;
        }
        return param;
    },
});