$(document).ready(function () {
    spinnerLoad     = new Spinner('load_import');

    $("#btn_import_booking_bottom").click(function () {
        importMapping();
    });
});
    $("#uploadExcel").on('change',(function(e) {
        e.preventDefault();
        spinnerLoad.show("Progressing");
        $.ajax({
            url: "/MappingController/Excel2TempData",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function() {
                spinnerLoad.hide();
                var table_import_option = {
                    "pageLenght": 10,
                    "processing": true,
                    "serverSide": true,
                    "columns": [
                        {"data": 'country_text'},
                        {"data": 'country_code'},
                        {"data": 'city_text'},
                        {"data": 'city_code'},
                        {"data": 'district_text'},
                        {"data": 'district_code'},
                        {"data": 'ward_text'},
                        {"data": 'ward_code'},
                        {"data": 'partner_address_code'},
                    ],
                    "ajax": {
                        url: "/MappingController/importEx",
                        dataType: 'json',
                        type: 'post',
                    }
                };
                $('#table_data_import').DataTable().clear().draw();
                var table_import = $('#table_data_import');
                table_import.DataTable().destroy();
                table_import.DataTable(table_import_option);
                $('#btn_import_booking_bottom').hide();
                $('#btn_import_booking_bottom').show();
            }
        });
    }));
function importMapping() {
    var r = confirm('Do you want import records selected ?');
    if (r == true) {
        var arr_data_insert = $('#table_data_import').DataTable().data().toArray();
        console.log(arr_data_insert);
        spinnerLoad.show("Progressing");
        $.ajax({
            url: '/MappingController/saveData',
            type: 'POST',
            success: function (data) {
                spinnerLoad.hide();
                if(data != null) {
                    alert('Import Success.');
                    $('#table_data_import').DataTable().clear().draw();
                    $('#btn_import_booking_bottom').hide();
                    window.location.href = "/mapping-import";
                }else{
                    if(data === 0) {
                        alert('Import Failed.');
                    }else{
                        alert(data);
                    }
                }
            }
        });
    }
}

