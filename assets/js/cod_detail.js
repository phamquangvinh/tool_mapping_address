if (site_lang == 'vi') {
    var language = {
        "sEmptyTable":   "Không có dữ liệu phù hợp",
        "sProcessing":   "Đang xử lý...",
        "sLengthMenu":   "Xem _MENU_ mục",
        "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
        "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
        "sInfoFiltered": "(được lọc từ _MAX_ mục)",
        "sInfoPostFix":  "",
        "sSearch":       "Tìm:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Đầu",
            "sPrevious": "Trước",
            "sNext":     "Tiếp",
            "sLast":     "Cuối"
        }
    };
}else{
    var language = {
        "sEmptyTable":     "No data available in table",
        "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
        "sInfoFiltered":   "(filtered from _MAX_ total entries)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "Show _MENU_ entries",
        "sLoadingRecords": "Loading...",
        "sProcessing":     "Processing...",
        "sSearch":         "Search:",
        "sZeroRecords":    "No matching records found",
        "oPaginate": {
            "sFirst":    "First",
            "sLast":     "Last",
            "sNext":     "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending":  ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    };
}
$(document).ready(function() {
    var data = JSON.parse(atob(dataTable));
    var table = $('.cod-list-detail-table').DataTable({
        "columns": [
            {'data': 'stt','width' :'4%'},
            {'data': 'awb_tracking_no','width': '5%'},
            {'data': 'order_no','width': '10%'},
            {'data': 'booking_date', 'width': '15%'},
            {'data': 'cod_value', 'width': '10%'},
            {'data': 'address_delivery', 'width': '10%'},
            {'data': 'cod', 'width': '10%'},
            {'data': 'cash', 'width': '7%'},            
            {'data': 'paid','width' :'4%'}
        ],
        "scrollX": true,
        "language": language,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ]
    });
    $('.cod-list-detail-table').DataTable().clear().draw();
    $('.cod-list-detail-table').DataTable().rows.add(data).draw();

    $('#print_detail_cod').click(function () {
        var cod_id = $('#print_detail_cod').val();
        $.ajax({
            url: "/ServiceController/printCOD",
            type: 'post',
            data: {
                'cod_id' : cod_id
            },
            //async: false,
            success: function (data) {
                window.open(data,'_blank');
            }
        });
    });
});