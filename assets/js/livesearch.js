$(document).ready(function() {
    $('#inputSearch').keyup(function() {
        liveSearch();
    });
    function liveSearch() {
        var input, filter, row, name, phone, i;
        input = document.getElementById('inputSearch');
        filter = input.value.toUpperCase();
        row = $('.resultRow');
        $(row).each(function() {
            name = $(this).find('.resultName');
            phone =  $(this).find('.resultPhone');
            if (name || phone) {
                if (name.text().toUpperCase().indexOf(filter) > -1 || phone.text().toUpperCase().indexOf(filter) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            }
        });
    }
})