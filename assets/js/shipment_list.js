var spinnerFrom,spinnerTo,spinnerLogin;
if (site_lang == 'vi') {
    var language = {
        "sEmptyTable":   "Không có dữ liệu phù hợp",
        "sProcessing":   "Đang xử lý...",
        "sLengthMenu":   "Xem _MENU_ mục",
        "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
        "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
        "sInfoFiltered": "(được lọc từ _MAX_ mục)",
        "sInfoPostFix":  "",
        "sSearch":       "Tìm:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Đầu",
            "sPrevious": "Trước",
            "sNext":     "Tiếp",
            "sLast":     "Cuối"
        }
    };
}else{
    var language = {
        "sEmptyTable":     "No data available in table",
        "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
        "sInfoFiltered":   "(filtered from _MAX_ total entries)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "Show _MENU_ entries",
        "sLoadingRecords": "Loading...",
        "sProcessing":     "Processing...",
        "sSearch":         "Search:",
        "sZeroRecords":    "No matching records found",
        "oPaginate": {
            "sFirst":    "First",
            "sLast":     "Last",
            "sNext":     "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending":  ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    };
}
$(document).ready(function() {
    var data = JSON.parse(atob(dataTable));
    var table = $('#shipment_list_table').DataTable({
        "columns": [
            {'data'  :'check_box','width' :'4%'},
            {'data': 'no', 'width': '5%'},
            {'data': 'awb_tracking_no','width': '10%'},
            {'data': 'services', 'width': '15%'},
            {'data': 'date_entered', 'width': '10%'},
            {'data': 'delivery_date', 'width': '10%'},
            {'data': 'receiver_name', 'width': '10%'},
            {'data': 'cod_value', 'width': '7%'},
            {'data': 'total_revenue', 'width': '7%'},
            {'data': 'status', 'width': '10%'},
            {'data': 'shipment_note', 'width': '10%'}
        ],
        "scrollX": true,
        "language": language,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "order": [[1, 'asc']]
    });
    $('#shipment_list_table').DataTable().clear().draw();
    $('#shipment_list_table').DataTable().rows.add(data).draw();

    // Handle click on "Select all" control
    $('#check_all').on('click', function(){
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#shipment_list_table tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#check_all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    $('#shipment-detail-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var shipment_id = button.data('detail_shipment'); // Extract info from data-* attributes
        var modal = $(this);
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            url: "ServiceController/getDetailShipment",
            type: "POST",
            dataType:"json",
            data: {
                'shipment_id' : shipment_id
            },
            success: function (data, status, jqXHR) {
                modal.find('#print_detail_shipment').val(shipment_id);
                modal.find('#shipment_title').text(data.awb_tracking_no);
                modal.find('#order_no').text(data.awb_tracking_no);
                modal.find('#company_name').text(data.company_name);
                modal.find('#shipper_name').text(data.shipper_name);
                modal.find('#io_undelivery').text(data.address_shipper);
                modal.find('#phone_shipper').text(data.phone_shipper);
                modal.find('#receiver_name').text(data.receiver_name);
                modal.find('#receiver_company').text(data.receiver_company);
                modal.find('#delivery_address').text(data.address_delivery);
                modal.find('#phone_delivery').text(data.phone_delivery);
                modal.find('#shipment_type').text(data.services);
                modal.find('#cod_value').text(data.cod_value);
                modal.find('#weight').text(data.consignment_weight);
                modal.find('#pcs_value').text(data.pcs_value);
                modal.find('#date_entered').text(data.date_entered);
                modal.find('#delivery_date').text(data.delivery_date);
                modal.find('#payment_method').text(data.payment_method);
                modal.find('#charge_to').text(data.charge_to);
                modal.find('#total_revenue').text(data.total_revenue);
                modal.find('#specical_req').text(data.specical_instruction);
                modal.find('#description').text(data.description);
                modal.find('#pod').text(data.pod);
                if (data.history.international == 1) {
                    modal.find('#history tbody').html(generateHistoryDomestic(data.history.international_data.history));    
                }else{
                    modal.find('#history tbody').html(generateHistoryDomestic(data.history.domestic_data.history));    
                }                
                loadingScreenJS(false);
            },
            beforeSend: function(){
                loadingScreenJS(true);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                loadingScreenJS(false);
            }
        });
    })
    $('#export_excel_shipment').click(function(){
        var arr_id = [];
        table.$('input[type=checkbox]:checked').each(function(i,e){
            arr_id.push($(this).val());
        });
        if (arr_id.length>0){
            $.ajax({
                url: "/ServiceController/ExportShipmentToExcel",
                type: 'post',
                data: {
                    'arr_id' : arr_id
                },
                //async: false,
                success: function (response) {
                    loadingScreenJS(false);
                    window.location.href = response.url;
                    //window.open(response.url,'_blank');
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                }
            });
        }else{
            alert(exportNull);
        }
    });
    $('#print_shipment').click(function(){
        var arr_id = [];
        table.$('input[type=checkbox]:checked').each(function(i,e){
            arr_id.push($(this).val());
        });
        if (arr_id.length>0){
            $.ajax({
                url: "/ServiceController/PrintShipment",
                type: 'post',
                data: {
                    'arr_id' : arr_id
                },
                //async: false,
                success: function (data) {
                    loadingScreenJS(false);
                    window.open(data,'_blank');
                },
                beforeSend: function(){                    
                    $('.booking-detail-modal').modal('hide');
                    loadingScreenJS(true);
                }
            });
        }else{
            alert(printNull);
        }
    });
    $('#print_detail_shipment').click(function () {
        var arr_id = [$('#print_detail_shipment').val()];
        $.ajax({
            url: "/ServiceController/PrintShipment",
            type: 'post',
            data: {
                'arr_id' : arr_id
            },
            //async: false,
            success: function (data) {
                loadingScreenJS(false);               
                window.open(data,'_blank');
            },
            beforeSend: function(){
                loadingScreenJS(true);
            }
        });
    });
    $('input[name="receiver_phone"]').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, F5, space
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 32) {
            e.preventDefault();
        }
    });
    $('input[name="receiver_phone"]').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });
});
function resetSearchShipment() {
    $('input[name="receiver_phone"]').val('');
    $('input[name="awb_tracking_no"]').val('');
    $('input[name="receiver_name"]').val('');
    $('input[name="delivery_date_from"]').val('');
    $('input[name="delivery_date_to"]').val('');
    $('select[name="status"]').prop('selectedIndex', 0);
    $('select[name="service"]').prop('selectedIndex', 0);
}
function generateHistoryInternational(history){
    var html_history = '';
    for (var i = 0; i < history.length; i++) {
        html_history += ''
            + '  <tr>'
            + '      <td>' + history[i]['datetime'] + '</td>'
            + '      <td>' + history[i]['location'] + '</td>'
            + '      <td>' + history[i]['activity'] + '</td>'
            + '  </tr>';
    }
    return html_history;
}
function generateHistoryDomestic(history){
    var html_history = '';
    var n = 0;
    for (var i = 0; i < history.length; i++) {
        n++;
        var class_check = '';
        if(history[i]['status'] == 'Handovered'){
            history[i]['status'] = 'In Transit';
        }
        html_history += ''
            + '  <tr>'
            + '      <td>'
            + n
            + '      </td>'
            + '      <td>'
            + history[i]['date_time']
            + '      </td>'
            + '      <td>'
            + history[i]['status']
            + '      </td>'
            + '      <td>'
            + history[i]['status_vn']
            + '      </td>'
            + '      <td>'
            + history[i]['reason_vn']
            + '      </td>'
            + '  </tr>';
    }
    return html_history;
}
