var spinnerFrom,spinnerTo,spinnerLogin;
var from_date = $("#from_date").datepicker({
    ignoreReadonly: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true
});

var to_date = $("#to_date").datepicker({
    ignoreReadonly: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true
});
if (site_lang == 'vi') {
    var language = {
        "sEmptyTable":   "Không có dữ liệu phù hợp",
        "sProcessing":   "Đang xử lý...",
        "sLengthMenu":   "Xem _MENU_ mục",
        "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
        "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
        "sInfoFiltered": "(được lọc từ _MAX_ mục)",
        "sInfoPostFix":  "",
        "sSearch":       "Tìm:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Đầu",
            "sPrevious": "Trước",
            "sNext":     "Tiếp",
            "sLast":     "Cuối"
        }
    };
}else{
    var language = {
        "sEmptyTable":     "No data available in table",
        "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
        "sInfoFiltered":   "(filtered from _MAX_ total entries)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "Show _MENU_ entries",
        "sLoadingRecords": "Loading...",
        "sProcessing":     "Processing...",
        "sSearch":         "Search:",
        "sZeroRecords":    "No matching records found",
        "oPaginate": {
            "sFirst":    "First",
            "sLast":     "Last",
            "sNext":     "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending":  ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    };
}
$(document).ready(function() {
    var data = JSON.parse(atob(dataTable));
    var table = $('#soa_list_table').DataTable({
        "columns": [
            {
                'data'  :'check_box',
                'width' :'4%',
                'render': function (data) {
                    return '<input type="checkbox" name="check_box[]" value='+data+'>';
                }
            },
            {'data': 'no', 'width': '5%'},
            {'data': 'soa_code','width': '10%'},
            {'data': 'from_date', 'width': '15%'},
            {'data': 'to_date', 'width': '10%'},
            {'data': 'shipment_no', 'width': '10%'},
            {'data': 'total_amount', 'width': '10%'},
            {'data': 'vat_invoice', 'width': '7%'},
            {'data': 'status', 'width': '7%'},
            {'data': 'paid_date', 'width': '7%'},
            {'data'  :'print','width' :'4%'}
        ],
        "scrollX": true,
        "language": language,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "order": [[1, 'asc']]
    });
    $('#soa_list_table').DataTable().clear().draw();
    $('#soa_list_table').DataTable().rows.add(data).draw();

    // Handle click on "Select all" control
    $('#check_all').on('click', function(){
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#soa_list_table tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#check_all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    $('#export_excel_soa').click(function(){
        var arr_id = [];
        table.$('input[type=checkbox]:checked').each(function(i,e){
            arr_id.push($(this).val());
        });
        console.log(arr_id);
        if (arr_id.length>0){
            $.ajax({
                url: "/ServiceController/ExportSOAToExcel",
                type: 'post',
                data: {
                    'arr_id' : arr_id
                },
                //async: false,
                success: function (response) {
                    window.location.href = response.url;
                    //window.open(response.url,'_blank');
                }
            });
        }else{
            alert(exportNull);
        }
    });

    if (!$('#from_date').val() && !$('#to_date').val()) {
        changeDateRange();
    }
});
function changeDateRange(){
    from_date.datepicker('setDate', $('#date_range :selected').attr('from'));
    to_date.datepicker('setDate', $('#date_range :selected').attr('to'));
}
function exportSOA(soa_id,div){
    $(div).closest('#export_soa').button('loading');
    $.ajax({
        url: '/accounting/statement_account/exportStatementCustomerSOA',
        type: 'post',
        dataType: "json",
        data: {
            "soa_id": soa_id
        },
        success: function (data) {
            $(div).closest('#export_soa').button('reset');
            if(data.status){
                window.open('/'+data.folder+'/'+data.file_name_export);
            }else{
                alert(data.message);
            }
        },
        error: function (xhr, textStatus, thrownError) {
            spinner.hide();
            alert('status:' + xhr.status + '\n' + thrownError);
        }
    });
}
function printDetailSOA(th) {
    var soa_id = $(th).val();
    $.ajax({
        url: "/ServiceController/printSOA",
        type: 'post',
        data: {
            'soa_id' : soa_id
        },
        //async: false,
        success: function (data) {
            loadingScreenJS(false);
            window.open(data,'_blank');
        },
        beforeSend: function(){
            loadingScreenJS(true);
        }
    });
}
function resetFormSOA() {
    $('#date_range').val('Last_30_Days');
    changeDateRange();
    $('#soa_code').val('');
    $('#status').val('');
}