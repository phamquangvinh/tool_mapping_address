var spinnerFrom,spinnerTo,spinnerLogin;
var from_date = $("#from_date").datepicker({
    ignoreReadonly: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true
});

var to_date = $("#to_date").datepicker({
    ignoreReadonly: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true
});
if (site_lang == 'vi') {
    var language = {
        "sEmptyTable":   "Không có dữ liệu phù hợp",
        "sProcessing":   "Đang xử lý...",
        "sLengthMenu":   "Xem _MENU_ mục",
        "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
        "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
        "sInfoFiltered": "(được lọc từ _MAX_ mục)",
        "sInfoPostFix":  "",
        "sSearch":       "Tìm:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Đầu",
            "sPrevious": "Trước",
            "sNext":     "Tiếp",
            "sLast":     "Cuối"
        }
    };
}else{
    var language = {
        "sEmptyTable":     "No data available in table",
        "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
        "sInfoFiltered":   "(filtered from _MAX_ total entries)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "Show _MENU_ entries",
        "sLoadingRecords": "Loading...",
        "sProcessing":     "Processing...",
        "sSearch":         "Search:",
        "sZeroRecords":    "No matching records found",
        "oPaginate": {
            "sFirst":    "First",
            "sLast":     "Last",
            "sNext":     "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending":  ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    };
}
$(document).ready(function() {
    var data = JSON.parse(atob(dataTable));
    var table = $('#cod_list_table').DataTable({
        "columns": [
            {'data': 'check_box','width' :'4%'},
            {'data': 'awb_tracking_no','width': '5%'},
            {'data': 'order_no','width': '10%'},
            {'data': 'delivery_date', 'width': '15%'},
            {'data': 'cod_value', 'width': '10%'},
            {'data': 'total_fee', 'width': '10%'},
            {'data': 'status', 'width': '10%'},
            {'data': 'paid_date', 'width': '7%'},            
            {'data': 'bangke_no','width' :'4%'},
            {'data': 'xem', 'width': '7%'}
        ],
        "scrollX": true,
        "language": language,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "order": [[1, 'asc']]
    });
    $('#cod_list_table').DataTable().clear().draw();
    $('#cod_list_table').DataTable().rows.add(data).draw();

    // Handle click on "Select all" control
    $('#check_all').on('click', function(){
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#cod_list_table tbody').on('change', 'input[type="checkbox"]', function(){
        // If checkbox is not checked
        if(!this.checked){
            var el = $('#check_all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if(el && el.checked && ('indeterminate' in el)){
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    $('#shipment-detail-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var shipment_id = button.data('detail_shipment'); // Extract info from data-* attributes
        var modal = $(this);
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            url: "ServiceController/getDetailShipment",
            type: "POST",
            dataType:"json",
            data: {
                'shipment_id' : shipment_id
            },
            success: function (data, status, jqXHR) {
                modal.find('#print_detail_shipment').val(shipment_id);
                modal.find('#shipment_title').text(data.awb_tracking_no);
                modal.find('#order_no').text(data.awb_tracking_no);
                modal.find('#company_name').text(data.company_name);
                modal.find('#shipper_name').text(data.shipper_name);
                modal.find('#io_undelivery').text(data.address_shipper);
                modal.find('#phone_shipper').text(data.phone_shipper);
                modal.find('#receiver_name').text(data.receiver_name);
                modal.find('#receiver_company').text(data.receiver_company);
                modal.find('#delivery_address').text(data.address_delivery);
                modal.find('#phone_delivery').text(data.phone_delivery);
                modal.find('#shipment_type').text(data.services);
                modal.find('#cod_value').text(data.cod_value);
                modal.find('#weight').text(data.consignment_weight);
                modal.find('#pcs_value').text(data.pcs_value);
                modal.find('#date_entered').text(data.date_entered);
                modal.find('#delivery_date').text(data.delivery_date);
                modal.find('#payment_method').text(data.payment_method);
                modal.find('#charge_to').text(data.charge_to);
                modal.find('#total_revenue').text(data.total_revenue);
                modal.find('#specical_req').text(data.specical_req);
                modal.find('#description').text(data.description);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    })

    $('#export_excel_cod').click(function(){
        var arr_id = [];
        var check = true;
        table.$('input[type=checkbox]:checked').each(function(i,e){
            if ($(this).val()){
                arr_id.push($(this).val());
            }else{
                check = false;
            }
        });
        if (arr_id.length>0 && check){
            $.ajax({
                url: "/ServiceController/ExportCODToExcel",
                type: 'post',
                data: {
                    'arr_id' : arr_id
                },
                //async: false,
                success: function (response) {                    
                    window.location.href = response.url;
                    //window.open(response.url,'_blank');
                }
            });
        }else{
            if (site_lang == 'en'){
                alert('No COD selected or shipment do not have COD');
            }else{
                alert('Chưa có bảng kê nào được chọn hoặc vận đơn chưa có bảng kê');
            }
        }
    });

    $('#print_detail_shipment').click(function () {
        var arr_id = [$('#print_detail_shipment').val()];
        $.ajax({
            url: "/ServiceController/PrintShipment",
            type: 'post',
            data: {
                'arr_id' : arr_id
            },
            //async: false,
            success: function (data) {                
                window.open(data,'_blank');
            }
        });
    });

    if (!$('#from_date').val() && !$('#to_date').val()) {
        changeDateRange();
    }
});
function changeDateRange(){
    from_date.datepicker('setDate', $('#date_range :selected').attr('from'));
    to_date.datepicker('setDate', $('#date_range :selected').attr('to'));
}
function resetSearchCOD() {
    $('input[name="awb_tracking_no"]').val('');
    $('input[name="cod_code"]').val('');
    $('select[name="status_cod"]').prop('selectedIndex', 0);
    changeDateRange();
}