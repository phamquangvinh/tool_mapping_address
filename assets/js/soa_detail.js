if (site_lang == 'vi') {
    var language = {
        "sEmptyTable":   "Không có dữ liệu phù hợp",
        "sProcessing":   "Đang xử lý...",
        "sLengthMenu":   "Xem _MENU_ mục",
        "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
        "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
        "sInfoFiltered": "(được lọc từ _MAX_ mục)",
        "sInfoPostFix":  "",
        "sSearch":       "Tìm:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Đầu",
            "sPrevious": "Trước",
            "sNext":     "Tiếp",
            "sLast":     "Cuối"
        }
    };
}else{
    var language = {
        "sEmptyTable":     "No data available in table",
        "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
        "sInfoFiltered":   "(filtered from _MAX_ total entries)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "Show _MENU_ entries",
        "sLoadingRecords": "Loading...",
        "sProcessing":     "Processing...",
        "sSearch":         "Search:",
        "sZeroRecords":    "No matching records found",
        "oPaginate": {
            "sFirst":    "First",
            "sLast":     "Last",
            "sNext":     "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending":  ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    };
}
$(document).ready(function() {
	var data = JSON.parse(atob(dataTable));
    var table = $('#table_list_shipment_soa').DataTable({
        "columns": [
            {'data': 'no', 'width': '5%'},
            {'data': 'date_entered','width': '10%',},
            {'data': 'awb_tracking_no', 'width': '15%'},
            {'data': 'send_address', 'width': '10%'},
            {'data': 'receive_place', 'width': '10%'},
            {'data': 'receive_address', 'width': '10%'},
            {'data': 'service', 'width': '7%'},
            {'data': 'weight', 'width': '7%'},
            {'data': 'transport_service', 'width': '10%'},
            {'data': 'bonus_fee', 'width': '10%'},
            {'data': 'other_fee', 'width': '10%'},
            {'data': 'total', 'width': '10%'},
            {'data': 'tax', 'width': '10%'},
            {'data': 'tax_fee', 'width': '10%'},
            {'data': 'cash', 'width': '10%'},
            {'data': 'cod', 'width': '10%'}
        ],
        "scrollX": true,
        "language": language,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "order": [[0, 'asc']]
    });
    $('#table_list_shipment_soa').DataTable().clear().draw();
    $('#table_list_shipment_soa').DataTable().rows.add(data).draw();

    $('#print_detail_soa').click(function () {
        var soa_id = $('#print_detail_soa').val();
        $.ajax({
            url: "/ServiceController/printSOA",
            type: 'post',
            data: {
                'soa_id' : soa_id
            },
            //async: false,
            success: function (data) {
                window.open(data,'_blank');
            }
        });
    });
});