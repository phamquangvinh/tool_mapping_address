$(document).ready(function(){
    spinnerLoad = new Spinner('load_export');
    $('.error').hide();

    // EXPORT EXCEL
    $('#export_partner').on('click', function(){
        var selected = $('#export_partner_select :selected').text();
        var r = confirm('Export Data For Partner '+ selected + '?');
        var rowCount = 2;
        if(selected != '') {
            if (r == true) {
                $('#export_partner').attr('href', 'MappingController/createXLSXForPartner/'+selected);
                $('#export_partner').attr('target', '_blank');
            }
        } else {
            alert('Please select partner!');
        }
    });

    $("#get_mapping_list").on("click", function(e){
        e.preventDefault();
        $('#mapping_view').hide();
        $('#mapping_edit').hide();
        $('#mapping_edit_create').hide();
        // DATATABLE SETTINGS
        var table_mapping_option = {
            "bSortCellsTop": true,
            "columns": [
                {"data": 'partner'},
                {"data": 'speedlink_address_code'},
                // {"data": 'zip_code'},
                {"data": 'country_text'},
                {"data": 'city_text'},
                {"data": 'district_text'},
                {"data": 'ward_text'},
                {"data": 'branch_code'},
                {"data": 'hubcode'},
                {"data": 'status'},
                {"data": 'note'},
                {"data": 0}
            ],
            "order": [
                [0, 'asc'], [1, 'asc'], [2, 'asc'], [3, 'asc'], [4, 'asc'], [5, 'asc'], [6, 'asc'], [7, 'asc']
            ],
            "columnDefs": [{
                "targets": -1,
                "data": 0,
                "defaultContent": "<a href='javascript:void(0);' class='fa fa-edit'></a>",
                "class": "text-center",
            }],
            "processing": true,
            "ajax": {
                url: "/MappingController/getMappingList",
                dataType: 'json',
            },
            "initComplete": function () {
                $('#table_mapping_list tbody').on("click", 'a', function (e) {
                    e.preventDefault();
                    var data_row = table_mapping.DataTable().row($(this).parents('tr')).data();
                    detailMapping(data_row);
                });
                $('#table_mapping_list thead tr#forFilters th').html('');
                var api = this.api();
                $('#table_mapping_list thead tr#forFilters th').each(function(i) {
                    if(i == 0 || i == 2 || i == 3 || i == 4 || i == 5 || i == 8 || i == 9) {
                        // GET SPECIFIC COLUMN
                        var column = api.column(i);
                        // CREATE SELECT LIST AND SEARCH OPERATION
                        var select = $('<select class="js-list-select2" style="width: 80px;"><option value=""></option></select>')
                            .appendTo($(this).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });
                        $('.js-list-select2').select2();
                        column.data().unique().sort().each(function (value, index) {
                            if(value!=='' && value != null) {
                                select.append('<option value="' + value + '">' + value + '</option>')
                            }
                        });
                    }
                });
            }
        };
        var table_mapping = $('#table_mapping_list');
        table_mapping.DataTable().destroy();
        table_mapping.DataTable(table_mapping_option);
    });
    // VALIDATION UPDATE FORM
    $("#update_form").submit(function(e){
        if($('#select2-country_select-container')[0].title == "") {
            $('#error_select_country').show();
            $('#error_select_city').show();
            $('#error_select_district').show();
            $('#error_select_ward').show();
            alert('Missing Field!');
        } else if($('#select2-city_select-container')[0].title == "") {
            $('#error_select_country').hide();
            $('#error_select_city').show();
            $('#error_select_district').show();
            $('#error_select_ward').show();
            alert('Missing Field!');
        } else if($('#select2-district_select-container')[0].title == "") {
            $('#error_select_country').hide();
            $('#error_select_city').hide();
            $('#error_select_district').show();
            $('#error_select_ward').show();
            alert('Missing Field!');
        } else if($('#select2-ward_select-container')[0].title == "") {
            $('#error_select_country').hide();
            $('#error_select_city').hide();
            $('#error_select_district').hide();
            $('#error_select_ward').show();
            alert('Missing Field!');
        } else if($('#suggest_code_exist > span').text() != "") {
            alert('Đã tồn tại!');
        }
        else {
            var r = confirm('Save data?');
            if (r == true) {
                $.ajax({
                    url: "/MappingController/updatedMapping",
                    type: "POST",
                    data: {
                        'ward': $('#select2-ward_select-container')[0].title,
                        'district': $('#select2-district_select-container')[0].title,
                        'city': $('#select2-city_select-container')[0].title,
                        'country': $('#select2-country_select-container')[0].title,
                        'address_code': $('#suggest_code').attr('placeholder'),
                        'partner': $("#partner").text(),

                        'ward_select': $("#ward_text_ro").attr('placeholder'),
                        'district_select': $("#district_text_ro").attr('placeholder'),
                        'city_select': $("#city_text_ro").attr('placeholder'),
                        'country_select': $("#country_text_ro").attr('placeholder'),
                    },
                    dataType: 'json',
                    success: function (msg) {
                        alert(msg);
                        // CLEAR INPUT FIELD
                        $('#select2-city_select-container').html('');
                        $('#select2-district_select-container').html('');
                        $('#select2-ward_select-container').html('');
                        $('#suggest_code').removeAttr('placeholder');
                        $('#select2-branch_select-container').html('');
                        $('#hub_select').removeAttr('placeholder');
                        $('#suggest_code_exist').hide();
                        $('#suggest_code_hint').hide();
                        $('#error_missing_zipcode').hide();
                        $('#zipcode').text("");
                        $('#zipcode').removeAttr('placeholder');
                        // CLEAR PARTNER FIELD
                        $("#country_text_ro").removeAttr("placeholder");
                        $("#country_code_ro").removeAttr("placeholder");
                        $("#city_text_ro").removeAttr("placeholder");
                        $("#city_code_ro").removeAttr("placeholder");
                        $("#district_text_ro").removeAttr("placeholder");
                        $("#district_code_ro").removeAttr("placeholder");
                        $("#ward_text_ro").removeAttr("placeholder");
                        $("#ward_code_ro").removeAttr("placeholder");
                        $("#address_code_ro").removeAttr("placeholder");
                        $("#hub_select").removeAttr("placeholder");
                        $("#zip_code_ro").removeAttr("placeholder");

                    },
                    beforeSend: function () {
                        if (!$('#zipcode').val()) {
                            $('#error_missing_zipcode').show();
                        }
                    }
                });
            }
        }
        e.preventDefault();
    });
});

function detailMapping(clicked_row) {
    var data_row = Object.values(clicked_row);
    $.ajax({
        url: "/MappingController/getFieldData",
        type: 'post',
        data: {
            parameter :data_row
        },
        dataType :'json',
        success: function (data) {
            $('#partner').html('');
            // CLEAR PLACEHOLDER
            $("#country_text_ro").removeAttr("placeholder");
            $("#country_code_ro").removeAttr("placeholder");
            $("#city_text_ro").removeAttr("placeholder");
            $("#city_code_ro").removeAttr("placeholder");
            $("#district_text_ro").removeAttr("placeholder");
            $("#district_code_ro").removeAttr("placeholder");
            $("#ward_text_ro").removeAttr("placeholder");
            $("#ward_code_ro").removeAttr("placeholder");
            $("#address_code_ro").removeAttr("placeholder");
            $("#hub_select").removeAttr("placeholder");
            $("#zip_code_ro").removeAttr("placeholder");
            $("#zipcode").removeAttr("placeholder");

            $('#partner').text(data_row[0]);
            // PLACEHOLDER
            $("#country_text_ro").attr("placeholder", data_row[2]);
            $("#country_code_ro").attr("placeholder", data[1]['country_code']);
            $("#city_text_ro").attr("placeholder", data_row[3]);
            $("#city_code_ro").attr("placeholder", data[2]['province_code']);
            $("#district_text_ro").attr("placeholder", data_row[4]);
            $("#district_code_ro").attr("placeholder", data[3]['district_code']);
            $("#ward_text_ro").attr("placeholder", data_row[5]);
            $("#ward_code_ro").attr("placeholder", data[0]['ward_code']);
            $("#address_code_ro").attr("placeholder", data[0]['code']);
            $("#zip_code_ro").attr("placeholder", data[0]['zip_code']);
            $('#mapping_view').show();

            // CLEAR SELECTED
            $('#suggest_code').removeAttr("placeholder");
            $('#mapping_edit').show();

            $('#mapping_edit_create').show();
        }
    });
}

function changeCountry() {
    $('#address_loading').show();
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify($('#country_select :selected').val()))))
        },
        success: function(data) {
            $('#address_loading').hide();
            if(data.status) {
                $('#city_select').html(data.data);
                $('#city_select').trigger('chosen:updated');
                $('#city_select').prop("disabled", false);
            }
            else {
                alert(data.message);
            }
        },
        beforeSend: function() {
            $('#city_select').html("");
            $('#city_select').prop("disabled", true);
            $('#district_select').html("");
            $('#district_select').prop("disabled", true);
            $('#ward_select').html("");
            $('#ward_select').prop("disabled", true);
            $('#branch_select').html("");
            $('#branch_select').prop("disabled", true);
            $("#hub_select").removeAttr("placeholder");
            $('#suggest_code').removeAttr("placeholder");
            $('#zipcode').removeAttr("placeholder");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#address_loading').hide();
            alert(xhr.status+"<br>"+thrownError);
        }
    })
}
function changeCity() {
    $('#address_loading').show();
    if($('#city_select :selected').val() != '') {
        $.ajax({
            url: "/apiController/getDistrictForCreateBooking",
            dataType: "json",
            type: 'post',
            data: {
                'city': btoa(unescape(encodeURIComponent(JSON.stringify($('#city_select :selected').val())))),
                'country': btoa(unescape(encodeURIComponent(JSON.stringify($('#country_select :selected').val()))))
            },
            success: function(data) {
                $('#address_loading').hide();
                if(data.status) {
                    $('#district_select').html(data.data);
                    $('#district_select').trigger('chosen:updated');
                    $('#district_select').prop("disabled", false);
                }
                else {
                    alert(data.message);
                }
            },
            beforeSend: function(){
                $('#district_select').html("");
                $('#district_select').trigger('chosen:updated');
                $('#district_select').prop("disabled", true);

                $('#ward_select').html("");
                $('#ward_select').trigger('chosen:updated');
                $('#ward_select').prop("disabled", true);
                $("#hub_select").removeAttr("placeholder");
                $('#suggest_code').removeAttr("placeholder");
                $('#zipcode').removeAttr("placeholder");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#address_loading').hide();
                alert(xhr.status+"<br>"+thrownError);
            }
        });
    } else {
        $('#district_select').html("");
        $('#district_select').trigger('chosen:updated');
        $('#district_select').prop("disabled", true);

        $('#ward_select').html("");
        $('#ward_select').trigger('chosen:updated');
        $('#ward_select').prop("disabled", true);

        $('#address_loading').hide();
    }
}
function changeDistrict() {
    $('#address_loading').show();
    if($('#district_select :selected').val()!='') {
        $.ajax({
            url: "/apiController/getWardForCreateBooking",
            dataType: "json",
            type: 'post',
            data: {
                'district': btoa(unescape(encodeURIComponent(JSON.stringify($('#district_select :selected').val())))),
                'city': btoa(unescape(encodeURIComponent(JSON.stringify($('#city_select :selected').val())))),
                'country': btoa(unescape(encodeURIComponent(JSON.stringify($('#country_select :selected').val())))),
            },
            success: function(data) {
                $('#address_loading').hide();
                if(data.status) {
                    $('#ward_select').html(data.data);
                    $('#ward_select').trigger("chosen: updated");
                    $('#ward_select').prop("disabled", false);
                }
                else {
                    alert(data.message);
                }
            },
            beforeSend: function() {
                $('#ward_select').html("");
                $('#ward_select').trigger('chosen:updated');
                $('#ward_select').prop("disabled", true);
                $("#hub_select").removeAttr("placeholder");
                $('#suggest_code').removeAttr("placeholder");
                $('#zipcode').removeAttr("placeholder");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#address_loading').hide();
                alert(xhr.status+"<br>"+thrownError);
            }
        });
    }
    else {
        $('#ward_select').html("");
        $('#ward_select').trigger('chosen:updated');
        $('#ward_select').prop("disabled", true);

        $('#address_loading').hide();
    }
}
function changeWard() {
    $('#address_loading').show();
    if($('#district_select :selected').val()!='') {
        $.ajax({
            url: "/MappingController/getCode",
            type: 'post',
            data: {
                'ward': $('#select2-ward_select-container')[0].title,
                'district': $('#select2-district_select-container')[0].title,
                'city': $('#select2-city_select-container')[0].title,
                'country': $('#select2-country_select-container')[0].title,
                'partner': $("#partner").text(),
            },
            dataType: 'json',
            success: function (data) {
                $('#address_loading').hide();
                $('#suggest_code_hint').html('');
                $("#suggest_code").attr("placeholder", data['address_code']);
                $('#zipcode').attr("placeholder", data['zip_code']);
                if (data['hint'] == 0) {
                    $('#suggest_code_exist').html('<span style="color: red">* Existing Code!</span>');
                    $('#suggest_code_exist').show();
                    $('#suggest_code_hint').html('');
                    $('#suggest_code_hint').hide();
                }
                 else {
                    $('#suggest_code_hint').html('<span style="color:green">* Code Suggesting!</span>');
                    $('#suggest_code_hint').show();
                    $('#suggest_code_exist').html('');
                    $('#suggest_code_exist').hide();
                }
                $.ajax({
                    url: "/MappingController/getListBranch",
                    dataType: 'json',
                    success: function (data) {
                        if(data.status) {
                            $('#branch_select').html(data.data);
                            $('#branch_select').trigger("chosen: updated");
                            $('#branch_select').prop("disabled", false);
                        }
                        else {
                            alert(data.message);
                        }
                    }
                });
            }
        });
    } else {
        $('#address_loading').hide();
    }
}
function changeBranch() {
    $('#address_loading').show();
    if($('#branch_select :selected').val()!='') {
        $.ajax({
           url: "/MappingController/getHubByBranch",
           dataType:'json',
           type:'post',
           data: {
               'branch': $('#select2-branch_select-container')[0].title,
           },
           success: function(data) {
               $('#address_loading').hide();
               $('#hub_select').attr("placeholder", data['hubcode']);
           }
        });
    }
}
