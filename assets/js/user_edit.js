if (site_lang == 'vi'){
    var text_upload_avatar = 'Bấm vào để chọn ảnh.';
}else{
    var text_upload_avatar = 'Click to chose image.';
}
$(document).ready(function() {
    validateInput();
    uploadAvatar();
});
    function changeCountry(){
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#city').html(data.data);
                $('#city').trigger('chosen:updated');
                $('#city').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#city").html("");
            $("#district").html("");
            $("#ward").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeCity(){
    $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#district').html(data.data);
                $('#district').trigger('chosen:updated');
                $('#district').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function validateInput() {
    //validator phone
    $('#phone').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, F5, space
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 32) {
            e.preventDefault();
        }
    });
    $('#phone').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });
}
function cancelEdit() {
    window.location.href='/user-profile';
}
function uploadAvatar() {
    var btnCust = '';
    $(".avatar-edit").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        showBrowse: false,
        browseOnZoneClick: true,
        removeLabel: '',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="'+avatar_user+'" alt="Your Avatar"><h6 class="text-muted">'+text_upload_avatar+'</h6>',
        layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
}