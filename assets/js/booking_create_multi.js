(function ($) {
    $.encodeURL = function (object) {

        // recursive function to construct the result string
        function createString(element, nest) {
            if (element === null) return '';
            if ($.isArray(element)) {
                var count = 0,
                    url = '';
                for (var t = 0; t < element.length; t++) {
                    if (count > 0) url += '&';
                    url += nest + '[]=' + element[t];
                    count++;
                }
                return url;
            } else if (typeof element === 'object') {
                var count = 0,
                    url = '';
                for (var name in element) {
                    if (element.hasOwnProperty(name)) {
                        if (count > 0) url += '&';
                        url += createString(element[name], nest + '.' + name);
                        count++;
                    }
                }
                return url;
            } else {
                return nest + '=' + element;
            }
        }

        var url = '?',
            count = 0;

        // execute a createString on every property of object
        for (var name in object) {
            if (object.hasOwnProperty(name)) {
                if (count > 0) url += '&';
                url += createString(object[name], name);
                count++;
            }
        }

        return url;
    };
})(jQuery);
$(document).ready(function() {
    $('.error').hide();
    getListAddress();
    spinnerCountry  = new Spinner('country_row');
    spinnerCity     = new Spinner('city_row');
    spinnerDistrict = new Spinner('district_row');

    $("#default").on('change', function() {
        if ($(this).is(':checked')) {
            $(this).attr('value', '1');
        } else {
            $(this).attr('value', '0');
        }
    });

    $('#list_shipment_type').DataTable({
        "paging": false,
        "searching": false,
        "info":     false,
        "columns": [
            {
                'data': 'check_box', 
                'width': '10%', 
                'render': function ( data, type, row ) {
                    return '<input type="radio" name="shipment_type" value="'+data+'">';
                },
                'className': "text-center"
            },
            {'data': 'service', 'width': '30%','className': "text-center"},
            {'data': 'price', 'width': '30%','className': "text-center"},
            {'data': 'description', 'width': '30%','className': "text-center"}
        ]
    });

    autoFillAddress($(".order-item-block").eq(0));
    $(".booking-many-section").on("click", ".clone-booking-btn", function(e) {
        e.stopPropagation(); // this stops the click on the holder
        var orginal = $(this).closest(".order-item-block");
        var cloned = orginal.clone();
 
        //get original selects into a jq object
        var originalSelects = orginal.find('select');        
        cloned.find('select').each(function(index, item) {
             //set new select to value of old select
             $(item).val( originalSelects.eq(index).val() );
         
        });
        //get original textareas into a jq object
        var originalTextareas = orginal.find('textarea');
         
        cloned.find('textarea').each(function(index, item) {
            //set new textareas to value of old textareas
            $(item).val(originalTextareas.eq(index).val());
        });
        var ele = cloned.insertAfter($(this).closest(".order-item-block"));
        // console.log(ele);
        autoFillAddress(ele);

        var clone_count = 1;
        $(".order-item-block").each(function(index,item){
            $(this).find('.number_order').text('#'+clone_count++);
        });
        validateInput();
    });

    $('.add-newBooking-btn').click(function() {
        $('.newBookingForm').clone().last().insertAfter('.order-item-block-hidden:last');
        $('.newBookingForm').last().find('input').val('');
        $('.order-item-block-hidden').last().find('.chosenSelect3').select2({
            language : site_lang,
            width: '100%',
            dropdownParent: $(this).parent()
        });
        autoFillAddress($(".newBookingForm").last());
        $('.order-item-block-hidden').last().removeClass('newBookingForm');
        $('.order-item-block-hidden').last().addClass('order-item-block');
        // $('.order-item-block-hidden').last().find('select[name="receiver_city[]"],select[name="receiver_district[]"],select[name="receiver_ward[]"]').select2("val", "");
        $('.order-item-block-hidden').last().find('input[name="date_entered[]"]').val(current_datetime);
        var clone_count = 1;
        $(".order-item-block-hidden").each(function(index,item){
            $(this).find('.number_order').text('#'+clone_count++);
        });
        validateInput();
    });

    $('#inputSearch').keyup(function() {
        liveSearch();
    });

    $("#default").on('change', function() {
        if ($(this).is(':checked')) {
            $(this).attr('value', '1');
        } else {
            $(this).attr('value', '0');
        }
    });
    $('.chosenSelect2').select2({
        language : site_lang,
        width: '100%',
        dropdownParent: $(this).parent()
    });
    validateInput();
    removeOrderItem();
});

function changeCountry(th){
    spinnerCountry.show("Processing");
    $('option[selected=selected]',th).removeAttr("selected");

    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: { 
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( th.val() ))))
        },
        //async: false,
        success: function (data) {
            spinnerCountry.hide();
            if(data.status) {
                $('option:selected',th).attr("selected","selected"); 
                th.parents('.order-item-block').find('select[name="receiver_city[]"]').html(data.data);
                th.parents('.order-item-block').find('select[name="receiver_city[]"]').trigger('chosen:updated');
                th.parents('.order-item-block').find('select[name="receiver_city[]"]').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){
            th.parents('.order-item-block').find('select[name="receiver_city[]"]').html("");
            th.parents('.order-item-block').find('select[name="receiver_city[]"]').prop('disabled', true);
            th.parents('.order-item-block').find('select[name="receiver_district[]"]').html("");
            th.parents('.order-item-block').find('select[name="receiver_district[]"]').prop('disabled', true);
            th.parents('.order-item-block').find('select[name="receiver_ward[]"]').html("");
            th.parents('.order-item-block').find('select[name="receiver_ward[]"]').prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            spinnerCountry.hide();
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeCity(th){
    spinnerCity.show("Processing");
    $('option[selected=selected]',th).removeAttr("selected");
    var country = th.parents('.order-item-block').find('select[name="receiver_country[]"]');
    var district = th.parents('.order-item-block').find('select[name="receiver_district[]"]');
    $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( th.val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( country.val() ))))
        },
        //async: false,
        success: function (data) {
            spinnerCity.hide();
            if(data.status) {
                $('option:selected',th).attr("selected","selected"); 
                district.html(data.data);
                district.trigger('chosen:updated');
                district.prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){
            th.parents('.order-item-block').find('select[name="receiver_district[]"]').html("");
            th.parents('.order-item-block').find('select[name="receiver_district[]"]').prop('disabled', true);
            th.parents('.order-item-block').find('select[name="receiver_ward[]"]').html("");
            th.parents('.order-item-block').find('select[name="receiver_ward[]"]').prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            spinnerCity.hide();
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeDistrict(th){
    spinnerDistrict.show("Processing");
    var country = th.parents('.order-item-block').find('select[name="receiver_country[]"]');
    var city = th.parents('.order-item-block').find('select[name="receiver_city[]"]');
    var ward = th.parents('.order-item-block').find('select[name="receiver_ward[]"]');
    $('option[selected=selected]',th).removeAttr("selected");
    $.ajax({
        url: "/apiController/getWardForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'district': btoa(unescape(encodeURIComponent(JSON.stringify( th.val() )))),
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( city.val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( country.val() ))))
        },
        //async: false,
        success: function (data) {
            spinnerDistrict.hide();
            if(data.status) {
                $('option:selected',th).attr("selected","selected"); 
                ward.html(data.data);
                ward.trigger('chosen:updated');
                ward.prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){
            th.parents('.order-item-block').find('select[name="receiver_ward[]"]').html("");
            th.parents('.order-item-block').find('select[name="receiver_ward[]"]').prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            spinnerDistrict.hide();
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeSelect(th){
    $('option[selected=selected]',th).removeAttr("selected");
    $('option:selected',th).attr("selected","selected"); 
}
function getListAddress(hasLoad = false){
    $.ajax({
        url: "/apiController/getListAddressForBooking",
        type: 'post',
        //async: false,
        success: function (data) {
            $('#list_address_body').html(data);
            if(hasLoad) {
                var address = $("#list_address_body input[name=address_choosen]:checked").val();
                var add = JSON.parse(atob(address));
                $('#short_name').html(add['name']);
                $('#mobile_phone').html(add['phone']);
                $('#address').html(add['address']);
                console.log(add);
                $('#shipper_city').val(add['city_id']);
                $('#shipper_country').val(add['country_id']);
                $('#shipper_district').val(add['district_id']);
                $('#shipper_ward').val(add['ward_id']);
                $('#shipper_address').val(add['address']);
            }
        }
    });
}
function changeCountryAddress(){
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#add_city').html(data.data);
                $('#add_city').trigger('chosen:updated');
                $('#add_city').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_city").html("");
            $("#add_city").prop('disabled', true);
            $("#add_district").html("");
            $("#add_district").prop('disabled', true);
            $("#add_ward").html("");
            $("#add_ward").prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeCityAddress(){
    $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            spinnerCity.hide();
            if(data.status) {
                $('#add_district').html(data.data);
                $('#add_district').trigger('chosen:updated');
                $('#add_district').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_district").html("");
            $("#add_district").prop('disabled', true);
            $("#add_ward").html("");
            $("#add_ward").prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeDistrictAddress(){
    $.ajax({
        url: "/apiController/getWardForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'district': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_district :selected').val() )))),
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            spinnerDistrict.hide();
            if(data.status) {
                $('#add_ward').html(data.data);
                $('#add_ward').trigger('chosen:updated');
                $('#add_ward').prop("disabled", false);
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_ward").html("");
            $("#add_ward").prop('disabled', true);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function saveAddress(){
    if (validateFormAddAddress()) {
        var parameters = {
            'user_name'     : $('#add_name').val(),
            'user_phone'    : $('#add_phone').val(),
            'user_street'   : $('#add_street').val(),
            'user_country'  : $('#add_country').val(),
            'user_city'     : $('#add_city').val(),
            'user_district' : $('#add_district').val(),
            'user_ward'     : $('#add_ward').val(),
            'postal_code'   : $('#add_zip_code').val(),
            'is_default'    : $('#default').val(),
            'deleted'       : 0        
        };
        $.ajax({
            url: "/BookingController/saveAddressShipper",
            dataType: "json",
            type: 'post',
            data: {
                'parameters': parameters
            },
            //async: false,
            success: function (data) {
                loadingScreenJS(false);
                if (data.err) {
                    alert(data.msg);
                    $('.add-info').modal('hide');
                    getListAddress(true);
                    resetFormAddAddress();
                }else{
                    alert(data.msg);
                }
            },
            beforeSend: function(){
                loadingScreenJS(true);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status+"<br>"+thrownError);
                loadingScreenJS(false);
            }
        });
    }else{
        if (site_lang == 'en'){
            alert('Please fill data required');
        }else{
            alert('Vui lòng điền đủ thông tin còn trống');
        }
    }
}
function changeAddress(){
    var address = $("input[name=address_choosen]:checked").val();
    if (address) {        
        var add = JSON.parse(atob(address));
        $('.shiper-info-modal').modal('hide');
        $('#short_name').html(add['name']);
        $('#mobile_phone').html(add['phone']);
        $('#address').html(add['address']);
        $('#shipper_city').val(add['city_id']);
        $('#shipper_country').val(add['country_id']);
        $('#shipper_district').val(add['district_id']);
        $('#shipper_ward').val(add['ward_id']);
        $('#shipper_address').val(add['address']);
        $.ajax({
            url: "/ApiController/updateDefaultAddress",
            dataType: "json",
            type: 'post',
            data: {
                'addr_id': add['addr_id']
            },
            //async: false,
            success: function (data) {

            },
            beforeSend: function(){
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status+"<br>"+thrownError);
            }
        });
    }else{
        alert('Vui long chon dia chi');
    }
}
function resetFormAddAddress(){
    $('#add_name').val('');
    $('#add_phone').val('');
    $('#add_street').val('');
    $('#add_zip_code').val('');
    $('#add_country').prop('selectedIndex',0);
    $('#select2-add_country-container').text('');
    $('#add_city').prop('selectedIndex',0);
    $('#select2-add_city-container').text('');
    $("#add_ward").html("");
    $("#add_district").html("");
}

function createMultiOrder(){
    // loadingScreenJS(true);
    if (validateForm()) {
        $("input[name='save_address[]']").each(function(index,item){
            if ($(this).is(':checked')) {
                $(this).attr('value', '1');
            } else {
                $(this).attr('value', '0');
            }
        });
        var data = $("#multi_booking_data").serialize();
        $.ajax({
            url: "/BookingController/createMultiOrder",
            dataType: "json",
            type: 'post',
            data: data,
            //async: false,
            success: function (data) {
                if (data.status == 1) {
                    alert(data.message);
                    location.href = '/booking-list';
                }else{
                    alert(data.message);
                }                
                loadingScreenJS(false);  
            },
            beforeSend: function(){
                loadingScreenJS(true);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+"<br>"+thrownError);
                loadingScreenJS(false);
            }
        });
    }else{
        if (site_lang == 'en'){
            alert('Please fill data required');
        }else{
            alert('Vui lòng điền đủ thông tin còn trống');
        }
    }
    
}
function validateForm(){
    $('.error').hide();
    var check = true;
    var shipper_country = $("#shipper_country").val();
    var shipper_city = $("#shipper_city").val();
    var shipper_district = $("#shipper_district").val();
    var shipper_address = $("#address").html();
    $(".order-item-block").each(function(index,item){
        var receiver_name       = $(this).find("input[name='receiver_name[]']").val();
        var receiver_country    = $(this).find("select[name='receiver_country[]']").val();
        var receiver_city       = $(this).find("select[name='receiver_city[]']").val();
        var receiver_district   = $(this).find("select[name='receiver_district[]']").val();
        var receiver_ward       = $(this).find("select[name='receiver_ward[]']").val();
        var receiver_address    = $(this).find("input[name='receiver_address[]']").val();
        var receiver_phone      = $(this).find("input[name='receiver_phone[]']").val();
        var weight              = $(this).find("input[name='weight[]']").val();

        if(receiver_name === ""){
            $(this).find('.error_receiver_name').show();
            check = false;
        }
        if(receiver_country===""){
            $(this).find('.error_receiver_country').show();
            check = false;
        }
        if(receiver_city===""){
            $(this).find('.error_receiver_city').show();
            check = false;
        }
        if(receiver_district===""){
            $(this).find('.error_receiver_district').show();
            check = false;
        }
        if(receiver_ward===""){
            $(this).find('.error_receiver_ward').show();
            check = false;
        }
        if(receiver_address===""){
            $(this).find('.error_receiver_street').show();
            check = false;
        }
        if(!shipper_country || !shipper_city || !shipper_district || !shipper_address ){
            $('#error_shipper_info').show();
            $('#error_shipper_info').css('position','inherit');
            check = false;
        }
        if(receiver_phone==="" || receiver_phone.length < 10 || receiver_phone.length > 11){
            $(this).find('.error_receiver_phone').show();
            check = false;
        }
        if(weight===""){
            $(this).find('.error_weight').show();
            check = false;
        }
    });
    return check;
}   
function changeService(th){
    changeSelect(th);
    var is_dox = th.parents('.order-item-block').find('select[name="is_dox[]"]');
    $('option[disabled=disabled]',is_dox).removeAttr("disabled");
    if (th.val() == 'd04ec06c-aa19-91ff-4fab-57482f5afd33' || th.val() == '77d6ed67-1625-d9df-7ac4-57482eac128f') {
        $('option[value="dox"]',is_dox).attr("disabled","disabled");
        is_dox.val('non_dox');
        changeSelect(is_dox);
    }
}
function deleteAddress(_this){
    var id = $(_this).data('id');
    if (confirm("Are you sure for delete address?")) {
        $.ajax({
            url: "/apiController/deleteAddressShipper",
            dataType: "json",
            type: 'post',
            data: {
                'id' : id
            },
            //async: false,
            success: function (data) {
                if (site_lang === 'en'){
                    alert('Deleted success');
                }else{
                    alert('Xóa địa chỉ thành công');
                }
                getListAddress();
            }
        });
    }
    return false;
}
function createAndPrintAWB(){
    if (validateForm()) {
        $("input[name='save_address[]']").each(function(index,item){
            if ($(this).is(':checked')) {
                $(this).attr('value', '1');
            } else {
                $(this).attr('value', '0');
            }
        });
        var data = $("#multi_booking_data :input").serialize();
        $.ajax({
            url: "/BookingController/createMultiOrderAndPrintAWB",
            type: 'post',
            data: data,
            //async: false,
            success: function (data) {
                console.log(data);
                if (data == 'Duplicate order no, please change.') {
                    alert(data);
                }else{
                    window.open(data,'_blank');
                }
                loadingScreenJS(false);
            },
            beforeSend: function(){
                loadingScreenJS(true);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status+"<br>"+thrownError);
                loadingScreenJS(false);
            }
        });
    }else{
        if (site_lang == 'en'){
            alert('Please fill data required');
        }else{
            alert('Vui lòng điền đủ thông tin còn trống');
        }
    }
}
function autoFillAddress(ele){
    ele.find('.autoFillBooking').autocomplete({
        source: function (request, response) {
            jQuery.get("/apiController/getListAddressForAutocomplete", {
                query: request.term
               
            }, function (data) {
                response(jQuery.parseJSON(data));
            });
        },
        // minLength: 2,
        select: function(event, ui) {
   
            event.preventDefault();
            ele.find('input[name="receiver_name[]"]').val(ui.item.name);
            ele.find('input[name="receiver_phone[]"]').val(ui.item.phone);
            ele.find('select[name="receiver_country[]"]').val(ui.item.country_id);
            ele.find('input[name="receiver_address[]"]').val(ui.item.street);
            changeCountryAsync(ui.item.country_id, ele, ui.item.city_id);
            changeCityAsync(ui.item.city_id, ele, ui.item.district_id);
            changeDistrictAsync(ui.item.district_id, ele, ui.item.ward_id);
        }
    }).data('ui-autocomplete')._renderItem = function(ul, item) {
        return $('<li>').addClass('info-item')
        .append('<div class = "row info-inner">' + '<div class = "col-xs-6 info-name">' +item.name+ '</div>' + '<div class = "col-xs-6 info-phone text-right">' +item.phone+ '</div>' + '</div>')
        .appendTo(ul);
    };
}
function changeCountryAsync(country_id, ele, city_id) {
    spinnerCountry.show("Processing");
    return $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( country_id ))))
        },
        async: true,
        success: function (data){
            ele.find('select[name="receiver_city[]"]').html(data.data);
            ele.find('select[name="receiver_city[]"]').val(city_id);
            ele.find('select[name="receiver_city[]"]').trigger('chosen:updated');
            ele.find('select[name="receiver_city[]"]').prop("disabled", false);
        }
    });
}


function changeCityAsync(city_id, ele, district_id) {
    spinnerCity.show("Processing");
    return $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( city_id ))))
        },
        async: true,
        success: function (data){
            ele.find('select[name="receiver_district[]"]').html(data.data);
            ele.find('select[name="receiver_district[]"]').val(district_id);
            ele.find('select[name="receiver_district[]"]').trigger('chosen:updated');
            ele.find('select[name="receiver_district[]"]').prop("disabled", false);
        }
    });
}

function changeDistrictAsync(district_id, ele, ward_id) {
    spinnerDistrict.show("Processing");
    return $.ajax({
        url: "/apiController/getWardForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'district': btoa(unescape(encodeURIComponent(JSON.stringify( district_id ))))
        },
        async: true,
        success: function (data) {
            ele.find('select[name="receiver_ward[]"]').html(data.data);
            ele.find('select[name="receiver_ward[]"]').val(ward_id);
            ele.find('select[name="receiver_ward[]"]').trigger('chosen:updated');
            ele.find('select[name="receiver_ward[]"]').prop("disabled", false);
        }
    });
}
function validateFormAddAddress(){
    $('.error').hide();
    var check = true;
    var add_name = $("#add_name").val();
    var add_country = $("#add_country").val();
    var add_city = $("#add_city").val();
    var add_district = $("#add_district").val();
    //var receiver_ward = $("#receiver_ward").val();
    var add_street = $("#add_street").val();
    var add_phone = $("#add_phone").val();
    
    if(!add_name){
        $('#error_add_address_receiver_name').show();
        check = false;
    }
    if(!add_country){
        $('#error_add_address_receiver_country').show();
        check = false;
    }
    if(!add_city){
        $('#error_add_address_receiver_city').show();
        check = false;
    }
    if(!add_district){
        $('#error_add_address_receiver_district').show();
        check = false;
    }
    // if(!receiver_ward){
    //     $('#error_receiver_ward').show();
    //     check = false;
    // }
    if(!add_street){
        $('#error_add_address_receiver_street').show();
        check = false;
    }
    if(!add_phone){
        $('#error_add_address_receiver_phone').show();
        check = false;
    }
    return check;
}
function liveSearch() {
    var input, filter, row, name, phone, i;
    input = document.getElementById('inputSearch');
    filter = input.value.toUpperCase();
    row = $('.resultRow');
    $(row).each(function() {
        name = $(this).find('.resultName');
        phone =  $(this).find('.resultPhone');
        if (name || phone) {
            if (name.text().toUpperCase().indexOf(filter) > -1 || phone.text().toUpperCase().indexOf(filter) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        }
    });
}
function validateInput() {
    //validate phone in add address
    $("#add_phone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, F5, space
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 32) {
            e.preventDefault();
        }
    });
    $("#add_phone").on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });
    $('.order-item-block').each(function () {
        //validate phone number, cod_value, shipment_value, pcs
        $(this).find("input[name='receiver_phone[]'], input[name='cod_value[]'], input[name='shipment_value[]'], input[name='pcs[]'], #add_phone").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter, F5, space
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 32) {
                e.preventDefault();
            }
        });
        $(this).find("input[name='receiver_phone[]'], input[name='cod_value[]'], input[name='shipment_value[]'], #add_phone").on('input', function() {
            var $this = $( this );
            // Get the value.
            var input = $this.val();
            var input = input.replace(/[\,_\-]+/g, "");
            if(input == '.' || input == ''){
                return $this.val('');
            }else if(input.split('.').length > 2) {
                input = input.slice(0, -1);
                return $this.val(input);
            }else if(input.substring(input.length - 1, input.length)=='.') {
                return;
            }
            $this.val(input);
        });

        //validate weight
        $(this).find("input[name='weight[]']").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter, F5, space
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
                // Allow: Comma
                (e.keyCode === 188) ||
                // Allow: Period
                (e.keyCode === 190) ||
                // Allow: Period
                (e.keyCode === 110) ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return true;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 32 ) {
                e.preventDefault();
            }
        });
        $(this).find("input[name='weight[]']").on('input', function() {
            var $this = $( this );
            // Get the value.
            var input = $this.val();
            var input = input.replace(/[\_\-]+/g, "");
            if(input == '.' || input == ''){
                return $this.val('');
            }else if(input.split('.').length > 2) {
                input = input.slice(0, -1);
                return $this.val(input);
            }else if(input.substring(input.length - 1, input.length)=='.') {
                return;
            }
            $this.val(input);
        });
    })
}
function removeOrderItem() {
    $(".booking-many-section").on("click", ".trash-btn", function(e) {
        if($('.order-item-block').length > 1) {
            e.stopPropagation(); // this stops the click on the holder
            $(this).closest(".order-item-block").remove();
        }else{
            $('.order-item-block').find('input').val('');
            $('.order-item-block').last().find('input[name="date_entered[]"]').val(current_datetime);
        }
    });
}