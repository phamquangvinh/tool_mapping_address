$(document).ready(function() {
    $(document).on('click', '.menu-mobile-toggle', function(e) {
        e.preventDefault();
        $('body').addClass('open-sidebar');
    });
    $(document).on('click', '.sidebar-close, .sidebar-close-bg', function(e) {
        $('body').removeClass('open-sidebar');
    });
    //backt-to-top
    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            $('.back-top-btn').fadeIn();
        }
        else {
            $('.back-top-btn').fadeOut();
        }
    });
    $('.back-top-btn').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 300);
    });
    function datePicker() {
        $('.datepickerBox').datepicker({
            ignoreReadonly: true,
            format: 'dd/mm/yyyy',
            todayHighlight: true
        });
    }
    function selectGroup() {
        $('.chosenSelect').each(function() {
            if($(this).attr('data-placeholder')) {
                var dataPlaceholder = $(this).attr('data-placeholder');
                $(this).select2({
                    placeholder: dataPlaceholder,
                    //allowClear: true,
                    width: '100%',
                    dropdownParent: $(this).parent()
                });
            } else {
                $(this).select2({
                    width: '100%',
                    dropdownParent: $(this).parent()
                });
            }
        });
    }
    function tableDatatable() {
        $('.importDatatable').DataTable({
            "scrollX": true,
        });
        $('.listviewDatatable').DataTable({
            "scrollX": true
        });
        $('.modalHasDatatable').on('shown.bs.modal', function() {
           //Get the datatable which has previously been initialized
            var dataTable= $('.listviewDatatable').DataTable();
            //recalculate the dimensions
            dataTable.columns.adjust();
        });
    }
    function uploadAvatar() {
        var btnCust = '';
        $(".avatar").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            showBrowse: false,
            browseOnZoneClick: true,
            removeLabel: '',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-2',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="assets/images/upload/user-avatar.png" alt="Your Avatar"><h6 class="text-muted">Bấm vào để chọn hình</h6>',
            layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
    }
    function showMoreInfoRegister() {
        $('.more-info-register').on('hide.bs.collapse', function () {
            $('.btn-show-register').html('<i class="fa fa-plus" aria-hidden="true"></i> <span>Đăng ký với tư cách công ty</span>');
        })
        $('.more-info-register').on('show.bs.collapse', function () {
          $('.btn-show-register').html('<i class="fa fa-minus" aria-hidden="true"></i> <span>Đăng ký với tư cách công ty</span>');
        })
    }
    function openOrderNoInput() {
        $(".booking-many-section").on("click", ".add-order-no", function(e) {
            e.stopPropagation(); // this stops the click on the holder
            e.preventDefault();
            $(this).siblings().show();
            $(this).hide();
        });
    }
    function hideOrderNoInput() {
        $(".booking-many-section").on("click", ".btn-remove", function(e) {
            e.stopPropagation(); // this stops the click on the holder
            e.preventDefault();
            $(this).parent().prev().show();
            $(this).parent().hide();
        });
    }
    function cloneOrderItem() {
        $('.clone-order-btn').click(function() {
            $('.order-item-block').clone().last().insertAfter('.order-item-block:last');
            $('.order-item-block').last().find('input').val('');
        });
    }
    function removeOrderItem() {
        $(".booking-many-section").on("click", ".trash-btn", function(e) {
            if($('.order-item-block').length > 1) {
                e.stopPropagation(); // this stops the click on the holder
                $(this).closest(".order-item-block").remove();
            }
        });
    }
    function cloneThisOrderItem(this_button) {
        //$('.booking-many-section').on('click', '.clone-btn', function(e) {
            var cloneContainer = $(this_button).closest('.order-item-block');
            cloneContainer.find($('.chosenSelect')).select2('destroy');
            var clone = cloneContainer.html();
            //e.stopPropagation(); // this stops the click on the holder
            // $(this).closest('.order-item-block').clone().insertAfter($(this).closest('.order-item-block'));
            //clone.insertAfter(cloneContainer);
            $('.booking-many-section').append(clone);
            // enable Select2 on the select elements
            //cloneContainer.find('.chosenSelect').select2();
            //clone.find('.chosenSelect').select2('destroy');
            $('body .booking-many-section .chosenSelect').select2();
        //});
    }
    function stickyHeader() {
        $(window).scroll(function(){
            if ($(window).scrollTop() >= 45 && $(window).width() > 991) {
               $('nav').addClass('fixed-header');
            }
            else {
               $('nav').removeClass('fixed-header');
            }
        });
    }
    function collapseMenu() {
        if($('.menu-item').hasClass('active')) {
            $('.active').find('.treeview').show();
        }
        $('.menu-item').on('click', function() {
            if(!$(this).hasClass('active')) {
                $(this).find('.treeview').toggle('300');
            }
        });
    }

    function fileUpload() {
        $(".fileUpload").fileinput({
            showClose: false,
            showUpload: false,
            uploadLabel: '',
            uploadTitle: 'Tải file',
            browseLabel: 'Chọn tệp',
            browseIcon: '',
            removeLabel: '',
            removeTitle: 'Xóa file',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>'
        });
    }
    function feedbackForm() {
        $('.editFeedbackBtn').click(function(event) {
            $('.modal').modal('hide');
            $('.add-feedback-modal').modal('show');
        });
    }
    $('.listSlimScroll').slimScroll({
        height: '250px'
    });
    function shipperListForm() {
        $('.callAddShipperFormBtn').click(function() {
            $('.modal').modal('hide');
            $('.add-info').modal('show');
        });
        $('.closeAddInfoModal').click(function() {
            $('.modal').modal('hide');
            $('.shiper-info-modal').modal('show');
        });
    }

    feedbackForm();
    datePicker();
    selectGroup();
    tableDatatable();
    uploadAvatar();
    showMoreInfoRegister();
    openOrderNoInput();
    hideOrderNoInput();
    cloneOrderItem();
    removeOrderItem();
    cloneThisOrderItem();
    stickyHeader();
    collapseMenu();
    fileUpload();
    shipperListForm();
});
