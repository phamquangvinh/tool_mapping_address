if (site_lang == 'en') {
    var error_username = 'Please fill email/ username';
    var error_password = 'Please fill password';
} else {
    var error_username = 'Vui lòng nhập thông tin đăng nhập';
    var error_password = 'Vui lòng nhập mật khẩu đăng nhập';
}
function sendRequestForgetPassword() {
    loadingScreenJS(true); 
	$('#forget-password-form #error_forget_password').hide();
	$('#forget-password-form #success_forget_password').hide();
	var form = $('form').get();
    var fData = new FormData(form);
    fData.append('email', $('#forget-password-form #email').val());
    $.ajax({
        url: '/UserController/resetPassword',
        type: 'POST',
        data: fData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(dt) {
        	if(dt.err) {
				$('#forget-password-form #error_forget_password').html(dt.msg).show();
        	}else{
				$('#forget-password-form #success_forget_password').html(dt.msg).show();
				$('#forget-password-form')[0].reset();
        	}
            loadingScreenJS(false);
        },
        error: function(xhr, ajaxOption, throwError) {
            loadingScreenJS(false);
        }
    });
}
function validateLogin(){
    $('#login-form').validate({
        rules: {
            username:{
                required: true
            },
            password:{
                required: true
            }
        },
        messages : {
            username:{
                required: error_username
            },
            password:{
                required: error_password
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
}