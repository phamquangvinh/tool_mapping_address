$(document).ready(function () {
    $('#table_data_import').DataTable({
        "lengthMenu": [[500, 1000, 1500], [500, 1000, 1500]],
        "columns": [
            {data: 'checkbox'},
            {data: 'booking_no'},
            {data: 'service_category'},
            {data: 'dox'},
            {data: 'is_cod'},
            {data: 'cod_value'},
            {data: 'weight'},
            {data: 'pickup_country'},
            {data: 'pickup_city'},
            {data: 'pickup_district'},
            {data: 'pickup_address'},
            {data: 'shipper_contact_name'},
            {data: 'shipper_phone'},

            {data: 'pickup_date'},
            {data: 'delivery_country'},
            {data: 'delivery_city'},
            {data: 'delivery_district'},
            {data: 'delivery_address'},
            {data: 'delivery_company_name'},
            {data: 'delivery_contact_name'},
            {data: 'delivery_contact_phone'},
            {data: 'payment_method'},
            {data: 'charge_to'},
            {data: 'shipment_value'},
            {data: 'special_instruction'},
            {data: 'specical_req'},
            {data: 'booking_note'},
        ],
        "order": [[1, 'desc']],
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "scrollX": true,
    });    
    $("#check_all").click(function () {
        checkAllCheckbox(this);
    });
    $("#btn_import_booking_top,#btn_import_booking_bottom").click(function () {
        importBooking();
    });
});
$("#uploadExcel").on('change',(function(e) {
    e.preventDefault();    
    var total_record = 0;
    var total_failed = 0;
    var total_selected = 0;
    $('#total_record_import').html(total_record);
	$('#total_failed_import').html(total_failed);
	$('#total_selected_import').html(total_selected);
	$('#table_data_import').DataTable().clear().draw();
	$('#img_loading').html('<img src="/assets/images/ajax-loader.gif">');
    $.ajax({
        url: "/BookingController/importExcel",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            $('#img_loading').html('');
            var data_array = JSON.parse(data);
            if(data_array.status===true) {
                data_html = data_array.html;
                data_db = data_array.insert;
                data_insert_contact = data_array.insert_contact;
                data_insert_account_contact = data_array.insert_account_contact;
                $("#data_insert_contact").val(btoa(unescape(encodeURIComponent(JSON.stringify(data_insert_contact)))));
                $("#data_insert_account_contact").val(btoa(unescape(encodeURIComponent(JSON.stringify(data_insert_account_contact)))));
                if (data_html.length > 0) {
                    for (i = 0; i < data_html.length; i++) {
                        if (parseFloat(data_html[i]['is_enable']) == 0) {
                            total_failed++;
                        }
                        total_record++;
                    }
                    total_selected = data_db.length;
                    $('#total_record_import').html(total_record);
                    $('#total_failed_import').html(total_failed);
                    $('#total_selected_import').html(total_selected);
                    $('#total_checkbox').val(total_selected);
                    $('#info_import').show();
                    $('#btn_import_booking_bottom').hide();
                    $('#btn_import_booking_bottom').show();
                    $('#table_data_import').DataTable().rows.add(data_html).draw();
                    checkDataInvalid();
                    $("#check_all").prop('checked', true);
                }
            }else{
                alert(data_array.message);
            }
        }         
    });
}));
//hàm kiểm tra checkbox check_all
function checkAllCheckbox(this_checkbox) {
    var data = $('#table_data_import').DataTable().data();
    var check_html = '';
    total_selected = 0;
    if ($(this_checkbox).is(':checked') == true) {
        check_html = ' checked="checked" ';
    } else {
        check_html = '';
    }
    for (var i = 0; i < data.length; i++) {
        if (data[i]['is_enable'] == 1) {
            if (check_html != '') {
                total_selected++;
                data[i]['is_check'] = 1;
            } else {
                data[i]['is_check'] = 0;
            }
            data[i]['checkbox'] = '<input type="checkbox" ' + check_html + ' onclick="checkCheckbox(this)"/>';
            $('#table_data_import').DataTable().row(i).data(data[i]);
        }
    }
    $('#total_selected_import').text(total_selected);
}
//hàm check từng checkbox
function checkCheckbox(this_checkbox) {
    var data = $('#table_data_import').DataTable().data();
    var index = $('#table_data_import').DataTable().row($(this_checkbox).closest('tr')).index();
    var row_temp = $('#table_data_import').DataTable().data()[index];
    var count_checked = 0;
    if ($(this_checkbox).is(":checked") == true) {
        row_temp['is_check'] = 1;
        row_temp['checkbox'] = '<input type="checkbox" checked="checked" onclick="checkCheckbox(this)"/>';
    } else {
        row_temp['is_check'] = 0;
        row_temp['checkbox'] = '<input type="checkbox" onclick="checkCheckbox(this)"/>';
    }
    $('#table_data_import').DataTable().row(index).data(row_temp);

    for (var i = 0; i < data.length; i++) {
        if (data[i]['is_check'] == 1) {
            count_checked++;
        }
    }
    $('#total_selected_import').text(count_checked);
    if (count_checked == parseInt($('#total_checkbox').val())) {
        $("#check_all").prop('checked', true);
    } else {
        $("#check_all").prop('checked', false);
    }
}
function checkDataInvalid() {
    var data = $('#table_data_import').DataTable().data();
    for (var i = 0; i < data.length; i++) {
        if (data[i]['is_enable'] == 0) {
            $($('#table_data_import').DataTable().rows(i).nodes()).addClass('highlight_row');
        }
    }
}
function importBooking() {
    if(confirm('Do you want import records selected ?')) {
        var data = $('#table_data_import').DataTable().data();
        var arr_data_insert = data_db;
        var arr_id_booking_insert = [];
        for (var i = 0; i < data.length; i++) {
            if (data[i]['is_check'] == 1 && data[i]['is_enable'] == 1) {
                arr_id_booking_insert[arr_id_booking_insert.length] = data[i]['id'];
            }
        }
        // arr_id_booking_insert.forEach(function (element) {
        //     arr_data_insert[arr_data_insert.length] = data_db[element];
        // });
        // console.log(data_db);
        $('#img_loading').html('<img src="/assets/images/ajax-loader.gif">');
        $.ajax({
            url: '/BookingController/insertBooking',
            type: 'POST',
            data: {
                data_insert: btoa(unescape(encodeURIComponent(JSON.stringify(arr_data_insert)))),
                data_list_id: btoa(unescape(encodeURIComponent(JSON.stringify(arr_id_booking_insert)))),
                data_insert_contact: $("#data_insert_contact").val(),
                data_insert_account_contact: $("#data_insert_account_contact").val(),
            },
            success: function (data) {
                $('#img_loading').html('');
                if(data == 1) {
                    alert('Import Success.');
                    $('#table_data_import').DataTable().clear().draw();
                    $('#total_record_import').html(0);
                    $('#total_failed_import').html(0);
                    $('#total_selected_import').html(0);
                    $('#total_checkbox').val(0);
                    $('#info_import').hide();
                    $('#btn_import_booking_bottom').hide();
                    window.location.href = "/booking-import";
                }else{
                    if(data === 0) {
                        alert('Import Failed.');
                    }else{
                        alert(data);
                    }
                }
            }
        });
    }
}