var spinnerFrom,spinnerTo,spinnerLogin;
if (site_lang == 'en'){
    var error_username = 'Please fill email/ username';
    var error_password = 'Please fill password';
}else{
    var error_username = 'Vui lòng nhập thông tin đăng nhập';
    var error_password = 'Vui lòng nhập mật khẩu đăng nhập';
}
$(document).ready(function() {
    //$('#shipping_lookup').hide();
    spinnerFrom = new Spinner('row_select_home_send_from');
    spinnerTo = new Spinner('row_select_home_send_to');
    spinnerLogin = new Spinner('login-form-loading');

    //validatorweight
    $('#weight').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, . .(number pad)and F5
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 110, 116]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('#weight').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });

     //COD 
    $('#cod_value').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and F5
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('#cod_value').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });

    $("#is_ras").on('change', function() {
        if ($(this).is(':checked')) {
            $(this).attr('value', '1');
        } else {
            $(this).attr('value', '0');
        }
    });

    if ($('#is_ward_ras').val() == 1){
        $('#is_ras').prop('checked', true);
        $('#check_ras_home').attr('id','check_ras_home_readonly');
    }
});
function login(){
    $.ajax({
        url: "/loginController/login",
        dataType: "json",
        type: 'post',
        data: {
            'parameters': btoa(unescape(encodeURIComponent(JSON.stringify(ITL.portal.getParam('login')))))
        },
        //async: false,
        success: function (data) {
            spinnerLogin.hide();
            if(data.status) {
                window.location = data.data;
            }else{
                alert(data.message);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            spinnerLogin.hide();
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}

function changeCity(type){
    switch (type){
        case "from":
            spinnerFrom.show("Processing")
            $.ajax({
                url: "/apiController/getDistrict",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters': btoa(unescape(encodeURIComponent(JSON.stringify( $('#shipper_city :selected').val() ))))
                },
                //async: false,
                success: function (data) {
                    spinnerFrom.hide();
                    if(data.status) {
                        $('#shipper_district').html(data.data);
                        $('#shipper_ward').html('');
                        $('#shipper_district').trigger('chosen:updated');
                    }else{
                        alert(data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    spinnerFrom.hide();
                    alert(xhr.status+"<br>"+thrownError);
                }
            });
            break;
        case "to":
            spinnerTo.show("Processing");
            $.ajax({
                url: "/apiController/getDistrict",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_city :selected').val() ))))
                },
                //async: false,
                success: function (data) {
                    spinnerTo.hide();
                    if(data.status) {
                        $('#receiver_district').html(data.data);
                        $('#receiver_ward').html('');
                        $('#receiver_district').trigger('chosen:updated');
                    }else{
                        alert(data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    spinnerTo.hide();
                    alert(xhr.status+"<br>"+thrownError);
                }
            });            
            $('#is_ras').prop('checked', false);
            $('#check_ras_home_readonly').attr('id','check_ras_home');
            $('#is_ward_ras').val('0');
            $('#is_ras').val('0');
            break;
    }
}
function changeDistrict(type){
    switch (type){
        case "from":
            spinnerFrom.show("Processing")
            $.ajax({
                url: "/apiController/getWard",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters': btoa(unescape(encodeURIComponent(JSON.stringify( $('#shipper_district :selected').val() ))))
                },
                //async: false,
                success: function (data) {
                    spinnerFrom.hide();
                    if(data.status) {
                        $('#shipper_ward').html(data.data);
                        $('#shipper_ward').trigger('chosen:updated');
                    }else{
                        alert(data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    spinnerFrom.hide();
                    alert(xhr.status+"<br>"+thrownError);
                }
            });
            break;
        case "to":
            spinnerTo.show("Processing");
            $.ajax({
                url: "/apiController/getWard",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters': btoa(unescape(encodeURIComponent(JSON.stringify( $('#receiver_district :selected').val() ))))
                },
                //async: false,
                success: function (data) {
                    spinnerTo.hide();
                    if(data.status) {
                        $('#receiver_ward').html(data.data);
                        $('#receiver_ward').trigger('chosen:updated');
                    }else{
                        alert(data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    spinnerTo.hide();
                    alert(xhr.status+"<br>"+thrownError);
                }
            });
            $('#is_ras').prop('checked', false);
            $('#check_ras_home_readonly').attr('id','check_ras_home');
            $('#is_ward_ras').val('0');
            $('#is_ras').val('0');
            break;
    }
}
function selectWard(el) {
    if($(el).find('option:selected').data('is-ras')==1) {
        $('#is_ras').prop('checked', true);
        $('#check_ras_home').attr('id','check_ras_home_readonly');
        $('#is_ward_ras').val('1');
        $('#is_ras').val('1');
    }else{
        $('#is_ras').prop('checked', false);
        $('#check_ras_home_readonly').attr('id','check_ras_home');
        $('#is_ward_ras').val('0');
        $('#is_ras').val('0');
    }
}
function validateLogin(){
    $('#login-form').validate({
        rules: {
            username:{
                required: true
            },
            password:{
                required: true
            }
        },
        messages : {
            username:{
                required: error_username
            },
            password:{
                required: error_password
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
}