$(document).ready(function() {
    checkFlashEnable();
    getListAddress();
    getListConsignee();
    $('.error').hide();
    if (site_lang == 'vi') {
        var language = {
            "sEmptyTable":   "Không có dữ liệu phù hợp",
            "sProcessing":   "Đang xử lý...",
            "sLengthMenu":   "Xem _MENU_ mục",
            "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
            "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix":  "",
            "sSearch":       "Tìm:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Đầu",
                "sPrevious": "Trước",
                "sNext":     "Tiếp",
                "sLast":     "Cuối"
            }
        };
    }else{
        var language = {
            "sEmptyTable":     "No data available in table",
            "sInfo":           "Showing _START_ to _END_ of _TOTAL_ entries",
            "sInfoEmpty":      "Showing 0 to 0 of 0 entries",
            "sInfoFiltered":   "(filtered from _MAX_ total entries)",
            "sInfoPostFix":    "",
            "sInfoThousands":  ",",
            "sLengthMenu":     "Show _MENU_ entries",
            "sLoadingRecords": "Loading...",
            "sProcessing":     "Processing...",
            "sSearch":         "Search:",
            "sZeroRecords":    "No matching records found",
            "oPaginate": {
                "sFirst":    "First",
                "sLast":     "Last",
                "sNext":     "Next",
                "sPrevious": "Previous"
            },
            "oAria": {
                "sSortAscending":  ": activate to sort column ascending",
                "sSortDescending": ": activate to sort column descending"
            }
        };
    }
    $('.info-edit-modal').on('show.bs.modal', function (event) {
        resetFormEditAddress();
        var button = $(event.relatedTarget); // Button that triggered the modal
        var addr_id = button.data('detail_address'); // Extract info from data-* attributes
        var type = button.data('type_address'); 
        var modal = $(this);
        if (type == 'shipper') {
            $.ajax({
                url: "apiController/getDetailAddressByUser",
                type: "POST",
                dataType:"json",
                data: {
                    'addr_id' : addr_id
                },
                success: function (data, status, jqXHR) {
                    modal.find('#edit_address_postal_code').val(data.postal_code);
                    modal.find('#edit_address_postal_code_old').val(data.postal_code);
                    modal.find('#btn_edit_address').data('type_address','shipper');
                    modal.find('#btn_edit_address').val(data.addr_id);
                    setValueSelectedCity(data.country_id,data.city_id);
                    setValueSelectedDistrict(data.city_id,data.district_id);
                    setValueSelectedWard(data.district_id,data.ward_id);
                    modal.find('#edit_address_name').val(data.name);
                    modal.find('#edit_address_name_old').val(data.name);
                    modal.find('#edit_address_phone').val(data.phone);
                    modal.find('#edit_address_phone_old').val(data.phone);
                    modal.find('#edit_address_street').val(data.street);
                    modal.find('#edit_address_street_old').val(data.street);
                    modal.find('#edit_address_country_old').val(data.country_id);
                    modal.find('#edit_address_city_old').val(data.city_id);
                    modal.find('#edit_address_district_old').val(data.district_id);
                    modal.find('#edit_address_ward_old').val(data.ward_id);
                    var cc = modal.find('#edit_address_country');
                    cc.val(data.country_id);
                    cc.siblings('.select2').find('.select2-selection__rendered').text(cc.find('option[value="'+data.country_id+'"]').text());
                    loadingScreenJS(false);
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    loadingScreenJS(false);
                }
            });
        }
        if (type == 'consignee') {
            $.ajax({
                url: "apiController/getDetailAddressConsignee",
                type: "POST",
                dataType:"json",
                data: {
                    'addr_id' : addr_id
                },
                success: function (data, status, jqXHR) {
                    modal.find('#edit_address_postal_code').val(data.postal_code);
                    modal.find('#edit_address_postal_code_old').val(data.postal_code);
                    modal.find('#btn_edit_address').data('type_address','consignee');
                    modal.find('#btn_edit_address').val(data.addr_id);
                    setValueSelectedCity(data.country_id,data.city_id);
                    setValueSelectedDistrict(data.city_id,data.district_id);
                    setValueSelectedWard(data.district_id,data.ward_id);
                    modal.find('#edit_address_name').val(data.name);
                    modal.find('#edit_address_name_old').val(data.name);
                    modal.find('#edit_address_phone').val(data.phone);
                    modal.find('#edit_address_phone_old').val(data.phone);
                    modal.find('#edit_address_street').val(data.street);
                    modal.find('#edit_address_street_old').val(data.street);
                    modal.find('#edit_address_country_old').val(data.country_id);
                    modal.find('#edit_address_city_old').val(data.city_id);
                    modal.find('#edit_address_district_old').val(data.district_id);
                    modal.find('#edit_address_ward_old').val(data.ward_id);
                    var cc = modal.find('#edit_address_country');
                    cc.val(data.country_id);
                    cc.siblings('.select2').find('.select2-selection__rendered').text(cc.find('option[value="'+data.country_id+'"]').text());
                    loadingScreenJS(false);
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    loadingScreenJS(false);
                }
            });
        }
    });
    $('#add_address_shipper').click(function(){
        $('#btn_save_address').data('address_type','shipper');
    });
    $('#add_address_consignee').click(function(){
        $('#btn_save_address').data('address_type','consignee');
    });
    $('#inputSearchShipper').keyup(function() {
        liveSearchShipper();
    });
    $('#inputSearchConsignee').keyup(function() {
        liveSearchConsignee();
    });
    $('#import').DataTable({
        "columns": [
            {'data': 'name'},
            {'data': 'phone'},
            {'data': 'address'},
            {'data': 'country'},
            {'data': 'city'},
            {'data': 'district'},
            {'data': 'ward'},
            {'data': 'postal_code'}
        ],
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "language": language,
        "order": [[0, 'asc']]
    });

    $('#file_upload').uploadify({
        'formData': {
            'timestamp': timestamp,
            'token': token
        },
        'swf': '/uploadify/uploadify.swf',
        'uploader': '/uploadify/uploadify.php?user_id=' + user_id,
        'fileTypeDesc': 'Excel Files',
        'fileTypeExts': '*.xls; *.xlsx',
        'buttonText':  site_lang == 'en' ? 'SELECT FILE' : 'CHỌN TẬP TIN',
        'onUploadComplete': function (file) {
            $('#import').DataTable().clear().draw();
            $('#img_loading').html('<img src="/assets/img/loading.gif">');
            $('#import_credit_value').val('');
            $.ajax({
                url: '/userController/getDataImport',
                type: 'post',
                data: {
                    file: file,
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#import_total_record').text(0);
                    $('#import_total_failed').text(0);
                },
                success: function (data) {
                    if(data < 2){
                        if (site_lang == 'en') {
                            alert('File upload is empty.');
                        }else{
                            alert('Dữ liệu đầu vào rỗng.');
                        }                        
                        $('#img_loading').html('');
                    }else if(data > 2001){
                        if (site_lang == 'en') {
                            alert('Data in file too big. System just can import maximum 2000 record.');
                        }else{
                            alert('Dữ liệu đầu vào quá lớn. Hệ thống chỉ chấp nhận dữ liệu ít hơn 2000 dòng.');
                        }                        
                        $('#img_loading').html('');
                    }else{
                        $('#img_loading').html('');
                        var data_html = data['data'];
                    //    data_db = data['update'];
                        if (Object.keys(data_html).length > 0) {
                            $('#import').DataTable().rows.add(data_html).draw();
                            $('#import_total_record').text(data.total.total_record);
                            $('#import_total_failed').text(data.total.total_failed);
                        }
                    }
                }
            });
        }
    });

    //validator phone
    $('#edit_address_phone, #add_phone').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, F5, space
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || e.keyCode == 32) {
            e.preventDefault();
        }
    });
    $('#edit_address_phone, #add_phone').on('input', function() {
        var $this = $( this );
        // Get the value.
        var input = $this.val();
        var input = input.replace(/[\,_\-]+/g, "");
        if(input == '.' || input == ''){
            return $this.val('');
        }else if(input.split('.').length > 2) {
            input = input.slice(0, -1);
            return $this.val(input);
        }else if(input.substring(input.length - 1, input.length)=='.') {
            return;
        }
        $this.val(input);
    });
});

function changeCountryAddress(){
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#add_city').html(data.data);
                $('#add_city').trigger('chosen:updated');
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_city").html("");
            $("#add_district").html("");
            $("#add_ward").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeCityAddress(){
    $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#add_district').html(data.data);
                $('#add_district').trigger('chosen:updated');
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_district").html("");
            $("#add_ward").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeDistrictAddress(){
    $.ajax({
        url: "/apiController/getWardForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'district': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_district :selected').val() )))),
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#add_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#add_ward').html(data.data);
                $('#add_ward').trigger('chosen:updated');
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#add_ward").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function resetFormAddAddress(){
    $('#add_name').val('');
    $('#add_phone').val('');
    $('#add_street').val('');
    $('#add_zip_code').val('');
    $('#add_country').prop('selectedIndex',0);
    $('#select2-add_country-container').text('');
    $("#add_ward").html("");
    $("#add_city").html("");
    $("#add_district").html("");
}
function resetFormEditAddress(){
    $('#edit_address_name').val($('#edit_address_name_old').val());
    $('#edit_address_phone').val($('#edit_address_phone_old').val());
    $('#edit_address_street').val($('#edit_address_street_old').val());
    $('#edit_address_postal_code').val($('#edit_address_postal_code_old').val());
    setValueSelectedCity($('#edit_address_country_old').val(),$('#edit_address_city_old').val());
    setValueSelectedDistrict($('#edit_address_city_old').val(),$('#edit_address_district_old').val());
    setValueSelectedWard($('#edit_address_district_old').val(),$('#edit_address_ward_old').val());
    var cc = $('#edit_address_country');
    cc.val($('#edit_address_country_old').val());
    cc.siblings('.select2').find('.select2-selection__rendered').text(cc.find('option[value="'+$('#edit_address_country_old').val()+'"]').text());
}
function saveAddress(_this){
    var parameters = {        
        'user_name'     : $('#add_name').val(),
        'user_phone'    : $('#add_phone').val(),
        'user_street'   : $('#add_street').val(),
        'user_country'  : $('#add_country').val(),
        'user_city'     : $('#add_city').val(),
        'user_district' : $('#add_district').val(),
        'user_ward'     : $('#add_ward').val(),
        'postal_code'   : $('#add_zip_code').val(),
        'is_default'    : 0,
        'deleted'       : 0
    };
    var type = $(_this).data('address_type');
    if(validateFormAddAddress()){
        if (type == 'shipper') {
            $.ajax({
                url: "/BookingController/saveAddressShipper",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters': parameters
                },
                //async: false,
                success: function (data) {
                    if (data.err) {
                        alert(data.msg);
                        $('.info-add-modal').modal('hide');
                        getListAddress();
                        resetFormAddAddress();
                        if ($('#default').val() == 1){
                            window.location.reload();
                        }
                    }else{
                        alert(data.msg);
                    }
                    loadingScreenJS(false);
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status+"<br>"+thrownError);
                    loadingScreenJS(false);
                }
            });
        }
        if (type == 'consignee') {
            $.ajax({
                url: "/BookingController/saveAddressConsignee",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters': parameters
                },
                //async: false,
                success: function (data) {
                    if (data.err) {
                        alert(data.msg);
                        $('.info-add-modal').modal('hide');
                        getListConsignee();
                        resetFormAddAddress();
                    }else{
                        alert(data.msg);
                    }
                    loadingScreenJS(false);
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status+"<br>"+thrownError);
                    loadingScreenJS(false);
                }
            });
        }
    }
}
function getListAddress(){
    $.ajax({
        url: "/apiController/getListAddressByUser",
        type: 'post',
        dataType: 'html',
        //async: false,
        success: function (data) {
            $('#list_shipper_contact').html(data);
        }
    });
}
function getListConsignee(){
    $.ajax({
        url: "/apiController/getListConsigneeByUser",
        type: 'post',
        //async: false,
        success: function (data) {
            $('#list_consignee_contact').html(data);
        }
    });
}
function setValueSelectedCity(country_id,city_id){
    $("#edit_address_city").html('');
    $.ajax({
        url: "/apiController/getCityForEditAddress",
        dataType: "json",
        type: 'post',
        data: {
            'country_id': country_id
        },
        //async: false,
        success: function (data) {
            if (data != ''){
                for (i in data) {
                    var is_selected = '';
                    if (data[i].id == city_id){
                        is_selected = 'selected';
                    }
                    $("#edit_address_city").append("<option value='"+data[i].id+"' "+is_selected+">"+data[i].name+"</option>");
                }
            }
        }
    });
}
function setValueSelectedDistrict(city_id,district_id){
    $("#edit_address_district").html('');
    $.ajax({
        url: "/apiController/getDistrictForEditAddress",
        dataType: "json",
        type: 'post',
        data: {
            'city_id': city_id
        },
        //async: false,
        success: function (data) {
            if (data != ''){
                for (i in data) {
                    var is_selected = '';
                    if (data[i].id == district_id){
                        is_selected = 'selected';
                    }
                    $("#edit_address_district").append("<option value='"+data[i].id+"' "+is_selected+">"+data[i].name+"</option>");
                }
            }
        }
    });
}
function setValueSelectedWard(district_id,ward_id){
    $("#edit_address_ward").html('');
    $.ajax({
        url: "/apiController/getWardForEditAddress",
        dataType: "json",
        type: 'post',
        data: {
            'district_id': district_id
        },
        //async: false,
        success: function (data) {
            if (data != ''){
                for (i in data) {
                    var is_selected = '';
                    if (data[i].id == ward_id){
                        is_selected = 'selected';
                    }
                    $("#edit_address_ward").append("<option value='"+data[i].id+"' "+is_selected+">"+data[i].name+"</option>");
                }
            }
        }
    });
}
function changeCountryEditAddress(){
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#edit_address_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#edit_address_city').html(data.data);
                $('#edit_address_city').trigger('chosen:updated');
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#edit_address_city").html("");
            $("#edit_address_district").html("");
            $("#edit_address_ward").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeCityEditAddress(){
    $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#edit_address_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#edit_address_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#edit_address_district').html(data.data);
                $('#edit_address_district').trigger('chosen:updated');
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#edit_address_district").html("");
            $("#edit_address_ward").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeDistrictEditAddress(){
    $.ajax({
        url: "/apiController/getWardForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'district': btoa(unescape(encodeURIComponent(JSON.stringify( $('#edit_address_district :selected').val() )))),
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#edit_address_city :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#edit_address_country :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#edit_address_ward').html(data.data);
                $('#edit_address_ward').trigger('chosen:updated');
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){            
            $("#edit_address_ward").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeAddress(_this){
    var id = $(_this).val();
    var type = $(_this).data('type_address');
    var parameters = {
        'user_name'         : $('#edit_address_name').val(),
        'user_phone'        : $('#edit_address_phone').val(),
        'user_street'        : $('#edit_address_street').val(),
        'user_district'     : $('#edit_address_district :selected').val(),
        'user_city'         : $('#edit_address_city :selected').val(),
        'user_country'      : $('#edit_address_country :selected').val() ,
        'user_ward'         : $('#edit_address_ward :selected').val(),
        'postal_code'       : $('#edit_address_postal_code').val()
    };
    if (validateFormEditAddress()){
        if (type=='shipper') {
            $.ajax({
                url: "/apiController/updateAddressShipper",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters' : parameters,
                    'id' : id
                },
                //async: false,
                success: function (data) {
                    alert('Update success');
                    $('.info-edit-modal').modal('hide');
                    getListAddress();
                    resetFormEditAddress();
                    loadingScreenJS(false);
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status+"<br>"+thrownError);
                    loadingScreenJS(false);
                }
            });
        }
        if (type=='consignee') {
            $.ajax({
                url: "/apiController/updateAddressConsignee",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters' : parameters,
                    'id' : id
                },
                //async: false,
                success: function (data) {
                    alert('Update success');
                    $('.info-edit-modal').modal('hide');
                    getListConsignee();
                    resetFormEditAddress();
                    loadingScreenJS(false);
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status+"<br>"+thrownError);
                    loadingScreenJS(false);
                }
            });
        }
    }
}
function deleteAddress(_this){
    var type = $(_this).data('type');
    var id = $(_this).data('id');
    if (confirm("Are you sure for delete address?")) {
        if (type=='shipper') {
            $.ajax({
                url: "/apiController/deleteAddressShipper",
                dataType: "json",
                type: 'post',
                data: {
                    'id' : id
                },
                //async: false,
                success: function (data) {
                    alert('Deleted success');
                    getListAddress();
                }
            });
        }
        if (type=='consignee') {
            $.ajax({
                url: "/apiController/deleteAddressConsignee",
                dataType: "json",
                type: 'post',
                data: {
                    'id' : id
                },
                //async: false,
                success: function (data) {
                    alert('Deleted success');
                    $('.info-edit-modal').modal('hide');
                    getListConsignee();
                }
            });
        }
    }
    return false;
}
function liveSearchShipper() {
    var input, filter, row, name, phone, i;
    input = document.getElementById('inputSearchShipper');
    filter = input.value.toUpperCase();
    row = $('.resultRowShipper');
    $(row).each(function() {
        name = $(this).find('.resultNameShipper');
        phone =  $(this).find('.resultPhoneShipper');
        if (name || phone) {
            if (name.text().toUpperCase().indexOf(filter) > -1 || phone.text().toUpperCase().indexOf(filter) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        }
    });
}
function liveSearchConsignee() {
    var input, filter, row, name, phone, i;
    input = document.getElementById('inputSearchConsignee');
    filter = input.value.toUpperCase();
    row = $('.resultRowConsignee');
    $(row).each(function() {
        name = $(this).find('.resultNameConsignee');
        phone =  $(this).find('.resultPhoneConsignee');
        if (name || phone) {
            if (name.text().toUpperCase().indexOf(filter) > -1 || phone.text().toUpperCase().indexOf(filter) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        }
    });
}
function importExcel(){
    $('#import_total_record').text(0);
    $('#import_total_failed').text(0);
    $('#import_total_selected').text(0);

    $('#import').DataTable().clear().draw();
    $("#importModal").modal();
}
function saveImport(){
    if ($('#import_total_failed').text() != 0) {
        if (site_lang == 'en') {
            alert('Import failed, please check your data.');    
        }else{
            alert('Thêm địa chỉ thất bại, vui lòng kiểm tra lại dữ liệu.');
        }
    }else{
        var type = $('.nav-tabs').find("li.active").data('name');
        var parameters = $('#import').DataTable().data();
        if (type == 'shipper') {
            $.ajax({
                url: "/userController/saveAddressShipper",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters': btoa(unescape(encodeURIComponent(JSON.stringify(parameters))))
                },
                //async: false,
                success: function (data) {
                    loadingScreenJS(false);
                    if(data.status === 404){                    
                        if (site_lang == 'en') {
                            alert('Import failed, please check your data.');    
                        }else{
                            alert('Thêm địa chỉ thất bại, vui lòng kiểm tra lại dữ liệu.');
                        }
                        var data_html = convertDataToImportTable(data.data);
                        $('#import').DataTable().clear().draw();
                        $('#import').DataTable().rows.add(data_html).draw();                    
                        $('#import_total_failed').text(data.message);
                    }else{
                        if (data.message == 'Success') {
                            if (site_lang == 'en') {
                                alert('Add address success');    
                            }else{
                                alert('Thêm địa chỉ thành công');
                            }
                            $('#importModal').modal('hide');
                            getListAddress();
                        }else{
                            alert(data.message);
                        }    
                    }                
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status+"<br>"+thrownError);
                }
            });
        }
        if (type == 'consignee') {
            $.ajax({
                url: "/userController/saveAddressConsignee",
                dataType: "json",
                type: 'post',
                data: {
                    'parameters': btoa(unescape(encodeURIComponent(JSON.stringify(parameters))))
                },
                //async: false,
                success: function (data) {
                    loadingScreenJS(false);
                    if(data.status === 404){                    
                        if (site_lang == 'en') {
                            alert('Import failed, please check your data.');    
                        }else{
                            alert('Thêm địa chỉ thất bại, vui lòng kiểm tra lại dữ liệu.');
                        }
                        var data_html = convertDataToImportTable(data.data);
                        $('#import').DataTable().clear().draw();
                        $('#import').DataTable().rows.add(data_html).draw();                    
                        $('#import_total_failed').text(data.message);
                    }else{
                        if (data.message == 'Success') {
                            if (site_lang == 'en') {
                                alert('Add address success');    
                            }else{
                                alert('Thêm địa chỉ thành công');
                            }
                            $('#importModal').modal('hide');
                            getListConsignee();
                        }else{
                            alert(data.message);
                        }    
                    }  
                },
                beforeSend: function(){
                    loadingScreenJS(true);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    // alert(xhr.status+"<br>"+thrownError);
                }
            });
        }
    }  
}
function validateFormAddAddress() {
    $('.error').hide();
    var name = $('#add_name').val();
    var phone = $('#add_phone').val();
    var street = $('#add_street').val();
    var country = $('#add_country').val();
    var city = $('#add_city').val();
    var district = $('#add_district').val();
    var check = true;

    if (!name){
        $('#error_add_address_name').show();
        check = false;
    }
    if (!phone){
        $('#error_add_address_phone').show();
        check = false;
    }else {
        if (phone.length < 10 || phone.length > 11) {
            $('#error_add_address_phone_length').show();
            check = false;
        }
    }
    if (!street){
        $('#error_add_address_street').show();
        check = false;
    }
    if (!country){
        $('#error_add_address_nation').show();
        check = false;
    }
    if (!city){
        $('#error_add_address_city').show();
        check = false;
    }
    if (!district){
        $('#error_add_address_district').show();
        check = false;
    }
    return check;
}
function validateFormEditAddress() {
    $('.error').hide();
    var name = $('#edit_address_name').val();
    var phone = $('#edit_address_phone').val();
    var street = $('#edit_address_street').val();
    var country = $('#edit_address_country').val();
    var city = $('#edit_address_city').val();
    var district = $('#edit_address_district').val();
    var check = true;

    if (!name){
        $('#error_edit_address_name').show();
        check = false;
    }
    if (!phone){
        $('#error_edit_address_phone').show();
        check = false;
    }else {
        if (phone.length < 10 || phone.length > 11) {
            $('#error_edit_address_phone_length').show();
            check = false;
        }
    }
    if (!street){
        $('#error_edit_address_street').show();
        check = false;
    }
    if (!country){
        $('#error_edit_address_country').show();
        check = false;
    }
    if (!city){
        $('#error_edit_address_city').show();
        check = false;
    }
    if (!district){
        $('#error_edit_address_district').show();
        check = false;
    }
    return check;
}
function checkFlashEnable(){
    try
        {
            var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
            if(fo) {
                $('#popup_flash').hide();
            }else{
                $('#popup_flash').show();
            }
        }
    catch(e)
        {
            if(navigator.mimeTypes ["application/x-shockwave-flash"] != undefined) {
                $('#popup_flash').hide();
            }else{
                $('#popup_flash').show();
            }
        }
}
function convertDataToImportTable(data){
    var data_converted = [];
    if (site_lang == 'vi') {
        for (var i = 0; i < data.length; i++) {
            data_converted[i] = {
                name          : data[i].user_name,
                phone         : data[i].user_phone,
                address       : data[i].user_street,
                country       : data[i].user_country == 'Not Found' ? '<span style="color:red">* Quốc gia không chính xác.</span>' : data[i].user_country,
                city          : data[i].user_city == 'Not Found' ? '<span style="color:red">* Thành phố không chính xác.</span>' : data[i].user_city,
                district      : data[i].user_district == 'Not Found' ? '<span style="color:red">* Quận không chính xác.</span>' : data[i].user_district,
                ward          : data[i].user_ward == 'Not Found' ? '<span style="color:red">* Phường không chính xác.</span>' : data[i].user_ward,
                postal_code   : data[i].postal_code
            };
        }
    }else{
        for (var i = 0; i < data.length; i++) {
            data_converted[i] = {
                name          : data[i].user_name,
                phone         : data[i].user_phone,
                address       : data[i].user_street,
                country       : data[i].user_country == 'Not Found' ? '<span style="color:red">* Country is not exist on system.</span>' : data[i].user_country,
                city          : data[i].user_city == 'Not Found' ? '<span style="color:red">* City is not exist on system.</span>' : data[i].user_city,
                district      : data[i].user_district == 'Not Found' ? '<span style="color:red">* District is not exist on system.</span>' : data[i].user_district,
                ward          : data[i].user_ward == 'Not Found' ? '<span style="color:red">* Ward is not exist on system.</span>' : data[i].user_ward,
                postal_code   : data[i].postal_code
            };
        }
    }
    return data_converted;
}