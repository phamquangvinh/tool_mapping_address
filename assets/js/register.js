if (site_lang == 'en'){
    var error_username = 'Please fill email/ username';
    var error_password = 'Please fill password';
}else{
    var error_username = 'Vui lòng nhập thông tin đăng nhập';
    var error_password = 'Vui lòng nhập mật khẩu đăng nhập';
}
$(document).ready(function() {
    $('.error').hide();
    changeCountryAddress();
    //$('#shipping_lookup').hide();
    spinnerFrom = new Spinner('row_select_home_send_from');
    spinnerTo = new Spinner('row_select_home_send_to');
    spinnerLogin = new Spinner('login-form-loading');
    showMoreInfoRegister();
});
function RegisterSubmit() {
    var response = grecaptcha.getResponse();
    var list_insert = {
        'account_name'              : $('#name_register').val(),
        'user_name'                 : $('#user_name_register').val(),
        'password_register'         : $('#password_register').val(),
        're_password_register'      : $('#re_password_register').val(),
        'mobile_register'           : $('#mobile_phone_register').val(),
        'email'                     : $('#email_register').val(),
        'primary_address_country'   : $('#country_register').val(),
        'primary_address_city'      : $('#city_register').val(),
        'receiver_district_1'       : $('#district_register').val(),
        'primary_address_street'    : $('#address_register').val(),
        'company_name'              : $('#company_name_register').val(),
        'department'                : $('#department_register').val(),
        'cmnd_register'             : $('#cmnd_register').val(),
        'captcha'                   : response,
        'rule'                      : $('#checkRule').is(":checked") ? 1 : 0
    };
    $.ajax({
        url: '/UserController/registerUser',
        type: 'post',
        data: {
            'list_insert': list_insert
        },
        dataType: 'json',
        success: function (data) {
            loadingScreenJS(false);
            if(data.result != 'success'){
                alert(data.result_check);
                var error = data.error;
                showErrorValidateForm(error);
            }else{
                alert(data.message);
                window.location.href = "/";
            }
        },
        beforeSend: function(){
            loadingScreenJS(true);
        },
        error: function () {
            alert('Error');
            loadingScreenJS(false);
        }
    });
}
function onlyPressNumberAndSpecialChar(event) {
    // Backspace, tab, enter, end, home, left, right
    // We don't support the del key in Opera because del == . == 46.
    var controlKeys = [8, 9, 13, 32, 35, 36, 37, 39];
    // IE doesn't support indexOf
    var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
    // Some browsers just don't raise events for control keys. Easy.
    // e.g. Safari backspace.
    if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
        (48 <= event.which && event.which <= 57) || // Always 1 through 9
        (48 == event.which && $(this).attr("value")) || // No 0 first digit
        isControlKey || String.fromCharCode(event.which) == "#" || String.fromCharCode(event.which) == "*" || String.fromCharCode(event.which) == "(" || String.fromCharCode(event.which) == ")" || String.fromCharCode(event.which) == "+" || String.fromCharCode(event.which) == "-" || String.fromCharCode(event.which) == ".") { // Opera assigns values for control keys.
        return;
    } else {
        event.preventDefault();
    }
}
function changeCountryAddress(){
    if ($('#country_register :selected').val()){
        var country = btoa(unescape(encodeURIComponent(JSON.stringify( $('#country_register :selected').val() ))));
    }else{
        var country = btoa(unescape(encodeURIComponent(JSON.stringify( 'VIETNAM' ))));
    }
    $.ajax({
        url: "/apiController/getCityForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'country': country
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#city_register').html(data.data);
                $('#city_register').trigger('chosen:updated');
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){
            $("#city_register").html("");
            $("#district_register").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function changeCityAddress(){
    $.ajax({
        url: "/apiController/getDistrictForCreateBooking",
        dataType: "json",
        type: 'post',
        data: {
            'city': btoa(unescape(encodeURIComponent(JSON.stringify( $('#city_register :selected').val() )))),
            'country': btoa(unescape(encodeURIComponent(JSON.stringify( $('#country_register :selected').val() ))))
        },
        //async: false,
        success: function (data) {
            if(data.status) {
                $('#district_register').html(data.data);
                $('#district_register').trigger('chosen:updated');
            }else{
                alert(data.message);
            }
        },
        beforeSend: function(){
            $("#add_district").html("");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status+"<br>"+thrownError);
        }
    });
}
function showErrorValidateForm(error){
    $('.error').hide();
    for (var i = 0; i < error.length; i++){
        if (error[i].name == "account_name") {
            $('#error_name_register').html(error[i].message);
            $('#error_name_register').show();
        }
        if (error[i].name == "user_name") {
            $('#error_user_name_register').html(error[i].message);
            $('#error_user_name_register').show();
        }
        if (error[i].name == "password_register") {
            $('#error_password_register').html(error[i].message);
            $('#error_password_register').show();
        }
        if (error[i].name == "re_password_register") {
            $('#error_re_password_register').html(error[i].message);
            $('#error_re_password_register').show();
        }
        if (error[i].name == "email") {
            $('#error_email_register').html(error[i].message);
            $('#error_email_register').show();
        }
        if (error[i].name == "mobile") {
            $('#error_mobile_phone_register').html(error[i].message);
            $('#error_mobile_phone_register').show();
        }
        if (error[i].name == "cmnd_register") {
            $('#error_cmnd_register').html(error[i].message);
            $('#error_cmnd_register').show();
        }
        if (error[i].name == "captcha") {
            $('#error_captcha').html(error[i].message);
            $('#error_captcha').show();
        }
        if (error[i].name == "rule") {
            $('#error_check_rule').html(error[i].message);
            $('#error_check_rule').show();
        }
    }
}
function validateLogin(){
    $('#login-form').validate({
        rules: {
            username:{
                required: true
            },
            password:{
                required: true
            }
        },
        messages : {
            username:{
                required: error_username
            },
            password:{
                required: error_password
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block error',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
}
function showMoreInfoRegister() {
    $('.more-info-register').on('hide.bs.collapse', function () {
        $('.btn-show-register').html('<i class="fa fa-plus" aria-hidden="true"></i> <span>'+register_as_company+'</span>');
    })
    $('.more-info-register').on('show.bs.collapse', function () {
        $('.btn-show-register').html('<i class="fa fa-minus" aria-hidden="true"></i> <span>'+register_as_company+'</span>');
    })
}
function ResetFormRegister() {
    $('#name_register').val('');
    $('#user_name_register').val('');
    $('#password_register').val('');
    $('#re_password_register').val('');
    $('#email_register').val('');
    $('#mobile_phone_register').val('');
    $('#cmnd_register').val('');
    $('#address_register').val('');
    $('#company_name_register').val('');
    $('#department_register').val('');
    $('#checkRule').prop('checked',false);
    $('#city_register').html('');
    $('#country_register').val('VIETNAM').trigger('change');
    changeCountryAddress();
    $('#district_register').html('');
}