<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
date_default_timezone_set("Asia/Ho_Chi_Minh");
// Define a destination
$targetFolder = '/uploads/import'; // Relative to the root
$user_id = $_GET['user_id'];

$verifyToken = md5('_ecos_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
	$targetFile = rtrim($targetPath,'/') . '/' . $user_id.'.'.end(explode('.', $_FILES['Filedata']['name']));
	$targetFile = rtrim($targetPath,'/') . '/'.$user_id.'--'.date('Y-M-d-H-i').'.' . end(explode('.', $_FILES['Filedata']['name'])) ;
	
	// Validate the file type
	$fileTypes = array('xls','xlsx','XLS','XLSX'); // File extensions
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	
	if (in_array($fileParts['extension'],$fileTypes)) {
		move_uploaded_file($tempFile,$targetFile);
		echo '1';
	} else {
		echo 'Invalid file type.';
	}
}
?>